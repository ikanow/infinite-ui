# Information Security Analytics #

Welcome to the repository for the Ikanow Infinit.e platform GUI module for information security analytics. This
GUI utilizes the ISA middleware module to create a set of tools for cyber analysts.

## Getting Started ##

### Prerequisites ###

- NodeJS 0.12 or greater is required.

### Quick Start ###

There are the steps needed to install grunt-cli, download the source code, setup the environment, and start the application.
It is still recommended to read 'Running this application' before attempting to start this application or you may 
have problems logging in.
```
npm install -g grunt-cli
cd ~/Workspaces
git clone https:/bitbucket.org/ikanow/infinite-ui.git
cd infinite-ui
npm install
grunt bower-plugins
grunt dev
```

### Generating and running html docs ###

This code is annotated with ngdoc style comments. These comments are used to generate a browseable HTML site.
Simple run `grunt docs`. The site will be updated each time a js file is saved within the application.

### Running this application ###

As a prerequisite, the grunt-cli node module must be globally installed `npm install -g grunt-cli`. With all required 
software installed, clone this repository into your workspaces folder:
```
cd ~/Workspaces
git clone https:/bitbucket.org/ikanow/infinite-ui.git
cd infinite-ui
```
NOTE: Windows users may need to set the git config option core.longpaths to true
`git config --global core.longpaths true`

At this point we need to update the dev environment config variables.
Open dev.config.json and update the destination URL for remote development.

Each key is a destination host. Each host contains a list of possible routes.
A route may be a string "/api" or an object with an http-proxy options array.


```
npm install
```
This will install any grunt plugins and node libraries required. Now start the development UI `grunt dev`. The dev task
includes a bower:install task to run the 'bower install' process and additionally copy the  which will fetch, and place all UI components into the /bower_compontents/ folder. 

## Extending ##

### Client-Side Plugins and Libraries ###

Some tools such as lodash or jquery which may or not have visual components are managed via bower. Each 
dependency is listed in bower.json and a version is provided or a GIT repository. If planning on using a git repository,
for compatibility reasons it's recommended to use the `https://host.org/username/repo.git` format. Using a git:// or git@ format
for SSH connection may cause issues on windows machines or environments without a proper ssh configuration.

To add a new plugin:
- Add the plugin and version ( or repo url if not published to bower ) to bower.json
- Run `grunt bower-plugins` to download and copy plugin files to /public/plugin/
- Add the package to rconfig.js
- Define dependencies in rconfig.js under the "shim" section.

Now the module will be available for define statements.
```
define(['angular', 'app', 'hashes'], function(angular, app, hashes){
```

NOTE: If `grunt bower-plugins` doesn't copy the correct files from /bower_components/{package} to /public/plugin/{package}/ 
an exception may need to be defined in the section "exportsOverride" of bower.json.

### Creating / Adding UI Components ###

To prevent issues while pulling upstream changes it is recommended to add new modules instead of modifying ones included in this 
package. There will be several places however where editing this packages's files will be necessary (eg. layout, left navigation ).