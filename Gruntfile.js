var pkg = require('./package.json');
var devEnv = require('./dev.config.json');
var _ = require('lodash');

var pub = pkg.paths.public;
var tmp = pkg.paths.temp;
var build = pkg.paths.build;
var target = pkg.paths.target;
var docs = pkg.paths.docs;
var kibana = pkg.paths.kibana;

var useLocalProxyMaps = false;

/**
 * @typedef {Object} MapDef
 * @property {String} dest Destination host
 * @property {Object} [options=null] Optional http-proxy options
 */

/**
 * @typedef {Object} ProxyConfig Http-Proxy config
 * @property {String} pattern URI To match
 * @property {Boolean} prependPath
 */

/**
 * Given an array of maps, return the dest url or proxy-config
 *
 * Examples of MapDef:
 *
 * Destination match with custom proxy config
 * "http://localhost:8000": [ { "pattern": "/infinit.e.flow.builder", "prependPath": false } ]
 *
 * Destination match with string and default proxy config
 * "http://api001.dev.isa.ik:8080": [ "/api", "/infinit.e.records/proxy" ]
 *
 * @param req
 * @param {Array<ProxyConfig>} maps Array of maps defined in dev.config.json
 * @return {MapDef|Boolean} MapDef if found, false if no match
 */
function checkProxyMaps( req, maps ){
  //console.log("CheckProxyMaps: " + req.url, maps);
  var i, contextResult;
  for (i in maps) {
    contextResult = hasContext(maps[i], req.url);
    if( contextResult === true ) return { dest : i };
    if( Object.prototype.toString.call( contextResult ) === '[object Object]') {
      return { dest: i, options: contextResult };
    }
  }
  return false;
}

/**
 * Test function to match url paths used for proxy map tests
 *
 * Examples of context
 * "/api"
 * { "pattern": "/infinit.e.flow.builder", "prependPath": false }
 * [ { "pattern": "/infinit.e.flow.builder", "prependPath": false }, "/api ]
 * [ "/api", "/infinit.e.records/proxy" ]
 *
 * @param {ProxyConfig|String|Array<ProxyConfig|String>} context
 * @param {String} uri URI To search for a map
 * @returns {ProxyConfig|Boolean} True or Proxy config on match, false otherwise
 */
function hasContext(context, uri) {
  var url = require('url'),
    urlPath = url.parse(uri).path,
    cIdx, tmpResult;

  // For strings, this is a simple test
  if (typeof context === 'string') {
    return urlPath.indexOf(context) === 0;
  }

  // Objects are custom http-proxy defs, use the pattern property
  if( Object.prototype.toString.call( context ) === '[object Object]') {
    return urlPath.indexOf(context.pattern) === 0 ? context : false;
  }

  // Arrays need recursion
  if( Object.prototype.toString.call( context ) === '[object Array]' ) {
    for (cIdx = 0; cIdx < context.length; cIdx++) {
      tmpResult = hasContext(context[cIdx], uri);
      if( tmpResult ) return tmpResult;
    }
  }
  return false;
}

/**
 * Create a proxy from a MapDef
 * @param req
 * @param res
 * @param {MapDef|Boolean} map
 */
function createProxy(req, res, map ){
  var opts = {
    target: map.dest,
    secure: false,
    changeOrigin: true
  };
  if( map.options ){
    opts = _.extend(opts, map.options )
  }
  return require('http-proxy').createProxyServer({}).web( req, res, opts );
}





// Grunt init
module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({

    // Include pkg in initConfig for <%= %> usage below
    pkg: pkg,

    turnOffPotatoDeclaration: {
      tmp: {
        expand: true,
        src: [
          tmp + '*/**/*.js',
          tmp + 'app.js'
        ]
      }
    },
    ngAnnotate: {
      tmp: {
        expand: true,
        src: [
          tmp + '*/**/*.js',
          tmp + 'app.js'
        ],
        ext: '.js', // Dest file paths will have this extension.
        extDot: 'last'
      }
    },
    turnOnPotatoDeclaration: {
      tmp: {
        expand: true,
        src: [
          tmp + '*/**/*.js',
          tmp + 'app.js'
        ]
      }
    },
    adjustTemplateUrls: {
      tmp: {
        options: {
          from: 'app',
          to: 'build'
        },
        expand: true,
        src: [
          tmp + '*/**/*.*',
          tmp + 'app.js'
        ]
      }
    },
    html2js: {
      options: {
        base: tmp,
        module: 'smart-templates',
        singleModule: true,
        rename: function (moduleName) {
          return 'build/' + moduleName;
        }
      },
      main: {
        src: [tmp + '**/*.tpl.html'],
        dest: tmp + 'smart-templates.js'
      }
    },
    addIncludes:{
      options:{
        appFile: tmp + 'app.js',
        includesFile: tmp + 'includes.js'
      },
      templates:{
        options:{
          angularModule: true,
          wrapToDefine: true,
          name: 'smart-templates',
          injectToApp: true
        },
        src: [
          tmp + 'smart-templates.js'
        ]

      }

    },
    uglify: {
      tmp: {
        expand: true,
        cwd: tmp,
        src: [
          '**/*.js'
        ],
        dest: tmp,
        ext: '.js'
      }
    },

    clean: {
      docs: {
        options: { force: true },
        src: [docs]
      },
      pre: {
        options: { force: true },
        src: [build, docs, target, kibana, tmp, "public/styles/css/*"]
      },
      post: {
        options: { force: true },
        src: [tmp]
      }
    },

    copy: {
      pre: {
        expand: true,
        cwd: pub + 'app/',
        src: [ '**' ],
        dest: tmp
      },
      post: {
        expand: true,
        cwd: tmp,
        src: [
          '*/**',
          'rconfig.js',
          '!**/*.tpl.html'
        ],
        dest: build
      }
    },

    requirejs: {
      compile: {
        options: {
          baseUrl: tmp,
          mainConfigFile: tmp + 'rconfig.js',
          name: "main",
          optimize: 'none',
          uglify2: {
            mangle: false
          },
          out: build + 'main.js',
          done: function (done, output) {
            console.log('done requirejs');
            done();
          }
        }
      }
    },

    //Documentation generator config
    ngdocs: {
      options: {
        dest: 'docs',
        html5Mode: false,
        title: "Ikanow View - Developer Docs"
        //template: "ngdoc.tmpl.htm"
      },
      api: {
        src: ['public/app/*/**/*.js', '!public/app/services/api/**/*.js'],
        title: 'Ikanow View'
      },
      infinitesdk: {
        api: true,
        src: ['public/plugin/infinite-js-sdk/*.js'],
        title: 'Infinite Platform SDK'
      }
    },

    //Development HTTP Server, HTTP Proxy, and live reload
    express: {
      docs:{
        options: {
          port: 8079,
          hostname: "0.0.0.0",
          bases: ["docs/"],
          open: true,
          livereload: 35728
        }
      },
      dev: {
        options: {
          port: devEnv.http.port,
          hostname: "0.0.0.0",
          bases: pub,
          livereload: 35727,
          open: true,
          middleware: [function(req,res,next){
            var map = false;
            //If developing the core module and hitting a module endpoint, use the local dev api
            if(req && req.url) {
              // If use local maps try to get a local map
              if(useLocalProxyMaps) map = checkProxyMaps(req, devEnv.proxy.localMaps);
              // If no local map try to get a remote map
              if(!map) map = checkProxyMaps(req, devEnv.proxy.remoteMaps);
            }

            // If there's a map use it.
            if( map ) return createProxy(req, res, map );

            next(); // Otherwise run next middleware
          }]
        }
      }
    },

    less: {
      all: {
        files: {
          "public/styles/css/ikanow.app.css": "public/styles/less/ikanow.app.less",
          "public/styles/LESS_FILES/UNMINIFIED_CSS/bootstrap.css": "public/styles/LESS_FILES/bootstrap.less",
          "public/styles/LESS_FILES/UNMINIFIED_CSS/smartadmin-production.css": "public/styles/LESS_FILES/smartadmin-production.less",
          "public/styles/LESS_FILES/UNMINIFIED_CSS/smartadmin-production-plugins.css": "public/styles/LESS_FILES/smartadmin-production-plugins.less",
          "public/styles/LESS_FILES/UNMINIFIED_CSS/font-awesome.css": "public/styles/LESS_FILES/library/fontawesome/font-awesome.less",
          "public/styles/LESS_FILES/UNMINIFIED_CSS/smartadmin-skins.css": "public/styles/LESS_FILES/smartadmin-skin/smartadmin-skins.less",
          "public/styles/css/fixes.css": "public/styles/less/fixes.less"
        }
      }
    },

    // MINIFY CSS
    cssmin: {
      minify: {
        expand: true,
        src: ['*.css', '!*.min.css'],
        dest: 'public/styles/css/',
        cwd: 'public/styles/LESS_FILES/UNMINIFIED_CSS/',
        extDot: 'last',
        ext: '.min.css'
      }
    },

    // grunt-watch will monitor the projects files
    watch: {
      dev: {
        files: ['public/styles/**/*.less'],
        tasks: ['less', 'cssmin'],
        options: {
          spawn: false,
          livereload:35727
        }
      },
      docs:{
        files: ['public/app/*/**/*.js'],
        tasks: ['ngdocs'],
        options: {
          livereload:35728
        }
      }
    },

    war: {
      target: {
        options: {
          war_dist_folder: target,    /* Folder where to generate the WAR. */
          war_name: '<%= pkg.name %>',   /* The name fo the WAR file (.war will be the extension) */
          webxml_welcome: 'index.html',
          webxml_display_name: 'Ikanow Infinite Platform'
        },
        files: [
          {
            expand: true,
            cwd: pub,
            src: ['!public/plugin/**','**'],
            dest: ''
          }
        ]
      }
    },

    easy_rpm: {
      options: {
        name: "<%= pkg.name %>",
        version: "<%= pkg.version %>",
        release: "1",
        buildArch: "noarch",
        summary: "Ikanow UI for the Infinit.e platform",
        description: "All UI components for the ISA tools.",
        license: "AFL-2.0",
        vendor: "Ikanow LLC"
      },
      release: {
        files: [
          { cwd: "target", src: "*.war", dest: "/opt/tomcat-infinite/interface-engine"}
        ]
      }
    }
  });

  //Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks("grunt-easy-rpm");
  grunt.loadNpmTasks('grunt-express');
  grunt.loadNpmTasks('grunt-html2js');
  grunt.loadNpmTasks('grunt-ngdocs');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-war');
  grunt.loadTasks('tasks');


  //Register new tasks
  grunt.registerTask('release', ['bower-plugins','default','war']);

  grunt.registerTask('default', [
    'clean:pre',
    'copy:pre',
    'turnOffPotatoDeclaration',
    'ngAnnotate:tmp',
    'turnOnPotatoDeclaration',
    'adjustTemplateUrls',
    'html2js',
    'addIncludes',
    'uglify',
    'requirejs',
    "less",
    "cssmin",
    'copy:post',
    'clean:post'
  ]);

  // Generate JSDocs
  grunt.registerTask('docs', [
    'clean:docs',
    'ngdocs',
    'express:docs',
    "watch:docs"
  ]);

  //Local Development server
  grunt.registerTask('dev', [
    'express:dev',
    'less',
    'cssmin',
    'watch:dev'
  ]);

  //Use to develop an app server locally as well.
  grunt.registerTask("dev-localapp", "Dev environment with additional local http-proxy maps enabled. Useful for local app server development.", function(){
    useLocalProxyMaps = true;
    grunt.task.run("dev");
  });

};
