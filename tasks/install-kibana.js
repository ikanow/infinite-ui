module.exports = function(grunt) {

  var cp = require('child_process'),
      fs = require('fs'),
      fse = require('fs-extra'),
      path = require("path");

  function pathExists(testPath){
    try {
      var testStat = fs.statSync(testPath);
    } catch(ignored){
      console.log("Stat Error: ", ignored);
      return false;
    }
    return true;
  }

  grunt.registerTask('install-kibana',
    'Compile and place kibana in the public/infinit.e.records/static/kibana folder',
    function() {

      var kibanaPath = path.resolve("node_modules/kibana"),
          kibanaDistPath = path.resolve(kibanaPath + "/dist"),
          kibanaDest = path.resolve("public/infinit.e.records/static/kibana");

      console.log("Kibana Module Dir: " + kibanaPath );

      if( !pathExists(kibanaPath) ){
        console.error("Kibana module not found. Exiting.");
        return;
      }

      // Exec commands in that folder
      console.log("Remove existing kibana build");
      fse.removeSync( kibanaDest );

      console.log("Installing kibana dependencies");
      cp.execSync( "npm install", { cwd: kibanaPath });

      console.log("Cleaning Kibana");
      cp.execSync( "grunt clean", { cwd: kibanaPath });

      console.log("Building Kibana");
      cp.execSync( "grunt build", { cwd: kibanaPath });

      console.log("Ensure " + kibanaDest );
      fse.ensureDirSync(kibanaDest);

      console.log("Copy " + kibanaDistPath + " -> " + kibanaDest );
      fse.copySync(kibanaDistPath, kibanaDest );

    }
  );

};