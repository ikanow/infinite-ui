/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


var __ikanowSourceTemplates = [
    {
        "name":"LogstashSourceTemplate",
        "displayName": "Logstash Import Source",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "Logstash Import Source Template",
                    "helperText": [
                        "Get started by entering the Amazon S3 URL to your data source as well as your S3 Username and Password.", // Each array item is a string, each string is a paragraph
                        "Standard characters have been selected for the quote and separators. You may chose from the list of other common options or add your own based off your data.",
                        "Add column headers and name them as you would like them to be displayed.",
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "showTest": true, //ADDED: show test results
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "source": {
                        "type": "input[select]", // Or something else
                        "title": "Source",
                        "order": 1, //ADDED
                        "column": 1,
                        "advanced": false,
                        "description": "File source",
                        "options": ["File/HDFS","S3"], //ADDED - list of dropdown values
                        //"value":"YYYY/MM/DD HH:mm:ss",
                        "required": true,
                        "value": "File/HDFS",
                        sourcekey: function(scope){
                            var s3 = "s3 {\n credentials => [\"%%USERNAME%%\",\"%%PASSWORD%%\"]\n bucket => \"%%SHARE%%\"\n prefix => '%%PREFIX%%' \n type => \"%%TYPE%%\"\n }", hdfs = "file {\n path => \"%%PATH%%\" \n start_position => \"beginning\" \n type => \"%%TYPE%%\"\n }";
                            if(this.value === "S3"){
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%INPUT%%/g, s3);
                            } else {
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%INPUT%%/g, hdfs);
                            }
                        }
                    },
                    "share": {
                        "type": "input[text]", // Or something else
                        "title": "Location",
                        "order": 2, //ADDED
                        "column": 2,
                        "description": "Enter a S3 URL or file path",
                        "required": true,
                        "value": "",
                        sourcekey: function(scope){
                            var strArray = this.value.replace('s3://','').split('/'),
                              bucket = strArray[0], prefix = "";

                            strArray.splice(0,1);
                            _.each(strArray, function(value){
                                prefix = prefix + value + '/';
                            })
                            prefix = prefix.replace('//','/');

                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%SHARE%%/g, bucket);
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%PREFIX%%/g, prefix);
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%PATH%%/g, this.value);
                        }
                    },
                    "username": {
                        "type": "input[text]", // Or something else
                        "title": "S3 Username",
                        "order": 3, //ADDED
                        "column": 1, //ADDED,
                        "description": "Username for the S3 share",
                        "required": true,
                        "hide": true,
                        "value": "",
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%USERNAME%%/g,this.value);
                        },
                        showhide: function(txt){
                            if(txt === "S3") {
                                this.hide = false;
                                this.required = true;
                            } else {
                                this.hide = true;
                                this.required = false;
                            }
                        }
                    },
                    "password": {
                        "type": "input[text]", // Or something else
                        "title": "S3 Password",
                        "order": 4, //ADDED
                        "column": 2, //ADDED
                        "description": "Password for the S3 share",
                        "required": true,
                        "hide": true,
                        "value": "",
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%PASSWORD%%/g,this.value);
                        },
                        showhide: function(txt){
                            if(txt === "S3") {
                                this.hide = false;
                                this.required = true;
                            } else {
                                this.hide = true;
                                this.required = false;
                            }
                        }
                    },
                    "quote": {
                        "type": "input[select]", // Or something else
                        "title": "Quote Character",
                        "order": 5, //ADDED
                        "column": 1, //ADDED
                        "description": "",
                        "required": true,
                        "value": "\"",
                        "options": ["\"","\'","Other"], //ADDED - list of dropdown values
                        "inlineEdit": true, //edit dropdown inline for custom text
                        sourcekey: function(scope){
                            if(this.value && this.value == '"') {
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%QUOTECHAR%%/g,"'\"'");
                            } else {
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%QUOTECHAR%%/g,'\"' + this.value + '\"');
                            }
                        }
                    },
                    "separator": {
                        "type": "input[select]", // Or something else
                        "title": "Separator",
                        "order": 6, //ADDED
                        "column": 2, //ADDED
                        "description": "",
                        "required": true,
                        "value": ",",
                        "options": [",","<TAB>","Other"], //ADDED - list of dropdown values
                        "inlineEdit": true, //edit dropdown inline for custom text
                        sourcekey: function(scope){
                            if(this.value && this.value == ",") {
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%SEPARATOR%%/g,',');
                            } else if(this.value && this.value == "<TAB>") {
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%SEPARATOR%%/g,'\t');
                            } else {
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%SEPARATOR%%/g,this.value);
                            }
                        }
                    },
                    "headers": {
                        "type": "input[text]", // Or something else
                        "title": "Column Headers",
                        "order": 8, //ADDED
                        "description": "Enter header column",
                        "required": false,
                        "multiple": true,
                        "advanced": false,
                        "values": [{"value":''}],
                        sourcekey: function(scope){
                            var cols = "\"", removeHeader = "";
                            _.each(this.values, function(v,idx){
                                //if no headers supplied or blank, default/add a "column1" to logstash config so it does not supply an empty column list and error
                                if(v.value && v.value != "" && idx === 0) {
                                    cols = cols + v.value + '","';
                                } else {
                                    cols = cols + "column1" + '","';
                                }
                                if(idx < 2) {
                                    removeHeader = removeHeader + '\nif [' + v.value + '] == \"' + v.value + '\" {\n  drop { }\n}';
                                }
                            })
                            cols = cols.replace(/,([^,]*)$/,'');
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%COLUMNS%%/g,cols);
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%REMOVEHEADER%%/g,removeHeader);
                        }
                    },
                    "geoip": {
                        "type": "input[text]",
                        "title": "Geo IP",
                        "order": 12, //ADDED
                        "advanced": true, //ADDED
                        "description": "Column name to use for IP geo tagging", //placeholder text
                        "required": false,
                        sourcekey: function(scope){
                            if(this.value){
                                if(scope.mock.source.processingPipeline[0].logstash.configGeo) {
                                    scope.mock.source.processingPipeline[0].logstash.configGeo = scope.mock.source.processingPipeline[0].logstash.configGeo.replace(/%%GEOIP%%/g,this.value);
                                    scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%CONFIGGEO%%/g,scope.mock.source.processingPipeline[0].logstash.configGeo);
                                    delete scope.mock.source.processingPipeline[0].logstash.configGeo;
                                }
                            } else {
                                if(scope.mock.source.processingPipeline[0].logstash.configGeo) {
                                    scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%CONFIGGEO%%/g,'');
                                    delete scope.mock.source.processingPipeline[0].logstash.configGeo;
                                }
                            }
                        }
                    },
                    "datecolumn": {
                        "type": "input[text]", // Or something else
                        "title": "Date Column",
                        "order": 9, //ADDED
                        "column": 1,
                        "advanced": false,
                        "description": "Column name that holds the date/time",
                        "required": false,
                        sourcekey: function(scope){
                            if(this.value) {
                                if(scope.mock.source.processingPipeline[0].logstash.configDate) {
                                    scope.mock.source.processingPipeline[0].logstash.configDate = scope.mock.source.processingPipeline[0].logstash.configDate.replace(/%%DATECOLUMN%%/g,this.value);
                                    scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%CONFIGDATE%%/g,scope.mock.source.processingPipeline[0].logstash.configDate);
                                    delete scope.mock.source.processingPipeline[0].logstash.configDate;
                                }
                            } else {
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%CONFIGDATE%%/g,'');
                                if(scope.mock.source.processingPipeline[0].logstash.configDate) {
                                    delete scope.mock.source.processingPipeline[0].logstash.configDate;
                                }
                            }
                        }
                    },
                    "datemask": {
                        "type": "input[select]", // Or something else
                        "title": "Date/Time Mask",
                        "order": 10, //ADDED
                        "column": 2,
                        "advanced": false,
                        "description": "Date/time format: MM/dd/yyyy HH:mm:ss",
                        "options": ["MM/DD/YYYY HH:mm:ss", "DD/MM/YYYY HH:mm:ss", "YYYY/MM/DD HH:mm:ss", "MM-DD-YYYY HH:mm:ss", "DD-MM-YYYY HH:mm:ss", "YYYY-MM-DD HH:mm:ss", "Other"], //ADDED - list of dropdown values
                        "inlineEdit": true, //edit dropdown inline for custom text
                        //"value":"YYYY/MM/DD HH:mm:ss",
                        "required": false,
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%DATEMASK%%/g,this.value);
                        }
                    },
                    "timezone": {
                        "type": "input[select]",
                        "title": "Timezone",
                        "order": 11, //ADDED
                        "advanced": false, //ADDED
                        "description": "", //placeholder text
                        "required": false,
                        "options": ["(GMT -12:00) Eniwetok, Kwajalein", "(GMT -11:00) Midway Island, Samoa", "(GMT -10:00) Hawaii", "(GMT -09:00) Alaska", "(GMT -08:00) Pacific Time (USA, Canada)", "(GMT -07:00) Mountain Time (USA, Canada)", "(GMT -06:00) Central Time (USA, Canada), Mexico City", "(GMT -05:00) Eastern Time (USA, Canada), Bogota", "(GMT -04:30) Caracas", "(GMT -04:00) Atlantic Time (Canada), La Paz", "(GMT -03:30) Newfoundland", "(GMT -03:00) Brazil, Buenos Aires, Georgetown", "(GMT -02:00) Mid-Atlantic", "(GMT -01:00) Azores, Cape Verde Islands", "(GMT +00:00) Greenwich, Western Europe, London", "(GMT +01:00) Brussels, Copenhagen, Madrid, Paris", "(GMT +02:00) Kaliningrad, South Africa, Cairo", "(GMT +03:00) Baghdad, Riyadh, Moscow", "(GMT +03:30) Tehran", "(GMT +04:00) Abu Dhabi, Muscat, Yerevan, Baku", "(GMT +04:30) Kabul", "(GMT +05:00) Ekaterinburg, Islamabad, Karachi", "(GMT +05:30) Mumbai, Kolkata, New Delhi", "(GMT +05:45) Kathmandu", "(GMT +06:00) Almaty, Dhaka, Colombo", "(GMT +06:30) Yangon, Cocos Islands", "(GMT +07:00) Bangkok, Hanoi, Jakarta", "(GMT +08:00) Beijing, Perth, Singapore, Hong Kong", "(GMT +09:00) Tokyo, Seoul, Osaka, Sapporo", "(GMT +09:30) Adelaide, Darwin", "(GMT +10:00) Eastern Australia, Guam", "(GMT +11:00) Magadan, Solomon Islands", "(GMT +12:00) Auckland, Wellington, Fiji"], //ADDED - list of dropdown values
                        "inlineEdit": false, //edit dropdown inline for custom text
                        "value": "(GMT -5:00) Eastern Time (USA, Canada), Bogota",
                        sourcekey: function(scope){
                            if(this.value){
                                var tz = "";
                                tz = this.value.substr(5,6).replace(":","");
                                scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace(/%%TIMEZONE%%/g,tz);
                            }
                        }
                    },
                    "type": {
                        "type": "input[select]",
                        "title": "Type",
                        "order": 12, //ADDED
                        "advanced": true, //ADDED
                        "description": "", //placeholder text
                        "required": false,
                        "options": ["Firewall", "Proxy", "RADIUS", "IDS", "IPS", "HBSS", "Web_Server", "DNS", "SQL", "Router", "DHCP", "Other"], //ADDED - list of dropdown values
                        "inlineEdit": true, //edit dropdown inline for custom text
                        "value": "Firewall",
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].logstash.config = scope.mock.source.processingPipeline[0].logstash.config.replace("%%TYPE%%",this.value);
                        }
                    }
                }

            },

            // This is the actual source file that will be used to creete output. Needs to populate based on user inputs from above
            "SOURCE":{
                "description": "Logstash S3 or HDFS",
                "extractType": "Logstash",
                "isPublic": true,
                "mediaType": "Record",
                "processingPipeline": [
                    {
                        "display": "Just contains a string in which to put the logstash configuration (minus the output, which is appended by Infinit.e)",
                        "logstash": {
                            "config": "input {\n  %%INPUT%% \n }\nfilter \n{\n csv\n { \n columns=> [\n %%COLUMNS%% \n ]\n quote_char => %%QUOTECHAR%% \n separator => \"%%SEPARATOR%%\" \n} \n%%REMOVEHEADER%% \n %%CONFIGDATE%% \n %%CONFIGGEO%%}\n",
                            "configDate": "\n mutate {\n  add_field => [ \"tz\", \"%%TIMEZONE%%\" ]\n  add_field => [\"dtg\", \"%{%%DATECOLUMN%%} %{tz}\"]\n}\ndate {\n  match => [\"dtg\", \"%%DATEMASK%% Z\"]\n}\nmutate{\n  remove => [ \"tz\" ]\n  remove => [ \"%%DATECOLUMN%%\" ]\n} \n",
                            "configGeo": "if [%%GEOIP%%]  {\n geoip {\n source => \"%%GEOIP%%\"\n target => \"geoip\"\n fields => [\"timezone\",\"location\",\"latitude\",\"longitude\", \"country_code2\", \"region_name\"]\n }\n mutate {\n convert => [ \"[geoip][coordinates]\", \"float\" ]\n }\n}\n"
                        }
                    },
                    {
                        "docMetadata":
                          {
                            "appendTagsToDocs": true,
                          }
                    }
                ],
                "tags": [
                    "Logs"
                ],
                "title": "Logtash S3 or HDFS"
            }
        }
    },
    {
        "name":"RSSSourceTemplate",
        "displayName": "RSS with NLP Source Template",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "RSS Feed",
                    "helperText": [
                        "Create a RSS feed as an input source by specifying the RSS feed URL(s). You may add multiple URLs by clicking on the green button.", // Each array item is a string, each string is a paragraph
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "anotherMetaProperty": "Some other property here that would be rendered in the UI", // Could be an obj/array/string TBD
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "urls": {
                        "type": "input[url]", //ADDED [url] type
                        "title": "URL(s)",
                        "order": 2, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].feed.extraUrls = [];
                            _.each(this.values, function(v){
                                scope.mock.source.processingPipeline[0].feed.extraUrls.push({'url':v.value, 'title':v.value})
                            })
                        },
                        "description": "Enter URLs http://url.here", //placeholder text
                        "required": true,
                        "multiple": true,
                        "values": [{"value":''}]
                    }
                }

            },

            // This is the actual source file that will be used to creete output. Needs to populate based on user inputs from above
            "SOURCE": {
                "description": "Get one of more RSS feeds, all the listed documents are extracted",
                "isPublic": true,
                "mediaType": "News",
                "processingPipeline": [
                    {
                        "display": "Get one of more RSS feeds, all the listed documents are extracted",
                        "feed": {"extraUrls": [{"url": "http://youraddress.com/then/e.g./news.rss"}]}
                    },
                    {
                        "display": "Extract the useful text from the HTML",
                        "textEngine": {"engineName": "default"}
                    },
                    {
                        "display": "Enrich the document metadata with entities (people, places) and associations generated using NLP",
                        "featureEngine": {"engineName": "default"}
                    },
                    {"docMetadata":
                      {
                        "appendTagsToDocs": true,
                      }
                    }
                ],
                "title": "Basic RSS Source Template"
            }
        }
    },
    {
        "name":"WebPageSourceTemplate",
        "displayName": "Web pages with NLP Source Template",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "Web Page",
                    "helperText": [
                        "Create a Web Page input source by specifying the RSS feed URL(s). You may add multiple URLs by clicking on the green button.", // Each array item is a string, each string is a paragraph
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "anotherMetaProperty": "Some other property here that would be rendered in the UI", // Could be an obj/array/string TBD
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "urls": {
                        "type": "input[url]",
                        "title": "URL(s)",
                        "order": 2, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].web.extraUrls = [];
                            _.each(this.values, function(v){
                                scope.mock.source.processingPipeline[0].web.extraUrls.push({'url':v.value, 'title':v.value})
                            })
                        },
                        "description": "Enter URLs http://url.here", //placeholder text
                        "required": true,
                        "multiple": true,
                        "values": [{"value":''}]
                    }
                }

            },

            // This is the actual source file that will be used to creete output. Needs to populate based on user inputs from above
            "SOURCE":{
                "description": "Extract each URL (re-extracting every 'updateCycle_secs') with the specified title and summary text",
                "isPublic": true,
                "processingPipeline": [
                    {
                        "display": "Extract each document (re-extracting every 'updateCycle_secs') with the specified title and summary text",
                        "web": {
                            "extraUrls": [{
                                "url": "http://youraddress.com/then/e.g./title.html"
                            }],
                            "updateCycle_secs": 86400
                        }
                    },
                    {
                        "display": "Extract the useful text from the HTML",
                        "textEngine": {"engineName": "default"}
                    },
                    {
                        "display": "Enrich the document metadata with entities (people, places) and associations generated using NLP",
                        "featureEngine": {"engineName": "default"}
                    },
                    {"docMetadata":
                      {
                        "appendTagsToDocs": true,
                      }
                    }
                ],
                "title": "Basic Web Page Source Template"
            }
        }
    },
    {
        "name":"YahooSourceTemplate",
        "displayName": "Yahoo search API Source Template",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "Yahoo Search",
                    "cost": 0.001875, //ADDED
                    "helperText": [
                        "Create a source using the Yahoo search API by entering a search term into the specified field. You may add multiple search terms by clicking on the green button.", // Each array item is a string, each string is a paragraph
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "anotherMetaProperty": "Some other property here that would be rendered in the UI", // Could be an obj/array/string TBD
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "terms": {
                        "type": "input[text]",
                        "title": "Search Term(s)",
                        "order": 2, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].web.extraUrls = [];
                            _.each(this.values, function(v){
                                scope.mock.source.processingPipeline[0].web.extraUrls.push({
                                    'url': 'https://yboss.yahooapis.com/ysearch/web?q=' + v.value + '&abstract=long&style=raw&format=json&start=0&count=50',
                                    'title': 'TAG: ' + v.value
                                })
                            })
                        },
                        "description": "Enter Search Term", //placeholder text
                        "required": true,
                        "multiple": true,
                        "values": [{"value":''}]//add a default blank value to render on screen
                    }
                }

            },

            // This is the actual source file that will be used to create output. Needs to populate based on user inputs from above
            "SOURCE":{
                "template_per_url_cost" : 0.001875,
                "extractType" : "Feed",
                "mediaType" : "News",
                "processingPipeline" : [{
                    "display" : "Extract each document (re-extracting every 'updateCycle_secs') with the specified title and summary text",
                    "web" : {
                        "extraUrls" : [{
                            "url" : "https://yboss.yahooapis.com/ysearch/web?q=??search_terms??&abstract=long&style=raw&format=json&start=0&count=50"
                        }]
                    }
                }, {
                    "harvest" : {
                        "duplicateExistingUrls" : false,
                        "searchCycle_secs" : 86400 // Integrate the yahoo cycle here from the UI
                    }
                }, {
                    "globals" : {
                        "scriptlang" : "javascript",
                        "scripts" : [
                            "function decode(api, doc_url, doc_desc)\n{     \n    var links= [];    \n    var web = api.bossresponse.web;\n    var results = web.results;\n    //add this set of search results on the stack\n    for ( var x in results )\n    {        \n        var result = results[x];\n        links.push({url:result[\"url\"],title:result[\"title\"],description:result[\"abstract\"], fullText:doc_desc});       \n    }\n    var new_start = Number(web.count) + Number(web.start);\n    \n    //check to see if we should get the next set of search results\n    if ( web.count == 50 )\n    {\n        //update start to next set\n        var new_url = doc_url.replace(/&start=\\d+/,\"&start=\" + new_start);        \n        links.push({url:new_url, spiderOut:true, description:doc_desc});\n    }\n    return links;\n}"
                        ]
                    }
                }, {
                    "display" : "",
                    "links" : {
                        "authExtractor" : "546df07ee4b0738701ee0cfa",
                        "authExtractorOptions" : {
                            "consumer_key" : "#IKANOW{546def72e4b0f7390e4ced8a.consumer_key}",
                            "consumer_secret" : "#IKANOW{546def72e4b0f7390e4ced8a.consumer_secret}"
                        },
                        "script" : "var links = decode(eval('('+text+')'), _doc.url, _doc.description); links;",
                        "scriptflags" : "dt",
                        "scriptlang" : "javascript",
                        "stopPaginatingOnDuplicate" : false
                    }
                }, {
                    "display" : "Extract the useful text from the HTML",
                    "textEngine" : {
                        "engineName" : "default",
                        "exitOnError" : true
                    }
                }, {
                    "display" : "Enrich the document metadata with entities (people, places) and associations generated using NLP",
                    "featureEngine" : {
                        "engineConfig" : {
                            "salience.decompose_categories" : "true",
                            "salience.generate_categories" : "true",
                            "salience.topics_to_entities" : "true"
                        },
                        "engineName" : "salience",
                        "exitOnError" : true
                    }
                  },
                  {"docMetadata":
                      {
                        "appendTagsToDocs": true,
                      }
                  }
                ],
                "title" : "Yahoo Search: ??search_terms??",
                "description" : "Yahoo API search with query: ??search_terms??",
                "tags" : ["yahoo"]
            }
        }
    },
    {
        "name":"S3DatasiftSourceTemplate",
        "displayName": "Datasift from S3 Source Template",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "Datasift",
                    "helperText": [
                        "Get started by created a Datesift source by entering the Amazon S3 URL to your data source as well as your S3 Username and Password.", // Each array item is a string, each string is a paragraph
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "anotherMetaProperty": "Some other property here that would be rendered in the UI", // Could be an obj/array/string TBD
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "username": {
                        "type": "input[text]", // Or something else
                        "title": "S3 Username",
                        "description": "Enter Datasift username",
                        "required": true,
                        "order": 2, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.file.username = this.value;
                        }
                    },
                    "password": {
                        "type": "input[text]", // Or something else
                        "title": "S3 Password",
                        "description": "Enter Datasift password",
                        "required": true,
                        "order": 3, //ADDED,
                        sourcekey: function(scope){
                            scope.mock.source.file.password = this.value;
                        }
                    },
                    "file": {
                        "type": "input[text]", // Or something else
                        "title": "S3 Location",
                        "description": "Enter a S3 URL eg: s3://location/path/",
                        "required": true,
                        "order": 1, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.url = this.value;
                        }
                    }
                    // ,
                    // "filter": {
                    //     "type": "input[text]",
                    //     "title": "Filter",
                    //     "order": 5, //ADDED
                    //     "description": "Enter a filter", //placeholder text
                    //     "required": false,
                    //     "advanced": true,
                    //     sourcekey: function(scope){
                    //         // Need correct path
                    //         // can probably remove this field per casey
                    //         // scope.mock.source.processingPipeline[0].feed.extraUrls = this.value;
                    //      // ??dunno where this used??
                    //     }
                    // }
                }

            },

            // This is the actual source file that will be used to creete output. Needs to populate based on user inputs from above
            "SOURCE":{
                "description": "Datasift",
                "extractType": "File",
                "file": {
                    "XmlPrimaryKey": "interaction.id",
                    "XmlRootLevelValues": ["interactions"],
                    "XmlSourceName": "",
                    "password": "MzkM9qEbwNSytrmkz3qxY8NT/ZfMx4gLT4quyoI9",
                    "pathExclude": ".*/todelete/.*",
                    "renameAfterParse": "$path/todelete/$name",
                    "username": "AKIAIRTUIFZQVFILL4HA"
                },
                "isPublic": false,
                "mediaType": "Social",
                "searchCycle_secs": 3600,
                "searchIndexFilter": {"metadataFieldList": ""},
                "structuredAnalysis": {
                    "associations": [
                        {
                            "assoc_type": "Fact",
                            "entity1": "$entName",
                            "entity1_index": "$SCRIPT( return _iterator.entName + '/' + _iterator.entType; )",
                            "entity2": "$theme",
                            "entity2_index": "$SCRIPT( return _iterator.theme + '/keyword'; )",
                            "iterateOver": "themes",
                            "verb": "thematically linked to",
                            "verb_category": "theme"
                        },
                        {
                            "assoc_type": "Fact",
                            "entity1_index": "$SCRIPT( return getGenericAuthor(_doc.metadata.json[0]) + '/' + getGenericAuthorType(_doc.metadata.json[0]);)",
                            "entity2": "Topic",
                            "iterateOver": "entity2/dummy",
                            "verb": "writes about",
                            "verb_category": "topic"
                        },
                        {
                            "assoc_type": "Fact",
                            "creationCriteriaScript": "$SCRIPT( return (_doc.metadata.json[0].twitter != null) && (null != _doc.metadata.json[0].twitter.retweet); )",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].twitter.retweeted.user.name;)",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].twitter.retweeted.user.screen_name + '/twitteruser';)",
                            "entity2": "Topic",
                            "iterateOver": "entity2/dummy",
                            "verb": "writes about",
                            "verb_category": "topic"
                        },
                        {
                            "assoc_type": "Event",
                            "creationCriteriaScript": "$SCRIPT( return ((null != _doc.metadata.json[0].twitter) && (null != _doc.metadata.json[0].twitter.retweet)); )",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].interaction.author.name;)",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].interaction.author.username + '/twitteruser';)",
                            "entity2": "$SCRIPT( return _doc.metadata.json[0].twitter.retweeted.user.name;)",
                            "entity2_index": "$SCRIPT( return _doc.metadata.json[0].twitter.retweeted.user.screen_name + '/twitteruser';)",
                            "verb": "retweets",
                            "verb_category": "retweets"
                        },
                        {
                            "assoc_type": "Event",
                            "entity1": "$SCRIPT( return  _doc.metadata.json[0].interaction.author.name;)",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].interaction.author.username + '/twitteruser';)",
                            "entity2": "$SCRIPT(return _value;)",
                            "entity2_index": "$SCRIPT(return _value + '/twitteruser';)",
                            "iterateOver": "json.twitter.mentions",
                            "verb": "mentions",
                            "verb_category": "mentions"
                        },
                        {
                            "assoc_type": "Event",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].interaction.author.name;)",
                            "entity1_index": "$SCRIPT( return  _doc.metadata.json[0].interaction.author.username + '/twitteruser';)",
                            "entity2": "HashTag",
                            "iterateOver": "entity2/dummy",
                            "verb": "writes about",
                            "verb_category": "hashtag"
                        },
                        {
                            "assoc_type": "Event",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].twitter.retweeted.user.name;)",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].twitter.retweeted.user.screen_name + '/twitteruser';)",
                            "entity2_index": "$SCRIPT( return _value + '/hashtag';)",
                            "iterateOver": "json.twitter.retweet.hashtags",
                            "verb": "writes about",
                            "verb_category": "hashtag"
                        },
                        {
                            "assoc_type": "Event",
                            "entity1_index": "$SCRIPT( return _value +  '/facebookuser'; )",
                            "entity2_index": "$SCRIPT( return _doc.metadata.json[0].interaction.author.name + '/facebookuser')",
                            "iterateOver": "json.facebook.likes.names",
                            "verb": "likes",
                            "verb_category": "likes"
                        },
                        {
                            "assoc_type": "Event",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].interaction.author.name + '/facebookuser')",
                            "entity2_index": "$SCRIPT( return _value +  '/facebookuser'; )",
                            "iterateOver": "json.facebook.to.names",
                            "verb": "mentions",
                            "verb_category": "mentions"
                        }
                    ],
                    "description": "$metadata.json.interaction.content",
                    "displayUrl": "$metadata.json.interaction.link",
                    "docGeo": {
                        "lat": "$SCRIPT( try { return _doc.metadata.json[0].interaction.geo.latitude; } catch (error) {} return null; )",
                        "lon": "$SCRIPT( try { return _doc.metadata.json[0].interaction.geo.longitude; } catch (error) {} return null; )"
                    },
                    "entities": [
                        {
                            "disambiguated_name": "$theme",
                            "iterateOver": "themes",
                            "relevance": "0.25",
                            "type": "Keyword"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( return _iterator.type != 'Place'; )",
                            "dimension": "$SCRIPT( return getSalienceDimension(_iterator.type); )",
                            "disambiguated_name": "$SCRIPT( return _iterator.name; )",
                            "iterateOver": "json.salience.content.entities",
                            "relevance": "0.2",
                            "sentiment": "$SCRIPT( try { return (parseInt(_iterator.sentiment)/10).toString(); } catch (err) { return null; } )",
                            "type": "$SCRIPT( return _iterator.type; )"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( return _iterator.type != 'Place'; )",
                            "dimension": "$SCRIPT( return getSalienceDimension(_iterator.type); )",
                            "disambiguated_name": "$SCRIPT( return _iterator.name; )",
                            "iterateOver": "json.salience.title.entities",
                            "relevance": "0.2",
                            "sentiment": "$SCRIPT( try { return (parseInt(_iterator.sentiment)/10).toString(); } catch (err) { return null; } )",
                            "type": "$SCRIPT( return _iterator.type; )"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( return _iterator.type == 'Place'; )",
                            "dimension": "$SCRIPT( return getSalienceDimension(_iterator.type); )",
                            "disambiguated_name": "$SCRIPT( return _iterator.name; )",
                            "geotag": {
                                "alternatives": [
                                    {"country": "$name"},
                                    {"city": "$name"}
                                ],
                                "stateProvince": "$name"
                            },
                            "iterateOver": "json.salience.content.entities",
                            "relevance": "0.2",
                            "sentiment": "$SCRIPT( try { return (parseInt(_iterator.sentiment)/10).toString(); } catch (err) { return null; } )",
                            "type": "$SCRIPT( return _iterator.type; )"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( return _iterator.type == 'Place'; )",
                            "dimension": "$SCRIPT( return getSalienceDimension(_iterator.type); )",
                            "disambiguated_name": "$SCRIPT( return _iterator.name; )",
                            "geotag": {
                                "alternatives": [
                                    {"country": "$name"},
                                    {"city": "$name"}
                                ],
                                "stateProvince": "$name"
                            },
                            "iterateOver": "json.salience.title.entities",
                            "relevance": "0.2",
                            "sentiment": "$SCRIPT( try { return (parseInt(_iterator.sentiment)/10).toString(); } catch (err) { return null; } )",
                            "type": "$SCRIPT( return _iterator.type; )"
                        },
                        {
                            "dimension": "What",
                            "disambiguated_name": "$SCRIPT( return _iterator.name; )",
                            "iterateOver": "json.salience.content.topics",
                            "relevance": "$SCRIPT( return _iterator.score; )",
                            "type": "Topic"
                        },
                        {
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( return getGenericAuthor(_doc.metadata.json[0]); )",
                            "linkdata": "$metadata.json.interaction.author.link",
                            "relevance": "1.0",
                            "sentiment": "$SCRIPT( try { return (parseInt(_doc.metadata.json[0].salience.content.sentiment)/10).toString(); } catch (err) { return null; } )",
                            "type": "$SCRIPT( return getGenericAuthorType(_doc.metadata.json[0]); )"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( return (_doc.metadata.json[0].demographic != null); )",
                            "dimension": "What",
                            "disambiguated_name": "$metadata.json.demographic.gender",
                            "relevance": "0.1",
                            "type": "Gender"
                        },
                        {
                            "actual_name": "$metadata.json.links.title",
                            "creationCriteriaScript": "$SCRIPT( return (_doc.metadata.json[0].links != null); )",
                            "dimension": "What",
                            "disambiguated_name": "$metadata.json.links.url",
                            "linkdata": "$metadata.json.links.url",
                            "relevance": "0.1",
                            "type": "URL"
                        },
                        {
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( return _value; )",
                            "iterateOver": "json.facebook.to.names",
                            "relevance": "0.8",
                            "type": "FacebookUser"
                        },
                        {
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( return _value; )",
                            "iterateOver": "json.facebook.likes.names",
                            "relevance": "0.2",
                            "type": "FacebookUser"
                        },
                        {
                            "actual_name": "$SCRIPT(return _doc.metadata.json[0].twitter.retweeted.user.name)",
                            "creationCriteriaScript": "$SCRIPT( return ((_doc.metadata.json[0].twitter != null) && (_doc.metadata.json[0].twitter.retweeted != null)); )",
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT(return _doc.metadata.json[0].twitter.retweeted.user.screen_name)",
                            "linkdata": "$SCRIPT(return 'http://www.twitter.com/' + _doc.metadata.json[0].twitter.retweeted.user.screen_name)",
                            "relevance": "0.9",
                            "type": "TwitterUser"
                        },
                        {
                            "actual_name": "$SCRIPT(return '@' + _value;)",
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT(return _value;)",
                            "iterateOver": "json.twitter.mentions",
                            "linkdata": "$SCRIPT(return 'http://www.twitter.com/' + _value ;)",
                            "relevance": "0.8",
                            "type": "TwitterUser"
                        },
                        {
                            "actual_name": "$metadata.json.twitter.place.name",
                            "creationCriteriaScript": "$SCRIPT( return ((_doc.metadata.json[0].twitter != null) && (_doc.metadata.json[0].twitter.place != null) && (_doc.metadata.json[0].twitter.geo != null)); )",
                            "disambiguated_name": "$metadata.json.twitter.place.full_name, $metadata.json.twitter.place.country",
                            "linkdata": "$metadata.json.twitter.place.url",
                            "relevance": "0.5",
                            "type": "$SCRIPT( return getLocationType(_doc.metadata.json[0].twitter.place.place_type); )",
                            "useDocGeo": true
                        },
                        {
                            "actual_name": "$metadata.json.twitter.place.name",
                            "creationCriteriaScript": "$SCRIPT( return ((_doc.metadata.json[0].twitter != null) && (_doc.metadata.json[0].twitter.place != null) && (_doc.metadata.json[0].twitter.geo == null) && (_doc.metadata.json[0].twitter.place.place_type == 'city')); )",
                            "disambiguated_name": "$metadata.json.twitter.place.full_name, $metadata.json.twitter.place.country",
                            "geotag": {"city": "$metadata.json.twitter.place.name"},
                            "linkdata": "$metadata.json.twitter.place.url",
                            "relevance": "0.5",
                            "type": "$SCRIPT( return getLocationType(_doc.metadata.json[0].twitter.place.place_type); )"
                        },
                        {
                            "actual_name": "$metadata.json.twitter.place.name",
                            "creationCriteriaScript": "$SCRIPT( return ((_doc.metadata.json[0].twitter != null) && (_doc.metadata.json[0].twitter.place != null) && (_doc.metadata.json[0].twitter.geo == null) && (_doc.metadata.json[0].twitter.place.place_type == 'admin')); )",
                            "disambiguated_name": "$metadata.json.twitter.place.full_name, $metadata.json.twitter.place.country",
                            "geotag": {"stateProvince": "$metadata.json.twitter.place.name"},
                            "linkdata": "$metadata.json.twitter.place.url",
                            "relevance": "0.5",
                            "type": "$SCRIPT( return getLocationType(_doc.metadata.json[0].twitter.place.place_type); )"
                        },
                        {
                            "actual_name": "$metadata.json.twitter.place.name",
                            "creationCriteriaScript": "$SCRIPT( return ((_doc.metadata.json[0].twitter != null) && (_doc.metadata.json[0].twitter.place != null) && (_doc.metadata.json[0].twitter.geo == null) && (_doc.metadata.json[0].twitter.place.place_type == 'country')); )",
                            "disambiguated_name": "$metadata.json.twitter.place.full_name, $metadata.json.twitter.place.country",
                            "geotag": {"country": "$metadata.json.twitter.place.name"},
                            "linkdata": "$metadata.json.twitter.place.url",
                            "relevance": "0.5",
                            "type": "$SCRIPT( return getLocationType(_doc.metadata.json[0].twitter.place.place_type); )"
                        },
                        {
                            "actual_name": "$SCRIPT(return '#' + _value;)",
                            "dimension": "What",
                            "disambiguated_name": "$SCRIPT(return _value;)",
                            "iterateOver": "json.twitter.hashtags",
                            "linkdata": "$SCRIPT(return 'http://analytics.topsy.com/?q=' + _value + '&period=1%20month';)",
                            "relevance": "0.75",
                            "type": "Hashtag"
                        },
                        {
                            "actual_name": "$SCRIPT(return '#' + _value;)",
                            "dimension": "What",
                            "disambiguated_name": "$SCRIPT(return _value;)",
                            "iterateOver": "json.twitter.retweet.hashtags",
                            "linkdata": "$SCRIPT(return 'http://analytics.topsy.com/?q=' + _value + '&period=1%20month';)",
                            "relevance": "0.65",
                            "type": "Hashtag"
                        }
                    ],
                    "fullText": "$metadata.json.interaction.content",
                    "metadataFields": "-themes,json",
                    "publishedDate": "$SCRIPT(return _doc.metadata.json[0].interaction.created_at.replace(/ +[0-9]{4}$/,'Z');)",
                    "rejectDocCriteria": "$SCRIPT( if ((null == _doc.metadata.json[0].interaction) || _doc.metadata.json[0].deleted) return 'reject'; )",
                    "script": "function truncate(str, length)\n{\n    if (str.length > length) {\n        return str.substring(0, length) + '...';\n    }\n    else {\n        return str;\n    }\n}\nfunction getGenericAuthor(json)\n{\n    if (null != json.reddit) {\n        return json.reddit.author.link.replace(/^http.*\\/([^\\/]+)[\\/]?$/, \"$1\");\n    } \n    if (null != json.facebook) {\n        return json.interaction.author.name;\n    } \n    if (null != json.twitter) {\n        return json.interaction.author.username;\n    } \n    else if (json.interaction.author.name != null) {\n        return json.interaction.author.name;\n    } else if (json.interaction.author.username != null) {\n        return json.interaction.author.username;\n    } else {\n        return null;\n    }        \n}\nfunction getGenericAuthorType(json)\n{\n    if (null != json.reddit) {\n        return 'RedditUser';\n    } \n    if (null != json.facebook) {\n        return 'FacebookUser';\n    } \n    if (null != json.twitter) {\n        return 'TwitterUser';\n    } \n    else if (json.interaction.author.name != null) {\n        return 'Person';\n    } else if (json.interaction.author.username != null) {\n        return 'Username';\n    } else {\n        return null;\n    }        \n}\nfunction getTitle(doc)\n{\n    if (null == doc.metadata.datasift) {\n        return null; //(means will be ignored until after UAH)\n    }\n    else {\n        var title = doc.metadata.datasift[0].interaction.type + ' ';\n        var author = getGenericAuthor(doc.metadata.datasift[0]);\n        if (null != author) {\n            title += '(' + author + '): ';\n        }\n        else {\n            title += '(unknown author): ';\n        }\n        if (null != doc.metadata.datasift[0].interaction.title) {\n            title += doc.metadata.datasift[0].interaction.title;\n        }\n        else {\n            title += truncate(doc.description, 40);      \n        }\n        title += ' (' + doc.publishedDate.toString() + ') ';\n        return title;\n    }\n}\nfunction getSalienceDimension(type) {\n    if (type == \"Person\") {\n        return \"Who\";\n    }\n    else if (type == \"Place\") {\n        return \"Where\";\n    }\n    else return \"What\";\n}\nfunction getLocationType(type) {\n    if (type == \"city\") {\n        return \"City\";\n    } else if (type == \"country\") {\n        return \"Country\";\n    } else if (type == \"admin\") {\n        return \"Region\";\n    } else {\n        return \"Place\";\n    }\n}\nfunction getLocationOntoType(type) {\n    if (type == \"city\") {\n        return \"city\";\n    } else if (type == \"country\") {\n        return \"country\";\n    } else if (type == \"admin\") {\n        return \"countrysubsidiary\";\n    } else {\n        return \"point\";\n    }\n}",
                    "scriptEngine": "javascript",
                    "title": "$SCRIPT( try { return getTitle(_doc) } catch (error) {} return '(no title)';  )"
                },
                "tags": [
                    "Twitter",
                    "datasift"
                ],
                "title": "Datasift",
                "unstructuredAnalysis": {
                    "meta": [
                        {
                            "context": "First",
                            "fieldName": "themes",
                            "flags": "m",
                            "script": "var retval = []; try { get_topics(_metadata.json[0].salience.content.entities, retval) } catch (error) {}; retval;",
                            "scriptlang": "javascript"
                        },
                        {
                            "context": "First",
                            "fieldName": "themes",
                            "script": "if (null == _iterator) _iterator = []; try { retval = get_topics(_metadata.json[0].salience.title.entities, _iterator) } catch (error) {}; _iterator;",
                            "scriptlang": "javascript"
                        },
                        {
                            "context": "First",
                            "fieldName": "datasift",
                            "flags": "m",
                            "script": "var retval = _metadata.json[0]; cleanse(retval); retval; ",
                            "scriptlang": "javascript"
                        }
                    ],
                    "script": "function get_topics(entities, retval)\n{\n    for (var e in entities) {\n        var ent = entities[e];\n        var name = ent.name;\n        var type = ent.type;\n        if (null != ent.themes) {\n            for (var t in ent.themes) {\n                var theme = ent.themes[t];\n                retval.push({entName:name, entType:type, theme:theme});\n            }\n        }\n    }\n}\nfunction truncate(str, length)\n{\n    if (str.length > length) {\n        return str.substring(0, length) + '...';\n    }\n    else {\n        return str;\n    }\n}\nfunction cleanse(json)\n{\n    retval.salience = null; \n    if (null != retval.interaction) {\n        retval.interaction.content = null; \n        var type = retval.interaction.type;\n        if ((type != null) && (null != json[type])) {\n            json[type].content = null;\n        }\n    }\n    return json;\n}",
                    "simpleTextCleanser": [
                        {
                            "field": "description",
                            "flags": "H",
                            "script": "/*",
                            "scriptlang": "xpath"
                        },
                        {
                            "field": "description",
                            "script": "var desc = truncate(text, 512); desc;",
                            "scriptlang": "javascript"
                        },
                        {
                            "field": "fullText",
                            "flags": "H",
                            "script": "/*",
                            "scriptlang": "xpath"
                        }
                    ]
                },
                "url": "s3://nsight.ikanow.com/",
                "useExtractor": "none",
                "useTextExtractor": "none"
            }
        }
    },
    {
        "name":"TwitterSourceTemplate",
        "displayName": "Twitter search API Source Template",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "Twitter",
                    "helperText": [
                        "Create your Twitter source by entering your Twitter API key, password and name / handle to the account to harvest. ", // Each array item is a string, each string is a paragraph
                        "Additionally, you may add hashtags to search on",
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "anotherMetaProperty": "Some other property here that would be rendered in the UI", // Could be an obj/array/string TBD
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "terms": {
                        "type": "input[text]",
                        "title": "Twitter Accounts and/or Hashtags to Harvest",
                        "multiple": true, //ADDED
                        "order": 3, //ADDED
                        //discuss if search terms is really the right input?  might be twitter accounts?
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].feed.extraUrls = [];
                            _.each(this.values, function(v){
                                scope.mock.source.processingPipeline[0].feed.extraUrls.push({'url':'https://api.twitter.com/1.1/search/tweets.json?include_entities=true&count=100&q=' + v.value});
                                if(v.value.substr(0,1) === "@"){
                                    scope.mock.source.processingPipeline[0].feed.extraUrls.push({'url':'https://api.twitter.com/1.1/statuses/user_timeline.json?include_entities=true&count=100&screen_name=' + v.value})
                                }
                            })
                        },
                        "description": "Enter Twitter @usernames or #hashtags", //placeholder text
                        "required": true,
                        "multiple": true,
                        "values": [{"value":''}]
                    },
                    "username": {
                        "type": "input[text]", // Or something else
                        "title": "API/Consumer Key",
                        "description": "Enter your Twitter API key",
                        "required": true,
                        "order": 1, //ADDED
                        sourcekey: function(scope){
                            // NEED TO DO SOME oAuth tranformation for API key and API secret/password
                            // scope.mock.source.processingPipeline[0].feed.httpFields.Authorization = this.value;
                        }
                    },
                    "password": {
                        "type": "input[text]", // Or something else
                        "title": "API Secret",
                        "description": "Enter your Twitter API secret/password",
                        "required": true,
                        "order": 2, //ADDED
                        sourcekey: function(scope){
                            // NEED TO COMBINE API KEY + SECRET/PASSWORD and transform to oAuth for twitter
                            // scope.mock.source.processingPipeline[0].feed.httpFields.Authorization += this.value;
                            // console.log(scope.mock.source.processingPipeline[0].feed.httpFields.Authorization);
                        }
                    }
                }

            },

            // This is the actual source file that will be used to creete output. Needs to populate based on user inputs from above
            // Infinite system template name: Twitter API v1.1 TEMPLATE
            "SOURCE":{
                "description": "Example of importing tweets based on API v1.1, runs hourly (currently TEMPLATE_USER, can be modified to different API calls)",
                "isPublic": false,
                "mediaType": "Social",
                "processingPipeline": [
                    {"feed": {
                        "extraUrls": [
                            {"url": "https://api.twitter.com/1.1/search/tweets.json?include_entities=true&count=100&q=@TEMPLATE_USER"},
                            {"url": "https://api.twitter.com/1.1/statuses/user_timeline.json?include_entities=true&count=100&screen_name=TEMPLATE_USER"}
                        ],
                        "httpFields": {"Authorization": "Bearer AAAAAAAAAAAAAAAAAAAAANfLRAAAAAAARvXJ2ewsYn1eopbVQM0%2BQRgDniM%3D9wJjyCWaiItOnM99yp1u73BWfsxhBgxbRrem1CjiPI"},
                        "waitTimeOverride_ms": 10000
                    }},
                    {"globals": {"scripts": ["function decode(text)\n{\n    var json = eval('('+text+')'); \n    var retval = []; \n    var jsonArray = json;\n    if (!(jsonArray instanceof Array)) {\n        jsonArray = json.statuses;\n    }\n    for (x in jsonArray) \n    { \n        var tweet = jsonArray[x]; \n        var retobj = { url: 'http://twitter.com/' + tweet.user.screen_name + '/status/' + tweet.id_str, fullText: tweet, title: tweet.text }; \n        retval.push(retobj);\n    } \n    if (null != json.next_page) retval.push({url: 'http://search.twitter.com/search.json' + json.next_page, spiderOut: true});\n    return retval; \n}"]}},
                    {"harvest": {"searchCycle_secs": 3600}},
                    {"links": {
                        "extraMeta": [{
                            "context": "First",
                            "fieldName": "_ONERROR_",
                            "script": "var x = 'Error: ' + text; x;",
                            "scriptlang": "javascript"
                        }],
                        "globals": "function decode(text)\n{\n    var json = eval('('+text+')'); \n    var retval = []; \n    var jsonArray = json;\n    if (!(jsonArray instanceof Array)) {\n        jsonArray = json.statuses;\n    }\n    for (x in jsonArray) \n    { \n        var tweet = jsonArray[x]; \n        var retobj = { url: 'http://twitter.com/' + tweet.user.screen_name + '/status/' + tweet.id_str, fullText: tweet, title: tweet.text }; \n        retval.push(retobj);\n    } \n    if (null != json.next_page) retval.push({url: 'http://search.twitter.com/search.json' + json.next_page, spiderOut: true});\n    return retval; \n}",
                        "maxDepth": 5,
                        "numPages": 10,
                        "numResultsPerPage": 1,
                        "script": "var retval = decode(text); retval;",
                        "scriptlang": "javascript",
                        "userAgent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:11.0) Gecko/20100101 Firefox/11.0"
                    }},
                    {"contentMetadata": [{
                        "fieldName": "json",
                        "script": " var json = eval('(' + text + ')'); json; ",
                        "scriptlang": "javascript"
                    }]},
                    {"docMetadata": {
                        "appendTagsToDocs": true,
                        "description": "$SCRIPT( try { var text = _doc.metadata.json[0].user.screen_name + ': ' + _doc.metadata.json[0].text; return text; } catch (error){  return null; } )",
                        "geotag": {
                            "lat": "$SCRIPT( try { return _doc.metadata.json[0].geo.coordinates[0]; } catch (error) { return '0.0'; } )",
                            "lon": "$SCRIPT( try { return _doc.metadata.json[0].geo.coordinates[1]; } catch (error) { return '0.0'; } )"
                        },
                        "publishedDate": "$SCRIPT( var date = _doc.metadata.json[0].created_at; return date; )"
                    }},
                    {"contentMetadata": [
                        {
                            "fieldName": "urls",
                            "script": " var json = eval('(' + text + ')'); json.entities.urls;",
                            "scriptlang": "javascript"
                        },
                        {
                            "fieldName": "user_mentions",
                            "script": " var json = eval('(' + text + ')'); json.entities.user_mentions; ",
                            "scriptlang": "javascript"
                        },
                        {
                            "fieldName": "hashtags",
                            "script": " var json = eval('(' + text + ')'); json.entities.hashtags; ",
                            "scriptlang": "javascript"
                        }
                    ]},
                    {"textEngine": {"engineName": "default"}},
                    {"entities": [
                        {
                            "creationCriteriaScript": "$SCRIPT( if ( _doc.metadata.json[0].user.screen_name == null) return false; else return true; )",
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( var x = _doc.metadata.json[0].user.screen_name; return x; )",
                            "linkdata": "$SCRIPT( return 'http://www.twitter.com/' + _doc.metadata.json[0].from_user; )",
                            "type": "TwitterHandle"
                        },
                        {
                            "actual_name": "$metadata.json.place.name",
                            "creationCriteriaScript": "$SCRIPT( if ( _doc.metadata.json[0].place == null) return false; else return true; )",
                            "dimension": "Where",
                            "disambiguated_name": "$metadata.json.place.full_name",
                            "geotag": {
                                "city": "$metadata.json.place.name",
                                "countryCode": "$metadata.json.place.country_code"
                            },
                            "type": "Location"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( if ( _doc.metadata.json[0].to_user == null) return false; else return true; )",
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( var x = _doc.metadata.json[0].to_user; return x; )",
                            "linkdata": "$SCRIPT( return 'http://www.twitter.com/' + _doc.metadata.json[0].to_user; )",
                            "type": "TwitterHandle"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( if ( _doc.metadata.json[0].user.name == null) return false; else return true; )",
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( var x = _doc.metadata.json[0].user.name; return x; )",
                            "type": "UserName"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( if ( _doc.metadata.json[0].to_user_name == null) return false; else return true; )",
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( var x = _doc.metadata.json[0].to_user_name; return x; )",
                            "type": "UserName"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( if ( _doc.metadata.hashtags == null) return false; else return true; )",
                            "dimension": "What",
                            "disambiguated_name": "$SCRIPT( var x = _iterator.text; return x; )",
                            "iterateOver": "hashtags",
                            "type": "HashTag"
                        },
                        {
                            "creationCriteriaScript": "$SCRIPT( if ( _doc.metadata.urls == null) return false; else return true; )",
                            "dimension": "What",
                            "disambiguated_name": "$SCRIPT( var x = _iterator.url; return x; )",
                            "iterateOver": "urls",
                            "linkdata": "$SCRIPT( return _iterator.expanded_url; )",
                            "type": "URL"
                        },
                        {
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( var x = _iterator.name; return x; )",
                            "iterateOver": "user_mentions",
                            "type": "UserName"
                        },
                        {
                            "dimension": "Who",
                            "disambiguated_name": "$SCRIPT( var x = _iterator.screen_name; return x; )",
                            "iterateOver": "user_mentions",
                            "linkdata": "$SCRIPT( return 'http://www.twitter.com/' + _iterator.screen_name; )",
                            "type": "TwitterHandle"
                        }
                    ]},
                    {"associations": [
                        {
                            "assoc_type": "Fact",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].user.name; )",
                            "entity2": "$SCRIPT( return _doc.metadata.json[0].user.screen_name; )",
                            "verb": "uses_handle",
                            "verb_category": "uses_handle"
                        },
                        {
                            "assoc_type": "Event",
                            "creationCriteriaScript": "$SCRIPT( return null != _doc.metadata.json[0].place; )",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].user.name; )",
                            "geo_index": "$SCRIPT( return _doc.metadata.json[0].place.full_name + '/location'; )",
                            "verb": "tweets_from",
                            "verb_category": "tweets_from"
                        },
                        {
                            "assoc_type": "Fact",
                            "creationCriteriaScript": "$SCRIPT( return null != _doc.metadata.to_user; )",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].to_user_name; )",
                            "entity2": "$SCRIPT( return _doc.metadata.json[0].to_user; )",
                            "verb": "uses_handle",
                            "verb_category": "uses_handle"
                        },
                        {
                            "assoc_type": "Event",
                            "creationCriteriaScript": "$SCRIPT( return null != _doc.metadata.to_user; )",
                            "entity1": "$SCRIPT( return _doc.metadata.json[0].user.screen_name; )",
                            "entity2": "$SCRIPT( return _doc.metadata.json[0].to_user; )",
                            "time_start": "$SCRIPT( return _doc.metadata.json[0].created_at; )",
                            "verb": "tweets_to",
                            "verb_category": "tweets_to"
                        },
                        {
                            "assoc_type": "Event",
                            "creationCriteriaScript": "$SCRIPT( return null != _doc.metadata.urls; )",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].user.screen_name + '/twitterhandle';)",
                            "entity2_index": "$SCRIPT( return _iterator.url + '/url'; )",
                            "iterateOver": "urls",
                            "time_start": "$SCRIPT( return _doc.metadata.json[0].created_at; )",
                            "verb": "tweets_link",
                            "verb_category": "tweets_link"
                        },
                        {
                            "assoc_type": "Event",
                            "creationCriteriaScript": "$SCRIPT( return null != _doc.metadata.hashtags; )",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].user.screen_name + '/twitterhandle';)",
                            "entity2_index": "$SCRIPT( return _iterator.text + '/hashtag'; )",
                            "iterateOver": "hashtags",
                            "time_start": "$SCRIPT( return _doc.metadata.json[0].created_at; )",
                            "verb": "tweets_about",
                            "verb_category": "tweets_about"
                        },
                        {
                            "assoc_type": "Event",
                            "entity1_index": "$SCRIPT( return _doc.metadata.json[0].user.screen_name + '/twitterhandle';)",
                            "entity2_index": "$SCRIPT( return _iterator.screen_name + '/twitterhandle'; )",
                            "iterateOver": "user_mentions",
                            "time_start": "$SCRIPT( return _doc.metadata.json[0].created_at; )",
                            "verb": "tweets_to",
                            "verb_category": "tweets_to"
                        },
                        {
                            "assoc_type": "Fact",
                            "entity1": "$SCRIPT( return _iterator.name + '/username'; )",
                            "entity2": "$SCRIPT( return _iterator.screen_name + '/twitterhandle'; )",
                            "iterateOver": "user_mentions",
                            "verb": "uses handle",
                            "verb_category": "uses handle"
                        }
                    ]}
                ],
                "searchCycle_secs": 3600,
                "tags": ["twitter"],
                "title": "Twitter API v1.1 TEMPLATE"
            }
        }
    },
    {
        "name":"LookupTableBuilderSourceTemplate",
        "displayName": "Lookup table builder Source Template",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "Lookup table builder Source Template",
                    "helperText": [
                        "Build your lookup table by indicating the JSON share where the lookup table is located and specifying the Key Field and Header Fields in your lookup table.", // Each array item is a string, each string is a paragraph
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "anotherMetaProperty": "Some other property here that would be rendered in the UI", // Could be an obj/array/string TBD
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "urls": {
                        "type": "input[text]",
                        "title": "Header Fields",
                        "order": 3, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].custom_file.XmlIgnoreValues = [];
                            scope.mock.source.processingPipeline[0].custom_file.XmlRootLevelValues = [];
                            _.each(this.values, function(v){
                                scope.mock.source.processingPipeline[0].custom_file.XmlIgnoreValues.push(v.value);
                                scope.mock.source.processingPipeline[0].custom_file.XmlRootLevelValues.push(v.value);
                            })
                        },
                        "description": "", //placeholder text
                        "required": true,
                        "multiple": true,
                        "values": [{"value":''}]
                    },
                    "fieldKey": {
                        "type": "input[text]", // Or something else
                        "title": "Key Field",
                        "description": "Source Field Key",
                        "required": true,
                        "order": 2, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[2].scriptingEngine.globalScript =  "//Global control variables:\n//(_memoryOptimized is set externally)\nvar logger = org.apache.log4j.Logger.getLogger(\"my.logging.prefix\");\nvar debug = true; // (set to false when not debugging)\n\n// Processing elements:\n\nfunction map(key, val) {\n    //if (debug) logger.debug('here');\n    try {\n        emit( {'id': val.metadata.csv[0]." + this.value + "}, val.metadata.csv[0]);\n    }\n    catch (e) {\n        if (debug) logger.error(e.message);\n    }\n} \n\nfunction reduce(key, vals) {   \n    // (if in non-memory optimized mode then this loop should be:\n    // for (x in vals) { var val = vals[x]; /* etc */ }\n    while (vals.hasNext()) {\n        var val = vals.next();\n        emit(key, val);\n        break;\n    }\n}\n\n// (default: combine == reducer .. prototype: combine(key, vals))\ncombine = reduce;\n";
                        },
                        "advanced": false //ADDED
                    },
                    "shareid": {
                        "type": "input[file]", // Or something else
                        "title": "ShareID",
                        "description": "Browse and upload a CSV file",
                        "required": true,
                        "order": 1, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].custom_file.url = "inf://share/" + this.value + "/lookupTable";
                        },
                        "advanced": false //ADDED
                    },
                    "description": {
                        "type": "input[text]", // Or something else
                        "title": "Description",
                        "order": 5, //ADDED
                        sourcekey: function(scope){
                            if(this.value) {
                                scope.mock.source.description = this.value;
                            }
                        },
                        "description": "",
                        "required": false,
                        "advanced": true
                    }
                }

            },

            // This is the actual source file that will be used to creete output. Needs to populate based on user inputs from above
            // Infinite system template name: TEMPLATE Lookup table builder - based on shares
            "SOURCE":{
                "description": "Give it a share of zipped CSVs (SHAREID), and a list of header fields (HEADERS/IGNORE_HEADERS), and the field to use as a key (KEYFIELD)",
                "extractType": "Custom",
                "isPublic": true,
                "mediaType": "Report",
                "processingPipeline": [
                    {
                        "custom_file": {
                            "XmlIgnoreValues": ["IGNORE_HEADERS"],
                            "XmlRootLevelValues": ["HEADERS"],
                            "type": "sv",
                            "url": "inf://share/SHAREID/shareTable"
                        },
                        "display": ""
                    },
                    {
                        "display": "",
                        "scheduler": {
                            "frequency": "once_only",
                            "runDate": ""
                        }
                    },
                    {
                        "display": "",
                        "scriptingEngine": {
                            "combineScript": "// (handled in globals)",
                            "globalScript": "//Global control variables:\n//(_memoryOptimized is set externally)\nvar logger = org.apache.log4j.Logger.getLogger(\"my.logging.prefix\");\nvar debug = faose; // (set to false when not debugging)\n\n// Processing elements:\n\nfunction map(key, val) {\n    //if (debug) logger.debug('here');\n    try {\n        emit( {'id': val.metadata.csv[0].KEYFIELD}, val.metadata.csv[0]);\n    }\n    catch (e) {\n        if (debug) logger.error(e.message);\n    }\n} \n\nfunction reduce(key, vals) {   \n    // (if in non-memory optimized mode then this loop should be:\n    // for (x in vals) { var val = vals[x]; /* etc */ }\n    while (vals.hasNext()) {\n        var val = vals.next();\n        emit(key, val);\n        break;\n    }\n}\n\n// (default: combine == reducer .. prototype: combine(key, vals))\ncombine = reduce;\n",
                            "mapScript": "// (handled in globals)",
                            "memoryOptimized": true,
                            "reduceScript": "// (handled in globals)",
                            "scriptLang": "javascript"
                        }
                    }
                ],
                "tags": ["lookup_table_builder"],
                "title": "TEMPLATE Lookup table builder - based on shares"
            }
        }
    },
    {
        "name":"LookupTableRecordsSourceTemplate",
        "displayName": "Lookup table applier to records Source Template",
        "source": {

            // This is the template meta obj that the UI will use to render different UI elements, both meta and user saved data
            "TEMPLATE":{

                //This is all the stuff that is displayed to the user in the UI, but NOT saved in the actual template:
                "meta": {
                    "title": "Lookup table applier to records Source Template",
                    "helperText": [
                        "Apply the lookup table by specifying the Record Type, Data Group of the record , Lookup Table Name as well as the Key Field.  ", // Each array item is a string, each string is a paragraph
                        "Proceed to the following page to configure, test and publish your source."
                    ],
                    "anotherMetaProperty": "Some other property here that would be rendered in the UI", // Could be an obj/array/string TBD
                    "anotherMetaProperty2": "Some other property here that would be rendered in the UI - 2" // Could be an obj/array/string TBD
                },

                // These are the items that we want to render and also save to the template, I envision these being the directive inputs:
                // Big question here is how to determine what UI element to render, ie input[text], radio, checkbox, select, etc
                "userInputs": {
                    "recordtype": {
                        "type": "input[text]", // Or something else
                        "title": "Record Type",
                        "description": "Type of records eg: Apache",
                        "required": true,
                        "order": 1, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[0].records_indexQuery.types.push(this.value);
                        },
                        "advanced": false //ADDED
                    },
                    "communities": {
                        "type": "input[text]", // Or something else
                        "title": "Communities",
                        "description": "Community or communities where records were uploaded",
                        "required": true,
                        "order": 2, //ADDED
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[2].extraInputSettings.groupOverrideRegex = "*" + this.value;
                        },
                        "advanced": false //ADDED
                    },
                    "lookuptable": {
                        "type": "input[text]", // Or something else
                        "title": "Lookup Table Name",
                        "description": "Enter the lookup table object eg: geoLookup",
                        "required": true,
                        "order": 3, //ADDED
                        sourcekey: function(scope){
                            //code to iterate in case we make this a multi input in the future
                            //var tables = "\"";
                            //_.each(this.value, function(v){
                            //    scope.mock.source.processingPipeline[1].artefacts.joinTables.push(v);
                            //    tables = tables + v + '","' 
                            //})
                            //tables = tables.replace(/,([^,]*)$/,'');
                            //scope.mock.source.processingPipeline[4].scriptingEngine.globalScript = scope.mock.source.processingPipeline[4].scriptingEngine.globalScript.replace(/%%LOOKUP%%/g,tables);
                            scope.mock.source.processingPipeline[1].artefacts.joinTables.push(this.value);
                            scope.mock.source.processingPipeline[4].scriptingEngine.globalScript = scope.mock.source.processingPipeline[4].scriptingEngine.globalScript.replace(/%%LOOKUP%%/g,this.value);
                        },
                        "advanced": false //ADDED
                    },
                    "description": {
                        "type": "input[text]", // Or something else
                        "title": "Key Field",
                        "order": 5, //ADDED
                        "advanced": false,
                        "description": "The key field specified when building the lookup table",
                        sourcekey: function(scope){
                            scope.mock.source.processingPipeline[4].scriptingEngine.globalScript = scope.mock.source.processingPipeline[4].scriptingEngine.globalScript.replace(/%%KEYFIELD%%/g,this.value);
                        },
                        "description": "",
                        "required": false
                    }
                }

            },

            // This is the actual source file that will be used to creete output. Needs to populate based on user inputs from above
            // Infinite system template name: TEMPLATE Lookup table applier - check records
            "SOURCE":{
                "description": "Check the record objects of type TYPES in the specified community/communities (EXTRA_COMMS), based on the lookup table with name LOOKUP, using KEYFIELD",
                "extractType": "Custom",
                "isPublic": true,
                "mediaType": "Report",
                "processingPipeline": [
                    {
                        "display": "",
                        "records_indexQuery": {
                            "streamingMode": "streaming",
                            "tmin": "now-2h",
                            "types": [] //TYPES map here
                        }
                    },
                    {
                        "artefacts": {
                            "joinTables": [], //LOOKUP maps here
                            "selfJoin": false
                        },
                        "display": ""
                    },
                    {
                        "display": "",
                        "extraInputSettings": {"groupOverrideRegex": "*EXTRA_COMMUNITIES"} //EXTRA_COMMS maps here
                    },
                    {
                        "display": "",
                        "scheduler": {
                            "frequency": "once_only",
                            "runDate": "",
                            "tmin_initialOverride": "now-100d"
                        }
                    },
                    {
                        "display": "",
                        "scriptingEngine": {
                            "combineScript": "// (handled in globals)",
                            //
                            "globalScript": "//Global control variables:\n//(_memoryOptimized is set externally)\nvar logger = org.apache.log4j.Logger.getLogger(\"my.logging.prefix\");\nvar debug = false; // (set to false when not debugging)\n\n// Processing elements:\n\nfunction map(key, val) {\n    \n    if (null == val.sip) {\n        return;\n    }    \n    var lookup = _custom['%%LOOKUP%%'].get(val['%%KEYFIELD%%']);\n    if (null == lookup) {\n        return;\n    }\n    var id = val._id;\n    val.alert = lookup;\n    delete val._id;\n    emit({'id':id}, val);\n} \n\nfunction reduce(key, vals) {   \n    reduce_internal(key, vals, true);\n}\nfunction combine(key, vals) {   \n    reduce_internal(key, vals, false);\n}\n\nfunction reduce_internal(key, vals, is_reducer) {   \n    var out = null;\n    while (vals.hasNext()) {\n        var val = vals.next();\n        if (null != val._id) { // this is an old record, don't re-emit\n            return;\n        }\n        emit(key, val);\n        break;\n    }//(end loop over records to combine)\n}\n\n",
                            "mapScript": "// (handled in globals)",
                            "memoryOptimized": "true",
                            "reduceScript": "// (handled in globals)",
                            "scriptLang": "javascript"
                        }
                    },
                    {
                        "display": "",
                        "tableOutput": {
                            "appendMode": "append_reduce",
                            "indexed": "true"
                        }
                    }
                ],
                "tags": ["lookup_table_apply"],
                "title": "TEMPLATE Lookup table applier - check records"
            }
        }
    },
    {
        "name":"AdvancedSourceBuilder",
        "displayName": "Advanced Source Builder"
    }
]