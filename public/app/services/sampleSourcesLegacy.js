/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


//Sample source "templates" for the newsource creator

var SAMPLE_SOURCES = {

	"empty":
	{
		"description": "",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Empty source template, eg for use with the Flow Builder UI",
		"processingPipeline": [
		]
	},
	"rss":
	{
		"description": "Get one of more RSS feeds, all the listed documents are extracted",
		"isPublic": true,
		"mediaType": "News",
		"title": "Basic RSS Source Template",
		"processingPipeline": [
			{
				"display": "Get one of more RSS feeds, all the listed documents are extracted",
				"feed": {
					"extraUrls": [
						{
							"url": "http://youraddress.com/then/e.g./news.rss"
						}
					]
				}
			},
			{
				"display": "Extract the useful text from the HTML",
				"textEngine": {
					"engineName": "default"
				}
			},
			{
				"display": "Enrich the document metadata with entities (people, places) and associations generated using NLP",
				"featureEngine": {
					"engineName": "default"
				}
			}
		]
	},
	"rss cyber": {
		"description": "Get one of more RSS feeds, all the listed documents are extracted",
		"extractType": "Feed",
		"isPublic": true,
		"mediaType": "News",
		"title": "Cyber RSS Source Template",
		"processingPipeline": [
			{
				"display": "Get one of more RSS feeds, all the listed documents are extracted",
				"feed": {
					"extraUrls": [
						{
							"url": "http://youraddress.com/then/e.g./news.rss"
						}
					]
				}
			},
			{
				"display": "",
				"harvest": {
					"duplicateExistingUrls": true,
					"searchCycle_secs": 1200
				}
			},
			{
				"display": "Extract the useful text from the HTML",
				"textEngine": {
					"engineName": "Boilerpipe",
					"exitOnError": true
				}
			},
			{
				"display": "Enrich the document metadata with entities (people, places) and associations generated using NLP",
				"featureEngine": {
					"assocFilter": "-.*",
					"engineName": "OpenCalais",
					"entityFilter": "+.*/(company|technology|emailaddress|industryterm|programminglanguage|country)$",
					"exitOnError": false
				}
			},
			{
				"contentMetadata": [
					{
						"fieldName": "Malware_Instance:Type",
						"flags": "i",
						"index": false,
						"script": "(harmful software| pup|malware|trojan|virus|downloader|down-loader|worm| bot|rootkit|root-kit|backdoor|back-door| rat|remote access tool|remote-access-tool|dropper|exploit kit|exploit-kit|exploitkit|ransomeware|ransome-ware|ransome ware)",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "Threat_Level",
						"flags": "i",
						"index": false,
						"script": "(((high|medium|low|severe|green|red|orange|yellow).(threat.level|threat|infocon|risk|risk.level)|(threat.level|threat|infocon|risk|risk.level)(:|:\\s|\\.|\\s)(high|medium|low|severe|green|red|orange|yellow)))",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "Threat_Actor:Category",
						"flags": "i",
						"index": false,
						"script": "(apt|apt.group|advanced.persistent.threat|threat.groups?|threat.group.actor|hackers?|hacking.group|nation.state.hackers?|espionage.group|malicious.attackers?|advanced.threat.actors?|attackers|state.sponsored|criminal.organization|organized.crime|hacktivist|hacktivist.group|hacktivist.network|cyber.terrorist) ",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "Classification_Name",
						"flags": "i",
						"index": false,
						"script": "((downloader|virus|trojan|dropper|intended|kit|garbage|pup|worm|rootkit|tr|vr|dr|gen|generic|virusorg|backdoor|hacktool|w32|email-worm|win-trogan|win32|pws-zbot|trojan-spy|net-worm)\\.([a-z0-9]{1,15}\\.){1,4}[a-z]{1})",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "ipv4-addr:public",
						"index": false,
						"replace": "$1",
						"script": "(?:^|[^0-9])([0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3})(?:$|[^0-9])",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "URL",
						"index": false,
						"script": "(https?|ftp)://[a-z0-9]{1}(\\S*|.*\\n)",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "Simple_Hash_Value:MD5",
						"index": false,
						"script": "([0-9a-fA-F]{32})",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "Simple_Hash_Value:SHA1",
						"index": false,
						"script": "([0-9a-fA-F]{40})",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "Simple_Hash_Value:SHA256",
						"index": false,
						"script": "([0-9a-fA-F]{64})",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "CVE_ID",
						"flags": "i",
						"index": false,
						"script": "(CVE-20[0-9]{2}-[0-9]{3,5})",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "Domain_Name",
						"flags": "i",
						"index": false,
						"script": "(^[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\\.([a-zA-Z]{1,6}|[a-zA-Z0-9-]{1,30}\\.[a-zA-Z]{2,4})$)",
						"scriptlang": "regex",
						"store": true
					},
					{
						"fieldName": "MSB_ID",
						"flags": "i",
						"script": "(MS[0-9]{2}-[a-zA-Z0-9]{3,4})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "RHSA_ID",
						"flags": "i",
						"script": "(Red Had Security Advisory 20[0-9]{2}-[0-9]{4})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "RHSA_ID",
						"flags": "i",
						"script": "(Red Had Security Advisory 20[0-9]{2}-[0-9]{4}-[0-9]{1,2})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "RHSA_ID",
						"flags": "i",
						"script": "((RSHA|RSEA)-20[0-9]{2}:[0-9]{4})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "RHSA_ID",
						"flags": "i",
						"script": "((RSHA|RSEA)-20[0-9]{2}:[0-9]{4}-[0-9]{1,2})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "USN_ID",
						"flags": "i",
						"script": "(USN-[0-9]{4}-[0-9]{2})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "HPSB_ID",
						"flags": "i",
						"script": "(HPSB[A-Z0-9]{5,8})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "CESA_ID",
						"flags": "i",
						"script": "(CESA-20[0-9]{2}:[0-9]{4})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "SUSEUA_ID",
						"flags": "i",
						"script": "(SUSE-S[U,A]-20[0-9]{2})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "APS_ID",
						"flags": "i",
						"script": "(APS[A-B]{1}[0-9]{2}-[0-9]{2})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "PANSA_ID",
						"flags": "i",
						"script": "(PAN-SA-20[0-9]{2}-[0-9]{4})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "OSXSU_ID",
						"flags": "i",
						"script": "(Security Update 20[0-9]{2}-[0-9]{3})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "CiscoSA_ID",
						"flags": "i",
						"script": "(cisco-sa-[0-9]{8}-[a-zA-Z]{1,10})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "CiscoSR_ID",
						"flags": "i",
						"script": "(cisco-sr-[0-9]{8}-[a-zA-Z]{1,10})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "VMSA_ID",
						"flags": "i",
						"script": "(VMSA-20[0-9]{2}-[0-9]{4})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "JSA_ID",
						"flags": "i",
						"script": "(JSA[0-9]{5})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "BNSD_ID",
						"flags": "i",
						"script": "(Security Definition [0-9]{1}.[0-9]{1}.[0-9]{5})",
						"scriptlang": "regex"
					},
					{
						"fieldName": "OCPU_ID",
						"flags": "i",
						"script": "(Critical Patch Update - [a-zA-Z0-9]{3,9} 20[0-9]{2}|Java SE Critical Patch Update - [a-zA-Z0-9]{3,9} 20[0-9]{2}|Java SE and Java for Business Critical Patch Update - [a-zA-Z0-9]{3,9} 20[0-9]{2}|Rev [0-9]{1}, [0-9]{2} [a-zA-Z0-9]{3,9} 20[0-9]{2})",
						"scriptlang": "regex"
					}
				],
				"display": ""
			},
			{
				"display": "",
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Malware_Instance:Type",
						"relevance": "1",
						"type": "Malware_Instance:Type",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Threat_Level",
						"relevance": "1",
						"type": "Threat_Level",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Threat_Actor:Category",
						"relevance": "1",
						"type": "Threat_Actor:Category",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "URL",
						"type": "URL",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Simple_Hash_Value:MD5",
						"type": "Simple_Hash_Value:MD5",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Simple_Hash_Value:SHA1",
						"type": "Simple_Hash_Value:SHA1",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Simple_Hash_Value:SHA256",
						"type": "Simple_Hash_Value:SHA256",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "ipv4-addr",
						"type": "ipv4-addr:public",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Classification_Name",
						"relevance": "1",
						"type": "Classification_Name",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Domain_Name",
						"type": "Domain_Name",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "CVE_ID",
						"relevance": "1.5",
						"type": "CVE_ID",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "MSB_ID",
						"relevance": "1.5",
						"type": "MSB_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "RHSA_ID",
						"relevance": "1.5",
						"type": "RHSA_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "USN_ID",
						"relevance": "1.5",
						"type": "USN_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "HPSB_ID",
						"relevance": "1.5",
						"type": "HPSB_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "CESA_ID",
						"relevance": "1.5",
						"type": "CESA_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "SUSEUA_ID",
						"relevance": "1.5",
						"type": "SUSEUA_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "APS_ID",
						"relevance": "1.5",
						"type": "APS_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "PANSA_ID",
						"relevance": "1.5",
						"type": "PANSA_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "OSXSU_ID",
						"relevance": "1.5",
						"type": "OSXSU_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "CiscoSA_ID",
						"relevance": "1.5",
						"type": "CiscoSA_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "CiscoSR_ID",
						"relevance": "1.5",
						"type": "CiscoSR_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "VMSA_ID",
						"relevance": "1.5",
						"type": "VMSA_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "JSA_ID",
						"relevance": "1.5",
						"type": "JSA_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "BNSD_ID",
						"relevance": "1.5",
						"type": "BNSD_ID"
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "OCPU_ID",
						"relevance": "1.5",
						"type": "OCPU_ID"
					}
				]
			}
		],
		"searchCycle_secs": 1200,
		"tags": [
			"Attackresearch",
			"RSS",
			"Cyber",
			"Feed"
		]
	},

	"web":
	{
		"description": "Extract each URL (re-extracting every 'updateCycle_secs') with the specified title and summary text",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Basic Web Page Source Template",
		"processingPipeline": [
			{
				"display": "Extract each document (re-extracting every 'updateCycle_secs') with the specified title and summary text",
				"web": {
					"extraUrls": [
						{
							"description": "Optional",
							"title": "Page Title",
							"url": "http://youraddress.com/then/e.g./title.html"
						}
					],
					"updateCycle_secs": 86400
				}
			},
			{
				"display": "Extract the useful text from the HTML",
				"textEngine": {
					"engineName": "default"
				}
			},
			{
				"display": "Enrich the document metadata with entities (people, places) and associations generated using NLP",
				"featureEngine": {
					"engineName": "default"
				}
			}
		]
	},

	"json_api_simple":
	{
		"description": "Specify one or more JSON (or XML or ...) endpoints from which to extract objects, each endpoint/URL generates a single document",
		"isPublic": true,
		"mediaType": "Record",
		"processingPipeline": [
			{
				"display": "Specify one or more JSON (or XML or ...) endpoints from which to extract objects, each endpoint/URL generates a single document",
				"web": {
					"extraUrls": [
						{
							"url": "http://youraddress.com/then/e.g./query?out=json",
							"title": "dummy: replaced below"
						}
					]
				}
			},
			{
				"harvest": {
					"duplicateExistingUrls": true,
					"searchCycle_secs": 600
				},
				"display": "Only check the API every 10 minutes (can be set to whatever you'd like)"
			},
			{
				"display": "Convert the text into a JSON object in the document's metadata field: _doc.metadata.json[0]",
				"contentMetadata": [
					{
						"fieldName": "json",
						"scriptlang": "javascript",
						"script": "var json = eval('('+text+')'); json; "
					}
				]
			},
			{
				"display": "Use the JSON fields to fill in useful document metadata, using js or substitutions, eg:",
				"docMetadata": {
					"appendTagsToDocs": true,
					"title": "$metadata.json.TITLE_FIELD",
					"publishedDate": "$SCRIPT( return _doc.metadata.json[0].DATE_FIELD; )",
					"fullText": "$metadata.json.TEXT_FIELD1 $metadata.json.TEXT_FIELD2"
				}
			},
			{
				"display": "Pass a text object generated from the object into the NLP for further enrichment, if enough text is present.",
				"featureEngine": {
					"engineName": "default",
					"exitOnError": true
				}
			},
			{
				"display": "For JSON records you often won't have enough text to perform NLP-based enrichment, but you can create entities directly from the fields, eg:",
				"entities": [
					{
						"useDocGeo": false,
						"type": "Person",
						"iterateOver": "metadata.json.people",
						"disambiguated_name": "$personName",
						"actual_name": "$personName",
						"dimension": "Who"
					},
					{
						"useDocGeo": false,
						"type": "Location",
						"disambiguated_name": "$metadata.json.placeName",
						"actual_name": "$metadata.json.placeName",
						"geotag": {
							"ontology_type": "point",
							"alternatives": [
								{
									"strictMatch": true,
									"country": "$metadata.json.country",
									"geoType": "auto",
									"city": "$metadata.json.city"
								}
							],
							"lon": "$metadata.json.longitude",
							"geoType": "manual",
							"lat": "$metadata.json.latitude"
						},
						"dimension": "Where"
					}
				]
			},
			{
				"display": "For JSON records you often won't have enough text to perform NLP-based enrichment, but you can create associations directly from the fields, eg:",
				"associations": [
					{
						"verb": "visits",
						"assoc_type": "Event",
						"entity1": "Person",
						"verb_category": "location",
						"iterateOver": "entity1,entity2",
						"entity2": "Location"
					},
					{
						"verb": "resides_at",
						"entity1_index": "${metadata.json.author.personName}/person",
						"assoc_type": "Fact",
						"entity2_index": "${metadata.json.author.homeAddress}/location",
						"verb_category": "location"
					},
					{
						"verb": "said",
						"entity1": "${metadata.json.author.personName}/person",
						"verb_category": "quotation",
						"entity2": "${metadata.json.quotation}"
					}
				]
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON object itself (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		],
		"title": "Basic Simple JSON API Source Template"
	},

	"json_api_links":
	{
		"description": "Specify one or more JSON (or XML or ...) endpoints from which to extract objects, each endpoint/URL generates one or more links that can be harvested from the web",
		"isPublic": true,
		"mediaType": "Social",
		"processingPipeline": [
			{
				"display": "Specify one or more JSON (or XML or ...) endpoints from which to extract objects, each endpoint/URL generates one or more links that can be harvested from the web",
				"web": {
					"extraUrls": [
						{
							"url": "http://youraddress.com/then/e.g./query?out=json",
							"title": "doesn't matter, this document is deleted by the splitter element"
						}
					]
				}
			},
			{
				"display": "A global space to group all the complex parsing and processing logic, can be called from anywhere",
				"globals": {
					"scripts": [
						"function create_links( urls, input_array )\n{\n    for (var x in input_array) {\n        var input = input_array[x];\n        urls.push( { url: input.url, title: input.title, description: input.desc, publishedData: input.date });\n    }\n}"
					],
					"scriptlang": "javascript"
				}
			},
			{
				"harvest": {
					"duplicateExistingUrls": true,
					"searchCycle_secs": 600
				},
				"display": "Only check the API every 10 minutes (can be set to whatever you'd like)"
			},
			{
				"display": "Convert API reply into the web documents to which they point",
				"links": {
					"scriptlang": "javascript",
					"numPages": 10,
					"extraMeta": [
						{
							"fieldName": "json",
							"script": "var json = eval('('+text+')'); json; ",
							"scriptlang": "javascript"
						}
					],
					"scriptflags": "m",
					"stopPaginatingOnDuplicate": false,
					"numResultsPerPage": 1,
					"script": "var urls = []; create_links( urls, _metadata.json[0].data ); urls;"
				}
			},
			{
				"display": "Extract the useful text from the HTML",
				"textEngine": {
					"engineName": "default"
				}
			},
			{
				"display": "Enrich the document metadata with entities (people, places) and associations generated using NLP",
				"featureEngine": {
					"engineName": "default"
				}
			}
		],
		"title": "Basic Complex JSON API Source Template #1 (link following)"
	},

	"json_api_complex":
	{
		"description": "Specify one or more JSON (or XML or ...) endpoints from which to extract objects, each endpoint/URL generates multiple documents",
		"isPublic": true,
		"mediaType": "Record",
		"processingPipeline": [
			{
				"display": "Specify one or more JSON (or XML or ...) endpoints from which to extract objects, each endpoint/URL generates multiple documents",
				"feed": {
					"extraUrls": [
						{
							"url": "http://youraddress.com/query?out=json"
						}
					]
				}
			},
			{
				"display": "A global space to group all the complex parsing and processing logic, can be called from anywhere",
				"globals": {
					"scripts": [
						"function create_links( urls, input_array )\n{\n    for (var x in input_array) {\n        var input = input_array[x];\n        urls.push( { url: input.url, title: input.title, description: input.desc, publishedData: input.date, fullText: input.text });\n    }\n}"
					],
					"scriptlang": "javascript"
				}
			},
			{
				"harvest": {
					"duplicateExistingUrls": true,
					"searchCycle_secs": 600
				},
				"display": "Only check the API every 10 minutes (can be set to whatever you'd like)"
			},
			{
				"display": "Convert the text into a JSON object in the document's metadata field: _doc.metadata.json[0]",
				"contentMetadata": [
					{
						"store": true,
						"fieldName": "json",
						"index": false,
						"script": "var json = eval('('+text+')'); json; ",
						"scriptlang": "javascript"
					}
				]
			},
			{
				"display": "Take the original documents, split them using their metadaata into new documents, and then delete the originals",
				"splitter": {
					"deleteExisting": true,
					"scriptflags": "m",
					"script": "var urls = []; create_links( urls, _metadata.json[0].data ); urls;",
					"scriptlang": "javascript"
				}
			},
			{
				"display": "Convert the text into a JSON object in the document's metadata field: _doc.metadata.json[0]",
				"contentMetadata": [
					{
						"store": true,
						"fieldName": "json",
						"index": false,
						"script": "var json = eval('('+text+')'); json; ",
						"scriptlang": "javascript"
					}
				]
			},
			{
				"display": "Use the JSON fields to fill in useful document metadata, using js or substitutions, eg:",
				"docMetadata": {
					"appendTagsToDocs": true,
					"title": "$metadata.json.TITLE_FIELD",
					"publishedDate": "$SCRIPT( return _doc.metadata.json[0].DATE_FIELD; )",
					"fullText": "$metadata.json.TEXT_FIELD1 $metadata.json.TEXT_FIELD2"
				}
			},
			{
				"display": "Pass a text object generated from the object into the NLP for further enrichment, if enough text is present.",
				"featureEngine": {
					"exitOnError": true,
					"engineName": "default"
				}
			},
			{
				"display": "For JSON records you often won't have enough text to perform NLP-based enrichment, but you can create entities directly from the fields, eg:",
				"entities": [
					{
						"useDocGeo": false,
						"type": "Person",
						"disambiguated_name": "$personName",
						"actual_name": "$personName",
						"iterateOver": "metadata.json.people",
						"dimension": "Who"
					},
					{
						"useDocGeo": false,
						"type": "Location",
						"disambiguated_name": "$metadata.json.placeName",
						"actual_name": "$metadata.json.placeName",
						"geotag": {
							"ontology_type": "point",
							"alternatives": [
								{
									"strictMatch": true,
									"country": "$metadata.json.country",
									"geoType": "auto",
									"city": "$metadata.json.city"
								}
							],
							"lon": "$metadata.json.longitude",
							"geoType": "manual",
							"lat": "$metadata.json.latitude"
						},
						"dimension": "Where"
					}
				]
			},
			{
				"display": "For JSON records you often won't have enough text to perform NLP-based enrichment, but you can create associations directly from the fields, eg:",
				"associations": [
					{
						"verb": "visits",
						"assoc_type": "Event",
						"entity1": "Person",
						"verb_category": "location",
						"iterateOver": "entity1,entity2",
						"entity2": "Location"
					},
					{
						"verb": "resides_at",
						"entity1_index": "${metadata.json.author.personName}/person",
						"assoc_type": "Fact",
						"entity2_index": "${metadata.json.author.homeAddress}/location",
						"verb_category": "location"
					},
					{
						"verb": "said",
						"entity1": "${metadata.json.author.personName}/person",
						"verb_category": "quotation",
						"entity2": "${metadata.json.quotation}"
					}
				]
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON object itself (the full text, entities etc still get indexed)",
				"searchIndex": {
					"indexOnIngest": true,
					"metadataFieldList": "+"
				}
			}
		],
		"title": "Basic Complex JSON API Source Template #2 (document splitting)"
	},

	"local_file":
	{
		"description": "For single node clusters: get files from the local file system (otherwise use 'remote file'/'amazon'). Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
		"extractType": "File",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Basic Local File Source Template (any file type)",
		"processingPipeline": [
			{
				"display": "For single node clusters: the location of the directory on the local file system (otherwise use 'remote file'/'amazon'). Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlRootLevelValues": [],
					"url": "file:///directory1/directory2/"
				}
			},
			{
				"display": "If a JSON/XML object you should do further extraction first, see JSON samples. For 'office' types, the text has been extracted already.",
				"featureEngine": {
					"engineName": "default"
				}
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML/CSV object itself if an object (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		]
	},

	"remote_file":
	{
		"description": "Get files from the Windows/Samba file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Basic Fileshare Source Template (any file type)",
		"processingPipeline": [
			{
				"display": "The location of the directory on the Windows/Samba file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlRootLevelValues": [],
					"domain": "DOMAIN",
					"password": "PASSWORD",
					"username": "USERNAME",
					"url": "smb://HOST:PORT/share/directory1/"
				}
			},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {
					"searchCycle_secs": 3600
				}
			},
			{
				"display": "If a JSON/XML object you should do further extraction first, see JSON samples. For 'office' types, the text has been extracted already.",
				"featureEngine": {
					"engineName": "default"
				}
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML/CSV object itself if an object (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		]
	},

	"remote_file_logs":
	{
		"description": "Extract line-separated (eg csv) files from the Windows/Samba file share",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Basic Fileshare Source Template (log file type)",
		"processingPipeline": [
			{
				"display": "The location of the directory containing line-separated (eg csv) files on the Windows/Samba file share",
				"file": {
					"XmlRootLevelValues": [],
					"type": "csv",
					"domain": "DOMAIN",
					"password": "PASSWORD",
					"username": "USERNAME",
					"url": "smb://HOST:PORT/share/directory1/"
				}
			},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {
					"searchCycle_secs": 3600
				}
			},
			{
				"display": "Use the columns to fill in useful document metadata, using js or substitutions, eg:",
				"docMetadata": {
					"appendTagsToDocs": true,
					"title": "$metadata.csv.TITLE_FIELD",
					"publishedDate": "$SCRIPT( return _doc.metadata.csv[0].DATE_FIELD; )",
					"fullText": "$metadata.csv.TEXT_FIELD1 $metadata.csv.TEXT_FIELD2"
				}
			},
			{
				"display": "For log records you often won't have enough text to perform NLP-based enrichment, but you can create entities directly from the columns, eg:",
				"entities": [
					{
						"useDocGeo": false,
						"type": "Person",
						"disambiguated_name": "$metadata.csv.personName",
						"actual_name": "$metadata.csv.personName",
						"dimension": "Who"
					},
					{
						"useDocGeo": false,
						"type": "Location",
						"disambiguated_name": "$metadata.csv.placeName",
						"actual_name": "$metadata.csv.placeName",
						"geotag": {
							"ontology_type": "point",
							"alternatives": [
								{
									"strictMatch": true,
									"country": "$metadata.csv.country",
									"geoType": "auto",
									"city": "$metadata.csv.city"
								}
							],
							"lon": "$metadata.csv.longitude",
							"geoType": "manual",
							"lat": "$metadata.csv.latitude"
						},
						"dimension": "Where"
					}
				]
			},
			{
				"display": "For log records you often won't have enough text to perform NLP-based enrichment, but you can create associations directly from the columns, eg:",
				"associations": [
					{
						"verb": "visits",
						"assoc_type": "Event",
						"entity1": "Person",
						"verb_category": "location",
						"iterateOver": "entity1,entity2",
						"entity2": "Location"
					},
					{
						"verb": "resides_at",
						"entity1_index": "${metadata.csv.personName}/person",
						"assoc_type": "Fact",
						"entity2_index": "${metadata.csv.homeAddress}/location",
						"verb_category": "location"
					},
					{
						"verb": "said",
						"entity1": "${metadata.csv.personName}/person",
						"verb_category": "quotation",
						"entity2": "${metadata.csv.quotation}"
					}
				]
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the CSV object itself",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		]
	},

	"amazon_file":
	{
		"description": "Get files from the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Basic Amazon S3 Source Template (and file type)",
		"processingPipeline": [
			{
				"display": "The location of the bucket/directory on the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlRootLevelValues": [],
					"password": "AWS_SECRETKEY",
					"username": "AWS_ACCESSID",
					"url": "s3://BUCKET_NAME/FOLDERS/"
				}
			},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {
					"searchCycle_secs": 3600
				}
			},
			{
				"display": "If a JSON/XML object you should do further extraction first, see JSON samples. For 'office' types, the text has been extracted already.",
				"featureEngine": {
					"engineName": "default"
				}
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML object itself if a JSON object (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		]
	},

	"infinite_share_upload":
	{
		"description": "Ingest a zip or JSON file uploaded to Infinit.e as a 'share'. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
		"extractType": "Ingest ZIP archives or JSON records from Infinit.e shares",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Title",
		"processingPipeline": [
			{
				"display": "The location of a zip or JSON file uploaded to Infinit.e as a 'share'. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlRootLevelValues": [],
					"url": "inf://share/SHAREID/miscDescription/"
				}
			},
			{
				"display": "If a JSON/XML object you should do further extraction first, see JSON samples. For 'office' types, the text has been extracted already.",
				"featureEngine": {
					"engineName": "default"
				}
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML object itself if a JSON object (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		]
	},

	"infinite_custom_ingest":
	{
		"description": "Point to an Infinit.e custom job and ingest the results as JSON.",
		"extractType": "File",
		"isPublic": true,
		"mediaType": "Report",
		"title": "Convert the output of Infinit.e custom analytics into documents",
		"processingPipeline": [
			{
				"display": "Point to an Infinit.e custom job and ingest the results as JSON.",
				"file": {
					"type": "json",
					"XmlRootLevelValues": [],
					"url": "inf://custom/JOBID_OR_JOBTITLE/miscDescription/"
				}
			},
			{
				"display": "Use the JSON fields to fill in useful document metadata, using js or substitutions, eg:",
				"docMetadata": {
					"appendTagsToDocs": true,
					"title": "$metadata.json.TITLE_FIELD",
					"publishedDate": "$SCRIPT( return _doc.metadata.json[0].DATE_FIELD; )",
					"fullText": "$metadata.json.TEXT_FIELD1 $metadata.json.TEXT_FIELD2"
				}
			},
			{
				"display": "For JSON records you often won't have enough text to perform NLP-based enrichment, but you can create entities directly from the fields, eg:",
				"entities": [
					{
						"useDocGeo": false,
						"type": "Person",
						"iterateOver": "metadata.json.people",
						"disambiguated_name": "$personName",
						"actual_name": "$personName",
						"dimension": "Who"
					},
					{
						"useDocGeo": false,
						"type": "Location",
						"disambiguated_name": "$metadata.json.placeName",
						"actual_name": "$metadata.json.placeName",
						"geotag": {
							"ontology_type": "point",
							"alternatives": [
								{
									"strictMatch": true,
									"country": "$metadata.json.country",
									"geoType": "auto",
									"city": "$metadata.json.city"
								}
							],
							"lon": "$metadata.json.longitude",
							"geoType": "manual",
							"lat": "$metadata.json.latitude"
						},
						"dimension": "Where"
					}
				]
			},
			{
				"display": "For JSON records you often won't have enough text to perform NLP-based enrichment, but you can create associations directly from the fields, eg:",
				"associations": [
					{
						"verb": "visits",
						"assoc_type": "Event",
						"entity1": "Person",
						"verb_category": "location",
						"iterateOver": "entity1,entity2",
						"entity2": "Location"
					},
					{
						"verb": "resides_at",
						"entity1_index": "${metadata.json.author.personName}/person",
						"assoc_type": "Fact",
						"entity2_index": "${metadata.json.author.homeAddress}/location",
						"verb_category": "location"
					},
					{
						"verb": "said",
						"entity1": "${metadata.json.author.personName}/person",
						"verb_category": "quotation",
						"entity2": "${metadata.json.quotation}"
					}
				]
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML object itself if a JSON object (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		]
	},

	"database":
	{
		"description": "Access a SQL database - the first time through 'query' is called, subsequently 'deltaQuery' is called.",
		"extractType": "Database",
		"isPublic": true,
		"mediaType": "Record",
		"title": "Basic SQL Database Source Template",
		"processingPipeline": [
			{
				"display": "The parameters necessary to access a SQL database - the first time through 'query' is called, subsequently 'deltaQuery' is called.",
				"database": {
					"authentication": {
						"password": "PASSWORD",
						"username": "USERNAME"
					},
					"databaseName": "DATABASE",
					"databaseType": "mysql",
					"deleteQuery": "",
					"deltaQuery": "select * from TABLE where TIME_FIELD >= (select adddate(curdate(),-7))",
					"hostname": "DB_HOST",
					"port": "3306",
					"primaryKey": "KEY_FIELD",
					"publishedDate": "TIME_FIELD",
					"query": "select * from TABLE",
					"snippet": "DESC_FIELD",
					"title": "TITLE_FIELD",
					"url": "jdbc:mysql://DB_HOST:3306/DATABASE"
				}
			},
			{
				"display": "(Only check the DB every 5 minutes - you can set this to whatever time you want)",
				"harvest": {
					"searchCycle_secs": 300
				}
			},
			{
				"display": "Use the SQL fields to fill in useful document metadata, using js or substitutions, eg:",
				"docMetadata": {
					"appendTagsToDocs": true,
					"title": "$metadata.TITLE_FIELD",
					"publishedDate": "$SCRIPT( return _doc.metadata.DATE_FIELD[0]; )",
					"fullText": "$metadata.TEXT_FIELD1 $metadata.TEXT_FIELD2"
				}
			},
			{
				"display": "For SQL records you often won't have enough text to perform NLP-based enrichment, but you can create entities directly from the SQL columns, eg:",
				"entities": [
					{
						"useDocGeo": false,
						"type": "Person",
						"disambiguated_name": "$metadata.personName",
						"actual_name": "$metadata.personName",
						"dimension": "Who"
					},
					{
						"useDocGeo": false,
						"type": "Location",
						"disambiguated_name": "$metadata.placeName",
						"actual_name": "$metadata.placeName",
						"geotag": {
							"ontology_type": "point",
							"alternatives": [
								{
									"strictMatch": true,
									"country": "$metadata.country",
									"geoType": "auto",
									"city": "$metadata.city"
								}
							],
							"lon": "$metadata.longitude",
							"geoType": "manual",
							"lat": "$metadata.latitude"
						},
						"dimension": "Where"
					}
				]
			},
			{
				"display": "For SQL records you often won't have enough text to perform NLP-based enrichment, but you can create associations directly from the SQL columns, eg:",
				"associations": [
					{
						"verb": "visits",
						"assoc_type": "Event",
						"entity1": "Person",
						"verb_category": "location",
						"iterateOver": "entity1,entity2",
						"entity2": "Location"
					},
					{
						"verb": "resides_at",
						"entity1_index": "${metadata.personName}/person",
						"assoc_type": "Fact",
						"entity2_index": "${metadata.homeAddress}/location",
						"verb_category": "location"
					},
					{
						"verb": "said",
						"entity1": "${metadata.personName}/person",
						"verb_category": "quotation",
						"entity2": "${metadata.quotation}"
					}
				]
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the DB fields themselves (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		]
	},

	"logstash": {
		"description": "Use logstash to pull records into the logstash-specific index (does not generate Infinit.e documents like other harvest types).",
		"extractType": "Logstash",
		"isPublic": true,
		"mediaType": "Record",
		"title": "Logstash template source",
		"processingPipeline": [
			{
				"display": "Just contains a string in which to put the logstash configuration (minus the output, which is appended by Infinit.e)",
				"logstash": {
					"config": "input {\n}\n\nfilter {\n}\n\n",
					"streaming": true,
					"distributed": false,
					"testDebugOutput": false,
					"testInactivityTimeout_secs": 10
				}
			}
		]
	},

	"Yahoo Boss Cyber": {
		"templates" : [{
			"template_type" : "yahoo",
			"template_per_url_cost" : 0.0015,
			"extractType" : "Feed",
			"mediaType" : "News",
			"processingPipeline" : [{
				"display" : "Extract each document (re-extracting every \u0027updateCycle_secs\u0027) with the specified title and summary text",
				"web" : {
					"extraUrls" : [{
						"url" : "https://yboss.yahooapis.com/ysearch/web?q\u003d??search_terms??\u0026abstract\u003dlong\u0026style\u003draw\u0026format\u003djson\u0026start\u003d0\u0026count\u003d50"
					},
						{"waitTimeOverride_ms": 20000
						}
					]
				}
			}, {
				"harvest" : {
					"duplicateExistingUrls" : false,
					"searchCycle_secs" : "??search_cycle??"
				}
			}, {
				"globals" : {
					"scriptlang" : "javascript",
					"scripts" : ["function decode(api, doc_url, doc_desc)\n{     \n    var links\u003d [];    \n    var web \u003d api.bossresponse.web;\n    var results \u003d web.results;\n    //add this set of search results on the stack\n    for ( var x in results )\n    {        \n        var result \u003d results[x];\n        links.push({url:result[\"url\"],title:result[\"title\"],description:result[\"abstract\"], fullText:doc_desc});       \n    }\n    var new_start \u003d Number(web.count) + Number(web.start);\n    \n    //check to see if we should get the next set of search results\n    if ( web.count \u003d\u003d 50 )\n    {\n        //update start to next set\n        var new_url \u003d doc_url.replace(/\u0026start\u003d\\d+/,\"\u0026start\u003d\" + new_start);        \n        links.push({url:new_url, spiderOut:true, description:doc_desc});\n    }\n    return links;\n}"]
				}
			}, {
				"display" : "",
				"links" : {
					"authExtractor" : "replace with share id of extractor JAR",
					"authExtractorOptions" : {
						"consumer_key" : "#IKANOW{replace with share id of keys.consumer_key}",
						"consumer_secret" : "#IKANOW{replace with share id of keys.consumer_secret}"
					},
					"maxDepth" : "??search_depth??",
					"script" : "var links \u003d decode(eval(\u0027(\u0027+text+\u0027)\u0027), _doc.url, _doc.description); links;",
					"scriptflags" : "dt",
					"scriptlang" : "javascript",
					"stopPaginatingOnDuplicate" : false
				}
			}, {
				"display" : "Extract the useful text from the HTML",
				"textEngine" : {
					"engineName" : "Boilerpipe",
					"exitOnError" : true
				}
			}, {
				"display" : "Enrich the document metadata with entities (people, places) and associations generated using NLP",
				"featureEngine" : {
					"engineName" : "OpenCalais",
					"exitOnError" : true
				}
			},
				{
					"contentMetadata": [
						{
							"fieldName": "Malware_Instance:Type",
							"flags": "i",
							"index": false,
							"script": "(harmful software| pup|malware|trojan|virus|downloader|down-loader|worm| bot|rootkit|root-kit|backdoor|back-door| rat|remote access tool|remote-access-tool|dropper|exploit kit|exploit-kit|exploitkit|ransomeware|ransome-ware|ransome ware)",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "Threat_Level",
							"flags": "i",
							"index": false,
							"script": "(((high|medium|low|severe|green|red|orange|yellow).(threat.level|threat|infocon|risk|risk.level)|(threat.level|threat|infocon|risk|risk.level)(:|:\\s|\\.|\\s)(high|medium|low|severe|green|red|orange|yellow)))",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "Threat_Actor:Category",
							"flags": "i",
							"index": false,
							"script": "(apt|apt.group|advanced.persistent.threat|threat.groups?|threat.group.actor|hackers?|hacking.group|nation.state.hackers?|espionage.group|malicious.attackers?|advanced.threat.actors?|attackers|state.sponsored|criminal.organization|organized.crime|hacktivist|hacktivist.group|hacktivist.network|cyber.terrorist) ",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "Classification_Name",
							"flags": "i",
							"index": false,
							"script": "((downloader|virus|trojan|dropper|intended|kit|garbage|pup|worm|rootkit|tr|vr|dr|gen|generic|virusorg|backdoor|hacktool|w32|email-worm|win-trogan|win32|pws-zbot|trojan-spy|net-worm)\\.([a-z0-9]{1,15}\\.){1,4}[a-z]{1})",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "ipv4-addr:public",
							"index": false,
							"replace": "$1",
							"script": "(?:^|[^0-9])([0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3})(?:$|[^0-9])",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "URL",
							"index": false,
							"script": "(https?|ftp)://[a-z0-9]{1}(\\S*|.*\\n)",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "Simple_Hash_Value:MD5",
							"index": false,
							"script": "([0-9a-fA-F]{32})",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "Simple_Hash_Value:SHA1",
							"index": false,
							"script": "([0-9a-fA-F]{40})",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "Simple_Hash_Value:SHA256",
							"index": false,
							"script": "([0-9a-fA-F]{64})",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "CVE_ID",
							"flags": "i",
							"index": false,
							"script": "(CVE-20[0-9]{2}-[0-9]{3,5})",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "Domain_Name",
							"flags": "i",
							"index": false,
							"script": "(^[a-zA-Z0-9][a-zA-Z0-9-_]{0,61}[a-zA-Z0-9]{0,1}\\.([a-zA-Z]{1,6}|[a-zA-Z0-9-]{1,30}\\.[a-zA-Z]{2,4})$)",
							"scriptlang": "regex",
							"store": true
						},
						{
							"fieldName": "MSB_ID",
							"flags": "i",
							"script": "(MS[0-9]{2}-[a-zA-Z0-9]{3,4})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "RHSA_ID",
							"flags": "i",
							"script": "(Red Had Security Advisory 20[0-9]{2}-[0-9]{4})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "RHSA_ID",
							"flags": "i",
							"script": "((RSHA|RSEA)-20[0-9]{2}:[0-9]{4})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "USN_ID",
							"flags": "i",
							"script": "(USN-[0-9]{4}-[0-9]{2})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "HPSB_ID",
							"flags": "i",
							"script": "(HPSB[A-Z0-9]{5,8})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "CESA_ID",
							"flags": "i",
							"script": "(CESA-20[0-9]{2}:[0-9]{4})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "SUSEUA_ID",
							"flags": "i",
							"script": "(SUSE-S[U,A]-20[0-9]{2})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "APS_ID",
							"flags": "i",
							"script": "(APS[A-B]{1}[0-9]{2}-[0-9]{2})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "PANSA_ID",
							"flags": "i",
							"script": "(PAN-SA-20[0-9]{2}-[0-9]{4})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "OSXSU_ID",
							"flags": "i",
							"script": "(Security Update 20[0-9]{2}-[0-9]{3})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "CiscoSA_ID",
							"flags": "i",
							"script": "(cisco-sa-[0-9]{8}-[a-zA-Z]{1,10})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "CiscoSR_ID",
							"flags": "i",
							"script": "(cisco-sr-[0-9]{8}-[a-zA-Z]{1,10})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "VMSA_ID",
							"flags": "i",
							"script": "(VMSA-20[0-9]{2}-[0-9]{4})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "JSA_ID",
							"flags": "i",
							"script": "(JSA[0-9]{5})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "BNSD_ID",
							"flags": "i",
							"script": "(Security Definition [0-9]{1}.[0-9]{1}.[0-9]{5})",
							"scriptlang": "regex"
						},
						{
							"fieldName": "OCPU_ID",
							"flags": "i",
							"script": "(Critical Patch Update - [a-zA-Z0-9]{3,9} 20[0-9]{2}|Java SE Critical Patch Update - [a-zA-Z0-9]{3,9} 20[0-9]{2}|Java SE and Java for Business Critical Patch Update - [a-zA-Z0-9]{3,9} 20[0-9]{2}|Rev [0-9]{1}, [0-9]{2} [a-zA-Z0-9]{3,9} 20[0-9]{2})",
							"scriptlang": "regex"
						}
					]
				},
				{
					"display": "",
					"entities": [
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Malware_Instance:Type",
							"relevance": "1",
							"type": "Malware_Instance:Type",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Threat_Level",
							"relevance": "1",
							"type": "Threat_Level",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Threat_Actor:Category",
							"relevance": "1",
							"type": "Threat_Actor:Category",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "URL",
							"type": "URL",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Simple_Hash_Value:MD5",
							"type": "Simple_Hash_Value:MD5",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Simple_Hash_Value:SHA1",
							"type": "Simple_Hash_Value:SHA1",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Simple_Hash_Value:SHA256",
							"type": "Simple_Hash_Value:SHA256",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "ipv4-addr:public",
							"type": "ipv4-addr:public",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Classification_Name",
							"relevance": "1",
							"type": "Classification_Name",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "Domain_Name",
							"type": "Domain_Name",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "CVE_ID",
							"relevance": "1.5",
							"type": "CVE_ID",
							"useDocGeo": false
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "MSB_ID",
							"relevance": "1.5",
							"type": "MSB_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "RHSA_ID",
							"relevance": "1.5",
							"type": "RHSA_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "USN_ID",
							"relevance": "1.5",
							"type": "USN_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "HPSB_ID",
							"relevance": "1.5",
							"type": "HPSB_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "CESA_ID",
							"relevance": "1.5",
							"type": "CESA_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "SUSEUA_ID",
							"relevance": "1.5",
							"type": "SUSEUA_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "APS_ID",
							"relevance": "1.5",
							"type": "APS_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "PANSA_ID",
							"relevance": "1.5",
							"type": "PANSA_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "OSXSU_ID",
							"relevance": "1.5",
							"type": "OSXSU_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "CiscoSA_ID",
							"relevance": "1.5",
							"type": "CiscoSA_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "CiscoSR_ID",
							"relevance": "1.5",
							"type": "CiscoSR_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "VMSA_ID",
							"relevance": "1.5",
							"type": "VMSA_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "JSA_ID",
							"relevance": "1.5",
							"type": "JSA_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "BNSD_ID",
							"relevance": "1.5",
							"type": "BNSD_ID"
						},
						{
							"dimension": "What",
							"disambiguated_name": "$SCRIPT( return _value;)",
							"iterateOver": "OCPU_ID",
							"relevance": "1.5",
							"type": "OCPU_ID"
						}
					]
				}
			],
			"title" : "Yahoo Search: ??search_terms??",
			"description" : "Yahoo API search with query: ??search_terms??",
			"tags" : ["yahoo"]
		}
		]
	},

	"iSight Report Template July 2015" : {
		"description": "iSight Report Template July 2015",
		"extractType": "File",
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "The location of the bucket/directory on the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlPreserveCase": false,
					"password": "password",
					"pathExclude": ".*/todelete/.*",
					"type": "json",
					"url": "s3://demo.cyber.ikanow.com/cyber_data/iSight/",
					"username": "username"
				}
			},
			{
				"display": "",
				"docMetadata": {
					"appendTagsToDocs": true,
					"description": "$SCRIPT( return createDescription(_doc.metadata); )",
					"fullText": "$SCRIPT( return JSON.stringify(_doc.metadata.json[0]); )",
					"publishedDate": "$metadata.json.report.publishDate",
					"title": "$metadata.json.report.title"
				}
			},
			{
				"globals": {
					"scriptlang": "javascript",
					"scripts": [
						"function translateEntityType(ent_name) {\n    if (null !== ent_name)\n    {\n        switch (ent_name.toUpperCase()) {\n            case \"E:F\":\n                return \"Functional\";\n                break;\n            case \"E:POC\":\n                return \"Proof-of-Concept\";\n                break;\n            case \"E:U\":\n                return \"Unproven\";\n                break;\n            case \"E:ND\":\n                return \"Not Defined\";\n                break;\n            case \"A:N\":\n                return \"None\";\n                break;\n            case \"A:P\":\n                return \"Partial\";\n                break;\n            case \"A:C\":\n                return \"Complete\";\n                break;\n            case \"RL:OF\":\n                return \"Official Fix\";\n                break;\n            case \"RL:TF\":\n                return \"Temporary Fix\";\n                break;\n            case \"RL:W\":\n                return \"Workaround\";\n                break;\n            case \"RL:U\":\n                return \"Unavailable\";\n                break;\n            case \"RL:ND\":\n                return \"Not Defined\";\n                break;\n            case \"C:N\":\n                return \"None\";\n                break;\n            case \"C:P\":\n                return \"Partial\";\n                break;\n            case \"C:C\":\n                return \"Complete\";\n                break;\n            case \"I:N\":\n                return \"None\";\n                break;\n            case \"I:P\":\n                return \"Partial\";\n                break;\n            case \"I:C\":\n                return \"Complete\";\n                break;\n            case \"AV:L\":\n                return \"Local\";\n                break;\n            case \"AV:A\":\n                return \"Partial\";\n                break;\n            case \"AV:N\":\n                return \"Complete\";\n                break;\n            case \"AC:H\":\n                return \"High\";\n                break;\n            case \"AC:M\":\n                return \"Medium\";\n                break;\n            case \"AC:L\":\n                return \"Low\";\n                break;\n            case \"Au:M\":\n                return \"Multiple\";\n                break;\n            case \"Au:S\":\n                return \"Single\";\n                break;\n            case \"Au:N\":\n                return \"None\";\n                break;\n            case \"RC:UC\":\n                return \"Unconfirmed\";\n                break;\n            case \"RC:UR\":\n                return \"Uncorroborated\";\n                break;\n            case \"RC:C\":\n                return \"Confirmed\";\n                break;\n            case \"RC:ND\":\n                return \"Not Defined\";\n                break;\n            case \"CDP:N\":\n                return \"None\";\n                break;\n            case \"CDP:L\":\n                return \"Low\";\n                break;\n            case \"CDP:LM\":\n                return \"Low-Medium\";\n                break;\n            case \"CDP:MH\":\n                return \"Medium-High\";\n                break;\n            case \"CDP:H\":\n                return \"High\";\n                break;\n            case \"CDP:ND\":\n                return \"Not Defined\";\n                break;\n            case \"TD:N\":\n                return \"None\";\n                break;\n            case \"TD:L\":\n                return \"Low\";\n                break;\n            case \"TD:M\":\n                return \"Medium\";\n                break;\n            case \"TD:H\":\n                return \"High\";\n                break;\n            case \"TD:ND\":\n                return \"Not Defined\";\n                break;\n\n            default:\n                return ent_name;\n        }\n    }\n    return null;\n}\n\nfunction createDescription(metadata) {\n    var description = \"<html>\";\n    if (null != metadata.json[0].report.analysis) {\n        description += metadata.json[0].report.analysis + \" \";\n    }\n        if (null != metadata.json[0].report.vendorFix) {\n        description += metadata.json[0].report.vendorFix + \" \";\n    }\n            if (null != metadata.json[0].report.mitigationDetails) {\n        description += metadata.json[0].report.mitigationDetails + \" \";\n    }\n                if (null != metadata.json[0].report.mitigationDetails) {\n        description += metadata.json[0].report.mitigationDetails + \" \";\n    }\n                if (null != metadata.json[0].report.execSummary) {\n        description += metadata.json[0].report.execSummary;\n    }\n    description += \"<\\/html>\";\n    return description;\n}"
					]
				}
			},
			{
				"contentMetadata": [
					{
						"fieldName": "Malware_Instance:Type",
						"flags": "m",
						"script": "(harmful software| pup|malware|trojan|virus|downloader|down-loader|worm| bot|rootkit|root-kit|backdoor|back-door| rat|remote access tool|remote-access-tool|dropper|exploit kit|exploit-kit|exploitkit|ransomeware|ransome-ware|ransome ware)",
						"scriptlang": "regex"
					},
					{
						"fieldName": "CVE_ID",
						"flags": "m",
						"script": "(CVE-20[0-9]{2}-[0-9]{3,5})",
						"scriptlang": "regex"
					}
				]
			},
			{
				"display": "Full reports",
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "Malware_Instance:Type",
						"relevance": "1.5",
						"type": "attack_name",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _value;)",
						"iterateOver": "CVE_ID",
						"relevance": "1.5",
						"type": "CVE_ID",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.cveIds.cveId",
						"type": "CVE_ID",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.ThreatScape.product",
						"type": "ThreatScape",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.riskRating",
						"type": "CVSS_Score",
						"useDocGeo": false
					},
					{
						"dimension": "Who",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.technologySection.technologies.vendor",
						"type": "Organization",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.exploitationVectors.exploitationVector",
						"type": "iSight:VulnerabilityCategory",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.exploitationConsequence",
						"type": "iSight:VulnerabilityCategory",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.sourceSection.source.urls.url",
						"type": "Source",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.vendorFixUrls.vendorFixUrl",
						"type": "VendorFixUrl",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.technologySection.technologies.technology",
						"type": "iSight:Technology",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.attackingEase",
						"type": "iSight:AttackingEase",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.availabilityImpact);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.availabilityImpact.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:AvailabilityImpact",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.exploitability);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.exploitability.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:Exploitability",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.mitigations.mitigation",
						"type": "iSight:Mitigation",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.remediationLevel);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.remediationLevel.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:RemediationLevel",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.confidentialityImpact);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.confidentialityImpact.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:ConfidentialityImpact",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.accessVector);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.accessVector.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:AccessVector",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.integrityImpact);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.integrityImpact.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:IntegrityImpact",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.accessComplexity);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.accessComplexity.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:AccessComplexity",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.reportConfidence);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.reportConfidence.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:ReportConfidence",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.authentication);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.authentication.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:Authentication",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.collateralDamagePotential);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.collateralDamagePotential.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:CollateralDamagePotential",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.targetDistribution);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return translateEntityType(_iterator.targetDistribution.toString());)",
						"iterateOver": "json.report",
						"type": "iSight:CVSS_Vector:TargetDistribution",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.tagSection.networks.network.domain",
						"type": "Domain_Name",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.tagSection.networks.network.ip",
						"type": "ipv4-addr:public",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.tagSection.files.file.fileName",
						"type": "File_Name",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.tagSection.files.file.md5",
						"type": "MD5",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.tagSection.files.file.sha1",
						"type": "SHA1",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "json.report.tagSection.files.file.sha256",
						"type": "SHA256",
						"useDocGeo": false
					}
				]
			},
			{
				"display": "",
				"searchIndex": {
					"indexOnIngest": true,
					"metadataFieldList": "+"
				}
			}
		],
		"tags": [
			"iSight",
			"Report",
			"Template",
			"June",
			"2015"
		],
		"title": "iSight Report Template July 2015"
	},

	"Cyveillance Global Intelligence Feed": {
		"description": "Cyveillance Global Intelligence Feed",
		"extractType": "File",
		"key": "s3...demo.cyber.ikanow.com.cyber_data.Cyveillance.GlogalIntelFee.",
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "The location of the bucket/directory on the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlPreserveCase": false,
					"XmlRootLevelValues": ["incidents"],
					"password": "password",
					"type": "json",
					"url": "s3://demo.cyber.ikanow.com/cyber_data/Cyveillance/GlogalIntelFeed/",
					"username": "username"
				}
			},
			{"globals": {
				"scriptlang": "javascript",
				"scripts": ["function translateEntityType(ent_name) {\n    if (null !== ent_name)\n    {\nreturn(ent_name.toLocaleString());\n}\n}"]
			}},
			{"docMetadata": {
				"appendTagsToDocs": true,
				"description": "${metadata.json.analysis}",
				"displayUrl": "${medadata.json.url}",
				"fullText": "${metadata.json.analysis}",
				"publishedDate": "$SCRIPT( return new Date(Number(_doc.metadata.json[0].date)).toString(); )",
				"title": "${metadata.json.headline}"
			}},
			{
				"display": "Enrich the document metadata with entities (people, places) and associations generated using NLP",
				"featureEngine": {
					"engineName": "OpenCalais",
					"exitOnError": false
				}
			},
			{"entities": [{
				"dimension": "What",
				"disambiguated_name": "$metadata.json.url",
				"type": "Source_URL",
				"useDocGeo": false
			}]},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {"searchCycle_secs": 600}
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML object itself if a JSON object (the full text, entities etc still get indexed)",
				"searchIndex": {"metadataFieldList": "+"}
			}
		],
		"searchCycle_secs": 600,
		"tags": [
			"Cyveillance",
			"Global",
			"Intelligence",
			"Feed"
		],
		"title": "Cyveillance Global Intelligence Feed"
	},

	"Deepsight Advanced IP Reputation Feed": {
		"description": "Deepsight Advanced IP Reputation Feed",
		"extractType": "File",
		"isPublic": true,
		"key": "s3...demo.cyber.ikanow.com.cyber_data.Deepsight.Advanced_IP_Repu.",
		"maxDocs": 20000,
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "The location of the bucket/directory on the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlIgnoreValues": [
						"address",
						"ip_version",
						"asn",
						"hostility",
						"confidence",
						"consecutive_listings",
						"listing_ratio",
						"reputation_rating",
						"first_seen",
						"last_seen",
						"attack_names_count",
						"organization_name",
						"organization_type",
						"naics",
						"isic",
						"continent",
						"country",
						"country_code",
						"region",
						"state",
						"city",
						"postal_code",
						"area_code",
						"time_zone",
						"latitude",
						"longitude",
						"carrier",
						"connection_type",
						"line_speed",
						"ip_routing",
						"anonymizer_status",
						"proxy_type",
						"proxy_level",
						"proxy_last-detected",
						"top-level_domain",
						"second-level_domain",
						"attack_name",
						"attack_category",
						"attack_description",
						"uniquedomains_count",
						"domain_name",
						"registration_person",
						"registration_email",
						"registration_organization",
						"registration_city",
						"registration_state",
						"registration_postal_code",
						"registration_country",
						"registration_create_date",
						"registration_update_date",
						"registration_registrar",
						"registration_nameservers",
						"url_count",
						"url"
					],
					"XmlPreserveCase": false,
					"XmlRootLevelValues": [
						"ipv4-addr:public",
						"ip_version",
						"asn",
						"hostility",
						"confidence",
						"consecutive_listings",
						"listing_ratio",
						"reputation_rating",
						"first_seen",
						"last_seen",
						"attack_names_count",
						"organization_name",
						"organization_type",
						"naics",
						"isic",
						"continent",
						"country",
						"country_code",
						"region",
						"state",
						"city",
						"postal_code",
						"area_code",
						"time_zone",
						"latitude",
						"longitude",
						"carrier",
						"connection_type",
						"line_speed",
						"ip_routing",
						"anonymizer_status",
						"proxy_type",
						"proxy_level",
						"proxy_last-detected",
						"top-level_domain",
						"second-level_domain",
						"attack_name",
						"attack_category",
						"attack_description",
						"uniquedomains_count",
						"domain_name",
						"registration_person",
						"registration_email",
						"registration_organization",
						"registration_city",
						"registration_state",
						"registration_postal_code",
						"registration_country",
						"registration_create_date",
						"registration_update_date",
						"registration_registrar",
						"registration_nameservers",
						"url_count",
						"URL"
					],
					"password": "password",
					"type": "csv",
					"url": "s3://demo.cyber.ikanow.com/cyber_data/Deepsight/Advanced_IP_Reputation/",
					"username": "username"
				}
			},
			{
				"display": "",
				"docMetadata": {
					"appendTagsToDocs": true,
					"description": "Malicious IP: ${metadata.csv.ipv4-addr:public}  Attack Type: ${metadata.csv.attack_name}",
					"fullText": "Malicious IP: ${metadata.csv.ipv4-addr:public}\nAttack Type: ${metadata.csv.attack_name}\nAttack Category: ${metadata.csv.attack_category}\nAttack Description: ${metadata.csv.attack_description}\nDomain Name: ${metadata.csv.domain_name}\nCountry Registration: ${metadata.csv.country}",
					"title": "Malicious IP: ${metadata.csv.ipv4-addr:public} Attack Type: ${metadata.csv.attack_name}"
				}
			},
			{"entities": [
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.ipv4-addr:public",
					"type": "ipv4-addr:public",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.ip_version",
					"type": "ip_version",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.asn",
					"type": "asn",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.hostility",
					"type": "hostility",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.confidence",
					"type": "confidence",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.consecutive_listings",
					"type": "consecutive_listings",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.listing_ratio",
					"type": "listing_ratio",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.reputation_rating",
					"type": "reputation_rating",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.first_seen",
					"type": "first_seen",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.last_seen",
					"type": "last_seen",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.attack_names_count",
					"type": "attack_names_count",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.organization_name",
					"type": "organization_name",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.organization_type",
					"type": "organization_type",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.naics",
					"type": "naics",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.isic",
					"type": "isic",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.continent",
					"type": "continent",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.country",
					"type": "country",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.country_code",
					"type": "country_code",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.region",
					"type": "region",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.state",
					"type": "state",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.city",
					"type": "city",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.postal_code",
					"type": "postal_code",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.area_code",
					"type": "area_code",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.time_zone",
					"type": "time_zone",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.latitude",
					"type": "latitude",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.longitude",
					"type": "longitude",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.carrier",
					"type": "carrier",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.connection_type",
					"type": "connection_type",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.line_speed",
					"type": "line_speed",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.ip_routing",
					"type": "ip_routing",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.anonymizer_status",
					"type": "anonymizer_status",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.proxy_type",
					"type": "proxy_type",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.proxy_level",
					"type": "proxy_level",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.proxy_last-detected",
					"type": "proxy_last-detected",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.top-level_domain",
					"type": "top-level_domain",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.second-level_domain",
					"type": "second-level_domain",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.attack_name",
					"type": "attack_name",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.attack_category",
					"type": "attack_category",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.attack_description",
					"type": "attack_description",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.uniquedomains_count",
					"type": "uniquedomains_count",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.domain_name",
					"type": "domain_name",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_person",
					"type": "registration_person",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_email",
					"type": "registration_email",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_organization",
					"type": "registration_organization",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_city",
					"type": "registration_city",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_state",
					"type": "registration_state",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_postal_code",
					"type": "registration_postal_code",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_country",
					"type": "registration_country",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_create_date",
					"type": "registration_create_date",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_update_date",
					"type": "registration_update_date",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_registrar",
					"type": "registration_registrar",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.registration_nameservers",
					"type": "registration_nameservers",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.url_count",
					"type": "url_count",
					"useDocGeo": false
				},
				{
					"dimension": "What",
					"disambiguated_name": "$metadata.csv.URL",
					"type": "URL",
					"useDocGeo": false
				}
			]},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {"searchCycle_secs": 600}
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML object itself if a JSON object (the full text, entities etc still get indexed)",
				"searchIndex": {"metadataFieldList": "+"}
			}
		],
		"tags": [
			"Deepsight",
			"Advanced",
			"IP",
			"Reputation",
			"Feed"
		],
		"title": "Deepsight Advanced IP Reputation Feed"
	},

	"Phishtank": {
		"description": "Phishtank - Blacklist",
		"extractType": "File",
		"isPublic": true,
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "The location of the bucket/directory on the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlIgnoreValues": [
						"phish_id",
						"url",
						"phish_detail_url",
						"submission_time",
						"verified",
						"verification_time",
						"online",
						"target"
					],
					"XmlPreserveCase": false,
					"XmlRootLevelValues": [
						"phish_id",
						"URL",
						"phish_detail_url",
						"submission_time",
						"verified",
						"verification_time",
						"online",
						"target"
					],
					"password": "..............",
					"type": "csv",
					"url": "s3://..........",
					"username": "..........."
				}
			},
			{
				"display": "",
				"docMetadata": {
					"appendTagsToDocs": true,
					"description": "Phishing URL: ${metadata.csv.URL}\n\nTime: ${metadata.csv.submission_time}",
					"fullText": "Phishing URL: ${metadata.csv.URL}\n\nTime: ${metadata.csv.submission_time}",
					"title": "Phishing URL: ${metadata.csv.URL}\n\nTime: ${metadata.csv.submission_time}"
				}
			},
			{
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.phish_id",
						"type": "Phish_ID",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.URL",
						"type": "URL",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.phish_detail_utl",
						"type": "phish_detail_url",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.submission_time",
						"type": "submission_time",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.verified",
						"type": "verified",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.verification_time",
						"type": "verification_time",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.online",
						"type": "online",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$metadata.csv.target",
						"type": "target",
						"useDocGeo": false
					}
				]
			},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {
					"searchCycle_secs": 600
				}
			},
			{
				"display": "Improve ingest performance by not full-text-indexing the JSON/XML object itself if a JSON object (the full text, entities etc still get indexed)",
				"searchIndex": {
					"metadataFieldList": "+"
				}
			}
		],
		"searchCycle_secs": 600,
		"tags": [
			"Phishtank",
			"blacklist_lookup"
		],
		"title": "Phishtank - Blacklist"
	},

	"FS-ISAC": {
		"description": "FS-ISAC Live Feed",
		"extractType": "File",
		"isPublic": true,
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "Stored on Amazon S3 now",
				"file": {
					"XmlAttributePrefix": "attr_",
					"XmlPreserveCase": false,
					"XmlRootLevelValues": [
						"stix:STIX_Package"
					],
					"password": "................",
					"type": "xml",
					"url": "s3://....",
					"username": ".........."
				}
			},
			{
				"display": "",
				"globals": {
					"scriptlang": "javascript",
					"scripts": [
						"function translateEntityType(ent_type) {\n    if (null !== ent_type)\n    {\n        switch (ent_type.toLowerCase()) {\n            case \"addressobj:addressobjecttype\":\n                return \"ipv4-addr\";\n                break;\n            case \"uriobj:uriobjecttype\":\n                return \"URL\";\n                break;\n            case \"domainnameobj:domainnameobjecttype, LTD\":\n                return \"Domain_Name\";\n                break;\n            case \"domainnameobj:domainnameobjecttype\":\n                return \"Domain_Name\";\n                break;\n            default:\n                return ent_type;\n        }\n    }\n    return null;\n}\n\nfunction getTitle(doc) {  \n    try { \n        return doc.metadata.title[0];\n    } catch (e) \n    { \n        try {\n            if (doc.metadata.type[0].hasOwnProperty(\"content\")) {\n                return doc.metadata.type[0].content;\n            }\n            else {\n                return doc.metadata.type[0];\n            }\n        } catch (e2) {\n            return \"Indicator\";\n        }\n    }\n}\n\n\nfunction getPublishedDate(doc) {  \n    try { \n        return doc.metadata.attr_timestamp[0].replace(/[.][0-9+:]+$/, \"\");\n    } \n    catch (e) \n    { \n        return doc.publishedDate;\n    }\n}"
					]
				}
			},
			{
				"display": "",
				"docMetadata": {
					"appendTagsToDocs": true,
					"publishedDate": "$SCRIPT( return getPublishedDate(_doc); )",
					"title": "$SCRIPT( return getTitle(_doc); )"
				}
			},
			{
				"display": "Entities",
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "handling.marking.marking_structure.statement",
						"type": "Marking",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "type",
						"type": "IndicatorType",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "producer.identity.name",
						"type": "Producer",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.value.content",
						"type": "Observable",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return(null != _iterator.attr_type && null != _iterator.value);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _iterator.value;)",
						"iterateOver": "observables.observable.object.properties",
						"type": "$SCRIPT( return translateEntityType(_iterator.attr_type.toString());)",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.address_value.content",
						"type": "Address",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return (null != _iterator.attr_type && null != _iterator.address_value);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _iterator.address_value;)",
						"iterateOver": "observable.object.properties",
						"type": "$SCRIPT( return translateEntityType(_iterator.attr_type.toString());)",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT( return (null != _iterator.address_value && null != _iterator.attr_type);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _iterator.address_value;)",
						"iterateOver": "observables.observable.object.properties",
						"type": "$SCRIPT( return translateEntityType(_iterator.attr_type.toString());)",
						"useDocGeo": false
					},
					{
						"creationCriteriaScript": "$SCRIPT(return (null != _iterator.value && null != _iterator.attr_type);)",
						"dimension": "What",
						"disambiguated_name": "$SCRIPT( return _iterator.value;)",
						"iterateOver": "observables.observable.object",
						"type": "$SCRIPT( return translateEntityType(_iterator.attr_type.toString());)",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observables.observable.description",
						"type": "Description",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.description",
						"type": "Address_Attributes",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.file_format",
						"type": "FileFormat",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.file_name",
						"type": "FileName",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.hashes.hash.simple_hash_value",
						"type": "HashValue",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_source.contributors.contributor.organization",
						"type": "ObservableContributor",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_composition.observable.object.properties.value",
						"type": "Observable",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_composition.observable.object.properties.address_value.content",
						"type": "Address",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_composition.observable.object.properties.address_value",
						"type": "Address",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_composition.observable.object.properties.file_format",
						"type": "FileFormat",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_composition.observable.object.properties.file_name",
						"type": "FileName",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_composition.observable.object.properties.hashes.hash.simple_hash_value",
						"type": "HashValue",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.observable_composition.observable.observable_source.contributors.contributor.organization",
						"type": "ObservableContributor",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "handling.marking.marking_structure.attr_color",
						"type": "TLP",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "kill_chain_phases.kill_chain_phase.attr_name",
						"type": "KillChainPhase",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.header.from.address_value.content",
						"type": "EmailFromAddress",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.header.from.address_value",
						"type": "EmailFromAddress",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.header.message_id.content",
						"type": "EmailMessageID",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.header.sender.address_value.content",
						"type": "EmailSenderAddress",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.header.sender.address_value",
						"type": "EmailSenderAddress",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.header.subject.content",
						"type": "EmailSubject",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.header.x_mailer.content",
						"type": "EmailXMailer",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.address_value",
						"type": "Observable",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "type.content",
						"type": "IndicatorType",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.file_name.content",
						"type": "FileName",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.hashes.hash.simple_hash_value.content",
						"type": "HashValue",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.size_in_bytes.content",
						"type": "SizeInBytes",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.hive.content",
						"type": "Hive",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.key.content",
						"type": "Key",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "observable.object.properties.values.value.data.content",
						"type": "Data",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "confidence.value.content",
						"type": "Confidence",
						"useDocGeo": false
					}
				]
			}
		],
		"tags": [
			"FS-ISAC",
			"Live",
			"Feed"
		],
		"title": "FS-ISAC Live Feed"
	},

	"Cyveillance Malware Feed": {
		"description": "Cyveillance Malware Feed",
		"extractType": "File",
		"isPublic": true,
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "The location of the bucket/directory on the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlPreserveCase": false,
					"password": "..........",
					"type": "xml",
					"url": "s3://..........",
					"username": "........."
				}
			},
			{
				"display": "",
				"docMetadata": {
					"appendTagsToDocs": true,
					"description": "Cyveillance Daily Malware Feed",
					"fullText": "Cyveillance Daily Malware Feed",
					"publishedDate": "$metadata.delivery_ts",
					"title": "Cyveillance Daily Malware Feed $metadata.delivery_ts"
				}
			},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {
					"searchCycle_secs": 3600
				}
			},
			{
				"display": "Full reports",
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "entries.entry.domain",
						"type": "Domain_Name",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "entries.entry.ip",
						"type": "ipv4-addr:public",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "entries.entry.url",
						"type": "URL",
						"useDocGeo": false
					}
				]
			}
		],
		"searchCycle_secs": 3600,
		"tags": [
			"Cyveillance",
			"Malware",
			"Feed"
		],
		"title": "Cyveillance Malware Feed"
	},

	"Cyveillance Phishing Feed": {
		"description": "Cyveillance Phishing Feed",
		"extractType": "File",
		"isPublic": true,
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "The location of the bucket/directory on the Amazon S3 file share. Type can be set to 'csv', 'json', 'xml', or 'office' - by default it will attempt to auto-detect",
				"file": {
					"XmlPreserveCase": true,
					"password": "..................",
					"type": "xml",
					"url": "s3://........",
					"username": ".........."
				}
			},
			{
				"display": "",
				"docMetadata": {
					"appendTagsToDocs": true,
					"description": "Cyveillance Daily Phishing Feed",
					"fullText": "Cyveillance Daily Phishing Feed",
					"publishedDate": "$metadata.delivery_ts",
					"title": "Cyveillance Daily Phishing Feed $metadata.delivery_ts"
				}
			},
			{
				"display": "(Only check the filesystem hourly - you can set this to whatever time you want)",
				"harvest": {
					"searchCycle_secs": 3600
				}
			},
			{
				"display": "Full reports",
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "entries.entry.domain",
						"type": "Domain_Name",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "entries.entry.ip",
						"type": "ipv4-addr:public",
						"useDocGeo": false
					},
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "entries.entry.url",
						"type": "URL",
						"useDocGeo": false
					}
				]
			}
		],
		"searchCycle_secs": 3600,
		"tags": [
			"Cyveillance",
			"Phishing",
			"Feed"
		],
		"title": "Cyveillance Phishing Feed"
	},

	"Malc0de Domain Blacklist": {
		"description": "Malc0de Domains - Blacklist",
		"extractType": "Feed",
		"isPublic": true,
		"maxDocs": 1,
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "Extract each document (re-extracting every 'updateCycle_secs') with the specified title and summary text",
				"web": {
					"extraUrls": [
						{
							"description": "Optional",
							"title": "Page Title",
							"url": "http://malc0de.com/bl/BOOT"
						}
					],
					"updateCycle_secs": 86400
				}
			},
			{
				"contentMetadata": [
					{
						"fieldName": "Domain_Name",
						"flags": "i",
						"index": false,
						"script": "(?<=PRIMARY ).*(?= blockeddomain)",
						"scriptlang": "regex",
						"store": true
					}
				]
			},
			{
				"display": "",
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "Domain_Name",
						"relevance": "1.0",
						"type": "Domain_Name",
						"useDocGeo": false
					}
				]
			},
			{
				"display": "Extract the useful text from the HTML",
				"textEngine": {
					"engineName": "Tika"
				}
			}
		],
		"tags": [
			"Malc0de",
			"blacklist_lookup"
		],
		"title": "Malc0de Domain - Blacklist"
	},

	"Malc0de IP Blacklist" : {
		"description": "Malc0de IP - Blacklist",
		"extractType": "Feed",
		"isPublic": true,
		"maxDocs": 1,
		"mediaType": "Report",
		"processingPipeline": [
			{
				"display": "Extract each document (re-extracting every 'updateCycle_secs') with the specified title and summary text",
				"web": {
					"extraUrls": [
						{
							"description": "Optional",
							"title": "Page Title",
							"url": "http://malc0de.com/bl/IP_Blacklist.txt"
						}
					],
					"updateCycle_secs": 86400
				}
			},
			{
				"contentMetadata": [
					{
						"fieldName": "ipv4-addr:public",
						"flags": "i",
						"index": false,
						"script": "[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}",
						"scriptlang": "regex",
						"store": true
					}
				]
			},
			{
				"display": "",
				"entities": [
					{
						"dimension": "What",
						"disambiguated_name": "$value",
						"iterateOver": "ipv4-addr:public",
						"relevance": "1.0",
						"type": "ipv4-addr:public",
						"useDocGeo": false
					}
				]
			},
			{
				"display": "Extract the useful text from the HTML",
				"textEngine": {
					"engineName": "Tika"
				}
			}
		],
		"tags": [
			"Malc0de",
			"blacklist_lookup"
		],
		"title": "Malc0de IP - Blacklist"
	},

	"CISCO PIX ASA": {
		"description": "CISCO PIX ASA",
		"extractType": "Logstash",
		"isPublic": true,
		"mediaType": "Record",
		"processingPipeline": [
			{
				"display": "Just contains a string in which to put the logstash configuration (minus the output, which is appended by Infinit.e)",
				"logstash": {
					"config": "input {\n  s3 {\n      credentials => [\"xxxxxxxxxxxxxxx\",\"xxxxxxxxxxxxxxxxx\"]\n      bucket => \"xxxxxxxxxxxxx\"\n      prefix => 'xxxxxxxxxxxxxx/' \n      type => \"Cisco_ASA\"\n          }\n      }\nfilter \n{\n        grok {\n            match => [ message,  \"(?<YEAR>20[0-9]{2})-(?<MONTHNUM>[0-9]{2})-(?<MONTHDAY>[0-9]{2}) (?<HOUR>[0-9]{2}):(?<MINUTE>[0-9]{2}):(?<SECOND>[0-9]{2})\" \n          ]\n        }\ngrok {\n      match => [\n        message, \"%{CISCOFW106001}\",\n        message, \"%{CISCOFW106006_106007_106010}\",\n        message, \"%{CISCOFW106014}\",\n        message, \"%{CISCOFW106015}\",\n        message, \"%{CISCOFW106021}\",\n        message, \"%{CISCOFW106023}\",\n        message, \"%{CISCOFW106100}\",\n        message, \"%{CISCOFW110002}\",\n        message, \"%{CISCOFW302010}\",\n        message, \"%{CISCOFW302013_302014_302015_302016}\",\n        message, \"%{CISCOFW302020_302021}\",\n        message, \"%{CISCOFW305011}\",\n        message, \"%{CISCOFW313001_313004_313008}\",\n        message, \"%{CISCOFW313005}\",\n        message, \"%{CISCOFW402117}\",\n        message, \"%{CISCOFW402119}\",\n        message, \"%{CISCOFW419001}\",\n        message, \"%{CISCOFW419002}\",\n        message, \"%{CISCOFW500004}\",\n        message, \"%{CISCOFW602303_602304}\",\n        message, \"%{CISCOFW710001_710002_710003_710005_710006}\",\n        message, \"%{CISCOFW713172}\",\n        message, \"%{CISCOFW733100}\"\n      ]\n    }\n                mutate {\n        add_field => [ \"tz\", \"-0500\" ]\n                 add_field => [\"dtg\", \"%{YEAR}-%{MONTHNUM}-%{MONTHDAY} %{HOUR}:%{MINUTE}:%{SECOND} %{tz}\"]\n        }\n    date {\n        match => [\"dtg\", \"YYYY-MM-dd HH:mm:ss Z\"]\n        }\n    mutate{\n        remove => [ \"tz\" ]\n        remove => [ \"YEAR\" ]\n        remove => [ \"MONTHNUM\" ]\n        remove => [ \"MONTHDAY\" ]\n        remove => [ \"HOUR\" ]\n        remove => [ \"MINUTE\" ]\n        remove => [ \"SECOND\" ]\n        remove => [ \"dtg\" ]\n              }\n    \n      if [dst_ip]  {\n    geoip {\n        source => \"dst_ip\"\n        target => \"geoip\"\n        fields => [\"timezone\",\"location\",\"latitude\",\"longitude\"]\n    }\n    mutate {\n        convert => [ \"[geoip][coordinates]\", \"float\" ]\n    }\n                }\n        \n            if [src_ip]  {\n    geoip {\n        source => \"src_ip\"\n        target => \"geoip\"\n        fields => [\"timezone\",\"location\",\"latitude\",\"longitude\"]\n    }\n    mutate {\n        convert => [ \"[geoip][coordinates]\", \"float\" ]\n    }\n                }\n    \n              }\n    \n\n    \n",
					"streaming": false,
					"testDebugOutput": false
				}
			}
		],
		"tags": [
			"Sample",
			"CISCO",
			"ASA",
			"Logs"
		],
		"title": "CISCO PIX ASA"
	}
};