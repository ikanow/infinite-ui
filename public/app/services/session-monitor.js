/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['app'], function (app) {
    "use strict";
    return app.factory('SessionMonitor', function ($rootScope, $timeout, $log, $injector, $interval, $document) {

        var startingSession = false;
        var sessionMonitor = {};
        var sessionTimeoutInstance = null;
        var currentSessionTime = 0;
        var activityInterval = null;
        var lastActivityCount = 0; // seconds

        // ********************************************************************* //
        //
        // PRIVATE INSTANCE METHODS
        //
        // ********************************************************************* //


        /**
         * If there haven't been any mouse clicks in sessionMaxInactivity time, continue
         * with the session timeout. Otherwise send a keepalive to restart the session.
         */
        var checkRecentActivity = function () {
            if ((lastActivityCount * 1000) >= appConfig.sessionMaxInactivity) {
                if (activityInterval) {
                    $interval.cancel(activityInterval);
                }
                showSessionWarning()
            } else {
                var authService = $injector.get('authService');
                var state = $injector.get('$state');
                authService.keepAlive().then(
                  function () {
                  },
                  function (err) {
                      state.transitionTo("logout");
                  });
            }
        };

        /**
         * Broadcast an event down from the rootScope so that all listeners can handle
         * the event.
         */
        var showSessionWarning = function () {
            //$rootScope.$broadcast(EVENTS.SESSION_WARNING);
            var state = $injector.get('$state');
            $log.debug("showSessionWarning called.");
            var authService = $injector.get('authService');
            var sessionMonitor = $injector.get('SessionMonitor');
            var modal = $injector.get('$uibModal');

            var countDownInterval = null;
            var modalInstance = modal.open({
                size: '',
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'app/services/session-monitor/sessionWarnModal.html',
                resolve: {
                    timeLeft: function() { return appConfig.sessionWarningBuffer; },
                },
                controller: function ($scope, $log, $uibModalInstance, timeLeft) {
                    /**
                     * Countdown by 1 second each call, and output the properly formatted text.
                     */
                    var countDown = function () {
                        timeLeft -= 1000;   // remove 1 second from the timer.
                        $log.debug('timeLeft: ', timeLeft);
                        var minutes = Math.floor(timeLeft / 60000);
                        var seconds = Math.floor((timeLeft % 60000) / 1000);
                        $scope.timeLeft = "in ";
                        if (minutes > 0) {
                            $scope.timeLeft += minutes + " minute";
                            if(minutes > 1) {
                                $scope.timeLeft += "s";
                            }
                        }
                        if (seconds > 0) $scope.timeLeft += " " + seconds + " seconds";
                        if (timeLeft <= 0) {
                            // logout will happen anyway, but we still want to close the modal.
                            $rootScope.autologout = true;
                            // Save the username so we know whether or not the same user is logging back in later.
                            $rootScope.lastUsername = $rootScope.currUsername;
                            $scope.logoutClicked();
                        }
                    };

                    countDownInterval = $interval(countDown, 1000);

                    $scope.logoutClicked = function () {
                        $uibModalInstance.close('logout');
                    };

                    $scope.stayLoggedIn = function () {
                        $uibModalInstance.dismiss('stay logged-in');
                    };
                }
            });

            // Declare what will happen when the modal is closed or dismissed
            modalInstance.result.then(
              // On close
              function () {
                  $interval.cancel(countDownInterval);
                  if($rootScope.autologout === true) {
                      state.transitionTo("expired-session");
                  }
                  else {
                      state.transitionTo("logout");
                  }
              },
              // On dismiss
              function () {
                  $interval.cancel(countDownInterval);
                  authService.keepAlive().then(
                    function () {
                        // Do functions on keepAlive() success, ie. SettingService.getSettings();
                    }, function (err) {
                        state.transitionTo("logout");
                    });
              });
        };

        /**
         * Calculate the buffered session time. The buffered session time is the time
         * at which the user will receive a warning about a pending expired session. The
         * buffer is configurable in the code via app config.
         *
         * @returns {number}
         */
        var getBufferedSessionTime = function () {
            return currentSessionTime - appConfig.sessionWarningBuffer;
        };

        var activityClickHandler = function () {
          lastActivityCount = 0;
        };

        // ********************************************************************* //
        //
        // PUBLIC INSTANCE METHODS
        //
        // ********************************************************************* //

        /**
         * Create a new instance of the session timeout. Broadcasts and event down
         * from the rootScope when the timeout is expired.
         *
         * @param sessionTime
         * @returns {*}
         */
        sessionMonitor.start = function (sessionTime) {

            if (startingSession) {
                //$log.warn('Already starting a session');
                return;
            }

            //$log.debug('Starting Session');

            var state = $injector.get('$state');
            if (state.current.name == "login") {
                //$log.warn('At login page, Session not started.');
                return;
            }

            var authService = $injector.get('authService');
            if(!authService.isAuthenticated()) {
                //$log.warn('Not logged in Session not started');
                return;
            }

            //Only start one at a time
            startingSession = true;

            // stop all existing session timers
            sessionMonitor.stopAll();

            // start new session timers
            //$log.debug('Starting session monitor session, using session time %d', sessionTime);
            currentSessionTime = sessionTime;
            sessionTimeoutInstance = $timeout(checkRecentActivity, getBufferedSessionTime());

            var countdownFunc = function() {
              lastActivityCount += 1;
            };

            if (activityInterval) {
              $interval.cancel(activityInterval);
            }
            lastActivityCount = 0;
            // Start timer since last mouse click.
            activityInterval = $interval(countdownFunc, 1000);
            // reset activityInterval on every click
            $document.bind('click', activityClickHandler);

            $rootScope.autologout = false;   // flag that the user hasn't been automatically logged out.
            $rootScope.lastUsername = undefined;

            startingSession = false;
            return sessionTimeoutInstance;
        };

        /**
         * Cancels all session timers, if active.
         * Currently, only the session timeout warning
         * timer is necessary.
         */
        sessionMonitor.stopAll = function () {
            sessionMonitor.stop(sessionTimeoutInstance);
            //$log.debug("Stop All");
        };

        /**
         * Cancels a running instance of the session timeout. If the supplied instance does not exist
         * the function will skip the cancel operation silently.
         *
         * @param instance
         */
        sessionMonitor.stop = function (instance) {
            $document.unbind('click', activityClickHandler);
            if (activityInterval) {
                $interval.cancel(activityInterval);
            }
            if (angular.isDefined(instance)) {
                $timeout.cancel(instance);
            }
        };



        /**
         * Reset the current session timer using the persisted session time
         * from the initial start command.
         *
         * @returns {*}
         */
        sessionMonitor.reset = function () {
            if (sessionTimeoutInstance !== null) {
                //$log.debug('Resetting session monitor. Using %d minute session', currentSessionTime / 60000);
                return sessionMonitor.start(currentSessionTime);
            } else {
                //$log.warn('Recieved session monitor reset event, but we have no active session to ' +
                //          'monitor. Session monitor started instead.');
                return sessionMonitor.start(appConfig.sessionTimeout);
            }
        };

        return sessionMonitor;
    });
});