/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['app', 'lodash'], function(app, _){
  "use strict";

  return app.factory('sourcesService', [
    "$rootScope", "$q", "$log", "SessionService", "dataGroupService", "InfiniteApi", "ShareApiService", "SourceApiService",
    function ($rootScope, $q, $log, SessionService, dataGroupService, InfiniteApi, ShareApiService, SourceApiService) {

      var sourcesService = {};

      //TODO Remove this
      sourcesService.activeSource = null;

      // ********************************************************************* //
      //
      // Main Service Methods
      //
      // ********************************************************************* //
      // TODO remove this? Add chained mappers on fetch results? Something...
      function _joinSourcesAndCommunityNames(sources) {
        dataGroupService.getAll().then(function (datagroups) {
          _.each(sources, function (source) {
            source.communities = [];
            _.each(source.communityIds, function (communityId) {
              var communityName = _.map(_.filter(datagroups, {_id: communityId}), 'name')[0];
              source.communities.push({name: communityName});
            })
          });
        });
        return sources;
      }

      // TODO use the chain mappers mentioned above?
      function _addOriginAndTypesToSources(sources) {
        var types = ['asset-database', 'firewall', 'ldap', 'network-scan', 'proxy', 'threat-intel', 'auxiliary', 'test'];

        _.each(sources, function (source) {
          var shareObj;
          //if it is a source share, parse the share to get tags and harvest info if exists
          if(source.share){
            shareObj = JSON.parse(source.share);
            source.tags = shareObj.tags;
          }
          // Add Origin
          if (_.indexOf(source.tags, 'internal') >= 0) {
            source.origin = 'Internal';
          }
          else if (_.indexOf(source.tags, 'external') >= 0) {
            source.origin = 'External';
          }
          else {
            source.origin = ''
          }

          // Add type
          _.each(types, function (type) {
            if (_.indexOf(source.tags, type) >= 0) {
              source.dataType = type;
            }
          });
        });
        return sources;
      }

      /**
       * Get all sources available to the current user
       * @param stripped
       * @returns {*}
       */
      sourcesService.getUserSources = function(stripped) {
        stripped = stripped !== false;
        var communityFilter = !$rootScope.isAdmin, userFilter = false;
        return SourceApiService.getUserSources(communityFilter, userFilter, stripped)
          .then(InfiniteApi.resolveWithData)
          .then(_joinSourcesAndCommunityNames)
          .then(_addOriginAndTypesToSources);
      };

      /**
       * Get all sources from shares API
       * @returns {Array<ShareDef>}
       */
      sourcesService.getSourcesShare = function() {
        return ShareApiService.searchByType('source').then(InfiniteApi.resolveWithData)
          .then(_addOriginAndTypesToSources);
      };

      /**
       * Get all sources from shares and the harvester.
       * @returns {*}
       */
      sourcesService.getAllSources = function() {
        return $q.all({
          harvestedSources: sourcesService.getHarvestedSources(),
          shareSources: sourcesService.getSourcesShare()
        }).then(function (results) {
          return _.union(results.harvestedSources, results.shareSources);
        });
      };

      /**
       * Get harvested sources in all communities or a limited set
       * @returns {Promise<Array<SourceDef>>} Resolves with a list of sources.
       */
      sourcesService.getHarvestedSources = function(communities) {
        if(_.isEmpty(communities)) communities = "*";
        return SourceApiService.getGood(communities)
          .then(InfiniteApi.resolveWithData)
          .then(_joinSourcesAndCommunityNames);
      };

      /**
       * Get a source from the harvester by ID
       * @param id
       * @returns {*}
       */
      sourcesService.getHarvestedSource = function(id) {
        return SourceApiService.get(id).then(InfiniteApi.resolveWithData);
      };

      /**
       * Sources can be retrieved by community id, but they must be retrieved
       * from three different endpoints: config/source/good, config/source/bad, and config/source/pending.
       * This will return an object with each community as as a key, and the value being an array of sources
       * for that community.
       */
      sourcesService.getSourcesByCommunity = function(communities) {

        function addStatus(i, status) {return _.map(i,function (source) {source.status = status; return source;});}
        function addGood(data){ return addStatus(data, "good"); }
        function addBad(data){ return addStatus(data, "bad"); }
        function addPending(data){ return addStatus(data, "pending"); }

        return $q.all({
          good: SourceApiService.getGood(communities).then(InfiniteApi.resolveWithData).then(addGood),
          bad: SourceApiService.getBad(communities).then(InfiniteApi.resolveWithData).then(addBad),
          pending: SourceApiService.getPending(communities).then(InfiniteApi.resolveWithData).then(addPending)
        }).then(function(results){

          var sourcesByCommunity = {};
          var combinedSources = _.union(results.good, results.bad, results.pending);

          console.log("Combined Sources: ", _.cloneDeep(combinedSources));

          // Loop through each source
          _.each(combinedSources, function (source) {
            // Loop through each community id in the source
            _.each(source.communityIds, function (community) {
              if (!_.has(sourcesByCommunity, community)) {
                sourcesByCommunity[community] = [source];
              } else {
                if (_.indexOf(sourcesByCommunity[community], source) == -1) {
                  sourcesByCommunity[community].push(source);
                }
              }
            });
          });

          return sourcesByCommunity;
        });
      };

      /**
       * Get sources loaded via static JSON definition
       */
      sourcesService.getSourcesLocal = function () {
        return __ikanowSourceTemplates;
      };

      /**
       * Get all source templates from legacy shares
       * @returns {*}
       */
      sourcesService.getLegacySourceTemplates = function () {
        return ShareApiService.searchByType('source_template', true).then(InfiniteApi.resolveWithData);
      };

      /**
       * Get source templates from json shares
       * @returns {*}
       */
      sourcesService.getSourceTemplatesShare = function () {
        return ShareApiService.searchByType('__ikanowSourceTemplates').then(InfiniteApi.resolveWithData);
      };

      /**
       * Get a source template by ID via the SharesApiService
       * @param id
       * @returns {Promise<SourceDef>}
       */
      sourcesService.getSourceTemplatesObj = function (id) {
        return ShareApiService.get(id, false, true).then(InfiniteApi.resolveWithData);
      };

      /**
       * Test a source
       * @param {Integer} returnItemCount Number of test records to return
       * @param {Boolean} returnFullText Set to true to return full text result
       * @param {Boolean} testUpdates
       * @param {SourceDef/} sourceDef Source definition
       * @return {Promise<ApiResponse>}
       */
      sourcesService.testSource = function (returnItemCount, returnFullText, testUpdates, sourceDef) {
        return SourceApiService.test(returnItemCount, returnFullText, testUpdates, sourceDef);
      };

      sourcesService.saveSource = function (source) {
        return ShareApiService.createFromObject(source).then(InfiniteApi.resolveWithData);
      };

      sourcesService.updateSource = function (source) {
        return ShareApiService.updateFromObject(source).then(InfiniteApi.resolveWithData);
      };

      sourcesService.uploadFile = function (source, fd) {
        var user = SessionService.getUserProfile();
        return ShareApiService.uploadFile(
          user.communities[0]._id,    //list of datagroups, we're just storing the upload in the users personal community(datagroup)
          'binary',                   //share type
          source.title,               //share title
          source.description,         //share description
          fd,                         //file data
          'undefined'                 //force content type, 'undefined' will let the browser automatically decide how to set content-type
        ).then(InfiniteApi.resolveWithData);
      };

      /**
       * Publish a source to begin harvesting
       * @param {SourceDef} source
       * @returns {Promise<Response>} Published source definition
       */
      sourcesService.publishSource = function (source) {
        console.log("[sourcesService.publishSource] Publishing: ", source);
        return SourceApiService.save(source.communityIds[0], source).then(InfiniteApi.resolveWithData);
      };

      /**
       * Suspend a published source in the harvester.
       * @param {SourceDef} source
       * @returns {Promise<SourceDef>} Suspended source definition
       */
      sourcesService.suspendSource = function (source) {
        var suspended = source.searchCycle_secs < 0;
        return SourceApiService.suspend(source._id, source.communityIds[0], !suspended)
          .then(InfiniteApi.resolveWithResponse);
      };

      /**
       *
       * @param {SourceDef} source
       * @returns {Promise<Response>} Response portion of apiResponse
       */
      sourcesService.deleteSource = function (source) {
        return SourceApiService.remove(source._id, source.communityIds[0]).then(InfiniteApi.resolveWithResponse);
      };

      sourcesService.deleteSourceShare = function (shareId) {
        return ShareApiService.remove(shareId);
      };

      return sourcesService
    }]);
});