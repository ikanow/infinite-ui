/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:SessionService
 *
 * @description
 * Handles all front-end in-memory and persisted session based data.
 *
 * Auth token is persisted, but never read, only checks for existence.
 * IF a token is present during initialization, an attempt to call the API to get the
 * current user will determine if the session is still active.
 *
 * UserProfile is cached in memory and not persisted, this ensures the user information
 * is up to date since we'll need to ft
 *
 */
define(['app', 'lodash'], function(app, _){
  "use strict";

  return app.factory('SessionService', ['$angularCacheFactory', '$log',

    //Factory function with injected dependencies
    function($angularCacheFactory, $log) {

      var userProfile = {},
        dataGroups = [],
        userGroups = [],
        userList = [],
        //get cache keys from config.
        authTokenKey = appConfig.cacheKeys.authToken;

      //All public methods
      return {
        //reset everything
        clearSession: clearSession,

        //Getters for stored data
        getAuthToken: getAuthToken,
        getUserAccountType: getUserAccountType,
        getUserDisplayName: getUserDisplayName,
        getDataGroups: getDataGroups,
        getUserGroups: getUserGroups,
        getUserId: getUserId,
        getUserList: getUserList,
        getUserProfile: getUserProfile,


        //Tests
        hasAuthToken: hasAuthToken,
        isAdmin: isAdmin,
        isNormalUser: isNormalUser,
        isPowerUser: isPowerUser,


        //Setters
        setAuthToken: setAuthToken,
        setDataGroups: setDataGroups,
        setUserGroups: setUserGroups,
        setUserList: setUserList,
        setUserProfile: setUserProfile
      };

      /**
       * Remove all local storage for session
       * isAuthenticated flag will be removed
       * auth token will be removed
       */
      function clearSession() {
        //user profile is in-memory only and set after successful authentication or updates
        // to the current user.
        userProfile = {};
        dataGroups = [];
        userGroups = [];

        //Set authenticated to false
        //Remove cached auth token
        var appCache = $angularCacheFactory.get(appConfig.appCacheName);
        appCache.remove(authTokenKey);
      }

      /**
       * Get the current auth token from html5 storage
       * @returns {*}
       */
      function getAuthToken() {
        var appCache = $angularCacheFactory.get(appConfig.appCacheName);
        return appCache.get(authTokenKey);
      }

      /**
       * Get the cached data groups
       * @returns {*}
       */
      function getDataGroups(){
        return _.cloneDeep(dataGroups);
      }

      /**
       * Get the current user's account type
       * @returns {*|string|string}
       */
      function getUserAccountType(){
        var acctType = "" + (userProfile.accountType || "user");
        return acctType.toLowerCase();
      }

      /**
       * Get the current user's display name or attempt to make one from first and last names
       * @returns {String}
       */
      function getUserDisplayName(){
        return userProfile.displayName || (userProfile.firstName + " " + userProfile.lastName);
      }

      /**
       * Get the cached user groups
       * @returns {*}
       */
      function getUserGroups(){
        return _.cloneDeep(userGroups);
      }

      /**
       * Get the user ID only from the user profile
       * @returns {*|null}
       */
      function getUserId(){
        //console.log("[sessionService.getUserId] ", userProfile);
        return userProfile._id || null;
      }

      /**
       * Get the cached user groups
       * @returns {Array<ProfileDef>}
       */
      function getUserList(){
        return _.cloneDeep(userList);
      }

      /**
       * Get the cached user profile
       * @returns {{}}
       */
      function getUserProfile() {
        return _.cloneDeep(userProfile);
      }

      /**
       * True if a token exists in storage
       */
      function hasAuthToken(){
        var appCache = $angularCacheFactory.get(appConfig.appCacheName);
        return appCache.info(authTokenKey);
      }

      /**
       * Simple test for current user's admin rights
       * @returns {boolean}
       */
      function isAdmin(){
        return (getUserAccountType() == "admin");
      }

      /**
       * Simple test for current user's admin rights
       * @returns {boolean}
       */
      function isPowerUser(){
        return (getUserAccountType() == "admin-enabled");
      }

      /**
       * Simple test to see if the current user's ais a normal user with no admin privileges.
       * @returns {boolean}
       */
      function isNormalUser(){
        return (getUserAccountType() == "user");
      }

      /**
       * Persist the auth token for re-init on page load later.
       * @param authToken
       */
      function setAuthToken(authToken) {
        var appCache = $angularCacheFactory.get(appConfig.appCacheName);
        appCache.put(authTokenKey, authToken);
      }

      /**
       * Save data groups to cache
       * @param newDataGroups
       */
      function setDataGroups(newDataGroups){
        //console.log("setUserDataGroups", dataGroups);
        if(_.isUndefined(newDataGroups) || _.isNull(newDataGroups)){
          $log.error("SessionService->setDataGroups: Cannot set data groups with invalid value. Aborting.", newDataGroups);
          return;
        }
        dataGroups = newDataGroups;
      }

      /**
       * Save user groups to cache
       * @param newUserGroups
       */
      function setUserGroups(newUserGroups){
        //console.log("setUserGroups", newUserGroups);
        if(_.isUndefined(newUserGroups) || _.isNull(newUserGroups)){
          $log.error("SessionService->setUserGroups: Cannot set user groups with invalid value. Aborting.", newUserGroups);
          return;
        }
        userGroups = newUserGroups;
      }

      /**
       * Save user list to cache
       * @param newUserList
       */
      function setUserList(newUserList){
        //console.log("setUserList", newUserList);
        userList = newUserList;
      }

      /**
       * Save the user profile to cache
       * @param profile
       */
      function setUserProfile(profile) {
        //console.log("setUserProfile", profile);
        userProfile = _.cloneDeep(profile);
      }

    }
  ]);
});