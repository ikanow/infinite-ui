/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:aclService
 * @description
 * Acl service handles permissions tests
 */
define(['angular', 'app', 'lodash'], function(angular, app, _){
  "use strict";

  return app.factory('aclService', [
    '$q', '$log', "SessionService",

    function($q, $log, SessionService) {

      var aclService = {};

      /**
       * @ngdoc method
       * @name hasPermissionToGroup
       * @methodOf app.service:aclService
       * @description
       * Check if the user has permission to a given group object
       * @param {Object} group Group object to check for membership
       * @returns {Boolean} True if the user is a member of the given group
       */
      aclService.hasPermissionToGroup = function( group ){
        return aclService.hasPermissionToGroupId(group._id);
      };

      /**
       * @ngdoc method
       * @name hasPermissionToGroupId
       * @methodOf app.service:aclService
       * @description
       * Check if the user has permission to a given group by ID
       * @param {String} groupId ID of group to check for permission
       * @returns {Boolean} True if the user is the owner if the given object
       */
      aclService.hasPermissionToGroupId = function( groupId ){
        return !_.isUndefined( _.find(SessionService.getDataGroups(), {_id:groupId}))
            || !_.isUndefined( _.find(SessionService.getUserGroups(), {_id:groupId}));
      };

      /**
       * @ngdoc method
       * @name isMemberOfGroup
       * @methodOf app.service:aclService
       * @description
       * Test if the current user is a member of a given user group OR data group
       * @param {Object} groupDef Group definition to check for ownership
       * @returns {Boolean} True if the user is the owner if the given object
       */
      aclService.isMemberOfGroup = function(groupDef){
        var userId = SessionService.getUserId();

        return _.some(groupDef.members, function(member) {
          // If the member is a user_group, get user group members and see if user is a member
          if(member.type && member.type === "user_group") {
            // Get the user_groups's members from the user group in the SessionService
            var userGroup = _.find(SessionService.getUserGroups(), {_id:member._id});

            if (angular.isDefined(userGroup) && angular.isDefined(userGroup.members)) {
              return angular.isDefined(_.find(userGroup.members, {_id: userId}));
            }
          }
          // If member is a user, check the user _id
          return member._id == userId;
        });
      };

      /**
       * @ngdoc method
       * @name isOwnerOf
       * @methodOf app.service:aclService
       * @description
       * Test an object for ownership by comparing an ownerId field to the current user id.
       * @param {Object} objectDef Object to check for ownership
       * @returns {Boolean} True if the user is the owner if the given object
       */
      aclService.isOwnerOf = function(objectDef){
        return objectDef && objectDef.ownerId && objectDef.ownerId == SessionService.getUserId();
      };

      /**
       * @ngdoc method
       * @name filterProjectsByDataGroups
       * @methodOf app.service:aclService
       * @description
       * Given a list of projects, reject the ones the current user does not have access to by checking for permissions
       * to the project data group.
       * @param {Array<Workspace>} projects An array of projects
       * @returns {Array<Workspace>} An array of projects the user has access to
       */
      aclService.filterProjectsByDataGroups = function(projects){
        //Admins skip filtering
        if( SessionService.isAdmin() ) {
          return projects;
        }

        //Non-admins need filtering
        // Filters the projects list and remove projects which do not
        // have a project data group in the list of user's data groups.
        return _.filter(projects, function(project){
          //for each project decode project details
          var projectDetails = angular.fromJson(project.share);
          return aclService.hasPermissionToGroupId(projectDetails.workspaceGroupId);
        });
      };

      aclService.filterDataGroupsByMembership = function(dataGroups) {
        return _.filter(dataGroups, function(dg) {
          return aclService.isMemberOfGroup(dg);
        });
      };

      //export module
      return aclService;
    }
  ]);
});