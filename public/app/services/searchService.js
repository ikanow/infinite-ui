/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:SearchService
 * @description
 * SearchService manages querying the API, storing user history, and caching current result sets.
 *
 */
define(['angular', 'app', 'sprintf', "lodash"], function(angular, app, sprintf, _){
  "use strict";

  return app.factory('SearchService', [
    '$q', '$log', "historyService", "DocumentApiService", "SearchQueryService", "SessionService", "workspaceService",

    function($q, $log, historyService, DocumentApiService, SearchQueryService, SessionService, workspaceService) {
      'use strict';

      /**
       * @typedef {object} Stats
       * @property {number} found
       * @property {number} avgScore
       * @property {number} maxScore
       * @property {number} start
       */

      /**
       * @typedef {object} ResponseDescription
       * @property {string} action
       * @property {string} message
       * @property {boolean} success
       * @property {number} time
       */

      /**
       * This is a huge object, see API Docs for now.
       * @typedef {object} InfiniteDocument
       */

      /**
       * Query Definition Structure
       * @typedef {Object} QueryDef
       * @property {?String}     id             QueryID UUID for query definition
       * @property {?Date}       datetime       Date and time last run
       * @property {String}      searchString
       * @property {EntityTypes} entities
       * @property {String[]}    tags
       * @property {String[]}    origin         'internal' and/or 'extrnal' tags
       * @property {String[]}    verbs
       * @property {WeightConfig} weightBy
       */

      /**
       * @typedef {?Object} QueryCache
       * @property {?InfiniteDocument[]} documents
       * @property {?Stats} stats
       */

      var searchService = {};

      var HISTORY_TYPE = "infinite";
      var currentQuery = null;              //most recent query definition set.

      /**
       * Query Response Cache for all types.
       * @type {?QueryCache}
       */
      var queryCache = null;

      // ********************************************************************* //
      // Search Config and persistence
      // ********************************************************************* //

      /**
       * Add a new query definition to the history.
       * @param {QueryDef} queryDef
       * @returns {Promise<QueryDef>}
       */
      function addQueryToHistory( queryDef ){
        $log.debug("[SearchService->addQueryToHistory] ", queryDef );

        //Share data is the userSearch config, cloned to prevent any accidental changes
        // and a customizer is used to use getConfig on InfiniteQuery objects.
        // During config loading, these objects are instantiated using setConfig.
        queryDef = _.cloneDeep( queryDef, function( value ){
          if( angular.isObject(value) && "getConfig" in value ){
            return value.getConfig();
          }
        });

        //Add it to the list
        return historyService.addQuery(HISTORY_TYPE, queryDef);
      }

      /**
       * Before each get* method, this will clear cache if the project has changed
       * and prepare a new queryCache if needed.
       */
      var lastProject = null;
      function prepareQueryCache(){
        var currentProject = workspaceService.getCurrentId();
        if( currentProject != lastProject ){
          queryCache = null;
          lastProject = currentProject;
        }
        if( queryCache == null ){
          queryCache = {};
        }
      }

      // ********************************************************************* //
      //
      // Main public API methods
      //
      // ********************************************************************* //

      /**
       * Base query function, returns promise.
       * Promise resolves to data object if successful
       * on failure, Promise rejects with an error message
       * @param {InfiniteApiQuery} infiniteQuery
       * @private
       * @returns {Promise<ApiResponse>} query promise
       */
      function _api_query( infiniteQuery ) {
        //If no project. then we reject this process
        if( workspaceService.getCurrent() == null ){
          return $q.reject("No current project");
        }

        //Run HTTP POST using current project ID and current data groups
        return DocumentApiService.query(
          workspaceService.getDataGroupIds(),
          null,
          infiniteQuery
        );
      }



      /**
       * Get the current user's search history, fetch from the server if needed.
       * @return {Promise} a promise of a request to the server
       */
      searchService.getSearchHistory = function(){
        //$log.debug("[SearchService->getSearchHistory]");

        return historyService.getSearchHistory(HISTORY_TYPE).then( function (history){
          //Iterate over history ite and create object instances
          return _.each( history, function(historicalQueryDef){
            return SearchQueryService.deserializeQuery(historicalQueryDef);
          });
        });
      };

      /**
       * Get the current query definition or a historical definition from history
       * @param {String} (queryUUID) Search query UUID
       * @returns {Promise<?QueryDef>}
       */
      searchService.getQuery = function(queryUUID){

        //If a specific ID was requested
        if( queryUUID ) {
          return searchService.getSearchHistory().then(
            //success
            function (history) {
              return _.find(history, function (h) {
                return h != null && ("id" in h) && h.id == queryUUID;
              });
            }
          );
        }

        //No UUID was given, try to use the current query definition
        if( currentQuery ) {
          return $q.when(currentQuery);
        }

        //Create a new one
        return searchService.setQuery(searchService.getDefaultQuery());
      };

      /**
       * Create a new search from user-entered query definition.
       * This will create a NEW currentSearch, add it to history, and clear cached data.
       * NOTE: No data is fetched until requested.
       *
       * When successful, the promise will resolve with the new QueryDef
       * When un-successful, the promise will reject with an error message.
       *
       *       //Set the current query using a historical UUID or
       *       //create a new item in history from a QueryDef object
       *
       * @param {QueryDef|String} query Configured InfiniteQuery from SearchQueryService.newInfiniteQuery
       * @returns {Promise<QueryDef>}
       */
      searchService.setQuery = function (query) {

        //If the query is an object, assume it's an instance of QueryDef
        if (angular.isObject(query) ) {

          //console.log("[SearchService->setQuery] New Query:", query );

          query.datetime = new Date();

          //add a new object
          return addQueryToHistory(_.cloneDeep(query)).then(function(queryDef){
            currentQuery = queryDef;
            searchService.clearCache();
            return queryDef;
          });

        } else if( historyService.isUUID(query) ){

          //try to look up a query from history
          return searchService.getSearchHistory().then(function(history){
            var queryDef =  _.find(history, { id: query });
            if( _.isEmpty(queryDef)) { return $q.reject("Historical query could not be found."); } // if no result its a bad query
            queryDef.datetime = new Date();
            currentQuery = queryDef;
            searchService.clearCache();
            return _.cloneDeep(queryDef);
          });

        }

        $log.error("[SearchService->setQuery] Invalid query");
        return $q.when(null);
      };

      /**
       * Remove an item from search history
       * @param queryUUID
       * @returns {Promise}
       */
      searchService.removeQuery = function( queryUUID ){
        return historyService.removeQuery(HISTORY_TYPE, queryUUID);
      };

      /**
       * Clear current result set's cache
       */
      searchService.clearCache = function(){
        queryCache = null;
      };


      //Direct reference to current query storage for watching, no deferred, immutable
      searchService.getCurrentQuery = function() {
        return _.cloneDeep(currentQuery);
      };

      /**
       * Create a new queryDefinition with default values.
       * Useful for tools like search bar which submit a NEW search and don't work with an existing one.
       * This default template contains all options with defaults.
       * @returns {QueryDef}
       */
      searchService.getDefaultQuery = function() {
        return {

          //String entered by user
          searchString: "*",

          // EntityTypes object
          entities: {
            person: false,
            place: false,
            organization: false,
            association: false,
            other: false
          },

          // tags is an array of strings
          tags: [],

          // origin of data - 'internal' and / or 'external'
          origin: [ 'internal', 'external' ],

          // Verbs is an array of strings
          verbs: [],

          // WeightConfig Object
          weightBy: {
            dateTime: null,   // null, or Date
            halfLife: "1d"    // 1 day
          }
        };
      };

      /**
       * Get documents using current query definition
       * @param count
       * @param timesInterval
       * @param skip
       * @returns {*}
       */
      searchService.getDocuments = function(count, timesInterval, skip){

        //Param checks
        if( count == 0 ) return $q.reject("Count may not be 0.");
        if( _.isNumber(skip) && skip < 0) return $q.reject("Skip ( if provided ) must be >= 0.");
        if( _.isString(timesInterval) && !/^(\d+[hdwmy])$/.test(timesInterval)) {
          return $q.reject("timesInterval is invalid.");
        }

        //Defaults
        count = !!count ? count : 100;
        skip = !!skip ? skip : 0;

        //Get the current query definition
        return searchService.getQuery().then(function(query) {

          //Generate a hash to represent the unique portion of this request
          var queryHash = sprintf.sprintf( "%s:documents:%s",
            SearchQueryService.hashQuery(query),
            timesInterval
          );

          //Make sure cache is in a good state before we check for existing results.
          prepareQueryCache();

          //If there's a cache for this same request
          if (_.has(queryCache, queryHash)) {

            //Get the cached response
            var cachedSet = queryCache[queryHash];

            console.log("Cached set is: " + cachedSet.data.length + " documents");

            //if( skip + count > cachedSet.length ) {
            //  //TODO make sure request doesn't create spacial cache
            //  //queryCache[queryHash].length >= skip + count
            //  console.log("SPACIAL SET");
            //}

            //It's possible to run out of data. completeSet is set
            if(cachedSet.isCompleteSet){
              console.log("Cache contains complete set.");
              if( cachedSet.data.length <= skip ){
                console.error("Requesting data past complete set");
                return $q.when(null);
              }
            }

            // If the cache is bigger than the position & data requested, use cache
            if (cachedSet.data.length >= skip + count) {
              // Resolve with cloned cache portion
              return $q.when( _.set( _.clone(cachedSet), "data", _.slice(cachedSet.data, skip, skip + count )));
            }

          }

          //Construct a infinite platform query
          var infiniteQuery = SearchQueryService.getDocumentQuery(query, count, skip, timesInterval);

          return _api_query(infiniteQuery).then(function (response) {
            console.log("Query response: ", response);
            //Cache response
            //splice results into place if there's already SOME cache for this request
            if (_.has(queryCache, queryHash)) {
              queryCache[queryHash].data = queryCache[queryHash].data.concat(response.data);
              console.log("Adding extra docs to cache", queryCache[queryHash]);
            } else {
              queryCache[queryHash] = response;
            }

            if( response.data.length < count ){
              console.log("Set is now complete");
              queryCache[queryHash].isCompleteSet = true;
            }

            //return the documents
            return response;
          });

        });
      };

      searchService.getDocumentsCache = function(timesInterval){
        //Get the current query definition
        return searchService.getQuery().then(function(query) {

          //Generate a hash to represent the unique portion of this request
          var queryHash = sprintf.sprintf( "%s:documents:%s",
            SearchQueryService.hashQuery(query),
            timesInterval
          );

          //Make sure cache is in a good state before we check for existing results.
          prepareQueryCache();

          //If there's a cache for this same request
          if (_.has(queryCache, queryHash)) {

            //Get the cached response
            return queryCache[queryHash];
          }
          return { data: [], stats: { found: "~" }}
        });
      };

      /**
       * Get documents using current query definition
       * @param count
       * @param skip
       * @returns {*}
       */
      searchService.getRecords = function(count, skip){

        //Param checks
        if( count == 0 ) return $q.reject("Count may not be 0.");
        if( _.isNumber(skip) && skip < 0) return $q.reject("Skip ( if provided ) must be >= 0.");

        //Defaults
        count = !!count ? count : 100;
        skip = !!skip ? skip : 0;

        //Get the current query definition
        return searchService.getQuery().then(function(query) {

          //Generate a hash to represent the unique portion of this request
          var queryHash = sprintf.sprintf( "%s:records",
            SearchQueryService.hashQuery(query)
          );

          //Make sure cache is in a good state before we check for existing results.
          prepareQueryCache();

          //If there's a cache for this same request
          if (_.has(queryCache, queryHash)) {

            //Get the cached response
            var cachedSet = queryCache[queryHash];

            console.log("Cached set is: " + cachedSet.data.length + " records");

            //if( skip + count > cachedSet.length ) {
            //  //TODO make sure request doesn't create spacial cache
            //  //queryCache[queryHash].length >= skip + count
            //  console.log("SPACIAL SET");
            //}

            // It's possible to run out of data. completeSet is set
            if(cachedSet.isCompleteSet){
              console.log("Cache contains complete set.");
              if( cachedSet.data.length <= skip ){
                console.error("Requesting data past complete set");
                return $q.when(null);
              }
            }

            // If the cache is bigger than the position & data requested, use cache
            if (cachedSet.data.length >= skip + count) {
              console.log("Resolve with portion", skip, skip + count, cachedSet);
              // Resolve with cloned cache portion
              return $q.when( _.set( _.clone(cachedSet), "data", _.slice(cachedSet.data, skip, skip + count )));
            }

          }

          //Construct a infinite platform query
          var infiniteQuery = SearchQueryService.getRecordsQuery(query, count, skip);

          return _api_query(infiniteQuery).then(function (response) {
            console.log("Query response: ", response);

            // Adjust format
            response.data = response.records.hits.hits;
            response.stats.found = response.records.hits.total;
            delete response.records.hits.hits;

            //Cache response
            //splice results into place if there's already SOME cache for this request
            if (_.has(queryCache, queryHash)) {
              console.log("Adding extra docs to cache", queryCache[queryHash]);
              queryCache[queryHash].data = queryCache[queryHash].data.concat(response.data);
            } else {
              queryCache[queryHash] = response;
            }

            if( response.data.length < count ){
              console.log("Set is now complete", response.data.length, count);
              queryCache[queryHash].isCompleteSet = true;
            }

            //return the documents
            return response;
          });
        });
      };

      searchService.getRecordsCache = function(){
        //Get the current query definition
        return searchService.getQuery().then(function(query) {

          //Generate a hash to represent the unique portion of this request
          var queryHash = sprintf.sprintf("%s:records",
            SearchQueryService.hashQuery(query)
          );

          //Make sure cache is in a good state before we check for existing results.
          prepareQueryCache();

          //If there's a cache for this same request
          if (_.has(queryCache, queryHash)) {

            //Get the cache
            return queryCache[queryHash];
          }
          return { data: [], stats: { found: "~" }}
        });
      };

      /**
       *
       * @param count
       * @param timesInterval
       * @returns {*|Promise.<?QueryDef>}
       */
      searchService.getEntities = function(count, timesInterval){

        //Param checks
        if( count == 0 ) return $q.reject("Count may not be 0.");
        if( _.isString(timesInterval) && !/^(\d+[hdwmy])$/.test(timesInterval)) {
          return $q.reject("timesInterval is invalid.");
        }

        //Defaults
        count = !!count ? count : 100;

        return searchService.getQuery().then(function(query){

          //Generate a hash to represent the unique portion of this request
          var queryHash = sprintf.sprintf( "%s:entities:%s",
            SearchQueryService.hashQuery(query),
            timesInterval
          );

          //Make sure cache is in a good state before we check for existing results.
          prepareQueryCache();

          if (_.has(queryCache, queryHash)) {

            //Get the cached response
            var cachedSet = queryCache[queryHash];

            //make sure cache has the # of entities requested ( or more )
            if (cachedSet.entities.length >= count) {
              $log.debug("[searchService.getEntities] Using cached set.");
              // Resolve with cloned cache portion
              return $q.when( _.set( _.clone(cachedSet), "entities", _.slice(cachedSet.entities, 0, count )));
            }

            $log.debug("[searchService.getEntities] Cache too small", queryHash, cachedSet);
          }

          //Construct a query
          var infiniteQuery = SearchQueryService.getEntities(query, count, timesInterval);

          //Run query and cache response
          return _api_query(infiniteQuery).then(function (response) {
            //console.log("[SearchService->getEntities]", infiniteQuery, response);
            queryCache[queryHash] = response; //Cache response - no splice needed
            return _.cloneDeep(response); //return the entities
          });
        });
      };

      /**
       *
       * @param timeInterval
       * @param geoCount
       * @param entityList
       * @returns {*|Promise.<?QueryDef>}
       */
      searchService.getMoments = function(timeInterval, geoCount, entityList){
        prepareQueryCache();
        return searchService.getQuery().then(function(query){
          var infiniteQuery = SearchQueryService.getMoments(currentQuery, timeInterval, geoCount, entityList);
          return _api_query(infiniteQuery).then(function (response) {
            //console.log("[SearchService->getMoments]", infiniteQuery, response);

            //return the moments
            return response;
          });
        });
      };


      return searchService;

    }
  ])});
