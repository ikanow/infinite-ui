/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:SuggestService
 * @description
 * Service to call all Ikanow suggestion endpoints
 */
define(['app', 'angular', 'lodash'], function(app, angular, _){

  "use strict";
  return app.factory('SuggestService', [
    '$q', 'FeatureApiService', "workspaceService",

    function($q, FeatureApiService, workspaceService) {
      'use strict';

      // ********************************************************************* //
      // Public methods
      // ********************************************************************* //

      /**
       * Query API for a list of suggested aliases in a dataGroup or data groups
       * @param {string} field              The entity field on which to search, ie "index", "disambiguated_name",
       *                                    or "actual_name"
       * @param {string} term               The value of the entity field on which to search for aliases.
       *                                    NOTE: this needs to be URL-escaped, eg "/" in the "index" would be
       *                                    represented by "%2F"
       * @param {string|Array} (dataGroupIds) The list of dataGroup IDs. If omitted, the current
       *                                    data group IDs will be used
       * @returns {Promise<ApiResponse>}
       */
      function suggestAliases(field, term, dataGroupIds){
        if(!dataGroupIds){
          dataGroupIds = workspaceService.getDataGroupIds();
        }
        return FeatureApiService.aliasSuggest(field, term, dataGroupIds);
      }


      /**
       * Query API for a list of suggested associations
       * @param {string} entity1            The first term (subject) of the event you are searching for, send "null"
       *                                    to match on all subject entities
       * @param {string} verb               The 2nd term (verb) of the event you are searching for, send "null"
       *                                    to match on all possible verbs
       * @param {string} entity2            The 3rd term (object) of the event you are searching for, send "null"
       *                                    to match on all object entities
       * @param {"entity1"|"entity2"|"verb"} searchField
       *                                    The field that you want to get suggestions for, can be "entity1", "verb",
       *                                    or "entity2".
       *                                    NOTE: currently only "indexed" entities (eg "disambiguated_name/type") are
       *                                    returned, and only the verb categories corresponding to verbs.
       * @param {string|Array} (dataGroupIds) The list of dataGroup IDs. If omitted, the current
       *                                    data group IDs will be used
       * @returns {Promise<ApiResponse>}
       */
      function suggestAssociations(entity1, verb, entity2, searchField, dataGroupIds){
        if(!dataGroupIds){
          dataGroupIds = workspaceService.getDataGroupIds();
        }
        return FeatureApiService.associationSuggest(searchField, dataGroupIds, entity1, verb, entity2);
      }

      /**
       * Verb association suggestions in a friendly format
       * @param entity1
       * @param verb
       * @param entity2
       * @param {string|Array} (dataGroupIds) The list of dataGroup IDs. If omitted, the current
       *                                      data group IDs will be used
       * @returns {*}
       */
      function suggestVerbAssociations( entity1, verb, entity2, dataGroupIds){
        if(!dataGroupIds){
          dataGroupIds = workspaceService.getDataGroupIds();
        }
        return FeatureApiService.verbAssociationSuggest(dataGroupIds, entity1, verb, entity2)
          .then(function(suggestions){

              //Parsed results
              var result = {};

              //Add a category to the result set if it doesn't exist
              function addVerbCategory(category){
                if (!(category in result)) {
                  result[category] = [];
                }
              }

              //Iterate over each suggestion, parse it if necessary, and build a UI friendly format
              _.each(suggestions.data, function(suggestion) {
                //if there's a set of parenthesis we need to parse the verb
                // check for the existence of a '(' character
                var pIndex = suggestion.indexOf("(");

                //This one has no parenthesis, it's just the category
                if (pIndex == -1) {
                  //add it if it doesn't exist and go to next suggestion
                  return addVerbCategory(suggestion);
                }

                //Parse the two parts "verb (verb_category)"
                var verbCat = suggestion.substring(pIndex + 1, suggestion.length - 1);
                var verb = suggestion.substring(0, pIndex).trim();

                //Add the verb to the category if known
                if (verbCat in result && verbCat != verb) {
                  addVerbCategory(verbCat);
                  result[verbCat].push(verb);
                }

              });

              return result;
            }
          );
      }


      /**
       * Query API for a list of suggested entities.
       * @param {string} fragment           One or more words (the last word can also be a fragment), which
       *                                    are "fuzzily" compared against known entities stored in Infinit.e
       * @param {boolean} (includeGeo)      Set to true to always include geo information on results, or false to discard
       * @param {boolean} (includeLinkData) Set to true to include additional link information, or false to discard
       * @param {string|Array} (dataGroupIds) The list of dataGroup IDs. If omitted, the current
       *                                      data group IDs will be used
       */
      function suggestEntities(fragment, includeGeo, includeLinkData, dataGroupIds){
        if(!dataGroupIds){
          dataGroupIds = workspaceService.getDataGroupIds();
        }
        return FeatureApiService.entitySuggest(fragment, dataGroupIds, includeGeo, includeLinkData);
      }


      /**
       * Query API for a list of geo suggestions
       * @param {string} fragment           A comma separated string of lat,lng (e.g. 10.5,-36.2) or one or more
       *                                    words (the last word can also be a fragment), which are "fuzzily"
       *                                    compared against known geo tags stored in Infinit.e
       * @param {string|Array} (dataGroupIds) The list of dataGroup IDs. If omitted, the current
       *                                      data group IDs will be used
       * @returns {Promise<ApiResponse>}
       */
      function suggestGeo(fragment, dataGroupIds){
        if(!dataGroupIds){
          dataGroupIds = workspaceService.getDataGroupIds();
        }
        return FeatureApiService.geoSuggest(fragment, dataGroupIds)
      }

      //Expose UI-Friendly suggestion functions
      return {
        suggestAliases: suggestAliases,
        suggestAssociations: suggestAssociations,
        suggestVerbAssociations: suggestVerbAssociations,
        suggestEntities: suggestEntities,
        suggestGeo: suggestGeo
      };

    }

  ])});
