/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['app', 'lodash'], function(app, _){
  "use strict";

  return app.factory('reportService', [ "$log", "InfiniteApi", "ShareApiService", "SessionService", "workspaceService", "userService",
    function($log, InfiniteApi, ShareApiService, SessionService, workspaceService, userService) {

      var reportService = {};

      reportService.currentReport = {};

      // ********************************************************************* //
      //
      // Main Service Methods
      //
      // ********************************************************************* //

      /**
       * Get a (array) list of reports the user has access to
       * @returns {Promise<Array<Object>>}
       */
      reportService.listReports = function(){
        return ShareApiService.search({'type':'binary'})
          .then(InfiniteApi.resolveWithData)
          .then(function listReportsSuccess(reports) {
            return _.filter(reports, function (report) {
              //find the prepended text "ISAREPORT:" so we know it's a saved off PDF report, then strip that text
              if (_.startsWith(report.title, 'ISAREPORT:')) {
                report.title = report.title.replace('ISAREPORT:', "");
                return true;
              } else {
                return false;
              }
            });
          })
          .then(function(filteredReports){
            userService.getAll().then(function(users){
              //go through list of repots and map the right owner displayName from the list of users
              _.each(filteredReports, function(report) {
                if(report){ //fix weird firefox bug
                  var owner = _.map(_.filter(users, {_id : report.owner._id}), 'displayName')[0];
                  report.owner.displayName = owner ? owner : report.owner.displayName;
                }
              });
            });
            return filteredReports;
          });
      };

      /**
       * Get a report
       * @param reportID <string>
       * opens new window to download/view PDF report
       */
      reportService.getFile = function(reportID){
        $log.debug('getting report ID: ', reportID);
        return ShareApiService.getFileBase64(reportID);
      };

      /**
       * Add a new report
       * @param reportTitle <string>
       * @param reportDesc <string>
       * @param fileData <file/blob>
       * @returns {Promise<Array<Object>>}
       */
      reportService.add = function(reportTitle, reportDesc, fileData){
        $log.debug('adding report');

        var projectDataGroupId = workspaceService.getWorkspaceGroupId();

        reportTitle = 'ISAREPORT:' + reportTitle; //prepend ISAREPORT: to the report title so we can filter for reports

        return ShareApiService.uploadFile(
          projectDataGroupId,               // Store the report in the current project datagroup
          'binary',                         //share type
          reportTitle,                      //share title
          reportDesc,                       //share description
          fileData,                         //file data
          'application/pdf'                 //force content type to PDF for reports
        ).then(InfiniteApi.resolveWithData);

      };

      /**
       * update a report
       * @param report {object}
       * @param reportTitle <string>
       * @param reportDesc <string>
       * @returns {Promise<Array<Object>>}
       */
        //WILL NOT WORK ON LOCAL ENVIRONMENT THROUGH THE PROXY, needs testing on a real cluster
      reportService.update = function(report, reportTitle, reportDesc){
        $log.debug('updating report ID', report._id);

        reportTitle = 'ISAREPORT:' + reportTitle; //prepend ISAREPORT: to the report title so we can filter for reports

        //fetch the file data from share, so we can re-upload it. limitation of API, can't just change title/desc without reuploading file
        return ShareApiService.get(report._id,null,null,true).then(function(fileData){
          return ShareApiService.uploadFile(
            report.communities[0]._id,        //list of datagroups/communities, just keeping the existing community for now
            'binary',                         //share type
            reportTitle,                      //share title
            reportDesc,                       //share description
            fileData,                         //file data
            'application/pdf'                 //force content type to PDF for reports
          ).then(function(data){
            reportService.deleteReport(report._id);
            return data.data;
          });
        });

      };

      /**
       * Remove a report from the platform
       * @param reportID (shareID)
       * @returns {*}
       */
      reportService.deleteReport = function(reportID){
        $log.debug('reportService.deleteReport', reportID);
        return ShareApiService.remove(reportID).then(InfiniteApi.resolveWithData);
      };

      return reportService;
    }]);

});