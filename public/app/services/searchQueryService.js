/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:SearchQueryService
 * @description
 * This service is used by search service to manage the construction of query objects for the infinite platform.
 */
define(['angular', 'app', 'hashes', 'lodash'], function(angular, app, hashes, _){
  "use strict";

  /**
   * See Infinite Document Query API Docs.
   * @typedef {Object} InfiniteApiQuery
   * @property {QueryTerm[]} qt
   * @property {String} logic
   * @property {Object} input
   * @property {String[]} input.tags
   * @property {Object} output
   * @property {Object} output.aggregation,
   * @property {Object} output.docs,
   * @property {Object} output.filter,
   * @property {Object} score
   * @property {Object} score.timeProx
   */

  /**
   *  @typedef {Object} EntityTypes
   *  @property {boolean} person
   *  @property {boolean} place
   *  @property {boolean} organization
   *  @property {boolean} association
   *  @property {boolean} other
   */

  /**
   *  @typedef {Object} WeightConfig
   *  @property {?Date} dateTime
   *  @property {?string} halfLife
   */

  /**
   *  @typedef {QueryDef} QueryConfig
   *  @property {number}  documentCount
   *  @property {boolean} documentEntities
   *  @property {boolean} documentGeo
   *  @property {boolean} documentEvents
   *  @property {boolean} documentEventsTimeline
   *  @property {boolean} documentFacts
   *  @property {boolean} documentSummaries
   *  @property {boolean} documentMetadata
   *  @property {?number} sourceMetadataCount
   *  @property {?number} entityCount
   *  @property {?number} eventsCount
   *  @property {?number} factsCount
   *  @property {?number} geoCount
   *  @property {string}  aggregationTimeInterval
   *  @property {string}  sortBy
   */

  /**
   *  Compound Entity query term
   *  @typedef {{entity: string}} QTCompoundEntity
   */

  /**
   *  Entity Query Term
   *  @typedef {Object} QTEntity
   *  @property {string} entityType
   *  @property {string} entityValue
   */

  /**
   *  Exact text query term
   *  @typedef {{etext:string}} QTExact
   */

  /**
   *  Free text query term
   *  @typedef {{ftext: string}} QTFree
   */

  /**
   *  @typedef {(QTExact|QTFree|QTEntity|QTCompoundEntity)} QueryTerm
   */

  /**
   *  @typedef {Object} QueryInputSection
   *  @property {string[]} tags
   */

  /**
   *  @typedef {Object} QueryOutputSection
   *  @property {Object} docs
   *  @property {Object} filter
   */

  /**
   *  @typedef {Object} QueryScoreSection
   */

  /**
   *  TODO add sorting
   *  @typedef {("-date" | "+date" | "relevance")} SortOption
   */

  return app.factory('SearchQueryService', [
    '$filter',

    function( $filter ) {
      'use strict';

      /**
       * Base API Query - Comprehensive and linted before submission
       * @param {QueryDef} queryDef
       * @returns {InfiniteApiQuery}
       */
      function newInfiniteKnowledgeQuery(queryDef){
        //Create a basic query object with default configuration and basic options applied
        return {
          qt: _compileQueryTerms(queryDef),
          logic: _compileQueryTermLogic(queryDef),

          // Only referenced in docs as a null parameter?
          // current not used.
          //qtOptions: null,

          // this structure will be lazily created when we optionally add tags.
          input: {
            tags: _compileTagsSection(queryDef)
          },

          output: {
            //  format: "json",
            aggregation: {
              sourceMetadata: 20,
              entsNumReturn: 100,
              timesInterval: "1d",
              //sources: 20,
              eventsNumReturn: 100,
              factsNumReturn: 100,
              geoNumReturn: 100,

              //Empty object, removed on lint unless options are set by query building function
              moments: {
                timesInterval: null,
                geoNumReturn: null,
                entityList: []
              }
            },
            docs: {
              enable: false,
              numReturn: 100,
              skip: 0,

              eventsTimeline: false,
              //    numEventsTimelineReturn: 1000,

              ents: true,
              geo: true,
              events: true,
              facts: true,
              summaries: true,
              metadata: true
            },
            records: {
              numReturn: null,
              skip: null
            },
            filter: {
              assocVerbs: _compileOutputFiltersVerbs(queryDef),
              entityTypes: _compileOutputFiltersEntities(queryDef)
            }
          },

          //This score section will be created using weighting options
          score: {

            //Geo options
            // TODO add geo config to query
            //geoProx: {
            //  ll: "41.742706,-76.398203",
            //  decay: ""
            //},

            // See preceding sections for other parameters
            timeProx: {
              time: null,
              decay: null
            }

            // See following sections for other parameters
            //adjustAggregateSig: null,
            //tagWeights: null,
            //relWeight: 32,
            //typeWeights: null,
            //sigWeight: 68,
            //sourceWeights: null,
            //scoreEnts: true,
            //numAnalyze: 1000

          }

        };
      }

      /**
       * Construct the query terms section of an API query
       * @param {QueryDef} queryDef
       * @returns {QueryTerm[]}
       * @private
       */
      function _compileQueryTerms(queryDef){

        //If this query has no string add a default wildcard
        if(_.isEmpty(queryDef.searchString)){
          queryDef.searchString = "*"
        }

        var terms = [];
        terms.push({ ftext: queryDef.searchString});
        return terms;
      }

      /**
       * Construct the query term logic definition for a set of API query terms
       * @param {QueryDef} queryDef
       * @returns {String}
       * @private
       */
      function _compileQueryTermLogic(queryDef){
        if( queryDef.searchString != "" ) {
          return "1";
        }
        return null;
      }

      /**
       * Construct the input section of an API query
       *
       * Output Example
       * ["tag1", "tag2"]
       *
       * @param {QueryDef} queryDef
       * @returns {QueryInputSection|*}
       * @private
       */
      function _compileTagsSection(queryDef){
        var useTags = [];
        // Only apply a origin tag if one or the other is in place.
        if( !_.isEmpty(queryDef.origin) && queryDef.origin.length == 1 ){
          useTags.push(queryDef.origin[0]);
        }
        // Append user supplied tags
        if( queryDef.tags != null && queryDef.tags.length > 0 ){
          useTags = useTags.concat( _.isString(queryDef.tags) ? queryDef.tags.split(",") : queryDef.tags );
        }
        return useTags;
      }

      /**
       * Prepare an array of verbs from user selections ready for the API to consume.
       * @param {QueryDef} queryDef
       * @returns string[]
       * @private
       */
      function _compileOutputFiltersVerbs(queryDef){
        //Verbs are stored as an array of string already
        return _.clone(queryDef.verbs);
      }

      /**
       * Generate a list of Entity types ready for usage in the filter section
       * If humanReadable is true, the "Other" type will be described.
       * @param {QueryDef} queryDef
       * @param {boolean} (humanReadable)
       * @returns string[]
       * @private
       */
      function _compileOutputFiltersEntities( queryDef, humanReadable ){

        var entityFilters = [];

        //If 'Other' is selected, the logic needs to be structured differently.
        //
        //  Other = !Person & !Place & !Organization & !Association
        //  Person + Other = !Place & !Organization & !Association

        if( queryDef.entities.other ){

          if( humanReadable == true ){
            entityFilters.push("Other");

            if (queryDef.entities.person) {
              entityFilters.push("Person");
            }

            if (queryDef.entities.place) {
              entityFilters.push("Place");
            }

            if (queryDef.entities.organization) {
              entityFilters.push("Organization");
            }

            if (queryDef.entities.association) {
              entityFilters.push("Association");
            }

          } else {
            entityFilters.push( queryDef.entities.person ? "Person" :       "-Person");
            entityFilters.push( queryDef.entities.person ? "Place" :        "-Place");
            entityFilters.push( queryDef.entities.person ? "Organization" : "-Organization");
            entityFilters.push( queryDef.entities.person ? "Association" :  "-Association");
          }

        } else {
          if( queryDef.entities.person) entityFilters.push("Person");
          if( queryDef.entities.place) entityFilters.push("Place");
          if( queryDef.entities.organization) entityFilters.push("Organization");
          if( queryDef.entities.association) entityFilters.push("Association");
        }

        return entityFilters;
      }

      /**
       * Construct the output section of an API Query
       * TODO: Implement this feature
       * @returns {QueryScoreSection}
       * @private
       */
      function _compileScoreSection(queryDef){


        var formattedDateTime;
        if( angular.isDate(queryDef.weightBy.dateTime)){
          formattedDateTime = $filter('date')(queryDef.weightBy.dateTime, "dd MMM yyyy HH:mm:ss", "UTC")
        } else {
          formattedDateTime = "now";
        }

        return {

          //Geo options
          // TODO add geo config to query
          //geoProx: {
          //  ll: "41.742706,-76.398203",
          //  decay: ""
          //},

          // See preceding sections for other parameters
          timeProx: {
            time: formattedDateTime,
            decay: queryDef.weightBy.halfLife
          }

          // See following sections for other parameters
          //adjustAggregateSig: null,
          //tagWeights: null,
          //relWeight: 32,
          //typeWeights: null,
          //sigWeight: 68,
          //sourceWeights: null,
          //scoreEnts: true,
          //numAnalyze: 1000

        };

      }

      /**
       * Lint an object deeply.
       * TODO check to be sure false values are not removed
       *
       * If a value is null or an object is empty, the key/value is removed.
       * @param {Object} o
       * @private
       */
      function _lintObject( o ){
        var k, v;
        for( k in o ) {
          if(o.hasOwnProperty(k) ) {
            v = o[k];
            if( angular.isObject(v) ){
              v = _lintObject(v);
            }
            if( v == null || (angular.isObject(v) && _.isEmpty(v)) ){
              delete o[k];
            }
          }
        }
        return o;
      }

      //Expose public search history functions
      return {

        //
        hashQuery: function( queryDef ){
          var SHA256 = new hashes.SHA256(),
              hashableQueryDef = _.cloneDeep(queryDef);

          //Remove timestamp from query
          delete hashableQueryDef.datetime;
          //delete hashableQueryDef.id;

          return SHA256.b64(angular.toJson(hashableQueryDef));
        },

        /**
         * A query definition may have objects such as a Date() which was
         * serialized into JSON for storage and is now a string. To ensure the
         * definition is in it's proper state, pass the definition(s) through this
         * function.
         *
         * @param {QueryDef} queryDef
         * @return {QueryDef}
         */
        deserializeQuery: function(queryDef){
          //console.log("Deseralize: ", _.cloneDeep(queryDef));
          if('datetime' in queryDef &&
            angular.isNumber(queryDef.datetime) || angular.isString(queryDef.datetime)) {
            queryDef.datetime = new Date(queryDef.datetime);
          }

          if(queryDef.weightBy && queryDef.weightBy.dateTime &&
            angular.isNumber(queryDef.weightBy.dateTime) ) {
            queryDef.weightBy.dateTime = new Date(queryDef.weightBy.dateTime);
          }
          return queryDef;
        },

        /**
         * Describe current query entities or a given queryDef
         * @param {QueryDef} queryDef
         * @return {String[]}
         */
        describeEntities: function(queryDef){
          return _compileOutputFiltersEntities(queryDef, true /* human readable */ );
        },

        /**
         * Build a query to fetch documents using base query config
         * @param {QueryDef} queryDef Query definition options
         * @param {Number} docCount Number of documents to fetch
         * @param {Number} (skip) Number of documents to seek, for paging
         * @return {InfiniteApiQuery}
         * @param timesInterval
         */
        getDocumentQuery: function( queryDef, docCount, skip, timesInterval){

          //Get a comprehensive query with defaults, and apply queryDef options
          var infiniteQuery = newInfiniteKnowledgeQuery(queryDef);

          //Set document-specific options.
          infiniteQuery.output.docs.enable = true;
          infiniteQuery.output.docs.numReturn = docCount;
          infiniteQuery.output.docs.skip = skip;

          infiniteQuery.output.aggregation.timesInterval = timesInterval;

          //Return infinite query
          return _lintObject(infiniteQuery);
        },

        getRecordsQuery: function( queryDef, count, skip ){
          var infiniteQuery = newInfiniteKnowledgeQuery(queryDef);

          // Set record specific values
          infiniteQuery.output.records.numReturn = count;
          infiniteQuery.output.records.skip = skip;

          //Return infinite query
          return _lintObject(infiniteQuery);
        },

        getEntities: function( queryDef, entityCount, timesInterval ){
          var infiniteQuery = newInfiniteKnowledgeQuery(queryDef);
          infiniteQuery.output.aggregation.entsNumReturn = entityCount;
          infiniteQuery.output.aggregation.timesInterval = timesInterval;
          return _lintObject(infiniteQuery);
        },

        getMoments: function( queryDef, timesInterval, geoCount, entityList){
          var infiniteQuery = newInfiniteKnowledgeQuery(queryDef);
          infiniteQuery.output.aggregation.moments.timesInterval = timesInterval;
          infiniteQuery.output.aggregation.moments.geoNumReturn = geoCount;
          infiniteQuery.output.aggregation.moments.entityList = entityList;
          return _lintObject(infiniteQuery);
        }

      };

    }

  ])});
