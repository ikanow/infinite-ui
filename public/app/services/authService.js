/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:authService
 * @description
 * Handles all front-end in-memory and persisted session based data.
 */

/**
 * @typedef {Object} UserProfile
 *
 */

define(['angular', 'app'], function(angular, app){
  "use strict";

  return app.factory('authService', function( $rootScope, $q, $state, $log, $angularCacheFactory, $interval,
                                              InfiniteApi, AuthApiService, dataGroupService, workspaceService,
                                              SessionService, SessionMonitor, userService, userGroupService) {

    //Auth token cache for isAuthenticated test.
    var currentToken = null;
    var authService = {};

    // ********************************************************************* //
    // Public Methods
    // ********************************************************************* //

    /**
     * @ngdoc method
     * @name login
     * @methodOf app.service:authService
     * @description
     * Authenticate the user against the remote API login endpoint.
     *
     * @param {String} username User name
     * @param {String} unhashedPassword Plain text password
     * @returns {Promise<UserProfile>} A promise which will resolve with the user's profile when complete.
     */
    authService.login = function(username, unhashedPassword) {

      if(!angular.isDefined(username)){
        return $q.reject("Missing required field: Username");
      }
      if(!angular.isDefined(unhashedPassword)){
        return $q.reject("Missing required field: Password.");
      }

      return AuthApiService.login(username, unhashedPassword, true)
        .then(InfiniteApi.resolveWithData)
        .then(function(newToken){
          //$log.debug('[authService.login] Login successful', apiResponse);
          //Persist auth token in GUI session ( and HTML5 storage )
          return authService.initLoggedInUser(newToken, true);
        })
        .catch(function authFailed(err){
          $log.debug('[authService.login] Login failed.', err);
          //Remove persisted session data
          SessionService.clearSession();
          //reject parent promise
          return $q.reject(err);
        });
    };

    /**
     * @ngdoc method
     * @name forgotPassword
     * @methodOf app.service:authService
     * @description
     * reset a forgotten password
     *
     * @param {String} username User name
     * @param {String} unhashedPassword Plain text password
     * @param {String} token plain text token provided by the API used to reset password
     * @returns {Promise<UserProfile>} A promise which will resolve with the user's profile when complete.
     */
    authService.forgotPassword = function(username, unhashedPassword, token, testValid) {
      var options = {};
      if(username){options.username = username;}
      if(token){options.password = token;}
      if(unhashedPassword){options.new_password = unhashedPassword;}
      if(testValid){options.test_token = testValid;}
      
      return AuthApiService.forgotPassword(options)
        .then(InfiniteApi.resolveWithResponse)
        .then(function(apiResponse){
          $log.debug('[authService.forgotPassword] Password reset sent', apiResponse);
          return apiResponse;
        })
        .catch(function resetFailed(err){
          $log.debug('[authService.forgotPassword] Password reset failed.', err);
          //reject parent promise
          return $q.reject(err);
        });
    };

    /**
     * @ngdoc method
     * @name initLoggedInUser
     * @methodOf app.service:authService
     * @description
     * After a successful login or finding a persistent token, use this
     * to fetch the user profile, set session vars, and $rootScope userVars.
     * @param {String} newToken Auth token to use for initialization.
     * @param {Boolean} [fromLogin=false] Set to true if coming from initial login
     * @returns {Promise<Workspace>} A promise which will resolve with the startup workspace
     */
    authService.initLoggedInUser = function(newToken, fromLogin){
      fromLogin = fromLogin === true;
      //Get current user calls getUser("") which in turn calls
      // getAll and updates the cache, then fetches the current user.
      return userService.getCurrentUser(true).then(
        function fetchProfileSuccess(userProfile) {

          //Cache new token
          currentToken = newToken;

          //Persist the auth token to speed up app init after reload.
          SessionService.setAuthToken(currentToken);
          SessionService.setUserProfile(userProfile);

          //Some root scope tracking of simple values.
          $rootScope.currUsername = userProfile.WPUserID;
          $rootScope.currFullname = userProfile.displayName;

          //globally be able to check isAdmin anywhere without having to use SessionService each time
          $rootScope.isAdmin = SessionService.isAdmin();

          //Get the data groups next
          return userGroupService.getAll(true)
            .then( function(){ return dataGroupService.getAll(true); } )
            .then( function(){ return workspaceService.getAll(true); })
            .then( workspaceService.workspaceInit );
        }
      );
    };

    /**
     * @ngdoc method
     * @name keepAlive
     * @methodOf app.service:authService
     * @description
     * Extends the user's session
     * @returns {Promise<ApiResponse>} A promise which will resolve with the api response when complete
     */
    authService.keepAlive = function() {
      //$log.debug('[authService.keepAlive] Attempting to extend user session.');
      return AuthApiService.keepAlive().then(
        function keepAliveSuccess(apiResponse){
          //$log.debug('[authService.keepAlive] Success:', apiResponse);
          SessionMonitor.reset();
          return apiResponse;
        },
        function keepAliveFailed(err){
          $log.debug('[authService.keepAlive] Failed:', err);
          SessionService.clearSession();
          return $q.reject(err);
        }
      );
    };

    /**
     * @ngdoc method
     * @name logout
     * @methodOf app.service:authService
     * @description
     * Performs a logout action for the current user and clears caches
     * @returns {Promise<true>} A promise which will resolve to true when complete.
     */
    authService.logout = function(){
      return _terminateSession().then( function(result){
        $log.debug("[authService.logout] Logout result: ", result);
        return result;
      });
    };

    /**
     * @ngdoc method
     * @name hasPersistedSession
     * @methodOf app.service:authService
     * @description
     * Determine if an authenticated user has a still valid session in the cache. It's
     * possible that the app was refreshed after they authenticated, so check the cache
     * to see if they're authenticated and that the timestamp of the last API activity
     * is not greater than the session timeout value.
     * @returns {Boolean} True if a token was found in HTML5 storage
     */
    authService.hasPersistedSession = function() {
      //console.log("[authService.hasPersistedSession]");
      return SessionService.hasAuthToken();
    };

    /**
     * @ngdoc method
     * @name isAuthenticated
     * @methodOf app.service:authService
     * @description
     * Simple test to see if the current session has been authenticated by checking to see if there's a currentToken
     * @returns {Boolean} True if a token was found in HTML5 storage
     */
    authService.isAuthenticated = function() {
      //console.log("[authService.isAuthenticated]");
      return !!currentToken;
    };

    // ********************************************************************* //
    // Private Methods
    // ********************************************************************* //

    /**
     * @ngdoc method
     * @name _terminateSession
     * @methodOf app.service:authService
     * @description
     * Invalidates the current user session, Continue as successful even if the API logout command fails.
     * @returns {Promise} A Promise which will always resolve successfully.
     * @private
     */
    function _terminateSession() {
      var deferred = $q.defer();
      $log.debug('[authService->_terminateSession]');
      AuthApiService.logout()
        .then(
          function logoutSuccess(response){
            return response;
          },
          function logoutFailure(err){
            $log.debug('[authService->_terminateSession] Api logout error:', err);
          }
        )
        .finally(function(){

          //Some root scope tracking of simple values.
          $rootScope.currUsername = undefined;
          $rootScope.currFullname = undefined;

          //Remove cached token
          currentToken = null;

          //SessionService HTML5 Storage
          SessionService.clearSession();
          SessionMonitor.stopAll();

          //always success
          deferred.resolve(true);
        });
      return deferred.promise;
    }

    // factory returns authService object
    return authService;
  });

});