/**
 * @ngdoc service
 * @name app.service:KibanaService
 * @description
 * KibanaService can be used to launch queries in a new Kibana window or modal.
 *
 */
define(['angular', 'app'], function(angular, app){
  "use strict";

  return app.factory('KibanaService', ['$q', '$log', '$uibModal', '$window', 'workspaceService',

    function($q, $log, $uibModal, $window, workspaceService) {
      'use strict';

      var kibanaURL = "/infinit.e.records/static/kibana/index.html#/dashboard/file/Kibana_LiveTemplate.json";
      var kibanaService = {};

      function openKibana (query, modal) {
        var modalInstance;
        var kibanaWindow;
        var dataGroupIds = workspaceService.getDataGroupIds().join(",");

        //Check the document readyState every 10ms and wait for the page to load
        var readyStateCheckInterval = 100;
        var readyStateCheckCount = 0;

        if (modal) {
          modalInstance = $uibModal.open({
            templateUrl: 'app/modules/workspace/views/kibana-modal.html',
            backdrop: 'static',
            windowClass: 'manager-modal-window'
          });
        } else {
          modalInstance = $window.open(kibanaURL);
        }

        //Start checking for initialization complete every 10ms
        var readyStateCheckReference = setInterval(function() {
          readyStateCheckCount++;

          if (modal) {
            if (!window.frames[0] && window.frames[0].kibanaJsApi) return;
            // Kibana is running in an iframe in a modal
            kibanaWindow = window.frames[0];
          } else {
            // Kibana is running in a new window
            kibanaWindow = modalInstance;
          }

          // Try to set the query until kibanaJsApi is initialized
          try {

            //Override get community IDs (dataGroupIds)
            kibanaWindow.infiniteJsConnector.getCommunityIds = function getCommunityIds(url){
              //console.log("Redirected getCommunityIds", dataGroupIds);
              return dataGroupIds;
            };

            //Override get mode
            kibanaWindow.infiniteJsConnector.getMode = function getMode(){
              //console.log("Redirected getMode");
              return 'live';
            };

            if (angular.isUndefined(query.from) && angular.isUndefined(query.to)) {
              query.from = "now-30d";
              query.to = "now";
            }

            // Set the kibana filter
            kibanaWindow.kibanaJsApi.setFilters([
              {
                "active": true,
                "field": "@timestamp",
                "from": query.from,
                "id": 0,
                "mandate": "must",
                "to": query.to,
                "type": "time"
              }
            ]);

            //Set the kibana query
            kibanaWindow.kibanaJsApi.setQueryList([
              {
                "query": query.search,
                "id": 0,
                "type": "lucene",
                "enable": true
              }
            ]);

            //Stop timer for init check
            clearInterval(readyStateCheckReference);
          }
          catch (err) {

          }
        }, readyStateCheckInterval);

      }

      kibanaService.openKibanaModal = function (query) {
        openKibana(query, true);
      };

      kibanaService.openKibanaTab = function (query) {
        openKibana(query);
      };


      return kibanaService;

    }
  ])});
