/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:userService
 * @description
 * userService handles all infinite platform endpoints supporting users
 *
 */
define(['angular', 'app', 'lodash'], function(angular, app, _){
  "use strict";

  return app.factory('userService', [
    '$q', '$rootScope', '$log', "SessionService", "InfiniteApi", "PersonApiService",

    function($q, $rootScope, $log, SessionService, InfiniteApi, PersonApiService) {
      'use strict';

      //Modification time stamp for watches
      var lastKnownModification = 0;

      //Method definitions

      /**
       * Add a new data group to an optional parent data group
       * @param userDef
       * @returns {Promise<*>}
       */
      function register( userDef ){
        return PersonApiService.register(userDef)
          .then( InfiniteApi.resolveWithData )
          .then( function(userDef){
            var userList = SessionService.getUserList();
            userList.push(userDef);
            SessionService.setUserList( userList );
            lastKnownModification = new Date().getTime();
            return _.cloneDeep(userDef);
          });
      }

      /**
       * Get a user by ID
       * @param id
       * @param (forceReload)  If true, the local cache will be ignored and a cache refresh will be forced
       * @returns {Promise<*>}
       */
      function get(id, forceReload){
        //Calling getAll will use cache if available
        return getAll(forceReload).then(function(users){
          var user = _.find(users, {_id:id});
          //resolve if found
          if(user) return $q.when(user);

          //Now that the user list is cached, check to see if we wanted the current user
          // TODO Remove this after auth/login provides user info
          if( id == "me" ){
            return PersonApiService.get("me").then(InfiniteApi.resolveWithData);
          }

          //No luck
          $log.error("[userService.get] Could not find user: " + id);
          return $q.reject("User not found or no access.");
        });
      }

      /**
       * Get all users
       * @param (forceReload)
       * @returns {Promise<*>}
       */
      function getAll(forceReload){
        if( forceReload !== true && lastKnownModification != 0 ){
          //$log.debug("[userService.getAll] Using cache");
          return $q.when(SessionService.getUserList());
        }

        return PersonApiService.list()
          .then(InfiniteApi.resolveWithData)
          .then(function(userList){
            SessionService.setUserList(userList);
            lastKnownModification = new Date().getTime();
            return SessionService.getUserList();
          });
      }


      /**
       * Remove a user
       * @param id
       * @returns {Promise<*>}
       */
      function remove(id){
        return PersonApiService.remove(id)
          .then(function removeSuccess(ignoredApiResponse){
            var userList = SessionService.getUserList();
            userList = _.reject(userList, {_id:id});
            SessionService.setUserList(userList);
            lastKnownModification = new Date().getTime();
            return true;
          });
      }

      /**
       * Update a user
       * @param userDef
       * @returns {Promise<*>}
       */
      function update(userDef){
        return PersonApiService.update(userDef)
          .then(InfiniteApi.resolveWithData)
          .then(function updateUserSuccess(updatedUserDef){
            lastKnownModification = new Date().getTime();

            // Get the full profile.
            // TODO Move this to js sdk
            return get(updatedUserDef._id, true)
              .then(function(fullProfile){
                //update cache
                var users = SessionService.getUserList();
                var userListIndex = _.findIndex( users, {_id: fullProfile._id});
                users.splice( userListIndex, 1, fullProfile);
                SessionService.setUserList(users);

                //if they updated their own account, update cached user profile and current username/displayname
                if(fullProfile._id == SessionService.getUserId()){
                  SessionService.setUserProfile(fullProfile);
                  $rootScope.currUsername = fullProfile.WPUserID;
                  $rootScope.currFullname = fullProfile.displayName;
                }
                return fullProfile;
              });
            }
          );
      }

      //Public methods
      return {

        //Cache watch integer
        lastKnownModification: lastKnownModification,

        //API Methods
        get: get,
        getAll: getAll,
        register: register,
        remove: remove,
        update: update,
        //updateEmails: updateEmails,
        //updatePassword: updatePassword,

        // For your convenience...
        getCurrentUser: function(forceRefresh){
          return get("me", forceRefresh);
        },

        // Backwards compat with possibly deprecated peopleService

        /**
         *
         * @param user
         */
        addUser: function(user){
          var newPerson = {
            "user": {
              "firstname":user.firstName || "",
              "lastname":user.lastName || "",
              "phone":user.phone || "",
              "email":[user.email]
            },
            "auth": {
              "password": user.password || '123456',
              "accountType": user.accountType || "user",
              "accountStatus": angular.uppercase(user.accountStatus)
            }
          };
          return register(newPerson);
        },

        /**
         *
         */
        deleteUser: remove,

        /**
         *
         */
        getUser: get,

        /**
         *
         */
        getUsersList: getAll,

        /**
         * updateUser matches legacy peopleService's behavior
         * @param user
         * @returns {Promise.<*>}
         */
        updateUser: function(user){
          var updatedPerson = {
            "user": {
              "WPUserID":user.WPUserID||user.email, //if updating e-mail address, WPUserID must be provided
              "firstname":user.firstName || "",
              "lastname":user.lastName || "",
              "phone":user.phone || "",
              "email":[user.email]
            },
            "auth": {
              "accountStatus": angular.uppercase(user.accountStatus)
            }
          };

          if (user.password) {
            updatedPerson.auth.password = user.password;
          }

          if (user.accountType) {
            updatedPerson.auth.accountType = user.accountType;
          }

          if (user.apiKey) {
            updatedPerson.auth.apiKey = user.apiKey;
          }

          return update(updatedPerson);
        }
      };

    }
  ]);
});