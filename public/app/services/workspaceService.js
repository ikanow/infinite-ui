/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:workspaceService
 * @description
 * Manages workspace information, cache, and state.
 *
 */
define(['angular', 'app', 'lodash'], function(angular, app, _) {
  "use strict";
  return app.factory('workspaceService', [
    '$q', '$rootScope', '$log', "SessionService", "userService", "dataGroupService", "WorkspaceApiService", "InfiniteApi",

    function ($q, $rootScope, $log, SessionService, userService, dataGroupService, WorkspaceApiService, InfiniteApi) {
      'use strict';

      var currentSpace = null;
      var workspaceService = {};

      // Watch timestamp
      workspaceService.lastKnownModification = 0;
      workspaceService.spaces = [];

      //join user list to projects/workspaces for correct owner display name
      function _joinUsers(spaces){
        //get user list
        userService.getAll().then(function(users){
          //TODO remove this when the API behaves as expected
          //go through list of projects/workspaces and map the right owner displayName from the list of users
          _.each(spaces, function(space) {
              if(space){ //fix weird firefox bug
                var owner = _.map(_.filter(users, {_id : space.owner._id}), 'displayName')[0];
                space.owner.displayName = owner ? owner : space.owner.displayName;
              }
          });
        });

        return spaces;
      }

      /**
       * Get all workspace definitions for this user
       * @param (forceReload) Set to true to force-reload cache
       * @return {*}
       */
      workspaceService.getAll = function(forceReload){
        forceReload = forceReload === true;
        $log.debug("workspaceService.getAll:forceReload", forceReload);
        if( forceReload !== true && workspaceService.lastKnownModification != 0 ){
          return $q.when(workspaceService.spaces).then(_joinUsers);
        }

        return WorkspaceApiService.getAll()
          .then(InfiniteApi.resolveWithData)
          .then(_joinUsers)
          .then(function(workspaces){
            workspaceService.spaces = _.sortBy(workspaces, 'title' );
            workspaceService.lastKnownModification = new Date().getTime();
            return workspaceService.spaces;
          });
      };

      /**
       * Get a workspace definition by id
       * @param id
       * @param forceReload
       */
      workspaceService.getById = function(id, forceReload){
        //$log.debug("workspaceService.getById", id, forceReload);
        return workspaceService.getAll(forceReload).then(function(spaces){
          $log.debug("workspaceService->getById] " + id, spaces);
          var result = _.find(spaces, {_id:id}) || null;
          if( result === null ){
            return $q.reject("Could not find workspace: " + id);
          }
          return result;
        });
      };

      /**
       * Create a new workspace
       */
      workspaceService.createWorkspace = function(name, description, dataGroups, members){
        //$log.debug("workspaceService.getById", arguments);
        return WorkspaceApiService.createWorkspace(name, description, dataGroups, members)
          .then(InfiniteApi.resolveWithData)
          .then(function(workspaceDef){
            return $q.all({
              workspace: workspaceService.getById(workspaceDef._id, true),
              dataGroups: dataGroupService.getAll(true)
            });
          }).then(function(cacheUpdateResults){
            //let other connected users know there was a change to a workspace
            return cacheUpdateResults.workspace;
          });
      };

      /**
       * Check to see if a workspace name is already taken
       * @param name
       */
      workspaceService.isNameValid = function(name){
        return WorkspaceApiService.isNamesValid([name])
          .then(InfiniteApi.resolveWithData)
          .then(function(nameResponses){ return nameResponses[name]; });
      };

      /**
       * Remove a workspace
       * @param id
       */
      workspaceService.removeWorkspace = function(id){
        return WorkspaceApiService.remove(id).then(function(ignoredApiResponse){
          workspaceService.spaces = _.reject(workspaceService.spaces, {_id: id});
          workspaceService.lastKnownModification = new Date().getTime();
          return true;
        });
      };

      /**
       * Update a workspace
       * this process uses the response to update the workspace master group cached copy as well.constructor
       * The cache update means an API query isn't needed to get the updated members.
       */
      workspaceService.updateWorkspace = function(id, name, description, dataGroupIds, members){

        //WorkspaceApiService needs dataGroups nor dataGroupsIds so we'll do a lookup here
        return dataGroupService.getAll().then(function(dataGroups){
          return WorkspaceApiService.update({
              _id: id,
              name: name,
              description: description,
              dataGroups: _.intersectionBy(InfiniteApi.idListAsObjects(dataGroupIds), dataGroups, "_id"),
              members: members
            })
            .then(InfiniteApi.resolveWithData)
            .then(function(workspace) {
              dataGroupService.cache_updateGroupMembers(workspace.workspaceGroupId, workspace.members);
              return workspace;
            })
            .then(function(workspace){
              var spacesIndex = _.findIndex(workspaceService.spaces, { _id: id });
              if(spacesIndex > -1){
                workspaceService.spaces.splice(spacesIndex, 1, workspace);
                workspaceService.lastKnownModification = new Date().getTime();
              }
              return workspace;
            });
        });
      };

      /**
       * Workspaces are considered 'ready' when there's a current workspace. Either from
       * restoring a session, or selecting a workspace.
       * @return {boolean}
       */
      workspaceService.isReady = function(){
        return currentSpace != null;
      };

      /**
       * Get the current workspace definition
       * @return {*}
       */
      workspaceService.getCurrent = function(){
        //$log.debug("[workspaceService.getCurrent]", currentSpace);
        return _joinUsers([currentSpace])[0];
      };

      /**
       * Get the current workspace's ID
       * @return {*}
       */
      workspaceService.getCurrentId = function(){
        var currentID = currentSpace ? currentSpace._id : null;
        //$log.debug("[workspaceService.getCurrentId]", currentID);
        return currentID;
      };

      /**
       * Get the IDs of all data groups in the current workspace as an arry of strings
       * @return {Array<String>}
       */
      workspaceService.getDataGroupIds = function(){
        return _.map(workspaceService.getDataGroups(), "_id")
      };

      /**
       * Get all data groups in this workspace from the workspace defintion
       * @return {*}
       */
      workspaceService.getDataGroups = function(){
        //$log.debug("[workspaceService.getDataGroups] currentSpace.dataGroups:", currentSpace.dataGroups);
        return currentSpace ? currentSpace.dataGroups : null;
      };

      /**
       * Get the Workspace master group ID
       * @return {*}
       */
      workspaceService.getWorkspaceGroupId = function(){
        //$log.debug("[workspaceService.getWorkspaceGroupId] currentSpace.workspaceGroupId:", currentSpace.workspaceGroupId);
        return currentSpace ? currentSpace.workspaceGroupId : null;
      };

      /**
       * Get the last selected workspace from HTML5 storage and attempt to resume state.
       * If the workspace no longer exists or the user no longer has access, attemp to get the
       * list of available workspaces and select the first.
       *
       * @returns {Promise<Workspace>}
       */
      workspaceService.workspaceInit = function(){

        $log.debug("workspaceService.workspaceInit");

        return workspaceService.getAll().then(function (workspaces) {
          return !_.isEmpty(workspaces) ? workspaceService.setCurrent(workspaces[0]) : null;
        });

      };

      /**
       * Set the current workspace, and persist the selection in HTML5 storage.
       * @param space
       * @return {Workspace|Boolean} Workspace definition on success. False on failure.
       */
      workspaceService.setCurrent = function(space){
        $log.debug("Set current workspace", space);
        //Should be true if currentProject == null OR project ids don't match
        if( !currentSpace || (space && currentSpace._id != space._id) || currentSpace.modified != space.modified){
          currentSpace = space;
          $rootScope.workspaceId = space._id;
          return space;
        }
        return false;
      };

      return workspaceService;
    }
  ]);
});
