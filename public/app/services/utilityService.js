define(['app'], function(app){
    "use strict";

  return app.factory('utilityService', function($state, $q, $log, $uibModal) {

    var utilityService = {};

    utilityService.confirmationModal = function(bodyText, confirmText, cancelText, headerText){
      var modalInstance = $uibModal.open({
        size: '',
        backdrop: 'static',
        keyboard: false,
        templateUrl: 'app/services/utility/confirmationModal.html',
        controller: function ($scope, $log, $uibModalInstance) {

          $scope.bodyText = bodyText.split("\n"); //preserve newlines/linebreaks when we display text

          if(cancelText) $scope.cancelText = cancelText;
          if(headerText) $scope.headerText = headerText;
          $scope.confirmText = confirmText;

          $scope.cancelClicked = function () {
            $uibModalInstance.dismiss('cancel');
          };

          $scope.confirmClicked = function () {
            $uibModalInstance.close('confirm');
          };
        }
      });

      return modalInstance;
    };
		
		return utilityService;
	});

});