/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:userGroupService
 * @description
 * userGroupService handles all user group requests and transparently filters objects based on permissions
 */
define(['angular', 'app', 'lodash'], function(angular, app, _){
  "use strict";

  return app.factory('userGroupService', [
    '$q', '$rootScope', '$log', "InfiniteApi", "UserGroupApiService", "aclService", "SessionService", "userService",

    function ($q, $rootScope, $log, InfiniteApi, UserGroupApiService, aclService, SessionService, userService) {
      'use strict';

      //Modification time stamp for watches
      var lastKnownModification = 0;

      function _joinUsersAndUserGroups(userGroups){
        //get user list
        userService.getAll().then(function(users){
          //loop through each user group
          _.each(userGroups, function(userGroup) {
            //make sure the owner display name is correct
            var owner = _.map(_.filter(users, {_id : userGroup.ownerId}), 'displayName')[0];
            userGroup.ownerDisplayName = owner ? owner : userGroup.ownerDisplayName;
            //loop through each user group member list, in which each member can be a user or user group
            _.each(userGroup.members, function(userGroupMember) {
              //make sure the user display name is correct
              var memberName = _.map(_.filter(users, {_id : userGroupMember._id}), 'displayName')[0];
              userGroupMember.displayName = memberName ? memberName : userGroupMember.displayName;
            });
          });
        });

        return userGroups;
      }

      //Method definitions

      /**
       * Add a new user group to an optional parent user group
       * @param userGroupDef
       * @param (parentId)
       * @returns {Promise<*>}
       */
      function add( userGroupDef, parentId){
        return UserGroupApiService.add(userGroupDef.name, userGroupDef.description, userGroupDef.tags, parentId)
          .then(InfiniteApi.resolveWithData)
          .then(function addGroupSuccess(newGroup){
            var userGroups = SessionService.getUserGroups();

            //Manage Cache
            userGroups.push(newGroup);
            SessionService.setUserGroups(userGroups);
            lastKnownModification = new Date().getTime();

            //resolve with new group
            return newGroup;
          });
      }

      /**
       * Get a user group by ID with admin and user membership filtering
       * @param id
       * @param (forceReload)  If true, the local cache will be ignored and a cache refresh will be forced
       * @returns {Promise<*>}
       */
      function get(id, forceReload){

        //Calling getAll will use cache if available
        return getAll(forceReload).then(function(groups){
          var group = _.find(groups, {_id:id});
          if(!group || !aclService.hasPermissionToGroup(group)){
            $log.error("[userGroupService.get] User group not found or no access. Requested ID: " + id, group);
            return $q.reject("User group not found or no access.");
          }
          return group;
        });
      }

      /**
       * Get all user groups
       * @param (forceReload)
       * @returns {Promise<*>}
       */
      function getAll(forceReload){
        if( forceReload !== true && lastKnownModification != 0 ){
          //$log.debug("[userGroupService.getAll] Using cache");
          return $q.when(_joinUsersAndUserGroups(SessionService.getUserGroups()));
        }

        return UserGroupApiService.getAll()
          .then(InfiniteApi.resolveWithDataOrArray)
          .then(function(groups){
            SessionService.setUserGroups(groups);
            lastKnownModification = new Date().getTime();
            return _joinUsersAndUserGroups(SessionService.getUserGroups());
          });
      }

      /**
       * Add members or user groups to a user group. Optionally skip invitation for admins.
       *
       * This requires some additional handling to figure out who succeeded and who
       * failed for bulk operations
       *
       * @param groupId
       * @param personIds
       * @returns {Promise<*>}
       */
      function inviteMembers( groupId, personIds ){
        $log.debug("[userGroupService->inviteMembers] Add " + personIds + " to group " + groupId);
        return UserGroupApiService.memberInvite( groupId, personIds)
          .then(InfiniteApi.resolveWithData)
          .then(function inviteSuccess(updatedGroup){
            var userGroups = SessionService.getUserGroups();
            var userGroupIndex = _.findIndex( userGroups, {_id: groupId});
            userGroups[userGroupIndex] = updatedGroup;
            SessionService.setUserGroups(userGroups);
            lastKnownModification = new Date().getTime();
            return updatedGroup;
          })
          .catch(function(err){
            return $q.reject("Add user to group failed: " + err);
          });
      }

      /**
       * Remove a user group
       * @param id
       * @returns {Promise<*>}
       */
      function remove(id){
        return UserGroupApiService.remove(id).then(
          function removeSuccess(apiResponse){
            var userGroups = SessionService.getUserGroups();

            //If deleted is in response message - this was the second call and the group is removed
            if(apiResponse.response.message.indexOf("deleted") > -1){
              userGroups = _.reject(userGroups, {_id:id});

            //Otherwise it's the first call, and the community is just disabled
            } else {
              _.each(userGroups, function(userGroup){
                if( userGroup._id == id) userGroup.communityStatus = "disabled";
              });
            }

            SessionService.setUserGroups(userGroups);
            lastKnownModification = new Date().getTime();

            return true;
          }
        )
      }

      /**
       * Update a user group. userGroupObject will need an ID
       * @param userGroupObject
       * @returns {Promise<*>}
       */
      function update(userGroupObject){
        userGroupObject.communityAttributes = communityAttributes; // force apply group community attributes
        return UserGroupApiService.update(userGroupObject)
          .then(InfiniteApi.resolveWithData)
          .then(function(updatedGroup){
            var userGroups = SessionService.getUserGroups();
            var userGroupIndex = _.findIndex( userGroups, {_id: userGroupObject._id});
            userGroups[userGroupIndex] = updatedGroup;
            SessionService.setUserGroups(userGroups);
            lastKnownModification = new Date().getTime();
            return updatedGroup;
          });
      }

      /**
       * Update a person or user group's status in a user group
       * @param groupId
       * @param personId
       * @param memberStatus "active", "disabled", "pending", "remove"
       * @returns {*}
       */
      function updateMemberStatus(groupId, personId, memberStatus){
        return UserGroupApiService.updateMemberStatus(groupId, personId, memberStatus)
          .then(InfiniteApi.resolveWithData);
      }

      /**
       * Update a person or user group's type in a user group
       * @param groupId
       * @param personId
       * @param memberType "owner"| "content_publisher" | "moderator" | "member"
       * @returns {Promise<*>}
       */
      function updateMemberType(groupId, personId, memberType){
        return UserGroupApiService.updateMemberType(groupId, personId, memberType)
          .then(InfiniteApi.resolveWithData);
      }

      /**
       * Remove users from a group, used updated group definition to update cache
       * @param groupId
       * @param memberIds
       * @return {*}
       */
      function removeMembers(groupId, memberIds){
        $log.debug("[userGroupService->removeMembers] Remove " + memberIds + " from group " + groupId);
        return UserGroupApiService.removeMembers(groupId, memberIds)
          .then(InfiniteApi.resolveWithData)
          .then(function removeMembersSuccess(updatedGroup){
            var userGroups = SessionService.getUserGroups();
            var userGroupIndex = _.findIndex( userGroups, {_id: groupId});
            userGroups[userGroupIndex] = updatedGroup;
            SessionService.setUserGroups(userGroups);
            lastKnownModification = new Date().getTime();
            return updatedGroup;
          })
          .catch(function(err){
            return $q.reject("Remove users from group failed: " + err);
          });
      }

      //Public methods
      return {

        //Cache watch integer
        lastKnownModification: lastKnownModification,

        //API Methods
        add: add,
        get: get,
        getAll: getAll,
        inviteMembers: inviteMembers,
        remove: remove,
        update: update,
        updateMemberStatus: updateMemberStatus,
        updateMemberType: updateMemberType,
        removeMembers: removeMembers,

        //Convenience methods

        /**
         * Forcefully removes a user group as quickly as possible by preforming both remove actions back to back
         * @param groupId
         * @returns {*|Promise.<*>}
         */
        forceRemove: remove
      };

    }
  ]);
});