/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc service
 * @name app.service:dataGroupService
 * @description
 * dataGroupService handles all data group requests and transparently filters objects based on permissions
 */
define(['angular', 'app', 'lodash'], function(angular, app, _){
  "use strict";

  return app.factory('dataGroupService', [
    '$q', '$rootScope', '$log', "InfiniteApi", "DataGroupApiService", "aclService", "SessionService", "userService", "userGroupService",

    function($q, $rootScope, $log, InfiniteApi, DataGroupApiService, aclService, SessionService, userService, userGroupService) {
      'use strict';

      //Modification time stamp for watches
      var lastKnownModification = 0;

      function _joinUsersAndUserGroups(dataGroups){

        //get user and user group list
        $q.all({
          userGroups: userGroupService.getAll(),
          users: userService.getAll()
        })
        .then(function(results){
          //loop through each data group
          _.each(dataGroups, function(dataGroup) {
            //make sure the owner display name is correct
            dataGroup.ownerDisplayName = _.map(_.filter(results.users, {_id : dataGroup.ownerId}), 'displayName')[0];
            //loop through member list, in which each member can be a user or user group
            _.each(dataGroup.members, function(dataGroupMember) {
              //make sure the user group name is correct
              if(dataGroupMember.type === "user_group"){
                var groupName = _.map(_.filter(results.userGroups, {_id : dataGroupMember._id}), 'name')[0];
                dataGroupMember.displayName = groupName ? groupName : dataGroupMember.displayName;
              } else { //make sure the user display name is correct
                var memberName = _.map(_.filter(results.users, {_id : dataGroupMember._id}), 'displayName')[0];
                dataGroupMember.displayName = memberName ? memberName : dataGroupMember.displayName;
              }
            });
          });
        });
        return dataGroups;
      }

      //Method definitions

      /**
       * Add a new data group to an optional parent data group
       * @param dataGroupDef
       * @param (parentId)
       * @returns {Promise<*>}
       */
      function add( dataGroupDef, parentId){
        return DataGroupApiService.add(dataGroupDef.name, dataGroupDef.description, dataGroupDef.tags, parentId)
          .then(InfiniteApi.resolveWithData)
          .then(function addGroupSuccess(newGroup){
            var dataGroups = SessionService.getDataGroups();

            //Manage Cache
            dataGroups.push(newGroup);
            SessionService.setDataGroups(dataGroups);
            lastKnownModification = new Date().getTime();

            //resolve with new group
            return newGroup;
          });
      }

      /**
       * Get a data group by ID with admin and user membership filtering
       * @param id
       * @param (forceReload)  If true, the local cache will be ignored and a cache refresh will be forced
       * @returns {Promise<*>}
       */
      function get(id, forceReload){

        //Calling getAll will use cache if available
        return getAll(forceReload).then(function(groups){
          var group = _.find(groups, {_id:id});
          if(!group || !aclService.hasPermissionToGroup(group)){
            $log.error("[dataGroupService.get] Data group not found or no access. Requested ID: " + id, group);
            return $q.reject("Data group not found or no access.");
          }
          return group;
        });
      }

      /**
       * Get all data groups
       * @param (forceReload)
       * @returns {Promise<*>}
       */
      function getAll(forceReload){
        if( forceReload !== true && lastKnownModification != 0 ){
          //$log.debug("[dataGroupService.getAll] Using cache");
          return $q.when(_joinUsersAndUserGroups(SessionService.getDataGroups()));
        }

        return DataGroupApiService.getAll()
          .then(InfiniteApi.resolveWithDataOrArray)
          .then(function(groups){
            groups = _.reject(groups, {isSystemCommunity: true});
            SessionService.setDataGroups($rootScope.isAdmin ? groups : aclService.filterDataGroupsByMembership(groups));
            lastKnownModification = new Date().getTime();
            return _joinUsersAndUserGroups(SessionService.getDataGroups());
          });
      }

      /**
       * Add members or user groups to a data group. Optionally skip invitation for admins.
       *
       * This requires some additional handling to figure out who succeeded and who
       * failed for bulk operations
       *
       * @param groupId
       * @param personIdsOrUserGroupIds
       * @param (skipInvite) Admins only, skips invitation and adds users and groups immediately
       * @returns {Promise<*>}
       */
      function inviteMembers( groupId, personIdsOrUserGroupIds, skipInvite){
        $log.debug("[dataGroupService->inviteMembers] Add " + personIdsOrUserGroupIds + " to group " + groupId);
        return DataGroupApiService.memberInvite( groupId, personIdsOrUserGroupIds, skipInvite)
          .then(InfiniteApi.resolveWithData)
          .then(function inviteSuccess(updatedGroup){
            var dataGroups = SessionService.getDataGroups();
            var dataGroupIndex = _.findIndex( dataGroups, {_id: groupId});
            dataGroups[dataGroupIndex] = updatedGroup;
            SessionService.setDataGroups(dataGroups);
            lastKnownModification = new Date().getTime();
            return updatedGroup;
          })
          .catch(function(err){
            return $q.reject("Add member to group failed: " + err);
          });
      }

      /**
       * Remove a data group
       * @param id
       * @returns {Promise<*>}
       */
      function remove(id){
        return DataGroupApiService.remove(id).then(
          function removeSuccess(apiResponse){
            var dataGroups = SessionService.getDataGroups();

            //If deleted is in response message - this was the second call and the group is removed
            if(apiResponse.response.message.indexOf("deleted") > -1){
              dataGroups = _.reject(dataGroups, {_id:id});

            //Otherwise it's the first call, and the community is just disabled
            } else {
              _.each(dataGroups, function(dataGroup){
                if( dataGroup._id == id) dataGroup.communityStatus = "disabled";
              });
            }

            SessionService.setDataGroups(dataGroups);
            lastKnownModification = new Date().getTime();

            return true;
          }
        );
      }

      /**
       * Update a data group. dataGroupObject will need an ID
       * @param dataGroupObject
       * @returns {Promise<*>}
       */
      function update(dataGroupObject){
        dataGroupObject.communityAttributes = communityAttributes; // force apply group community attributes
        return DataGroupApiService.update(dataGroupObject)
          .then(InfiniteApi.resolveWithData)
          .then(function(updatedGroup){
            var dataGroups = SessionService.getDataGroups();
            var dataGroupIndex = _.findIndex( dataGroups, {_id: dataGroupObject._id});
            dataGroups[dataGroupIndex] = updatedGroup;
            SessionService.setDataGroups(dataGroups);
            lastKnownModification = new Date().getTime();
            return updatedGroup;
          });
      }

      /**
       * Update a person or user groups status in a data group
       * @param groupId
       * @param personIdsOrUserGroupIds
       * @param memberStatus "active", "disabled", "pending", "remove"
       * @returns {*}
       */
      function updateMemberStatus(groupId, personIdsOrUserGroupIds, memberStatus){
        return DataGroupApiService.updateMemberStatus(groupId, personIdsOrUserGroupIds, memberStatus)
          .then(InfiniteApi.resolveWithData);
      }

      /**
       * Update a person or user groups type in a data group
       * @param groupId
       * @param personIdOrUserGroupId
       * @param memberType "owner"| "content_publisher" | "moderator" | "member"
       * @returns {Promise<*>}
       */
      function updateMemberType(groupId, personIdOrUserGroupId, memberType){
        return DataGroupApiService.updateMemberType(groupId, personIdOrUserGroupId, memberType)
          .then(InfiniteApi.resolveWithData);
      }

      /**
       * Remove users from a group, used updated group definition to update cache
       * @param groupId
       * @param memberIds
       * @return {*}
       */
      function removeMembers(groupId, memberIds){
        $log.debug("[dataGroupService->removeMembers] Remove " + memberIds + " from group " + groupId);
        return DataGroupApiService.removeMembers(groupId, memberIds)
          .then(InfiniteApi.resolveWithData)
          .then(function removeMembersSuccess(updatedGroup){
            var dataGroups = SessionService.getDataGroups();
            var dataGroupIndex = _.findIndex( dataGroups, {_id: groupId});
            dataGroups[dataGroupIndex] = updatedGroup;
            SessionService.setDataGroups(dataGroups);
            lastKnownModification = new Date().getTime();
            return updatedGroup;
          })
          .catch(function(err){
            return $q.reject("Remove users from group failed: " + err);
          });
      }

      //Public methods
      return {

        //Cache watch integer
        lastKnownModification: lastKnownModification,

        //API Methods
        add: add,
        get: get,
        getAll: getAll,
        inviteMembers: inviteMembers,
        remove: remove,
        update: update,
        updateMemberStatus: updateMemberStatus,
        updateMemberType: updateMemberType,
        removeMembers: removeMembers,

        //Convenience methods

        /**
         * Forcefully removes a data group as quickly as possible by preforming both remove actions back to back
         * @param dataGroupId
         * @returns {*|Promise.<*>}
         */
        forceRemove: remove,

        cache_updateGroupMembers: function(groupId, members){
          var dataGroups = SessionService.getDataGroups();
          var groupIndex = _.findIndex(dataGroups, {_id: groupId});
          dataGroups[groupIndex].members = members;
          SessionService.setDataGroups(dataGroups);
          lastKnownModification = new Date().getTime();
        }

      };

    }
  ]);
});