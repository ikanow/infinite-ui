/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['app', 'json3'], function(app, JSON){
  "use strict";

  return app.factory('communityService', function($http, $q) {

    var communityService = {};

    // ********************************************************************* //
    //
    // Main Service Methods
    //
    // ********************************************************************* //

    // GET COMMUNITIES:
    // BASEURL/social/community/get/4e3706c48d26852237079004
    // BASEURL/social/community/getprivate
    // BASEURL/social/community/getpublic
    // BASEURL/social/community/getall

    /* returns array of community objects */
    communityService.getCommunityByFilter = function(filter, type){ // filter = all || private || public
      var q = $q.defer();
      $http.get(createURL(type) + "get" + filter).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data.data);
        } else {
          q.reject(null);
        }
      });
      return q.promise;
    };

    communityService.getCommunity = function(id){
      var q = $q.defer();
      $http.get(createURL() + "get/" + id).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data.data);
        } else {
          q.reject(null);
        }
      });
      return q.promise;
    };

    communityService.addCommunity = function(dg){
      var q = $q.defer();
      $http.get(createURL(dg.type) + "add/" + dg.name + "/" + dg.description + "/" + dg.tags ).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data.data);
        } else {
          q.reject(null);
        }
      });
      return q.promise;
    };

    // JOIN:
    // Current user to a group:
    // BASEURL/social/community/member/join/{communityid} //54b602c0e4b0e57ecbde6762
    // communityService.joinCommunity = function(id){
    //   var q = $q.defer();
    //   $http.get(createURL() + "member/join/" + id).then(function (data) {
    //     if (data.data.response.success) {
    //       q.resolve(data.data.data);
    //     } else {
    //       q.reject(null);
    //     }
    //   });
    //   return q.promise;
    // };

    communityService.joinCommunity = function(dgID, userIDs){
      var q = $q.defer();
      var url = createURL() + "member/invite/" + dgID + '/$users?skipinvitation=true&infinite_api_key=key123456&users=' + userIDs;
      $http.get(url).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data);
        } else {
          q.reject(null);
        }
      });
      return q.promise;
    };

    // UPDATE MEMBER STATUS:
    // BASEURL/social/community/member/update/status/{communityid}/{personid}/{userstatus}
    // available statuses: "active", "disabled", "pending", "remove", the last of these removes the user from the community altogether

    // http://infinite.ikanow.com/api/social/community/member/update/status/50a7a290fbf01a3c159895c4/$status/remove?status=5233614ee4b02e1667b3b822,506c8616fbf042893dd6b3f3,50bcd83cfbf0fd0b27875a82,50bcd83cfbf0fd0b27875aaa
    communityService.updateMemberStatus = function(dgID, uID, status){
      var q = $q.defer();
      var url = createURL() + "member/update/status/" + dgID + (status == 'remove' ? "/$status/remove?status=" + uID : "/" + uID + "/" + status);
      $http.get(url).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data);
        } else {
          q.reject(null);
        }
      });
      return q.promise;
    };

    // UPDATE MEMBER TYPE:
    // BASEURL/social/community/member/update/status/{communityid}/{personid}/{usertype}
    // available types: "owner", "content_publisher", "moderator", "member" - note "content_publisher"s are members who can also publish sources
    communityService.updateMemberType = function(id, uID, type){
      var q = $q.defer();
      $http.get(createURL() + "member/update/type/" + id + "/" + uID + "/" + type).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data.data);
        } else {
          q.reject(null);
        }
      });
      return q.promise;
    };

    // REMOVE COMMUNITY:
    // BASEURL/social/community/remove/{id} // 2 times - first one "hides", second one "actually deletes"
    communityService.removeCommunity = function(id){
      var q = $q.defer();
      $http.get(createURL() + "remove/" + id).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data.data);
        } else {
          q.reject(null);
        }
      });
      return q.promise;
    };

    // COMMUNITY UPDATE:
    // BASEURL/social/community/update/{communityid}
    communityService.updateCommunity = function(dg){
      var q = $q.defer();
      $http.post(createURL() + "update/" + dg._id, dg).then(function (data) {
        if (data.data.response.success) {
          q.resolve(data.data.data);
        } else {
          q.reject(JSON.stringify(data.data.response.message));
        }
      });
      return q.promise;
    };

    // ********************************************************************* //
    // Community URLs
    // ********************************************************************* //
    function createURL(type){
      var url = appConfig.apiBaseDomain + (type ? "social/group/" + type + "/" : "social/community/");
      return url;
    }


    return communityService;

  })

});