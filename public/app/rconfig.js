var require = {
    baseUrl: "app/",
    waitSeconds: 0,
    paths: {

        // app js file includes
        'appConfig': '../app.config',
        'modules-includes': 'includes',

        // Static Libs

        'nv-d3' : '../plugin/nvd3/nv.d3',//'../lib/nvd3-1.8.1-2015.03.10/nv.d3',

        // Packages

        'angular': '../plugin/angular/angular',
        'angular-animate': '../plugin/angular-animate/angular-animate',
        'angular-bootstrap': '../plugin/angular-bootstrap/ui-bootstrap-tpls',
        'angular-busy': '../plugin/angular-busy/angular-busy',
        'angular-cookies': '../plugin/angular-cookies/angular-cookies',
        'angular-couch-potato': '../plugin/angular-couch-potato/angular-couch-potato',
        'angular-google-maps': '../plugin/angular-google-maps/angular-google-maps',
        'angular-json-human': "../plugin/angular-json-human/angular-json-human",
        'angular-resource': '../plugin/angular-resource/angular-resource',
        'angular-sanitize': '../plugin/angular-sanitize/angular-sanitize',
        'angular-ui-codemirror': '../plugin/angular-ui-codemirror/ui-codemirror',
        'angular-ui-router': '../plugin/angular-ui-router/angular-ui-router',
        'angular-websocket': "../plugin/angular-websocket/angular-websocket.min",

        'bootstrap': '../plugin/bootstrap/js/bootstrap',
        'bootstrap-colorpicker': '../plugin/bootstrap-colorpicker/bootstrap-colorpicker',
        'bootstrap-duallistbox': '../plugin/bootstrap-duallistbox/jquery.bootstrap-duallistbox',
        'bootstrap-progressbar': '../plugin/bootstrap-progressbar/bootstrap-progressbar',
        'bootstrap-tagsinput': '../plugin/bootstrap-tagsinput/bootstrap-tagsinput',
        'bootstrap-validator': '../plugin/bootstrapvalidator/bootstrapValidator',

        "canvg": "../plugin/canvg/canvg",
        'chartjs': '../plugin/chartjs/Chart',
        'clockpicker': '../plugin/clockpicker/jquery-clockpicker',
        'codemirror': '../plugin/codemirror/codemirror',

        'datatables': '../plugin/datatables/js/jquery.dataTables',
        'datatables-colvis': '../plugin/datatables-colvis/dataTables.colVis',
        'datatables-bootstrap': '../plugin/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap',
        'datatables-responsive': '../plugin/datatables-responsive/js/datatables.responsive',
        'datatables-tools': '../plugin/datatables-tabletools/dataTables.tableTools',
        'domReady': '../plugin/requirejs-domready/domReady',
        'dropzone': '../plugin/dropzone/dropzone',

        'fastclick': '../plugin/fastclick/fastclick',
        'fuelux-wizard': '../plugin/fuelux/wizard',

        "hashes": "../plugin/jshashes/hashes",
        "html2canvas": "../plugin/html2canvas/html2canvas",

        'infinite-api': '../plugin/infinite-js-sdk/infinite-js-sdk',
        "ionslider": "../plugin/ion.rangeSlider/ion.rangeSlider",

        'jquery': "../plugin/jquery/jquery",
        'jquery-color': '../plugin/jquery-color/jquery.color',
        'easy-pie': '../plugin/jquery.easy-pie-chart/dist/jquery.easypiechart',
        'jquery-form': '../plugin/jquery-form/jquery.form',
        'jquery-jvectormap': '../lib/vectormap/jquery-jvectormap-1.2.2.min',
        'jquery-jvectormap-world-mill-en': '../lib/vectormap/jquery-jvectormap-world-mill-en',
        'jquery-knob': '../plugin/jquery-knob/jquery.knob',
        'jquery-maskedinput': '../plugin/jquery-maskedinput/jquery.maskedinput',
        'jquery-nestable': '../plugin/jquery-nestable/jquery.nestable',
        'jquery-ui': '../plugin/jquery-ui/jquery-ui',
        'jquery-validation': '../plugin/jquery-validation/jquery.validate',
        'jspdf': '../plugin/jspdf/jspdf.debug',
        'json3': "../plugin/json3/json3",

        'lodash': '../plugin/lodash/lodash',

        'moment': '../plugin/moment/moment-with-locales',
        'moment-timezone': '../plugin/moment-timezone/moment-timezone',
        "morris": "../plugin/morris.js/morris",

        'ng-tags-input': "../plugin/ng-tags-input/ng-tags-input",

        "pace": "../plugin/pace/pace",

        "raphael": "../plugin/raphael/raphael",
        "rgbcolor": "../plugin/rgb-color/rgb-color",

        'select2': '../plugin/select2/select2',
        'sparkline': '../plugin/relayfoods-jquery.sparkline/jquery.sparkline',
        'sprintf': "../plugin/sprintf/sprintf",
        'superbox': '../plugin/superbox/superbox.min',

        'x-editable': '../plugin/x-editable/js/bootstrap-editable',


        // SmartAdmin artifacts - should be reviewed if needed at all.
        'flot': '../lib/flot/jquery.flot.cust.min',
        'flot-fillbetween': '../lib/flot/jquery.flot.fillbetween.min',
        'flot-orderBar': '../lib/flot/jquery.flot.orderBar.min',
        'flot-pie': '../lib/flot/jquery.flot.pie.min',
        'flot-resize': '../lib/flot/jquery.flot.resize.min',
        'flot-time': '../lib/flot/jquery.flot.time.min',
        'flot-tooltip': '../lib/flot/jquery.flot.tooltip.min',
        'fullcalendar': '../smartadmin-plugin/fullcalendar/jquery.fullcalendar.min',
        'notification': '../smartadmin-plugin/notification/SmartNotification.min',
        'smartwidgets': '../smartadmin-plugin/smartwidgets/jarvis.widget.min'
    },
    shim: {
        'angular': {'exports': 'angular', deps: ['jquery']},
        'angular-animate': { deps: ['angular'] },
        'angular-bootstrap': { deps: ['angular'] },
        'angular-busy': { deps: ['angular'] },
        'angular-cookies': { deps: ['angular'] },
        'angular-couch-potato': { deps: ['angular'] },
        'angular-google-maps': { deps: ['angular'] },
        'angular-json-human': { deps: ['angular','lodash'] },
        'angular-resource': { deps: ['angular'] },
        'angular-sanitize': { deps: ['angular'] },
        'angular-ui-codemirror': {
            deps: ['angular', 'codemirror' ],
            // after deps are loaded, codemirror is not a global, but angular-ui-codemirror expects
            // to see a global, so this function fixes that requirement.
            init: function(angular, codemirror) {
                window.CodeMirror = codemirror;
            }
        },
        'angular-ui-router': { deps: ['angular'] },
        'angular-websocket': { deps: ['angular'] },
        'anim-in-out': { deps: ['angular-animate'] },

        'bootstrap':{deps: ['jquery']},
        'bootstrap-colorpicker':{deps: ['jquery']},
        'bootstrap-daterangepicker': { deps: ['jquery', 'moment']},
        'bootstrap-duallistbox':{deps: ['jquery']},
        'bootstrap-progressbar': { deps: ['bootstrap']},
        'bootstrap-tagsinput':{deps: ['jquery']},
        'bootstrap-validator':{deps: ['jquery']},

        'clockpicker':{deps: ['jquery']},

        'datatables':{deps: ['jquery']},
        'datatables-bootstrap':{deps: ['datatables','datatables-tools','datatables-colvis']},
        'datatables-colvis':{deps: ['datatables']},
        'datatables-responsive': {deps: ['datatables']},
        'datatables-tools':{deps: ['datatables']},
        'dropzone': { deps: ['jquery']},

        'easy-pie': { deps: ['jquery']},

        'flot': { deps: ['jquery']},
        'flot-fillbetween': { deps: ['flot']},
        'flot-orderBar': { deps: ['flot']},
        'flot-pie': { deps: ['flot']},
        'flot-resize': { deps: ['flot']},
        'flot-time': { deps: ['flot']},
        'flot-tooltip': { deps: ['flot']},
        'fuelux-wizard':{deps: ['jquery']},

        'infinite-api': {deps: ['angular', 'sprintf', 'lodash', 'hashes', 'moment']},
        'ionslider':{deps: ['jquery']},

        'jquery-color':{deps: ['jquery']},
        'jquery-form':{deps: ['jquery']},
        'jquery-jvectormap': { deps: ['jquery']},
        'jquery-jvectormap-world-mill-en': { deps: ['jquery', 'jquery-jvectormap']},
        'jquery-knob':{deps: ['jquery']},
        'jquery-maskedinput':{deps: ['jquery']},
        'jquery-nestable': { deps: ['jquery']},
        'jquery-ui': { deps: ['jquery']},
        'jquery-validation':{deps: ['jquery']},

        'lodash' : { exports : "_ " },

        'modules-includes': { deps: ['angular']},
        'moment': { exports: 'moment'},
        'moment-helper': { deps: ['moment-timezone-data']},
        'moment-timezone': { deps: ['moment']},
        'moment-timezone-data': { deps: ['moment']},
        'morris': {deps: ['raphael']},

        'ng-tags-input': { deps: ['angular'] },
        'notification': { deps: ['jquery']},

        'select2': { deps: ['jquery']},
        'smartwidgets': { deps: ['jquery-ui']},
        'socket.io': { deps: ['angular'] },
        'sparkline': { deps: ['jquery']},
        'superbox': { deps: ['jquery']},

        'x-editable':{deps: ['jquery']}
    },
    priority: [
        'jquery',
        'bootstrap',
        'angular'
    ]
};

//global group/project settings/parameters
var communityAttributes = {
    "isPublic": { "type":"boolean", "value":"false" },
    "registrationRequiresApproval": { "type":"boolean", "value":"false" },
    "usersCanCreateSubCommunities": { "type":"boolean", "value":"true" },
    "usersCanSelfRegister": { "type":"boolean", "value":"true" },
    "publishMemberOverride": { "type":"boolean", "value":"true" }
};
