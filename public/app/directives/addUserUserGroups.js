/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['app', 'lodash'], function(app, _) {
  "use strict";

  return app.directive('addUserUserGroups', function($rootScope, $state, $log, SessionService, userGroupService) {
    return {
      restrict: "EA",
      replace: true,
      templateUrl: "app/directives/addUserUserGroups.tpl.html",
      scope: false,
      link: function($scope, element, attr) {

        $scope.updateAddedUsers = function(){
          $scope.allUsersAdded = _.filter($scope.allUsers, {addedToUsers:true}).length;
          $scope.usersAdded =  _.filter( _.filter($scope.allUsers, {addedToUsers:true}), function(i){return i.email}).length;
          $scope.userGroupsAdded =  _.reject( _.filter($scope.allUsers, {addedToUsers:true}), 'email').length;
          updateAvailUsers();
        };

        var isNewProject = $scope.$eval(attr.newProject);

        $scope.showUser = function (user) {
          if (isNewProject) {
            return user._id !== $scope.currUserId;
          } else {
            return user._id !== $scope.projectDataGroup.ownerId;
          }
        };

        function updateAvailUsers(){
          //if the user creating the project is in the datagroup they added, OR if the project owner  is in the datagroup they added, adjust the available user count
          if ( _.find($scope.allUsers, { '_id':SessionService.getUserId() }) || ($scope.currentProject && $scope.currentProject.owner && _.find($scope.allUsers, { '_id':$scope.currentProject.owner._id })) ) {
            $scope.allUsersAvail = $scope.allUsers.length - $scope.allUsersAdded - 1;
            $scope.usersAvail =  _.filter($scope.allUsers, function(i){return i.email}).length - $scope.usersAdded - 1;
          } else {
            $scope.allUsersAvail = $scope.allUsers.length - $scope.allUsersAdded;
            $scope.usersAvail =  _.filter($scope.allUsers, function(i){return i.email}).length - $scope.usersAdded;
          }
          $scope.userGroupsAvail =  _.reject($scope.allUsers, 'email').length - $scope.userGroupsAdded;
        }

        function updateUsers(){
          var currentSelectedIDs = _.map(_.filter($scope.allUsers, {addedToUsers: true}), '_id');
          currentSelectedIDs = currentSelectedIDs.length > 0 ? currentSelectedIDs : _.map($scope.projectDataGroupMembersOrig, '_id');

          $scope.users = _.uniq(_.flatten(_.map(_.filter($scope.dataGroups, {added: true}), 'members')), '_id');
          //$scope.allUsers = _.reject($scope.users, {userType: 'owner'});
          //NGU-542 don't hide the owner from the available user list
          $scope.allUsers = $scope.users;

          //add [members] of the usergroup to each allUser item that is a user group, so we can display them
          userGroupService.getAll().then(function(userGroups) {
            _.each($scope.allUsers, function(item) {
              if(item.type === "user_group"){
                var members;
                members = _.map(_.filter(userGroups, {_id : item._id}), 'members')[0];
                item.members = members ? members : [];
              }
            });
          });

          _.each(currentSelectedIDs, function(i){
            if ( _.find($scope.allUsers, {'_id':i}) ) _.find($scope.allUsers, {'_id':i}).addedToUsers = true;
          });

          $scope.updateAddedUsers();
        }

        // For creating:
        $scope.$watch('stepTwoValid', function(a,b){
          updateUsers();
        });

        // For editing
        $scope.$watch('dataGroups', function(a,b){
          updateUsers();
        });
      }
    };
  });
});