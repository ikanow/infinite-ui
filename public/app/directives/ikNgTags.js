/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['app', 'lodash'], function(app, _) {
  "use strict";

  return app.directive('ikTagsInput', function($log, $timeout) {
    return {
      restrict: "E",
      templateUrl: "app/directives/ikNgTags.tpl.html",
      scope: {
        tagsAsArray: "=",
        tagsAsString: "=?",
        readonly: "=?",
        autocompleteSource: "=?"
      },
      controller: function ($scope) {
        $scope.tags = [];
        $scope.tagsAsString = {};

        if (angular.isDefined($scope.autocompleteSource)) {
          $scope.showAutoComplete = true;
        }

        if ($scope.tagsAsArray && $scope.tagsAsArray.length > 0) {
          if (angular.isDefined($scope.tagsAsString)) {
            $scope.tagsAsString.val = $scope.tagsAsArray.toString();
          }

          _.map($scope.tagsAsArray, function (tag) {
            $scope.tags.push({text: tag});
          });
        }

        $scope.updateTags = function () {
          $scope.tagsAsArray = _.map($scope.tags, 'text');
          if (angular.isDefined($scope.tagsAsString)) {
            $scope.tagsAsString.val = $scope.tagsAsArray.toString();
          }

          // Call valid() to update the error messages
          $timeout(function () {
            $('#tagsInputField').valid();
          }, 50)

        };
      }
    };
  });
});