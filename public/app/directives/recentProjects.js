/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc directive
 * @name app.directive:recentProjects
 * @description
 * Recent projects drop down
 */
define(['app', 'lodash'], function(app, _) {
  "use strict";

  return app.directive('recentProjects', function($rootScope, $uibModal, $state, $log, workspaceService) {
    return {
      restrict: "EA",
      replace: true,
      templateUrl: "app/directives/ikanow-recent-projects.tpl.html",
      scope: true,
      link: function(scope, element) {

        scope.clearProjects = function() {
          scope.projects = [];
        };

        scope.$watch(
          function() { return $rootScope.workspaceId},
          function (oldVal, newVal) {
            if (oldVal !== newVal) {
              scope.currentProject = workspaceService.getCurrent();
            }
          }
        );

        //Bind the projects list to the workspaceService cached list
        // using the lastKnownModification timestamp to determine updates
        scope.$watch(
          function() { return workspaceService.lastKnownModification },
          function() {
            scope.projects = workspaceService.spaces;
            scope.currentProject = workspaceService.getCurrent();

            // If user is admin, remove other user's personal workspaces
            if ($rootScope.isAdmin) {
              scope.projects = _.reject(scope.projects, function(project) {
                return project.isManaged;
              })
            }
          }
        );

        scope.openProject = function(project) {
          //True if a change occurred.
          if(workspaceService.setCurrent(project)) {
            $state.reload().then(function(){
              $state.go($rootScope.defaultLocation, { urlWorkspaceId: project._id });
            });
          }
        };

        scope.projectModal = function () {
          var modalInstance = $uibModal.open({
            templateUrl: 'app/project/views/project-create.html',
            backdrop: 'static',
            size: 'lg',
            controller: 'ProjectCreateWizardCtrl'
          });

          modalInstance.result.then(function () {
            $log.info('Modal closed at: ' + new Date());
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });

        };

        scope.currentProject = undefined;
        scope.currentProject = workspaceService.getCurrent();
        scope.currentProject = scope.currentProject ? scope.currentProject : { title: "Select a Project"};
      }
    };
  });
});