define(['search/module', 'jquery'], function(module){
  "use strict";

  /**
   * @ngdoc directive
   * @name app.search.directive:searchFilters
   * @description
   * Search query options form
   */
  module.registerDirective("searchFilters", function($log, $compile, SuggestService){
    return {
      restrict: 'EA',
      templateUrl: 'app/search/views/search-filters.html',
      replace: true,
      scope: {
        filters: "=",
        submitFunction: "="
      },
      link: function(scope, element, attrs){

        //console.log("Filters scope at init", scope);
        scope.verbSuggestions = true; //true indicated loading
        scope.tagSuggestions = [];
        scope.tagsInputSelections = []; //ngTagsInput model. Used for default value.

        //Date picker config
        scope.datePickerOpen = false;
        scope.dateOptions = {
          formatYear: 'yy',
          startingDay: 1
        };

        //Date picker click handler
        scope.openDatePicker = function($event){
          $event.preventDefault();
          $event.stopPropagation();
          scope.datePickerOpen = true;
        };

        scope.runSubmitFunction = function(){
          scope.submitFunction();
        };

        /**
         * Parse a suggested verb into a usable value for query
         * @param {String} verb suggestion
         * @return {String} verb value
         */
        scope.verbValue = function( verb ){

          //Ensure verb is a string.
          if( !angular.isString(verb) ) return "";

          //Use only the verb from the suggestion.
          var ind = verb.indexOf("(");
          return verb.substring( 0, ind > -1 ? ind : verb.length).trim();
        };

        /**
         * Check the current verb list and invert it's selected state.
         * @param {String} verb
         */
        scope.toggleVerb = function(verb){
          verb = scope.verbValue(verb); //cleanup the display to a real value
          var currentIndex = scope.filters.verbs.indexOf(verb);
          if (currentIndex > -1) {
            scope.filters.verbs.splice(currentIndex, 1);
          } else {
            scope.filters.verbs.push(verb);
          }
        };

        /**
         * Simple test to see if a verb is in the list of selected verbs
         * @param {String} verb
         * @returns {boolean}
         */
        scope.verbIsSelected = function(verb){
          return ( scope.filters.verbs.indexOf(verb) > -1 );
        };

        /**
         * TODO Get tag suggestions
         * @param $query
         * @returns {*} Promise, resolves to array of strings new value
         */
        scope.autocompleteTags = function($query){
          return $.when(['reports','isight']);
        };

        /**
         * Initialize Advanced Search Suggestions
         * TODO: Cache suggestions
         */
        SuggestService.suggestVerbAssociations(null, null, null).then(
          //success
          function(suggestions){
            //console.log("Fetched suggestions: ", suggestions);
            scope.verbSuggestions  = Object.keys(suggestions).length == 0 ? false : suggestions;
          },
          //Error
          function(err){
            scope.verbSuggestions = null;
            $log.error("[searchFilters] Could not load verb suggestions: ", err);
          }
        );

      }
    };
  });

});
