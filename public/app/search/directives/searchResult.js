define(['search/module', 'jquery'], function(module, $){
    "use strict";

    /**
     * @ngdoc directive
     * @name app.search.directive:searchResult
     * @restrict EA
     * @scope
     * @description
     * Single search result item with expanding details
     */
    module.registerDirective("searchResult", function(){

      return {
        restrict: 'EA',
        templateUrl: 'app/search/views/search-result.html',
        replace: true,
        scope: true,
        link: function(scope, element, attrs){

          //Hide Details / Show Details Label
          scope.hideDetails = true;
          scope.$watch("hideDetails", function(hideDetails){
            scope.detailLabel = hideDetails ? "Show Details" : "Hide Details" ;
          });

        }
      };
    });
});