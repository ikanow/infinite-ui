define(['search/module'], function(module){
  "use strict";

  /**
   * @ngdoc directive
   * @name app.search.directive:searchResultDetails
   * @restrict EA
   * @description
   * Expanded search result details
   */
  return module.registerDirective("searchResultDetails", function($uibModal){

    return {
      restrict: 'EA',
      templateUrl: 'app/search/views/search-result-details.html',
      replace: true,
      link: function(scope, element, attrs){

        scope.metadata = scope.result.metadata;

        /**
         * Open a modal window and display the meta data using angular-json-human
         */
        scope.showMetadata = function(){
          $uibModal.open({
            size: "lg",
            scope: scope,
            templateUrl: 'app/search/views/search-result-metadata.html',
            controller: function($scope, $uibModalInstance){
              $scope.close = function(){
                $uibModalInstance.close();
              };
            }
          });
        };

      }
    };
  });
});