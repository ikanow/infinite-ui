define(['search/module'], function(module){
    "use strict";

    /**
     * @ngdoc directive
     * @name app.search.directive:searchSort
     * @restrict EA
     * @description
     * Set of options for sort methods in search results view
     */
    return module.registerDirective("searchSort", function(){

        return {
            restrict: 'EA',
            templateUrl: 'app/search/views/search-sort.html',
            replace: true,
            link: function($scope, element, attrs){
                var orderExpressions = {
                  recent: "-publishedDateUnix",
                  oldest: "+publishedDateUnix",
                  relevance: "-queryRelevance"
                };
                $scope.sortBy = "recent";
                $scope.orderExpr = orderExpressions.recent;

                $scope.$watch(function() {
                  return $scope.sortBy;
                }, function(newValue) {
                  $scope.orderExpr = orderExpressions[newValue];
                });
            }
        };
    });
});