define(['search/module', 'jquery'], function (module, $) {
  "use strict";

  /**
   * @ngdoc directive
   * @name app.search.directive:searchBar
   * @restrict EA
   *
   * @description
   * This directive contains the layout for the search bar and the mobile search menu. Submission of new searches
   * is also handed here.
   */
  module.registerDirective("searchBar", function ($rootScope, $log, $compile, $state, $window, SearchService, workspaceService, KibanaService) {

    /**
     * Open a new tab with kibana. Set the dataGroupIds to the current dataGroupIds and set mode to live
     * @param searchString
     */
    function openKibana( searchString ){
      KibanaService.openKibanaTab({search: searchString});
    }

    return {
      restrict: 'EA',
      templateUrl: 'app/search/views/search-bar.html',
      replace: true,
      link: function (scope, element, attrs) {

        //$log.debug("[SearchBar->link] init");

        // Filters object is modified by the search-filters form directive
        // take updates from the currentSearch but do not push updates back
        // on-way-binding
        if (!SearchService.getCurrentQuery()) {
          scope.filters = SearchService.getDefaultQuery();
          scope.filters.searchString = "";
        } else {
          scope.filters = SearchService.getCurrentQuery();
        }

        //Current selections and Drop down options for documents or records
        scope.searchTypeLabel = "Documents";
        scope.searchType = "docs";
        scope.searchTypes = {
          docs: "Documents",
          recs: "Records"
        };

        //Doc / Record selection drop down state
        scope.showSearchTypeDropDown = false;

        //Drop down for search options ( filters ) state
        scope.showFiltersDropDown = false;

        //Select a search type from the drop down
        scope.selectSearchType = function(searchType){
          scope.searchType = searchType;
          scope.searchTypeLabel = scope.searchTypes[searchType];
        };

        scope.toggleFiltersDropDown = function($event){
            $event.preventDefault();
            $event.stopPropagation();
            scope.showFiltersDropDown = !scope.showFiltersDropDown;
        };

        //Listen for filters drop down and disable auto complete when it's visible
        scope.dropDownToggle = function(showFilters){
          if( showFilters ){
            $(element).find(".header-search > input").attr('autocomplete', 'off');
          } else {
            $(element).find(".header-search > input").removeAttr('autocomplete');
          }
        };

        //Add a doSearch function to scope, accept the searchString as 'query'
        scope.doSearch = function () {

          //Don't do anything is no search was given
          if(scope.filters.searchString == ""){
            return;
          }

          //If search Type is records, just open the kibana popup
          if( scope.searchType == 'recs' ){

            //Create a static list of data groups to use for this kibana instance
            openKibana( scope.filters.searchString );

            //stop here
            return;
          }

          // Default external and internal toggle filters set to true
          if (_.indexOf(scope.filters.origin, "internal") < 0) {
            scope.filters.origin.push("internal");
          }
          if (_.indexOf(scope.filters.origin, "external") < 0) {
            scope.filters.origin.push("external");
          }

          //Call the search service with the new search string
          SearchService.setQuery(scope.filters).then(
            //After creating the QueryDef, navigate to the results
            function (queryDef) {
              scope.showFiltersDropDown = false;
              scope.filters = queryDef;

              $state.go('app.search.results', {
                queryId: queryDef.id,
                forceReload: true,
                urlWorkspaceId: $rootScope.workspaceId
              },{
                reload: true
              });
            },

            function (errMsg) {
              $.bigBox({
                title: "Search Error",
                content: errMsg,
                color: "#C46A69",
                timeout: 10000,
                icon: "fa fa-times fadeInRight animated",
                number: "2"
              });
            }
          );

        }; //end doSearch

      } //end link

    };
  });
});