define(['search/module', 'jquery'], function(module){
  "use strict";

  /**
   * @ngdoc directive
   * @name app.search.directive:searchNav
   * @description
   * Builds a node for the left nav bar
   */
  module.registerDirective("searchNav", function(){
    return {
      restrict: 'EA',
      template:
        '<li data-menu-collapse ui-sref-active="open active">' +
          '<a title="Search">' +
            '<i class="fa fa-lg fa-fw fa-search"></i>' +
            '<span class="menu-item-parent">Search</span>' +
          '</a>' +
          '<ul>' +
            '<li ui-sref-active="active"><a data-ui-sref="app.search.results({urlWorkspaceId:workspaceId})">Results</a></li>' +
            '<li ui-sref-active="active"><a data-ui-sref="app.search.history({urlWorkspaceId:workspaceId})">Search History</a></li>' +
          '</ul>' +
        '</li>',
      replace: true
    };
  });

});
