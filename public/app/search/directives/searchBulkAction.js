define(['search/module', 'jquery'], function(module){
    "use strict";

    /**
     * @ngdoc directive
     * @name app.search.directive:searchBulkAction
     * @restrict EA
     * @description
     * Display and maintain a drop down to select bulk actions on search results
     *
     */
    module.registerDirective("searchBulkAction", function(){

        var actionLabels = {
            report: "Add to Report",
            bucket: "Add to Bucket",
            select: "Select All",
            deselect: "De-Select All"
        };

        return {
            restrict: 'EA',
            templateUrl: 'app/search/views/search-bulk-action.html',
            replace: true,
            link: function($scope, element, attrs){
                $scope.bulkAction = "report";
                $scope.bulkActionLabel = "Add to Report";

                $scope.$watch("bulkAction", function(newValue){
                    $scope.bulkActionLabel = actionLabels[newValue];
                });
            }
        };
    });
});