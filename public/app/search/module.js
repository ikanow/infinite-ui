/**
 * @ngdoc overview
 * @name app.search
 * @description
 * Infinite Platform searching
 */
define([
  'angular', 'angular-couch-potato', 'angular-ui-router', 'angular-resource', 'ng-tags-input', 'angular-json-human'
  ], function (ng, couchPotato) {
  'use strict';

  var module = ng.module('app.search', ['ui.router', 'ngTagsInput', 'yaru22.jsonHuman']);

  module.config(function ($stateProvider, $couchPotatoProvider) {

    $stateProvider
      .state('app.search', {
        abstract: true
      })
      .state('app.search.advanced', {
        url: '/:urlWorkspaceId/search/advanced',
        params: {
          queryId: null,
          urlWorkspaceId: null
        },
        views: {
          "content@app": {
            templateUrl: 'app/search/views/search-advanced.html',
            controller: "SearchAdvancedCtrl",
            resolve: {
              deps: $couchPotatoProvider.resolveDependencies([
                'search/controllers/SearchAdvancedCtrl',
                'directives/ikNgTags'
              ])
            }
          }
        }
      })
      .state('app.search.results', {
        url: '/:urlWorkspaceId/search/results/:queryId',
        params: {
          queryId: null,
          forceReload: null,
          urlWorkspaceId: null
        },
        views: {
          "content@app": {
            templateUrl: 'app/search/views/search-results.html',
            controller: "SearchResultsCtrl",
            resolve: {
              deps: $couchPotatoProvider.resolveDependencies([
                'search/controllers/SearchResultsCtrl',
                'search/directives/searchSort',
                'search/directives/searchBulkAction',
                'search/directives/searchResultDetails',
                'search/directives/searchFilters',
                'search/directives/searchResult'
              ])
            }
          }
        }
      })
      .state('app.search.history', {
        url: '/:urlWorkspaceId/search/history',
        params: {
          urlWorkspaceId: null
        },
        data: {
          title: 'Search History'
        },
        views: {
          "content@app": {
            controller: "SearchHistoryCtrl as searchHistoryCtrl",
            templateUrl: 'app/search/views/search-history.html',
            resolve: {
              deps: $couchPotatoProvider.resolveDependencies([
                'search/controllers/SearchHistoryCtrl',
                'modules/tables/directives/datatables/datatableBasic'
              ])
            }
          }
        }
      })
  });

  couchPotato.configureApp(module);

  module

    /**
     * @ngdoc filter
     * @name app.search.filter:htmlToPlaintext
     * @description
     * Strip HTML Tags and return plain text
     */
    .filter('htmlToPlaintext', function() {
      return function(text) {
        return String(text).replace(/<[^>]+>/gm, '');
      }
    })

    /**
     * @ngdoc filter
     * @name app.search.filter:more
     * @description
     * If length of given string is longer then {length}, truncate the string to length and append an ellipsis.
     */
    .filter("more", function () {
      return function (input, length) {
        if (isNaN(length)) return input;
        if (parseInt(length) < 0) return input;
        if (input && input.length && input.length > length) {
          return input.substr(0, length) + "...";
        }
        return input;
      };
    })

    /**
     * @ngdoc filter
     * @name app.search.filter:domain
     * @description
     * Return the domain portion of a given URL
     */
    .filter("domain", function () {
      return function (input) {
        return input;
      }
    })

    /**
     * @ngdoc filter
     * @name app.search.filter:parseDate
     * @description
     * Parse a date time string into a Date object
     */
    .filter("parseDate", function () {
      return function (input) {
        return Date.parse(input);
      }
    })

    /**
     * @ngdoc filter
     * @name app.search.filter:defaultTo
     * @description
     * If input is empty, return defaultString
     */
    .filter("defaultTo", function(){
      return function( input, defaultString ){
        return input == "" ? defaultString : input;
      }
    })

    .run(function ($couchPotato) {
      module.lazy = $couchPotato;
    });

  return module;
});