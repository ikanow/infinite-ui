/**
 * @ngdoc controller
 * @name app.search.controller:SearchHistoryCtrl
 */
define(['search/module'], function (module) {
  'use strict';

  return module.registerController('SearchHistoryCtrl', function ($state, SearchService, SearchQueryService ) {

    // Named reference to 'this' to match 'controller as' name
    var searchHistoryCtrl = this;

    //Get the search history stripped of results.
    SearchService.getSearchHistory().then(
      function success(result){
        searchHistoryCtrl.searches = result;
      },
      function error(err){
        $log.error("[SearchHistoryCtrl] Could not get search history");
      }
    );

    /**
     *
     * @param queryDef
     * @returns {*}
     */
    searchHistoryCtrl.getEntitiesDescription = function(queryDef){
      return SearchQueryService.describeEntities(queryDef);
    };

    //Delete item click handler
    searchHistoryCtrl.deleteItem = function(itemId){
      SearchService.removeQuery(itemId);
    };

  });
});