/**
 * @ngdoc controller
 * @name app.search.controller:SearchAdvancedCtrl
 */
define(['search/module'], function (module) {
  'use strict';

  return module.registerController('SearchAdvancedCtrl',

    function ($rootScope, $scope, $state, $stateParams, $log, SearchService) {

      //Filters object is modified by the search-filters form directive
      $scope.filters = SearchService.getDefaultQuery();

      //Check to see if we're editing an existing query
      var editQueryIdParam = $stateParams.queryId;
      if( editQueryIdParam ){
        SearchService.getQuery(editQueryIdParam).then( function(queryDef) {
          //console.log("editing item:", editSearchId, editObject.infiniteQuery.getConfig() );

          //Set the state of the filters using the infinite query config
          $scope.filters = queryDef;
        });
      }


      //Attach a function to scope to trigger the advanced search
      $scope.doAdvancedSearch = function () {

        //Create the new SearchObject using the configured InfiniteQuery
        SearchService.setQuery($scope.filters).then(

          //Success
          function (queryDef) {

            //Navigate to the results.
            $state.go('app.search.results', {
              queryId: queryDef.id,
              forceReload: true,
              urlWorkspaceId: $rootScope.workspaceId
            },{
              reload: true
            });
          },

          //Error
          function (errMsg) {
            $.bigBox({
              title: "Search Error",
              content: errMsg,
              color: "#C46A69",
              timeout: 10000,
              icon: "fa fa-times fadeInRight animated",
              number: "2"
            });
          }
        );

      };

    }
  );
});