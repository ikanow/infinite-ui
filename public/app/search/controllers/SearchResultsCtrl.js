/**
 * @ngdoc controller
 * @name app.search.controller:SearchResultsCtrl
 */
define(['search/module', 'jquery', 'lodash'], function (module, $, _) {
    'use strict';

    return module.registerController('SearchResultsCtrl', function ($scope, $state, $stateParams, $log, $q, $window, SearchService, InfiniteApi) {

      var docsToLoad = 100;
      /**
       * State
       */
      $scope.isSearching = true;

      //Defaults for search results view
      $scope.bulkAction = "report";
      $scope.results = null; //null is a temp state to indicate loading
      $scope.resultType = ""; // 'documents' - defaults ot "documents" at the end of this function in init

      // Tallys
      $scope.docsVisible = 0;
      $scope.docsTotal = 0;
      $scope.recsVisible = 0;
      $scope.recsTotal = 0;

      $scope.isCompleteSet = false;

      $scope.hideFilters = true;
      $scope.filters = SearchService.getDefaultQuery();
      $scope.toggles = {};
      $scope.toggles.internalSwitch = true;
      $scope.toggles.externalSwitch = true;

      /**
       * Display an error message to the user
       * @param errorMessage
       */
      function searchError(errorMessage){
        $.bigBox({
          title: "Search Error",
          content: errorMessage,
          color: "#C46A69",
          timeout: 2500,
          icon: "fa fa-times fadeInRight animated"
        });
      }

      /**
       * Handle an API Response containing documents
       * @param apiResponse
       */
      function displayResults(apiResponse){

        $log.debug("[SearchResultsCtrl->displayResults]", apiResponse);

        if(apiResponse == null){
          //NO data or at end of set
          return;
        }

        //Update each result with the entity counts
        var results = _.map( apiResponse.data,
          function( result ) {

            result.isDocument = true;

            //For each result create an empty set of totals.
            result.entityTotals = {
              person: 0,
              place: 0,
              organization: 0,
              association: 0,
              other: 0
            };

            //Iterate over each entity to count the types.
            _.each(result.entities, function(entity){
              switch ( entity.type ){
                case "Person":
                  result.entityTotals.person++;
                  break;
                case "Place":
                  result.entityTotals.place++;
                  break;
                case "Organization":
                  result.entityTotals.organization++;
                  break;
                case "Association":
                  result.entityTotals.association++;
                  break;
                default:
                  result.entityTotals.other++;
                  break;
              }
            });


            // Determine if source is Internal or External based on tag
            if( result.tags && _.indexOf(result.tags, "internal") > -1) {
              result.sourceOrigin = "Internal";
            } else if (result.tags && _.indexOf(result.tags, "external") > -1) {
              result.sourceOrigin = "External";
            }

            //Convert date to a sortable number
            result.publishedDateUnix = InfiniteApi.dateToMoment(result.publishedDate).unix();

            //Map the updated object
            return result;
          }
        );

        //Added during caching
        if(apiResponse.isCompleteSet){
          $scope.isCompleteSet = true;
        }

        //If no start value or 0, just use the documents as is
        if( $scope.results == null ){
          $scope.results = results;

        //If there is a start value, append the new results to the result set.
        } else {
          $scope.results = $scope.results.concat(results);
        }

      }

      /**
       * Query the API for more documents
       * @param {number} additionalResultCount Number of additional documents to fetch
       */
      function loadMoreResults(additionalResultCount){
        $scope.isSearching = true;
        $scope.noDataGroups = false;

        //Determine how many documents to skip
        var skip = $scope.results == null ? 0 : $scope.results.length || 0;

        //Get data required from search service and display
        return SearchService.getDocuments(additionalResultCount, null, skip ).then( function(apiResponse) {
            displayResults(apiResponse);
            $scope.docsVisible = $scope.results.length;
            $scope.docsTotal = apiResponse.stats.found;
            $scope.isSearching = false;
            return true;
          },
          //Cannot fetch docs
          function fetchError(err){
            $scope.isSearching = false;
            $log.error("[SearchResultsCtrl->init] Data fetch error", err);
            $scope.results = false;
            if (err === "Data group IDs are missing. To search all use '*'.") {
              $scope.noDataGroups = true;
            }
            searchError(err);
          }
        );
      }

      /**
       * Update the search based on the Internal/External toggles.
       */
      $scope.$watch(function () {
          return $scope.toggles.internalSwitch;
        }, function (newVal, oldVal) {
        if (newVal !== oldVal) {
          if (newVal && _.indexOf($scope.filters.origin, "internal") < 0) {
            $scope.filters.origin.push("internal");
            $scope.updateSearch();
          } else if (!newVal) {
            _.remove($scope.filters.origin, function (item) {
              return item === "internal";
            });
            // If the other toggle is off, don't update search, remove documents, reset counts
            if (!$scope.toggles.externalSwitch) {
              $scope.results = null;
              if ($scope.stats) {
                $scope.displayedCount = $scope.stats.found = 0;
              }
              return;
            }
            $scope.updateSearch();
          }
        }
      });
      $scope.$watch(function () {
        return $scope.toggles.externalSwitch;
      }, function (newVal, oldVal) {
        if (newVal !== oldVal) {
          if (newVal && _.indexOf($scope.filters.origin, "external") < 0) {
            $scope.filters.origin.push("external");
            $scope.updateSearch();
          } else if (!newVal) {
            _.remove($scope.filters.origin, function (item) {
              return item === "external";
            });
            if (!$scope.toggles.internalSwitch) {
              $scope.results = null;
              if ($scope.stats) {
                $scope.displayedCount = $scope.stats.found = 0;
              }
              return;
            }
            $scope.updateSearch();
          }
        }
      });

      /**
       * Watcher on window scrolling to determine if more results need to be reloaded.
       */
      $scope.$watch(function () {
        return $window.scrollY;
      }, function (scrollY) {
        // limit is max scrollY
        // h is viewport height
        var limit = Math.max(
            document.body.scrollHeight,
            document.body.offsetHeight,
            document.documentElement.clientHeight,
            document.documentElement.scrollHeight,
            document.documentElement.offsetHeight
          ),
          h = Math.max(
            document.documentElement.clientHeight,
            window.innerHeight || 0
          );

        //If we're at the bottom, search service is idle, and we're not sure if there's more results
        // then try to load more results
        if( scrollY + h == limit && $scope.isSearching == false && !$scope.isCompleteSet ){
          loadMoreResults(docsToLoad);
        }
      });

      /**
       * Run a new search if filters have changed
       */
      $scope.updateSearch = function(){
        //Set the current query using the new updated query def
        SearchService.setQuery(_.cloneDeep($scope.filters)).then(
          //After setting the query using a Query Definition,
          // a new search history item was added and the QueryDef
          // contains a ID now.
          function (queryDef) {
            $state.go('app.search.results', {
              queryId: queryDef.id,
              forceReload: true,
              urlWorkspaceId: $rootScope.workspaceId
            },{
              reload: true
            });
          },
          function (errMsg) {
            searchError(errMsg);
          }
        );
      };

      /**
       * Init Results
       */

      //Get state param values
      var queryIdParam = $stateParams.queryId;
      var forceReload = $stateParams.forceReload;

      //To force reload, just force clear the cache
      if(forceReload){
        SearchService.clearCache();
      }


      /**
       * If there's a query ID passed in the ui state, set the SearchService's current query.
       * Otherwise get the current query from the search service.
       */
      var queryDefPromise = queryIdParam ? SearchService.setQuery(queryIdParam) : SearchService.getQuery();

      //When we have the queryDefinition, cache it, and begin to load documents.
      queryDefPromise.then(

        function setQueryDefSuccess(queryDef){
          $log.debug('[SearchResultsCtrl->init] queryDef application success', queryDef );

          //Update filter options
          $scope.filters = queryDef;

          $scope.toggles.internalSwitch = _.indexOf($scope.filters.origin, "internal") > -1;
          $scope.toggles.externalSwitch = _.indexOf($scope.filters.origin, "external") > -1;

          loadMoreResults(docsToLoad)
        },

        function setQueryDefError(err){
          $log.error('[SearchResultsCtrl->init] setQuery from param ERROR', queryIdParam, err );
          $scope.results = false;
          searchError(err);
        }
      );

    });
});