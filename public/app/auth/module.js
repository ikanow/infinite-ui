/**
 * @ngdoc overview
 * @name app.auth
 * @description
 * UI authentication
 */
define([
  'angular',
  'angular-couch-potato',
  'angular-ui-router'
], function (ng, couchPotato) {
  "use strict";

  var module = ng.module('app.auth', ['ui.router']);
  couchPotato.configureApp(module);

  module.config(function ($stateProvider, $couchPotatoProvider) {

    $stateProvider

      .state('login', {
        url: '/login/:message',
        params: {
          message: {
            value: null,
            squash: true
          }
        },
        views: {
          root: {
            templateUrl: "app/auth/views/login.html",
            controller: 'LoginCtrl',
            resolve: {
              deps: $couchPotatoProvider.resolveDependencies([
                'auth/directives/loginInfo',
                'auth/login/LoginCtrl',
                'modules/forms/directives/validate/smartValidateForm'
              ])
            }
          }
        },
        data: {
          title: 'Login',
          rootId: 'extra-page'
        }
      })

      .state('forgotpassword', {
        url: '/forgotpassword',
        authenticate: false,
        views: {
          root: {
            templateUrl: "app/auth/views/forgotpassword.html",
            controller: 'LoginCtrl',
            resolve: {
              deps: $couchPotatoProvider.resolveDependencies([
                'auth/directives/loginInfo',
                'auth/login/LoginCtrl',
                'modules/forms/directives/validate/smartValidateForm'
              ])
            }
          }
        },
        data: {
          title: 'Forgot Password',
          rootId: 'extra-page'
        }
      })

      .state('resetpassword', {
        url: '/resetpassword/:token',
        authenticate: false,
        views: {
          root: {
            templateUrl: "app/auth/views/resetpassword.html",
            controller: 'LoginCtrl',
            resolve: {
              deps: $couchPotatoProvider.resolveDependencies([
                'auth/directives/loginInfo',
                'auth/login/LoginCtrl',
                'modules/forms/directives/validate/smartValidateForm'
              ])
            }
          }
        },
        data: {
          title: 'Forgot Password',
          rootId: 'extra-page'
        }
      })

      .state('logout', {
        url: '/logout',
        views: {
          root: {
            templateUrl: "app/auth/views/login.html"
          }
        }
      })

      .state('expired-session', {
        url: '/logout',
        views: {
          root: {
            templateUrl: "app/auth/views/login.html"
          }
        }
      })

      .state('resume', {
        url: '/resume',
        params: {
          resumeState: null
        },
        views: {
          root: {
            template: '',
            controller: function ($rootScope, $location, $log, $state, $stateParams, authService, SessionService) {

              //Initialize user session using saved token
              authService.initLoggedInUser(SessionService.getAuthToken())

                // initLoggedInUser resolves with the result from 'workspaceInit'
                // if null, no workspaces are available. But login was successful
                .then(function (initResult) {
                  if (initResult == null) {
                    $log.debug("Login success. No workspaces.");
                    $state.transitionTo("app.project.none");
                  } else {
                    var toState = $stateParams.resumeState || $rootScope.defaultLocation;
                    $log.debug("Login success. Navigating to " + toState + " with workspace: ", initResult);
                    $location.path(toState);
                  }
                })
                .catch(function (err) {
                  $log.error("App init user info fail:", err);
                  authService.logout().then(function () {
                    $state.transitionTo("login");
                  });
                });

            }
          }
        }
      });


  });

  module.run(function ($couchPotato) {
    module.lazy = $couchPotato;
  });
  return module;
});