/**
 * @ngdoc directive
 * @name app.auth.directive:loginInfo
 * @description
 * Login info directive
 */
define(['auth/module'], function(module){
    "use strict";

    return module.registerDirective('loginInfo', function() {

        return {
            restrict: 'E',
            templateUrl: 'app/auth/directives/login-info.tpl.html',
            replace: true
        };
    });
});
