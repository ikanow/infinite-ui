/**
 * @ngdoc controller
 * @name app.auth.controller:LoginCtrl
 * @description
 * UI State controller for log in form
 */
define(['auth/module'], function (module) {
    "use strict";

    module.registerController('LoginCtrl', function ($rootScope, $scope, $log, $state, $stateParams, authService) {

        $scope.passwordPopover = "<div style='padding\:5px\;'><strong>Password must be 8 or more characters and contain at least one number and one letter</strong></div>";

        $scope.loadingMessage = 'Logging in...';
        $scope.loadingPromise = null;

        var $form = $("form:first");

        //if login was passed a message, display it (for password resets etc)
        if ($stateParams.message) {
          $scope.loginMessage = $stateParams.message;
          //remove parameter from URL
          $state.go('login', {
            message: null
          },{
            notify: false
          });
        }

        //test to make sure password reset token is valid
        if($stateParams.token) {
          //test for valid token
          authService.forgotPassword(null,null,$stateParams.token,true).then(function(response){
            //token valid, do nothing
          }, function(err){
            //token invalid, display error
            $log.error('ERROR: ',err);
            $scope.resetErrorMessage = 'There was an error resetting your password. Your temporary token has expired or is invalid. Please close and click the forgot password link to reset your password.';
          });
        }

        /**
         * Attempt to login the user
         */
        $scope.submitLoginForm = function() {

            //TODO stop submission if validation fails

            $rootScope.currUsername = undefined;

            var loginSuccess = function(userProfile) {
              //if they hit a direct link while not authenticated, go to that state after login
              if($rootScope.fromState) {
                $state.go($rootScope.fromState, $rootScope.fromParams, { reload: false });
                $rootScope.fromState = null;
              }
              // if the user's session timed out and they're logging back in, then restore the session state
              else if($rootScope.autologout === true && $rootScope.lastUsername == $rootScope.currUsername) {
                $state.go($rootScope.lastActiveSession);
                $rootScope.lastActiveSession = undefined;
                $rootScope.autologout = false;
              }
              // otherwise load the analyze view
              else {
                $state.go($rootScope.defaultLocation);
              }
            };

            var loginFailure = function() {
              $scope.loginErrorMessage = 'The login was unsuccessful. Please check your email/password and try again.';
              $scope.user.password = '';
            };

            $scope.loginInProgress = true;
            $scope.loginErrorMessage = null;
            $scope.loginMessage = null;

            ($scope.loadingPromise = authService.login($scope.user.username, $scope.user.password))
              .then(loginSuccess, loginFailure);
        };

        $scope.logout = function() {
            authService.logout().then(function(){
                $state.transitionTo("login");
            })
        };

        /**
         * request API to to send a forgot password link
         */
        $scope.submitForgotPassword = function() {
          var forgotPasswordForm = $('form[name="forgot-password-form"]');
          if (!forgotPasswordForm.valid()) {
            forgotPasswordForm.data('validator').focusInvalid();
            return false;
          } else {
            $scope.loadingMessage = "Submitting request...";
            ($scope.loadingPromise = authService.forgotPassword($scope.email)).then(function(){
              $scope.formSubmitted = true;
            }, function(err){
              $log.error('ERROR: ', err);
              $scope.forgotErrorMessage = err;
              $scope.formSubmitted = true;
            });
            
          }

        };

        /**
         * use API-supplied hash to reset users password
         */
        $scope.submitResetPassword = function() {
          var resetPasswordForm = $('form[name="reset-password-form"]');
          if (!resetPasswordForm.valid()) {
            resetPasswordForm.data('validator').focusInvalid();
            return false;
          } else {
            $scope.loadingMessage = "Resetting password...";
            ($scope.loadingPromise = authService.forgotPassword(null,$scope.password,$stateParams.token)).then(function(response){
              $log.debug('reset password response ', response)
              $state.go('login', {
                message: 'Your password has been successfully updated. Please log in below'
              });
            }, function(err){
              $log.error('ERROR: ', err);
              $scope.resetErrorMessage = 'There was an error resetting your password. Your temporary token has expired or is invalid. Please close and click the forgot password link to reset your password.';
            });
          }
        };

        $.validator.addMethod("validEmail", function (email) {
          return email.match(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$/);
        }, "Please enter a VALID email address");

        $.validator.addMethod('alphaNumPassword', function(password) {
            return (password.match(/[a-zA-Z]/) && password.match(/[0-9]/));
        }, "Password must contain at least one number and one letter");

        $.extend($.validator.messages, {
          required: "This field is required.",
          email: "Please enter a VALID email address",
          maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
          minlength: jQuery.validator.format("Please enter at least {0} characters.")
        });

      $form.validate(angular.extend({
        // Rules for form validation
        rules: {
            email: {
                required: true,
                validEmail: true
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 20,
                alphaNumPassword: true
            },
            passwordConfirm: {
                required: true,
                minlength: 8,
                maxlength: 20,
                equalTo: '#password'
            },
        },
        // Messages for form validation
        messages: {
            email: {
                required: 'Please enter an email address'
            }
        },
        ignore: '.ignore-validation, :hidden',
        errorElement: 'em',
        errorClass: 'invalid',
        highlight: function(element, errorClass, validClass) {
            if(!($(element).attr('placeholder') == 'Filter')){
                $(element).addClass(errorClass).removeClass(validClass);
                $(element).parent().addClass('state-error').removeClass('state-success');
            }
        },
        unhighlight: function(element, errorClass, validClass) {
            if(!($(element).attr('placeholder') == 'Filter')){
                $(element).removeClass(errorClass).addClass(validClass);
                $(element).parent().removeClass('state-error').addClass('state-success');
            }
        },
        errorPlacement : function(error, element) {
            error.insertAfter(element.parent());
        }

      }, $form.validateOptions));

    });
});