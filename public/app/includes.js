define([
    // account
    'auth/module',

    // layout

    'layout/module',
    'layout/actions/minifyMenu',
    'layout/actions/toggleMenu',
    'layout/actions/fullScreen',
    'layout/actions/resetWidgets',
    'layout/actions/searchMobile',
    'layout/directives/demo/demoStates',
    'layout/directives/smartInclude',
    'layout/directives/smartLayout',
    'layout/directives/smartRouterAnimationWrap',
    'layout/directives/radioToggle',
    'layout/directives/dismisser',
    'layout/directives/smartMenu',
    'layout/directives/stateBreadcrumbs',
    'layout/directives/hrefVoid',
    'layout/service/SmartCss',
    'modules/widgets/directives/widgetGrid',
    'modules/widgets/directives/jarvisWidget',
    'modules/widgets/services/add-widget-provider',

    // tables
    'modules/tables/module',

    // widgets
    'modules/widgets/module',

   // misc
    'modules/misc/module',

    // IKANOW MANAGER
    'manager/module',

    // IKANOW PROJECT
    'project/module',
    'project/directives/projectName',

    //IKANOW SOURCES
    'source/module',

    //IKANOW REPORTS
    'report/module',

    //Ikanow-View Services
    'lib/angular-cache-mod',
    'services/aclService',
    'services/authService',
    'services/dataGroupService',
    'services/sessionService',
    'services/session-monitor',
    'services/kibanaService',
    'services/manager/communityService',
    'services/reportService',
    'services/historyService',
    'services/suggestService',
    'services/userService',
    'services/userGroupService',
    'services/utilityService',
    'services/workspaceService',

    //IKANOW CUSTOM DIRECTIVES:
    'directives/recentProjects',
    'directives/addUserUserGroups',
    'directives/fileInput',
    'directives/ikNgTags',

    //IKANOWTestVizService
    'services/vizTestService',

    //IKANOW SEARCH MODULE
    'services/searchService',
    'services/searchQueryService',
    'search/module',
    'search/directives/searchBar',
    'search/directives/searchNav',
    'search/directives/searchFilters',

    // IKANOW CUSTOM DATA SOURCES
    'services/ikanow_source_templates', // Mock Source Objs
    'services/sampleSourcesLegacy', // Legacy Source Objs
    'services/sourcesService',

    'modules/tables/directives/ikanow-datatables/ikanow-datatables'

], function () {
    'use strict';
});
