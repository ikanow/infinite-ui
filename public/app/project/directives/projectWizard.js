/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['project/module', 'lodash'], function (module, _) {
    'use strict';

    /**
     * @ngdoc directive
     * @name app.source.directive:projectWizard
     * @restrict A
     * @description
     * Project Creation Wizard
     */
    return module.registerDirective('projectWizard', function ($rootScope, $log, $timeout, workspaceService) {
        return {
            restrict: 'A',
            // scope: {
            //     'smartWizardCallback': '&'
            // },
            link: function (scope, element, attributes) {

                var stepsCount = $('[data-smart-wizard-tab]').length;
                var currentStep = 1;
                var validSteps = [];
                var $form = element.closest('form');
                var $prev = $('[data-smart-wizard-prev]', element);
                var $next = $('[data-smart-wizard-next]', element);

                scope.checkingProjectName = false;
                scope.projectNameInvalid = false;

                $.validator.addMethod("validProjectName", function() {
                  return !scope.projectNameInvalid;
                }, "Please enter a unique project name");

                $form.validate(angular.extend({
                    // Rules for form validation
                    rules: {
                        projectName: {
                            required: true,
                            validProjectName: true
                        },
                        projectDesc: {
                          required: true
                        },
                        tagsInput: {
                            required: true,
                            tagsInput: true
                        }
                    },
                    // Messages for form validation
                    messages: {
                        projectName: {
                            required: 'Please enter a project name'
                        },
                        projectDesc: {
                          required: "Please enter a project description"
                        }
                    },
                    ignore: '.ignore-validation, :hidden',
                    errorElement: 'em',
                    errorClass: 'invalid',
                    highlight: function(element, errorClass, validClass) {
                        if(!($(element).attr('placeholder') == 'Filter')){
                            $(element).addClass(errorClass).removeClass(validClass);
                            $(element).parent().addClass('state-error').removeClass('state-success');
                        }
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        if(!($(element).attr('placeholder') == 'Filter')){
                            $(element).removeClass(errorClass).addClass(validClass);
                            $(element).parent().removeClass('state-error').addClass('state-success');
                        }
                    },
                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());
                    }

                }, $form.validateOptions));

                function setStep(step) {
                  currentStep = step;
                  $('[data-smart-wizard-pane=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-pane]').removeClass('active');
                  $('[data-smart-wizard-tab=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-tab]').removeClass('active');

                  if (step === 4) {
                      $next.find('a').text("Save").addClass("btn-success").removeClass("txt-color-darken");
                  } else {
                      $next.find('a').text("Next").removeClass("btn-success").addClass("txt-color-darken");
                  }

                  $prev.toggle(step != 1)
                }

                element.on('click', '[data-smart-edit-btn]', function (e) {
                    setStep(parseInt($(this).attr('data-smart-edit-btn')));
                    e.preventDefault();
                });

                function setCompletedClass(step) {
                  element.find('[data-smart-wizard-tab=' + step + ']')
                    .addClass('complete')
                    .find('.step')
                    .html('<i class="fa fa-check"></i>');
                };

                $next.on('click', function (e) {

                    if (currentStep == 1) {
                      scope.checkingProjectName = true;
                      scope.projectNameInvalid = false;
                      scope.nextStep = false;
                      e.preventDefault();

                      if(!$form.valid()) { return; }
                      
                      if (!scope.projectName) {
                        scope.projectNameInvalid = true;
                        $form.valid();
                        return;
                      }

                      // If the project still has original name, skip validation
                      if (scope.currentProject) {
                        if (scope.currentProject.title === scope.projectName) {
                          scope.nextStep = true;
                          validSteps.push(currentStep);
                          setCompletedClass(currentStep);
                          setStep(2);
                          return;
                        }
                      }

                      scope.uniqueProjectPromise = workspaceService.isNameValid(scope.projectName);

                      scope.uniqueProjectPromise.then(function (response) {
                        scope.checkingProjectName = false;
                        if (response == true) {
                          scope.nextStep = true;
                          validSteps.push(currentStep);
                          setCompletedClass(currentStep);
                          setStep(2);
                        } else {
                          scope.projectNameInvalid = true;
                          $form.valid();
                        }
                      });
                      return false;
                    }

                    if(currentStep == 2 && scope.stepTwoValid.length <= 0){
                        scope.nextStep = false;
                        e.preventDefault();
                        return false;
                    }

                    if ($form.data('validator')) {
                        if (!$form.valid()) {
                            validSteps = _.without(validSteps, currentStep);
                            $form.data('validator').focusInvalid();
                            return false;
                        } else {
                            validSteps = _.without(validSteps, currentStep);
                            validSteps.push(currentStep);
                            setCompletedClass(currentStep);
                        }
                    }
                    if (currentStep < stepsCount) {
                        setStep(currentStep + 1);
                    } else {
                        if (validSteps.length < stepsCount) {
                            var steps = _.range(1, stepsCount + 1);

                            _(steps).forEach(function (num) {
                                if (validSteps.indexOf(num) == -1) {
                                    setStep(num);
                                    return false;
                                }
                            })
                        } else {
                            var data = {};
                            _.each($form.serializeArray(), function(field){
                                data[field.name] = field.value
                            });
                            scope.wizard1CompleteCallback(data);
                        }
                    }

                    e.preventDefault();
                });

                $prev.on('click', function (e) {
                    if (currentStep == 2) {
                      scope.projectNameInvalid = false;
                    }

                    if (!$prev.hasClass('disabled') && currentStep > 0) {
                        if (currentStep == 2) {
                          setStep(currentStep - 1);
                          $form.valid();
                        } else {
                          setStep(currentStep - 1);
                        }
                    }
                    e.preventDefault();
                });

                setStep(currentStep);

            }
        }
    });
});