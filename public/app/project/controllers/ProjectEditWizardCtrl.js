/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['project/module', 'lodash', 'notification'], function(module, _) {
    "use strict";

    /**
     * @ngdoc controller
     * @name app.project.controller:ProjectEditWizardCtrl
     * @description
     * Project edit wizard
     */
    module.registerController('ProjectEditWizardCtrl', function($scope, $rootScope, $uibModalInstance, $log, $state, $q, workspaceService, sourcesService, userGroupService, dataGroupService) {
        // Load existing project information
        var dataGroupIds = [];     // holds the ids of the existing data groups in the project.

        $scope.projectDataGroupMembersOrig = [];
        $scope.owner = {};
        $scope.currentProject = workspaceService.getCurrent();
        $scope.projectName = "";
        $scope.projectDesc = "";

        function getProjectDetails() {
            var deferred = $q.defer();
            
            function checkInputData(shouldReject) {
                if(shouldReject) {  
                    deferred.reject(new Error("Project missing data."));
                } else {
                    deferred.resolve("All Data Found");
                }
            }
            
            // If a project is currently opened, then update the project information.
            if($scope.currentProject) {
                var shouldReject = false;
                $log.debug('$scope.currentProject: ', $scope.currentProject);
                // Read project title
                if($scope.currentProject.title) {
                    $scope.projectName = $scope.currentProject.title;
                } else {
                    $scope.projectName = "";
                    $log.error("Project does not have an existing title.");
                    shouldReject = true;
                }
                $scope.projectDesc = $scope.currentProject.description ? $scope.currentProject.description : "";
                // Read project owner display name
                if($scope.currentProject.owner && $scope.currentProject.owner.displayName) {
                    $scope.owner.displayName = $scope.currentProject.owner.displayName;
                    $scope.ownerDisplayName = $scope.owner.displayName; //fix for now
                    $scope.owner.id = $scope.currentProject.owner._id;
                } else {
                    $scope.owner.displayName = "Owner Unspecified";
                    $log.error("Project does not have an owner.");
                    shouldReject = true;
                }
                
                // Read the project Data & Data Groups
                if(workspaceService.getDataGroupIds()) {
                    dataGroupIds = workspaceService.getDataGroupIds();
                } else {
                    $scope.dataGroupIds = [];
                    $log.error("Project does not have existing data groups.");
                    shouldReject = true;
                }

                // Read the User & User Groups and the Access settings
                if(workspaceService.getWorkspaceGroupId()) {
                    dataGroupService.get(workspaceService.getWorkspaceGroupId()).then(function(data) {
                        $scope.projectDataGroup = data;
                        $scope.projectDataGroupMembersOrig = angular.copy(data.members);
                        $scope.projectDataGroupMembersOrig = _.reject($scope.projectDataGroupMembersOrig, {userType: "owner"});                      
                        checkInputData(shouldReject);
                    }, function(err) {
                        $log.error('ERROR: Cannot download the project data group with the id: ', workspaceService.getWorkspaceGroupId());
                        checkInputData(shouldReject);
                    });
                }
                else {
                    checkInputData(shouldReject);
                }
            }
            else {
                checkInputData(shouldReject);
            }
            return deferred.promise;
        }

        getProjectDetails().then(function() {
            // Now that existing project data is loaded, prepare the modal forms.
            $scope.nextStep = true;
            $scope.stepTwoValid = true;

            $scope.wizard1CompleteCallback = function(wizardData) {
                $log.log('wizard1CompleteCallback', wizardData);
                $rootScope.transmitting = true;
                $.smallBox({
                    title: "Please wait, updating server",
                    color: "#3276B1",
                    iconSmall: "fa fa-bell swing animated",
                    timeout: 2000,
                    number: "1"
                }, function () {
                    //scope.eg2();
                });
                saveProject();
            };

            $scope.enableForm = function(success) {
                if(success) {
                    $.smallBox({
                        title: "Success! Server updated",
                        color: "#739E73",
                        timeout: 2000,
                        iconSmall: "fa fa-check",
                        number: "2"
                    });

                    $state.reload();
                } else {
                    $.smallBox({
                        title: "Error! Server update unsuccessful",
                        color: "#ed1c24",
                        timeout: 2000,
                        iconSmall: "fa fa-exclamation-circle",
                        number: "2"
                    });
                }
                $rootScope.transmitting = false;
            };

            $scope.updateDGs = function(){
                $scope.stepTwoValid = _.filter($scope.dataGroups, {added:true});
            };

            function loadDataGroups() {
                dataGroupService.getAll().then(function(dataGroups) {
                    //get sources and map them to datagroups
                    sourcesService.getUserSources().then( function(sources) {
                        _.each(dataGroups, function(dataGroup) {
                            dataGroup.sources = _.filter(sources, {communityIds : [dataGroup._id]});
                        });
                    });

                    $scope.allDataGroups = dataGroups;
                    $scope.dataGroups = _.chain(dataGroups)
                      .reject({isPersonalCommunity: true})
                      .reject({description: '__projectShare'})
                      .reject({tags:['workspaceMaster']})
                      .value();
                    _.each(dataGroupIds, function(i){
                        if ( _.find($scope.dataGroups, {'_id':i}) ) _.find($scope.dataGroups, {'_id':i}).added = true;
                    });
                    loadUserGroups();
                }, function(err) {
                    $log.error('ERROR: Cannot download the dataGroups.');
                });
            }

            function loadUserGroups() {
                userGroupService.getAll().then(function(data) {
                    $scope.userGroups = data;
                }, function(err) {
                    $log.error('ERROR: Cannot download the userGroups.', err);
                });
            }

            function getProjects() {
                workspaceService.getAll().then(function(spaces) {
                    $scope.projects = spaces;
                }, function(err) {
                    $log.error('ERROR: Cannot download the projects.', err);
                });
            }

            function saveProject() {
                //id, title, description, dataGroups, members
                workspaceService.updateWorkspace(
                  $scope.currentProject._id,
                  $scope.projectName,                                       // Name
                  $scope.projectDesc,                                       // Description
                  _.map(_.filter($scope.dataGroups, {added:true}), '_id'),  // Data Groups
                  _.filter( $scope.allUsers, {addedToUsers:true})           // Members
                ).then(function(workspaceDef){
                    //console.log("ProjectEditWizard - Update Complete", projectDef);
                    workspaceService.setCurrent(workspaceDef);
                    closeEdit();
                }).catch(function(err){
                    //console.error("ProjectEditWizard - Update Failed", err);
                    $scope.enableForm(false);
                });
            }

            function closeEdit(){
                $state.transitionTo("app.project.settings");
                $uibModalInstance.close();
                $scope.enableForm(true);
                $state.reload();
            }

            getProjects();
            loadDataGroups();
        });
    });
});