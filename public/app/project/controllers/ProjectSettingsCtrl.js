/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['project/module', 'lodash'], function(module, _) {
    "use strict";

    module.registerController('ProjectSettingsCtrl', function($scope, $rootScope, $uibModal, $q, $log, $state, userGroupService, sourcesService, workspaceService, SessionService, communityService, dataGroupService) {
        // Holds Owner Information
        $scope.projectDataGroup = {};
        $scope.isAdmin = SessionService.isAdmin();

        $scope.currentProject = workspaceService.getCurrent();

        //refresh info if changes detected in workspaces
        $scope.$watch(
            function() { return workspaceService.lastKnownModification },
            function() { getProjectDetails(); }
        );

        function getProjectDetails() {
            var titleFound = false,
                createFound = false,
                ownerFound = false,
                dataFound = false,
                userFound = false;
            
            /**
             * Get the dataGroup with the specified id.
             * @param {String} id The identifying hex for the desired data group.
             */
            function getDataGroup(id) {
                dataGroupService.get(id).then(function(data) {
                    sourcesService.getSourcesByCommunity(id).then( function(sources) {
                        if(_.has(sources, id)) {
                            data.sources = sources[id];
                        } else {
                            data.sources = [];
                        }
                        $scope.dataGroups.push(data);
                    });
                    dataFound = true;
                }, function(err) {
                    $log.error('ERROR: Cannot download the dataGroup with the id: ', id);
                });
            }
            
            $q.when(workspaceService.getCurrent()).then(function(currentProject){
                $scope.currentProject = currentProject;
                // If a project is currently opened, then update the project information.
                if($scope.currentProject) {
                    // Read project title
                    if($scope.currentProject.title) {
                      $scope.projectTitle = $scope.currentProject.title;
                      titleFound = true;
                    }
                    // Read project description
                    $scope.projectDesc = $scope.currentProject.description ? $scope.currentProject.description : "";
                    // Read project creation date
                    if($scope.currentProject.created && $scope.currentProject.created.length >= 16) {
                        $scope.projCreationDate = $scope.currentProject.created.substring(0, $scope.currentProject.created.length - 16);
                        $scope.projCreationTime = $scope.currentProject.created.substring($scope.currentProject.created.length - 15, $scope.currentProject.created.length);
                        createFound = true;
                    }
                    // Read project owner display name
                    if($scope.currentProject.owner && $scope.currentProject.owner.displayName) {
                        $scope.projectDataGroup.ownerDisplayName = $scope.currentProject.owner.displayName;
                        $scope.projectDataGroup.ownerId = $scope.currentProject.owner._id;
                        ownerFound = true;
                        $scope.showEditSettings = SessionService.getUserId() === $scope.projectDataGroup.ownerId || $scope.isAdmin;
                    }
                    
                    // Read the project Data & Data Groups
                    if(workspaceService.isReady()) {
                        _.each(workspaceService.getDataGroupIds(), function(i){
                            getDataGroup(i);
                        })
                    }
                    
                    // Read the User & User Groups and the Access settings
                    var workspaceGroupId = $scope.currentProject.workspaceGroupId;
                    if(workspaceGroupId) {
                        dataGroupService.get(workspaceGroupId).then(function(data) {
                            // Get the User & User Groups
                            if(data.members) {
                                $scope.allUsers = _.reject(data.members, {userType:"owner"});
                                userFound = true;

                                //add [members] of the usergroup to each allUser item that is a user group, so we can display them
                                userGroupService.getAll().then(function(userGroups) {
                                    _.each($scope.allUsers, function(item) {
                                        if(item.type === "user_group"){
                                            var members;
                                            members = _.map(_.filter(userGroups, {_id : item._id}), 'members')[0];
                                            item.members = members ? members : [];
                                        }
                                    });
                                });

                                $scope.userCounts = {
                                    user: _.filter($scope.allUsers, function(i){return i.email}),
                                    group: _.reject($scope.allUsers, 'email')
                                }
                            }
                        }, function(err) {
                            alert("Sorry, it appears that this Project Data Group ID no longer exists. Please choose another one.");
                            $log.error('ERROR: Cannot download the project data group with the id: ', workspaceGroupId);
                        });
                    }
                }
                // If a project title wasn't found, set the default title.
                if(!titleFound) $scope.projectTitle = "Untitled Project";
                // If a creation date wasn't found, set the default creation date and time.
                if(!createFound) {
                    $scope.projCreationDate = "Creation Date Unspecified";
                    $scope.projCreationTime = "Creation Time Unspecified";
                }
                // If a project owner wasn't found, set the default owner.
                if(!ownerFound) $scope.projectDataGroup.ownerDisplayName = "Owner Unspecified";
                // Holds Help popover
                $scope.helpPopoverTxt = "Please contact " + $scope.projectDataGroup.ownerDisplayName + ", owner of this project, to make any edits or updates.";
                // If project data groups weren't found, set an empty array of data groups.
                if(!dataFound) $scope.dataGroups = [];
                // If project user and user groups weren't found, set an empty array of user and user groups.
                if(!userFound) {
                    $scope.allUsers = [];
                    $scope.userCounts = { user: 0, group: 0 };
                }
            });
        }
        //getProjectDetails();

        $scope.editProjectSettings = function() {
            var modalInstance = $uibModal.open({
                templateUrl: 'app/project/views/project-edit.html',
                size: 'lg',
                backdrop: 'static',
                controller: 'ProjectEditWizardCtrl'
            });

            modalInstance.result.then(function () {
                $log.info('Modal closed at: ' + new Date());
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    });
});