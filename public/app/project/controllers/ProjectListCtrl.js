/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['project/module', 'lodash', 'notification'], function(module, _){
    "use strict";


    module.registerController('ProjectListCtrl', function($rootScope, $scope, $state, $log, SessionService, workspaceService, communityService,
                                                          $location, $uibModal, $uibModalInstance, utilityService){

        $scope.dev = $location.$$search.dev;
        $scope.isAdmin = SessionService.isAdmin();
        $scope.currUserId = SessionService.getUserId();

        var projectListPromise = null;

        $scope.loadingMessage = 'Loading Data...';
        $scope.loadingPromise = null;

        //Bind the projects list to the workspaceService cached list
        // using the lastKnownModification timestamp to determine updates
        $scope.$watch(
          function() { return workspaceService.lastKnownModification },
          function() {
            $log.debug("[ProjectListCtrl] Project list changed");
            loadProjects();
          }
        );

        $scope.projects = [];

        $scope.options = {
          columnFilters: false,
          tableFilter: true,
          userDisplayLengthOptions: [10,25,50,100],
          predicate: 'titleFilter',
          sortingReverse: true,
          resetPageOnDataReload: false
        };
        $scope.filters = [
          {
            model: "titleFilter",
            colHeader: "Title",
            dataProperty: "title",
            sortProperty: function(d){
              return _.trim(d.title.toLowerCase());
            },
            enabled: true
          },
          {
            model: "dateFilter",
            colHeader: "Creation Date",
            dataProperty: function(d){
              return d.created;
            },
            sortProperty: function(d){
              return new Date(d.created).valueOf();
            },
            enabled: true
          },
          {
            model: "ownerFilter",
            colHeader: "Owner",
            dataProperty: function(d){
              return d.owner.displayName;
            },
            sortProperty: function(d){
              return _.trim(d.owner.displayName.toLowerCase());
            },
            enabled: true
          },
          {
            model: "descriptionFilter",
            colHeader: "Description",
            dataProperty: "description",
            enabled: true
          },
          {
            model: "actionFilter",
            colHeader: "Actions",
            dataProperty: "title",
            enabled: true,
            template: "<td class=\"action-button-td\">"
                          +"<div class=\"btn-group\" ng-if=\"item.actionButtons.length > 0\">"
                            +"<button class=\"{{item.actionButtons[0].buttonClass}}\" ng-click=\"item.actionButtons[0].click()\" ng-class=\"{'no-dropdown-button': item.actionButtons.length < 2, 'action-button-dropdown': item.actionButtons.length > 1}\">"
                              +"<i class=\"{{item.actionButtons[0].iconClass}}\"></i>"
                              +"{{item.actionButtons[0].text}}"
                            +"</button>"
                            +"<button ng-if=\"item.actionButtons.length > 1\" class=\"{{item.actionButtons[0].buttonClass}} dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"caret\"></span></button>"
                            +"<ul class=\"dropdown-menu\" dropdown-append-to-body>"
                              +"<li ng-repeat=\"button in item.actionButtons.slice(1)\">"
                                +"<a ng-click=\"button.click()\"><i class=\"{{button.iconClass}}\"></i>{{button.text}}</a>"
                              +"</li>"
                            +"</ul>"
                          +"</div>"
                        +"</td>"
          }
        ];
      
        $scope.deleteProject = function(project){
          $rootScope.transmitting = true;
          var id =  project._id,
              title = project.title;
          var confirmationModal = utilityService.confirmationModal("Are you sure you want to delete " + title + "?", //text to display in the confirmation popup
            'Confirm', //text of the confirm/ok button
            'Cancel', //text of the cancel/close button
            'Confirm Delete'); // header text

          confirmationModal.result.then(
            // On close (user confirmed, perform delete)
            function () {
              workspaceService.removeWorkspace(id).then(
                function(ignored){ $scope.enableForm(true);},
                function(ignored){ $scope.enableForm(false);}
              );
            },
            // On dismiss
            function () {
              $rootScope.transmitting = false;
            }
          );
        };

        $scope.openProject = function(project) {
          //True if a change occurred.
          if(workspaceService.setCurrent(project)) {
            $uibModalInstance.close();
            $state.go($rootScope.defaultLocation, { urlWorkspaceId: project._id });
          }
        };

        function loadProjects(){
          
          projectListPromise = workspaceService.getAll();

          $log.debug("[ProjectListCtrl.loadProjects]");
          projectListPromise.then(function(projects){
              // Remove all personal workspaces
              projects = _.reject(projects, function(project) {
                return project.isManaged;
              });

              _.each(projects,function(item){
                var authorized;
                item.actionButtons = [];

                if ($scope.isAdmin || item.owner._id === $scope.currUserId) {
                  authorized = true;
                }

                item.actionButtons.push(
                  {
                    text: ' Launch',
                    buttonClass: 'btn btn-success btn-sm',
                    iconClass: 'fa fa-arrow-circle-right',
                    click: function() {
                      $scope.openProject(item);
                    }
                  }
                );

                if (authorized && !item.isManaged) {
                  item.actionButtons.push(
                    {
                      text: ' Delete',
                      buttonClass: 'btn btn-danger btn-sm',
                      iconClass: 'fa fa-times-circle',
                      click: function() {
                        $scope.deleteProject(item);
                      }
                    }
                  );
                }
              });

              //Set data to scope, table will re-init
              $scope.projects = _.cloneDeep(projects);
          }, function(err) {
              $log.error('ERROR: Cannot download the projects. ',err)
          });

          $scope.loadingPromise = projectListPromise;
        }

        $scope.projectModal = function () {
          var modalInstance = $uibModal.open({
              templateUrl: 'app/project/views/project-create.html',
              backdrop: 'static',
              size: 'lg',
              controller: 'ProjectCreateWizardCtrl'
          });

          modalInstance.result.then(function () {
              $log.info('Modal closed at: ' + new Date());
          }, function () {
              $log.info('Modal dismissed at: ' + new Date());
          });

        };

        $scope.enableForm = function(success) {
            if(success) {
                $.smallBox({
                    title: "Success! Server updated",
                    color: "#739E73",
                    timeout: 2000,
                    iconSmall: "fa fa-check",
                    number: "2"
                });
            } else {
                $.smallBox({
                    title: "Error! Server update unsuccessful",
                    color: "#ed1c24",
                    timeout: 2000,
                    iconSmall: "fa fa-exclamation-circle",
                    number: "2"
                });
            }
            $rootScope.transmitting = false;
        };

    });
});