/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['project/module', 'lodash', 'notification'], function(module, _){
    "use strict";

    /**
     * @ngdoc controller
     * @name app.project.controller:ProjectCreateWizardCtrl
     * @description
     * Project creation wizard
     */
    module.registerController('ProjectCreateWizardCtrl', function($scope, $rootScope, $uibModalInstance, $log, $state,
                                                                  workspaceService, dataGroupService, userGroupService, SessionService, sourcesService){

        $scope.nextStep = false;
        $scope.stepTwoValid = false;
        $scope.ownerDisplayName = SessionService.getUserDisplayName();
        $scope.currUserId = SessionService.getUserId();

        $scope.wizard1CompleteCallback = function(wizardData){
            console.log('wizard1CompleteCallback', wizardData);
            $rootScope.transmitting = true;
            $.smallBox({
                title: "Please wait, updating server",
                color: "#3276B1",
                iconSmall: "fa fa-bell swing animated",
                timeout: 2000,
                number: "1"
            }, function () {
                //scope.eg2();
            });

            saveProject().then(
              function(project){
                console.log("[ProjectCreateWizardCtrl] Save success", project);
                workspaceService.setCurrent(project);
                $state.reload().then(function(){
                  //$state.transitionTo("app.project.list");
                  $state.go($rootScope.defaultLocation);
                });
                $uibModalInstance.close();
                $scope.enableForm(true);
              },
              function(err){
                $log.error('ERROR: Cannot save project', err);
                $scope.enableForm(false);
              }
            );
        };

        $scope.enableForm = function(success){
          if(success){
            $.smallBox({
                title: "Success! Server updated",
                color: "#739E73",
                timeout: 2000,
                iconSmall: "fa fa-check",
                number: "2"
            });
          } else {
            $.smallBox({
                title: "Error! Server update unsuccessful",
                color: "#ed1c24",
                timeout: 2000,
                iconSmall: "fa fa-exclamation-circle",
                number: "2"
            });
          }
          $rootScope.transmitting = false;
        };

        $scope.updateDGs = function(){
          $scope.stepTwoValid = _.filter($scope.dataGroups, {added:true});
        };

        function loadDataGroups(){
          return dataGroupService.getAll().then(function ( dataGroups ){
            //get sources and map them to datagroups
            sourcesService.getUserSources().then( function(sources) {
                _.each(dataGroups, function(dataGroup) {
                    dataGroup.sources = _.filter(sources, {communityIds : [dataGroup._id]});
                });
            });
            $scope.allDataGroups = dataGroups;
            $scope.dataGroups = _.chain(dataGroups)
              .reject({isPersonalCommunity: true})
              .reject({description: '__projectShare'})
              .reject({tags:['workspaceMaster']})
              .value();

            loadUserGroups();
            return dataGroups; //resolve with dataGroups
          }, function(err) {
            $log.error('ERROR: Cannot download the dataGroups. Error: ', err);
            return $q.reject(err);
          });
        }

        function loadUserGroups(){
          return userGroupService.getAll().then(function(userGroups) {
            $scope.userGroups = userGroups;
            return userGroups; //resolve with userGroups
          }, function(err) {
            $log.error('Cannot download the userGroups. Error: ', err);
            return $q.reject(err);
          });
        }

        function getProjects(){
          return workspaceService.getAll().then(function(spaces){
            $scope.projects = spaces;
            return spaces;
          }, function(err) {
            $log.error('Cannot download the projects. Error: ', err);
            return $q.reject(err);
          });
        }

        function saveProject(){
          //projectsService.addProject( title, description, dataGroups, members, [projectDataGroupId], [ownerId] )
          return workspaceService.createWorkspace(
            $scope.projectName,                                                         // Name
            $scope.projectDesc,                                                         // Description
            _.map(_.filter($scope.dataGroups, {added:true}), '_id'),                   // Data Groups
            _.map(_.filter($scope.allUsers, {addedToUsers:true}), '_id')               // Users and User Groups
          ).catch(function(err) {
              $log.error('Cannot save the Project. Error: ', err);
              $scope.enableForm(false);
            }
          );
        }

        $scope.stepTwoValid = [];
        getProjects();
        loadDataGroups();
        
    });
});
