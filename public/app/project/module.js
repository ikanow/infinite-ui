/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc overview
 * @name app.project
 * @description
 * Project management UI components
 */
define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-resource'
], function (ng, couchPotato) {
    'use strict';

    var module = ng.module('app.project', [
        'ui.router',
        'ngResource'
    ]);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        var modalInstance;
        $stateProvider
            .state('app.project', {
                abstract:true,
                data: {
                    title: 'Project'
                }
            })
            .state('app.project.list', {
              authenticate: true,
              url: '/project/list',
              resolve: {
                deps: $couchPotatoProvider.resolveDependencies([
                  'project/controllers/ProjectListCtrl',
                  'directives/recentProjects'
                ]),
                dev: ['$stateParams', function($stateParams){
                  return $stateParams.dev;
                }]
              },
              onEnter: function($stateParams, $state, $uibModal, $log, $rootScope) {
                var previousState = $rootScope.previousState;
                modalInstance = $uibModal.open({
                  templateUrl: 'app/project/views/project-modal.html',
                  backdrop: 'static',
                  windowClass: 'manager-modal-window',
                  controller: 'ProjectListCtrl'
                });

                modalInstance.result.then(function () {
                  if (previousState.name === "login") {
                    $state.transitionTo($rootScope.defaultLocation);
                  } else {
                    $state.transitionTo(previousState);
                  }
                }, function () {
                  if (previousState.name === "login") {
                    $state.transitionTo($rootScope.defaultLocation);
                  } else {
                    $state.transitionTo(previousState);
                  }
                });
              }
            })
            .state('app.project.none', {
                authenticate:true,
                url: '/project/none',
                views: {
                    "content@app": {
                        templateUrl: 'app/project/views/no-project.html'
                    }
                }
            })
            .state('app.project.create', {
                authenticate:true,
                url: '/project/create',
                onEnter: ['$stateParams', '$state', '$uibModal', '$resource', '$log',
                    function($stateParams, $state, $uibModal, $resource, $log) {
                        modalInstance = $uibModal.open({
                            templateUrl: 'app/project/views/project-edit.html',
                            backdrop: 'static',
                            resolve: {
                              deps: $couchPotatoProvider.resolveDependencies([
                                    'modules/forms/directives/validate/smartValidateForm',
                                    'modules/forms/directives/wizard/smartFueluxWizard',
                                    'modules/forms/directives/input/smartMaskedInput',
                                    'directives/addUserUserGroups',
                                    'project/controllers/ProjectCreateWizardCtrl'
                                ])
                            },
                            size: 'lg',
                            controller: 'ProjectCreateWizardCtrl'
                        });
                        modalInstance.result.then(function() {
                            //modal success
                            $log.debug('Modal closed at: ' + new Date());
                        }, function() {
                            //modal dismiss
                            $log.debug('Modal dismissed at: ' + new Date());
                        });
                    }
                ],
                onExit: function() {
                    if (modalInstance) {
                        modalInstance.close();
                    }
                }
            })
            .state('app.project.settings', {
                authenticate: true,
                url: '/:urlWorkspaceId/project/settings',
                params: {
                    urlWorkspaceId: null
                },
                views: {
                    "content@app": {
                        controller: 'ProjectSettingsCtrl',
                        templateUrl: 'app/project/views/project-settings.html',
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                'project/controllers/ProjectSettingsCtrl'
                            ])
                        }
                    }
                },
                data:{
                    title: 'Settings'
                }
            })
            .state('app.project.edit', {
                    authenticate:true,
                    url: '/project/edit',
                    onEnter: ['$stateParams', '$state', '$uibModal', '$resource', '$log',
                        function($stateParams, $state, $uibModal, $resource, $log) {
                            $uibModal.open({
                                templateUrl: 'app/project/views/project-edit.html',
                                resolve: {
                                  deps: $couchPotatoProvider.resolveDependencies([
                                        'modules/forms/directives/validate/smartValidateForm',
                                        'modules/forms/directives/wizard/smartFueluxWizard',
                                        'modules/forms/directives/input/smartMaskedInput',
                                        'project/controllers/ProjectEditWizardCtrl'
                                    ])
                                },
                                size: 'lg',
                                controller: 'ProjectEditWizardCtrl'
                            }).result.then(function() {
                                //modal success
                                $log.debug('Modal closed at: ' + new Date());
                            }, function() {
                                //modal dismiss
                                $log.debug('Modal dismissed at: ' + new Date());
                            });
                        }
                    ]
                });
    });

    couchPotato.configureApp(module);

    module.run(function($couchPotato){
        module.lazy = $couchPotato;
    });

    return module;
});