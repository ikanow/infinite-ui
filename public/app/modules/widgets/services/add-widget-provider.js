/**
 * @ngdoc service
 * @name app.widgets.service:widgetProvider
 * @description
 * Provider service to dynamically add widgets.
 */
define([
    'angular',
    'modules/widgets/module'
    ],

    function (angular, module) {

    "use strict";

    return module.registerFactory('widgetProvider', function ($q, $log, $compile) {

        var blankWidget =
            "<div data-widget-colorbutton='false' data-widget-editbutton='false' data-widget-color='blueDark' data-jarvis-widget data-widget-deletebutton='false' data-widget-togglebutton='false' data-widget-fullscreenbutton='false'>"
            +"<header>"
            +"<span class='widget-icon'> <i class='fa fa fa-table'></i> </span>"
            +"<h2>New Widget</h2>"
            +"</header>"
            +"<div>"
            +"<div class='widget-body no-padding'>"
            +"<span>Added a new Widget Frame</span>"
            +"<div>"
            +"</div>"
            +"</div>"
            ;

        /**
        * Add Jarvis Widget Frame to an element, $compile into Angular scope 
        * @param {Angular Scope Object} scope
        * @param {String jquery selector | angular.element} element to append the widget to
        */
        function addWidget(scope, element){
            if(!scope){
                $log.warn("Cannot append widget without passing scope to addWidget")
            }
            var parentElement = typeof element === 'string' ? angular.element(element) : element;
            var childElement = parentElement.append($compile(blankWidget)(scope));
            	return true;
        }


        return {
        	'addWidget': addWidget
        };

    });

});
