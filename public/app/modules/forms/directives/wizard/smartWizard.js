define(['modules/forms/module', 'lodash'], function (module, _) {

    'use strict';

    return module.registerDirective('smartWizard', function ($rootScope, $log, communityService) {
        return {
            restrict: 'A',
            // scope: {
            //     'smartWizardCallback': '&'
            // },
            link: function (scope, element, attributes) {

                var stepsCount = $('[data-smart-wizard-tab]').length;
                var currentStep = 1;
                var validSteps = [];
                var $form = element.closest('form');
                var $prev = $('[data-smart-wizard-prev]', element);
                var $next = $('[data-smart-wizard-next]', element);

                $form.validate(angular.extend({
                    // Rules for form validation
                    rules: {
                        projectName: {
                            required: true
                        },
                        tagsInput: {
                            required: true,
                            tagsInput: true
                        }
                    },
                    // Messages for form validation
                    messages: {
                        projectName: {
                            required: 'Please enter a project name'
                        }
                    },
                    ignore: '.ignore-validation, :hidden',
                    errorElement: 'em',
                    errorClass: 'invalid',
                    highlight: function(element, errorClass, validClass) {
                        if(!($(element).attr('placeholder') == 'Filter')){
                            $(element).addClass(errorClass).removeClass(validClass);
                            $(element).parent().addClass('state-error').removeClass('state-success');
                        }
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        if(!($(element).attr('placeholder') == 'Filter')){
                            $(element).removeClass(errorClass).addClass(validClass);
                            $(element).parent().removeClass('state-error').addClass('state-success');
                        }
                    },
                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());
                    }

                }, $form.validateOptions));

                function setStep(step) {
                  if(!scope.projectDataGroup && step === 2){
                      addUserGroup()
                  } else {
                      currentStep = step;
                      $('[data-smart-wizard-pane=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-pane]').removeClass('active');
                      $('[data-smart-wizard-tab=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-tab]').removeClass('active');

                      if (step === 4) {
                          $next.find('a').text("Save").addClass("btn-success").removeClass("txt-color-darken");
                      } else {
                          $next.find('a').text("Next").removeClass("btn-success").addClass("txt-color-darken");
                      }

                      $prev.toggle(step != 1)
                  }
                }
                    //temporarily remove clicking the "step" buttons to navigate the wizard
//                    element.on('click', '[data-smart-wizard-tab]', function (e) {
//                    if(!scope.nextStep){
//                        e.preventDefault();
//                        return false;
//                    }
//
//                    if(currentStep == 2 && !scope.stepTwoValid && $(this).attr('data-smart-wizard-tab') != '1'){
//                        e.preventDefault();
//                        return false;
//                    }
//
//                    if(currentStep == 1 && scope.nextStep){
//                        validSteps = _.without(validSteps, currentStep);
//                        validSteps.push(1);
//                        element.find('[data-smart-wizard-tab=1]')
//                            .addClass('complete')
//                            .find('.step')
//                            .html('<i class="fa fa-check"></i>');
//                        setStep(2);
//                        e.preventDefault();
//                        return false;
//                    }
//
//                    validSteps = _.without(validSteps, currentStep);
//                    validSteps.push(currentStep);
//                    if(parseInt($(this).data('smartWizardTab')) > currentStep){
//                        element.find('[data-smart-wizard-tab=' + currentStep + ']')
//                            .addClass('complete')
//                            .find('.step')
//                            .html('<i class="fa fa-check"></i>');
//                    }
//                    setStep(parseInt($(this).data('smartWizardTab')));
//                    e.preventDefault();
//                });


                element.on('click', '[data-smart-edit-btn]', function (e) {
                    setStep(parseInt($(this).attr('data-smart-edit-btn')));
                    e.preventDefault();
                });

                $next.on('click', function (e) {
                    if(currentStep == 2 && scope.stepTwoValid.length <= 0){
                        scope.nextStep = false;
                        e.preventDefault();
                        return false;
                    }

                    if ($form.data('validator')) {
                        if (!$form.valid()) {
                            validSteps = _.without(validSteps, currentStep);
                            $form.data('validator').focusInvalid();
                            return false;
                        } else {
                            validSteps = _.without(validSteps, currentStep);
                            validSteps.push(currentStep);
                            element.find('[data-smart-wizard-tab=' + currentStep + ']')
                                .addClass('complete')
                                .find('.step')
                                .html('<i class="fa fa-check"></i>');
                        }
                    }
                    if (currentStep < stepsCount) {
                        setStep(currentStep + 1);
                    } else {
                        if (validSteps.length < stepsCount) {
                            var steps = _.range(1, stepsCount + 1)

                            _(steps).forEach(function (num) {
                                if (validSteps.indexOf(num) == -1) {
                                    console.log(num);
                                    setStep(num);
                                    return false;
                                }
                            })
                        } else {
                            var data = {};
                            _.each($form.serializeArray(), function(field){
                                data[field.name] = field.value
                            });
                            scope.wizard1CompleteCallback(data)
                        }
                    }

                    e.preventDefault();
                });

                $prev.on('click', function (e) {
                    if (!$prev.hasClass('disabled') && currentStep > 0) {
                        setStep(currentStep - 1);
                    }
                    e.preventDefault();
                });

                function addUserGroup(){
                  communityService.curDataGroupId = '';
                  $rootScope.transmitting = true;
                  var dataGroupObj = {
                    name: scope.projectName + "_PROJECTDATAGROUP",
                    description: "__projectShare",
                    tags: "__projectShare",
                    type: "data"
                  }
                  communityService.addCommunity(dataGroupObj).then(function(data){
                    scope.projectDataGroup = data;
                    scope.projectDataGroup.communityAttributes = communityAttributes;
                    communityService.curDataGroupId = data._id;
                    scope.enableForm(true);
                    setStep(2);
                  }, function(err){
                    $log.error('ERROR: Cannot download the dataGroups.')
                    scope.enableForm(false);
                  });
                }

                setStep(currentStep);

            }
        }
    });
});
