/**
 * @ngdoc overview
 * @name app
 * @description
 * Ikanow View - HTML5 front end for Infinit.e platform.
 */
define([
  'angular',
  'angular-couch-potato',
  'angular-ui-router',
  'angular-animate',
  'angular-bootstrap',
  'smartwidgets',
  'notification',
  'angular-ui-codemirror',
  'angular-busy',
  'angular-sanitize',
  'infinite-api'
], function (ng, couchPotato) {
    "use strict";
    var app = ng.module('app', [
        'ngSanitize',

        'scs.couch-potato',
        'ngAnimate',
        'ui.router',
        'ui.bootstrap',
        'jmdobry.angular-cache',
        'ui.codemirror',
        // App
        'app.auth',
        'app.layout',
        'app.manager',
        'app.project',
        'app.source',
        'app.report',
        'app.tables',
        'app.widgets',
        'app.misc',
        'app.search',
        'cgBusy',
        'infinite-api'
    ]);

    couchPotato.configureApp(app);

    app.config(function ($provide, $httpProvider, $logProvider, $locationProvider, $angularCacheFactoryProvider) {

        // Intercept http calls.
        $provide.factory('ErrorHttpInterceptor', function ($q, $injector, $angularCacheFactory, SessionMonitor) {
            var errorCounter = 0;

            var isSuccessfulAPICall = function(res) {
              return !angular.isUndefined(res.data.response) && !angular.isUndefined(res.data.response.success);
            };

            function notifyError(rejection){
                $.bigBox({
                    title: rejection.status + ' ' + rejection.statusText,
                    content: rejection.data,
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    number: ++errorCounter,
                    timeout: 6000
                });
            }

            return {
                //request: function(req) {
                //  authToken = $angularCacheFactory.get(appConfig.appCacheName).get(appConfig.cacheKeys.authToken);
                //  if (req.url.indexOf(appConfig.apiBaseDomain) > -1 && !angular.isUndefined(authToken)) {
                //    //req.headers[appConfig.authTokenHeaderName] = authToken;
                //    var sym = _.indexOf(req.url, '?') >= 0 ? "&" : "?";
                //    req.url = req.url + sym + appConfig.authTokenHeaderName + '=' + authToken;
                //  }
                //
                //  return req || $q.when(req);
                //},

                // On request failure
                requestError: function (rejection) {

                  // show notification
                  notifyError(rejection);

                  // Return the promise rejection.
                  return $q.reject(rejection);
                },

                response: function(res) {
                  if (isSuccessfulAPICall(res)) {
                    $angularCacheFactory.get(appConfig.appCacheName).put(appConfig.cacheKeys.lastActionTimestamp, new Date().getTime());
                    if (!res.config.url.match(/.*\/auth\/logout$/)
                        && !res.config.url.match(/.*\/social\/workspace$/)
                        && !res.config.url.match(/.*\/social\/group\/data$/)
                        && !res.config.url.match(/.*\/social\/group\/user$/)) {
                      SessionMonitor.reset();
                    }
                  }
                  return res || $q.when(res);
                },

                // On response failure
                responseError: function (rejection) {
                  if(rejection.status === 401){
                    var authService = $injector.get('authService');
                    var $uibModalStack = $injector.get('$uibModalStack');
                    $uibModalStack.dismissAll();
                    authService.logout().then(function(){
                      var $state = $injector.get('$state');
                      $state.transitionTo("login");
                    });
                    rejection.data = "You've been logged out due to inactivity";
                  } else if(rejection.status == 404) {
                    rejection.data = "Requested resource does not exist: " + (rejection.config.url || "");
                  } else if( rejection.data && rejection.data.response && rejection.data.response.message ){
                      rejection.data = rejection.data.response.message
                  }

                  // show notification
                  // TODO: Improve messaging
                  // The message may be incorrect. But it's better than
                  // "this should not have happened, so i'll log you out"

                  notifyError(rejection);

                  // Return the promise rejection.
                  return $q.reject(rejection);
                }
            };
        });

        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('ErrorHttpInterceptor');

        $logProvider.debugEnabled(true);

        $angularCacheFactoryProvider.setCacheDefaults({
          maxAge: 3600000, // Items added to this cache expire after 1 hour.
          storageMode: 'localStorage' // This cache will sync itself with HTML5 Local Storage.
        });

        /**
         * Watch the application cache for update events. An update
         * event means that one or more of the files declared in the manifest
         * have a pending update.
         */

        if (!!window.applicationCache) {
          window.applicationCache.addEventListener('updateready', function () {
            console.log('appCache status: ' + window.applicationCache.status);

            if (window.applicationCache.status === window.applicationCache.UPDATEREADY) {
              window.applicationCache.swapCache();
              window.applicationCache.update();
              window.location.reload();
            }
          });
        }
        else {
          console.warn('Browser does not support HTML5 application cache');
        }

    });

    app.run(function ($couchPotato, $rootScope, $state, $uibModalStack, $stateParams, $angularCacheFactory, $log, $location, authService, SessionMonitor, workspaceService, SessionService, utilityService) {

        // Setup a cache factory for the services to use. The services
        // will 'get' the factory as they need it using the configuration
        // parameter 'appCacheName'
        $angularCacheFactory(appConfig.appCacheName);

        app.lazy = $couchPotato;
        $rootScope.currentYear = new Date().getFullYear();
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.workspaceId = null;

        //default location, IE: logging in, launching a new project
        $rootScope.defaultLocation = 'app.search.results';

        $rootScope.$on("$locationChangeStart", function (event, newUrl, oldUrl) {
          if (!newUrl.match(/.*\/manager\/user/)
            && !newUrl.match(/.*\/manager\/usergroups/)
            && !newUrl.match(/.*\/manager\/datagroups/)
            && !newUrl.match(/.*\/source\/list/)
          ) {
            $uibModalStack.dismissAll();
          }
        });

        $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {

          //$log.debug("[$stateChangeStart]", toState);
          $rootScope.previousState = fromState;

          // make sure that all session timers are stopped when the user is not logged in.
          if(toState.name == "login" || toState.name == "resume") {
            SessionMonitor.stopAll();
            return;
          }

          //If being directed to logout or expired-session, save the last UI-state
          // in root scope, if a user logs back in, they'll be redirected to where they were.
          if(toState.name === "logout" || toState.name === "expired-session") {
            //$log.debug("[$stateChangeStart] Beginning log out");
            if(toState.name === "expired-session") {
              $rootScope.lastActiveSession = fromState;
            }

            // Dismiss any modals that may still be open
            $uibModalStack.dismissAll();
            //Attempts to call API logout, then clears (some) persisted session info.
            authService.logout().then(function(){
              $state.transitionTo("login");
            });
            //event.preventDefault();
            return;
          }

          //Not needed, a keepalive is sent all the time.
          //if the keep alive  fails, it logs the user out and brings them to the login screen
          if (!authService.isAuthenticated() && toState.authenticate !== false) {
            //$log.debug("[$stateChangeStart] Requested state required authentication. Going to login");
            // User isn’t authenticated
            //store the state they are trying to access, so we can direct them there after login
            if(toState.name !== "login" && toState.name !== "resume" && toState.name !== "logout"){
              $rootScope.fromState = toState.name;
              $rootScope.fromParams = toParams;
            }
            $state.transitionTo("login");
            event.preventDefault();
            return;
          } 

          if (authService.isAuthenticated()){
            // Simple state change, user has access, just send a keep alive.
            authService.keepAlive().then(
              function keepAliveSuccess(){
                SessionMonitor.reset();
              },
              function keepAliveFail(err){
                //$log.error("[$stateChangeStart] Keep alive failed, session has expired.", err);
                authService.logout().then(function(){
                  $state.transitionTo("login");
                });
              }
            );
          }

          //$log.debug("[$stateChangeStart] Normal state change, sending keep alive.");

          if(toParams.urlWorkspaceId) {
            workspaceService.getById(toParams.urlWorkspaceId).then(function(space){
              //set current workspace if it is not managed
              if(!space.isManaged) {
                workspaceService.setCurrent(space);
              //if not managed and does not belong to current user, show error
              } else if(space.workspaceUserId !== SessionService.getUserId()) {
                utilityService.confirmationModal("You do not have permission to the workspace you are trying to access", //text to display in the error popup
                  'Close', //text of the close/accept/okay/etc button
                  null, //cancel button text, n/a
                  "Alert") //header text
                .result.then(function(){
                  $state.go($rootScope.defaultLocation);
                }, function(){
                  $state.go($rootScope.defaultLocation);
                });
              } else {
                workspaceService.setCurrent(space);
              }
            }, function(err){
              //workspace did not exist, or user does not have access.
              $log.error('ERROR: ', err);
              utilityService.confirmationModal("The workspace you are trying to access does not exist or you do not have permission", //text to display in the error popup
                  'Close', //text of the close/accept/okay/etc button
                  null, //cancel button text, n/a
                  "Alert") //header text
                .result.then(function(){
                  $state.go($rootScope.defaultLocation);
                }, function(){
                  $state.go($rootScope.defaultLocation);
                });
            });

          }

        });

        //Last thing to do is direct the UI to the correct state.
        // if there's a stored token, try to resume the session
        if( authService.hasPersistedSession() ) {
          $log.debug("App init: Persisted session found. Resuming session.");
          $state.transitionTo('resume', { resumeState: $location.path() });
        } else {
          $log.debug("App init: No persisted session. Default init.");
        }

    });

    //add date methods to detect daylight savings
    Date.prototype.stdTimezoneOffset = function() {
        var jan = new Date(this.getFullYear(), 0, 1);
        var jul = new Date(this.getFullYear(), 6, 1);
        return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
    };

    Date.prototype.isDST = function() {
        return this.getTimezoneOffset() < this.stdTimezoneOffset();
    };

    return app;
});
