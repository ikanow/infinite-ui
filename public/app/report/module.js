/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc overview
 * @name app.report
 * @description
 * Report management UI components
 */
define([
  'angular',
  'angular-couch-potato',
  'angular-ui-router',
  'angular-resource',
  'jspdf'
], function (ng, couchPotato, ngResource, jsPDF) {
  'use strict';

  var module = ng.module('app.report', [
    'ui.router',
    'ngResource'
  ]);

  module.config(function ($stateProvider, $couchPotatoProvider) {
    var modalInstance;
    $stateProvider
      .state('app.report', {
        abstract: true,
        data: {
          title: 'Reports'
        }
      })
      .state('app.report.list', {
        authenticate: true,
        url: '/:urlWorkspaceId/report/list',
        params: {
          urlWorkspaceId: null
        },
        views: {
          "content@app": {
            templateUrl: 'app/report/views/report-list.html',
            controller: "ReportListCtrl",
            resolve: {
              deps: $couchPotatoProvider.resolveDependencies([
                'report/controllers/ReportListCtrl'
              ])
            }
          }
        }
      })
  });

  couchPotato.configureApp(module);

  module.run(function ($couchPotato) {
    module.lazy = $couchPotato;
  });

  return module;
});