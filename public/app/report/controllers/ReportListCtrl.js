/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['report/module', 'lodash'], function(module, _){
  "use strict";

  module.registerController('ReportListCtrl', function($rootScope, $scope, $state, $log, reportService,
                                                       workspaceService, utilityService, InfiniteApi) {

    var loadingPromise = reportService.listReports();

    $scope.reports = [];
    $scope.noDataMessage = null;

    $scope.options = {
      columnFilters: false,
      tableFilter: true,
      userDisplayLengthOptions: [10,25,50,100],
      predicate: 'createdFilter',
      sortingReverse: false,
      resetPageOnDataReload: true
    };

    $scope.filters = [
      {
        model: "createdFilter",
        colHeader: "Creation Date/Time",
        dataProperty: "created",
        sortProperty: function (item) {
          return InfiniteApi.dateToMoment(item.created).unix();
        },
        enabled: true
      },
      {
        model: "titleFilter",
        colHeader: "Title",
        dataProperty: "title",
        enabled: true
      },
      {
        model: "authorFilter",
        colHeader: "Author",
        dataProperty: function (item) {
          return item.owner.displayName;
        },
        enabled: true
      },
      {
        model: "actionFilter",
        colHeader: "Actions",
        dataProperty: "title",
        enabled: true,
        template: "<td class=\"action-button-td\">"
          +"<div class=\"btn-group\" ng-if=\"item.actionButtons.length > 0\">"
          +"<button class=\"{{item.actionButtons[0].buttonClass}}\" ng-click=\"item.actionButtons[0].click()\" ng-class=\"{'no-dropdown-button': item.actionButtons.length < 2, 'action-button-dropdown': item.actionButtons.length > 1}\">"
          +"<i class=\"{{item.actionButtons[0].iconClass}}\"></i>"
          +"{{item.actionButtons[0].text}}"
          +"</button>"
          +"<button ng-if=\"item.actionButtons.length > 1\" class=\"{{item.actionButtons[0].buttonClass}} dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"caret\"></span></button>"
          +"<ul class=\"dropdown-menu\" dropdown-append-to-body>"
          +"<li ng-repeat=\"button in item.actionButtons.slice(1)\">"
          +"<a ng-click=\"button.click()\"><i class=\"{{button.iconClass}}\"></i>{{button.text}}</a>"
          +"</li>"
          +"</ul>"
          +"</div>"
          +"</td>"
      }
    ];
    var init = function () {
      loadingPromise.then( function(reports) {
        reports = _.filter(reports, function (report) {
          if (report.communities.length < 1) {
            return false;
          }
          return report.communities[0]._id === workspaceService.getWorkspaceGroupId();
        });

        angular.forEach(reports, function(report) {
          report.actionButtons = [];
          report.actionButtons.push(
            {
              text: ' View',
              buttonClass: 'btn btn-info btn-sm',
              iconClass: 'fa fa-eye',
              click: function() {
                var reportWindow = window.open();
                reportService.getFile(report._id).then(function (pdfBase64) {
                  reportWindow.location.href = "data:application/pdf;base64," + escape(pdfBase64);
                });
              }
            },
            {
              text: ' Download',
              buttonClass: 'btn btn-success btn-sm',
              iconClass: 'fa fa-download',
              click: function() {
                reportService.getFile(report._id).then(function (pdfBase64) {
                  var hiddenElement = document.createElement('a');
                  hiddenElement.href = 'data:application/pdf;base64,' + escape(pdfBase64);
                  hiddenElement.target = '_blank';
                  hiddenElement.download = report.title + ".pdf";
                  hiddenElement.click();
                });
              }
            },
            {
              text: ' Delete',
              buttonClass: 'btn btn-danger btn-sm',
              iconClass: 'fa fa-times-circle',
              click: function() {
                // Delete

                var confirmationModal = utilityService.confirmationModal("Are you sure you want to delete " + report.title + "?", //text to display in the confirmation popup
                'Confirm', //text of the confirm/ok button
                'Cancel', //text of the cancel/close button
                'Confirm Delete'); //header text

                confirmationModal.result.then(
                  // On close (user confirmed, perform delete)
                  function () {
                    reportService.deleteReport(report._id).then(function () {
                      $scope.reports = _.reject($scope.reports, {_id: report._id});
                    });
                  },
                  // On dismiss
                  function () {
                    //do nothing, user canceled/closed modal
                  }
                );
              }
            }
          );
        });
        $scope.reports = reports;
        if (reports.length < 1) {
          $scope.noDataMessage = 'There are not any reports to view. You may create a new report from the Dashboard.'
        } else {
          $scope.noDataMessage = null
        }
      });
    };
    init();
  });
});