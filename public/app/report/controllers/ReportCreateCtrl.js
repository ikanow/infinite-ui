/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['report/module', 'lodash'], function(module, _){
  "use strict";

  /**
   * @ngdoc controller
   * @name app.report.controller:ReportCreateCtrl
   * @description
   * Report builder controller
   */
  module.registerController('ReportCreateCtrl', function($scope, $rootScope, $timeout, $uibModal, $uibModalInstance, $log,
                                                         $state, $stateParams, reportService){

    var init = function () {
      $scope.title = reportService.currentReport.title;
      $scope.canvas = reportService.currentReport.canvas;
      $scope.description = "";

      // Add the report preview
      $timeout(function () {
        $('#report-content').append($scope.canvas);
        $('canvas').width($('#widget-content').width() - 100);
      }, 1000);
    };

    var createPDF = function (canvas) {
      var canvasOffsetY = 32;
      var canvasOffsetX = 6;
      var logoOffsetY = 1;
      var logoOffsetX = 1;
      var titleOffsetY = 25;
      var titleOffsetX = 6;
      var line1OffsetY = 17;
      var line2OffsetY = 27;

      var canvasImgData = canvas.toDataURL('image/jpeg', 1);

      // Ikanow logo needs to be base64 encoded to be added to PDF
      var logoImgData = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAAA5CAMAAACGeNCUAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDE0IDc5LjE1Njc5NywgMjAxNC8wOC8yMC0wOTo1MzowMiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6N0MwM0ZEMjU2MTUyMTFFNUJEREVEN0I0NEIwNTVFQzIiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6N0MwM0ZEMjQ2MTUyMTFFNUJEREVEN0I0NEIwNTVFQzIiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTQgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0RjZEOEM3OUEwMUUxMUU0QTg3Rjk2NkYzODQ4ODE3RSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0RjZEOEM3QUEwMUUxMUU0QTg3Rjk2NkYzODQ4ODE3RSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjkvDM8AAAJ/UExURf///yFPiixYkE90oktwoH+avHSRtitXjzFbkovDSq7VgWiHrzlily9akau90+3x9cjU4svW42GCrCVSjEpvoClVjiNQiyRSjP7+/zJdkzJck1p8qPH0+JWrxyZTjSJQiuTq8WWFridUjaK1zoigwG6Ms3OQtVN2pImiwTtkmDpjl6u80ihVjlh6p3qVufn6/NLb5+/z9+Pp8E1yoUxxoFt9qS5ZkczW5MnU446lw/3+/s/Z5tzj7SRRi5muyZiuyVl7p32Yu7/M3a2+1HyXukJpm2mIsOXq8T9nmoOdvkhun3GOtDRelCpWj9Pc6PDz91R4pezw9VZ5pp+zzfz9/sHO3ufs8r7L3cDN3uHn7z5mmcXR4LjH2t7l7qq70pesyISevp2yzFV4psbS4X6Zu7vJ21p9qOvv9NTd6bbF2C1ZkNHa59/l7p6yzImhwai60djg6nuWuUNqnFF1o6O2zpCnxO7y9vj5+6e50XmVuLrI29rh63WSt+Lo8D9mmoujwnWRtkZtnnaTt2iIsERrnJKpxmKDrFd6prLC14ykwtbf6b3L3HKPtWODrVB0o+7x9rnI2qS3z6vUfGeGrzNdlMPP33COtJarx8jiqPT2+TxkmG2LssfT4kdtnmqJsfX3+eHo787Y5Z/Oalx+qUVrnUlvn+fs86G0zWyKsYafv6W40Km70qPPcL/N3vL1+JuwyrTE2G6MsitXkNXe6VJ2pKy908rV45CnxU5zouru9ODm7jliljZglY2lw/X3+tri7GCBq0Bom5qvyjdglcvkrUZsnW+Ns/z8/WaGrneTt5Sqx4Ocvc3Y5Y+mxPf5+8TQ4DVflfr7/F1/qmGBq66/1AcqsooAAAPwSURBVHja7JjXXxNBEMdnuNNEBVGSQApdilQpKoqAoiIKqIhg77333hv23nvvvffee/+DnNnbJEZ8gbycfu73kJnZvdl892529xIAQ4YMGTJkyJAhQ4YMGdKndozQK9nwkwBJ2yDMApOTAY7v1Rdd+Q4IQQBMh4AAiCevwXY4nK0jwHwHNGLAegxYnwEb8r3Uj0yBNQHJ+0cAWynKmJ4AGxRF6SQa+pwomjEjOSFc9o9XWE+PyLCHCJVCLYoj9wbZ8LZrigLLnGaz2fFiQHZpDsCb14riyhQXVXHCZC1huqJ0qCWgDTG2F8BURHzP8dES8vDHWNndYhUK9ZbxSC3EWyKKIa8NgL0L+si0EKA5WW3Gi7hJtbObFYaYVEtAM2JQMEBjGqQ9hQsH8HCFle7cZvI7k/ZpcRMZT5jE0QHymgLcz/cFxHUAeRbE72KKS7glcjD7Q8lrW3fA4QAZyTxagSf1ciBifr9r1BbhBXQV0ceYXIriNcCOZIL2jN/Q8VLfBUMSKVhJfWmItsVk20VhpAlxG7mVKqLlSd0BO+dcdZKxdfKmFlD8FtbTk9kZ7AFcd5tn8ZyiUg0whUw3dwpf7CJ7XhZGNeLNFYgxXMChiB2g7oCaurbxZvZ8iBjaDoBr7JMHsDN044c2ESBBA+Q6mOXO2RKEmEj2XmtEPrjKEc9VIToztboc4jfg0N8yl1K8key0VERHLzfgfPhMzwpbzhX1T4C9yUx154Q7EGeys4wmkQfR3REHhdPHVvhGC27nFj8AbWbiwNT2nsTTdBPCFhRHR/cJoY7mbsAVVFiRouGd9w7O8wA6tTsIu0TlzqNxi2EmT5RzC8EPwNmZeRV8DxPmyr4PFIROMJHoDuD1LAm4jGwEd32M89agZ3H2j5WF1uIZ4nLeie7aIY63l/l02TF/AK8APOZ1iWv6i67oAN/No54ETCCb24qcVy7vKnZEDKoaOPBL35QKuYoBptAcZpeJ9dMPMerOAyqLSn8AxbMdxij7c9hN5yWzOoS1mnlMizXAUu6c1FpiE+Dg0D/2waVi6HGyeTTAHKfmFtT+qJMnyQXPVtcsityoM3bI4BEnugfhKoy3i5Oki2hIkTC06HM3+fLFThNX2DeLyManknaFJav2gGmqWkFn8Wiram2stWzdrKqHStZC9SnVOt0zSEa2qirF0ENVrXFayyiryhpH7pwpiUu+moUcySVnD8qctdxvTWf3p3BH/aNvM5aymoCWVB0BLk+rCejqqq+3/hqAelOIL+B23QHOavk74MWXuvzp6QXUqeg1iQB36xfQ/IgBh5n1/NeHrrbnv0lXP9f/pphy4w80Q4YM/f/6JcAAAyShVEm4CBMAAAAASUVORK5CYII=";
      var doc = new jsPDF('p', 'mm');
      var splitTitle = doc.splitTextToSize($scope.title, 180);

      // Set font
      doc.addFont('arial');
      doc.setFont("arial");

      // Draw first HR
      doc.setLineWidth(0.2);
      doc.setDrawColor(85,85,85);
      doc.line(6, line1OffsetY, 204, line1OffsetY);

      // Draw second HR
      doc.setLineWidth(0.2);
      doc.setDrawColor(85,85,85);
      doc.line(6,line2OffsetY, 204, line2OffsetY);

      // Add title
      doc.text(titleOffsetX, titleOffsetY, splitTitle);
      doc.addImage(logoImgData, 'png', logoOffsetX, logoOffsetY);
      doc.addImage(canvasImgData, 'JPEG', canvasOffsetX, canvasOffsetY, 198, 210);

      return btoa(doc.output());
    };

    $scope.savingPromise = null;

    $scope.save = function () {
      $scope.savingPromise = reportService.add($scope.title, $scope.description, createPDF($scope.canvas));

      $scope.savingPromise.then(function () {
        $uibModalInstance.close();
        $scope.modalInstance = $uibModal.open({
          templateUrl: 'app/report/views/save-results-modal.html',
          size: '',
          backdrop: 'static',
          controller: 'ReportCreateCtrl'
        });
      });
    };

    $scope.viewReports = function () {
      $uibModalInstance.dismiss();
      $state.go('app.report.list', {
        urlWorkspaceId: $rootScope.workspaceId
      },{
        reload: true
      });
    };

    $scope.return = function () {
      $uibModalInstance.dismiss();
      $state.reload();
    };

    init();
  });
});
