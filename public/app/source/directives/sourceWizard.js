/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['source/module', 'lodash'], function (module, _) {
    'use strict';

    /**
     * @ngdoc directive
     * @name app.source.directive:sourceWizard
     * @restrict A
     * @description
     * Source creation wizard
     */
    return module.registerDirective('sourceWizard', function () {
        return {
            restrict: 'A',
            //scope: {
            //    'smartWizardCallback': '&'
            //},
            link: function (scope, element, attributes) {

                var stepsCount = $('[data-smart-wizard-tab]').length;
                var currentStep = 1;
                var validSteps = [];
                var propertyLocation;
                var $form = element.closest('form');
                var $prev = $('[data-smart-wizard-prev]', element);
                var $next = $('[data-smart-wizard-next]', element);
                scope.showPublish = false;

                //override jquery validation plugin checkForm function, to work with ng-repeated inputs
                $.validator.prototype.checkForm = function () {
                    var inputs, self = this;
                    self.prepareForm();
                    _.each((this.currentElements = this.elements()),function(item) {

                        inputs = item.name ? $("input[name=" + item.name + "]") : [];
      
                        if(inputs.length > 1){
                            _.each(inputs,function(elem) {
                                if(elem.required){ self.check(elem); }
                            });
                        } else {
                            if(item.required) { self.check(item); }
                        }
                        
                    });
                    return self.valid();
                }

                $.extend($.validator.messages, {
                    required: "This field is required.",
                    remote: "Please fix this field.",
                    email: "Please enter a valid email address.",
                    url: "Please enter a valid URL http://...",
                    date: "Please enter a valid date.",
                    dateISO: "Please enter a valid date (ISO).",
                    number: "Please enter a valid number.",
                    digits: "Please enter only digits.",
                    creditcard: "Please enter a valid credit card number.",
                    equalTo: "Please enter the same value again.",
                    accept: "Please enter a value with a valid extension.",
                    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
                    minlength: jQuery.validator.format("Please enter at least {0} characters."),
                    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
                    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
                    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
                    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
                });

                $form.validate(angular.extend({
                    // Rules for form validation
                    rules: {
                        repeatedname: {
                            required: true
                        },
                        sourcename: {
                            required: true
                        },
                        datagroup: {
                            required: true
                        },
                        url: {
                            required:true,
                            url: true
                        }
                    },
                    // Messages for form validation
                    messages: {
                        repeatedname: {
                            required: 'This field is required'
                        },
                        sourcename: {
                            required: 'Enter a source name'
                        },
                        datagroup: {
                            required: 'Select a data group'
                        },
                        date: {
                            required: ''
                        },
                        time: {
                            required: 'Enter a valid time'
                        },
                        url: {
                            required: '',
                            url: "Please enter a valid URL http://..."
                        },
                        filename: {
                            required: 'You must browse and select a file to upload'
                        }
                    },
                    ignore: '.ignore-validation, :hidden',
                    errorElement: 'em',
                    errorClass: 'invalid',
                    highlight: function(element, errorClass, validClass) {
                        if(!($(element).attr('placeholder') == 'Filter')){
                            $(element).addClass(errorClass).removeClass(validClass);
                            $(element).parent().addClass('state-error').removeClass('state-success');
                        }
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        if(!($(element).attr('placeholder') == 'Filter')){
                            $(element).removeClass(errorClass).addClass(validClass);
                            $(element).parent().removeClass('state-error').addClass('state-success');
                        }
                    },
                    errorPlacement : function(error, element) {
                        error.insertAfter(element.parent());
                    }

                }, $form.validateOptions));

                function setStep(step) {
                    currentStep = step;
                    $('[data-smart-wizard-pane=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-pane]').removeClass('active');
                    $('[data-smart-wizard-tab=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-tab]').removeClass('active');
                    if (step === 3) {
                        $next.find('a').text("Save Source").addClass("btn-success").removeClass("txt-color-darken");
                        scope.showPublish = true;
                        if(scope.sourceType === "Advanced Source Builder" ){
                            scope.cm.refresh();
                            scope.cm.focus();
                            scope.setValues();
                        }
                    } else {
                        $next.find('a').text("Next").removeClass("btn-success").addClass("txt-color-darken");
                        scope.showPublish = false;
                    }
                    $prev.toggle(step != 1)
                }

                $next.on('click', function (e) {
                    
                    if(scope.mock && _.has(scope.mock.meta,'cost')) { scope.getCost() }

                    if ($form.data('validator')) {
                        var children = $form.find('input[readonly]');
                        children.prop('readonly', false);

                        if (!$form.valid()) {
                            validSteps = _.without(validSteps, currentStep);
                            $form.data('validator').focusInvalid();
                            children.prop('readonly', true);
                            e.preventDefault();
                            return false;
                        } else {
                            children.prop('readonly', true);
                            validSteps = _.without(validSteps, currentStep);
                            validSteps.push(currentStep);
                            element.find('[data-smart-wizard-tab=' + currentStep + ']')
                                .addClass('complete')
                                .find('.step')
                                .html('<i class="fa fa-check"></i>');
                        }
                    }
                    if (currentStep < stepsCount) {
                        setStep(currentStep + 1);
                    } else {
                        if (validSteps.length < stepsCount) {
                            var steps = _.range(1, stepsCount + 1)

                            _(steps).forEach(function (num) {
                                if (validSteps.indexOf(num) == -1) {
                                    setStep(num);
                                    return false;
                                }
                            })
                        } else {
                            var data = {};
                            _.each($form.serializeArray(), function(field){
                                data[field.name] = field.value
                            });
                            scope.wizard1CompleteCallback();
                        }
                    }

                    e.preventDefault();
                });

                $prev.on('click', function (e) {
                    if (!$prev.hasClass('disabled') && currentStep > 0) {
                        setStep(currentStep - 1);
                    }
                    e.preventDefault();
                });

                setStep(currentStep);

            }
        }
    });
});
