/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc overview
 * @name app.source
 * @description
 * Data source management UI components
 */
define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-resource',
    'angular-ui-codemirror'
], function (ng, couchPotato) {
    'use strict';

    var module = ng.module('app.source', [
        'ui.codemirror',
        'ui.router',
        'ngResource'
    ]);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        var modalInstance;
        $stateProvider
            .state('app.source', {
                abstract:true,
                data: {
                    title: 'Sources'
                }
            })
            .state('app.manager.modal.sourceList', {
                authenticate:true,
                url: '/source/list/:sourceIdToView',
                params: {
                  searchText: {
                    value: null,
                    squash: true
                  },
                  sourceIdToView: null
                },
                views: {
                  "managerView@": {
                    templateUrl: 'app/source/views/source-list.html',
                    controller: 'SourceListCtrl',
                    resolve: {
                      deps: $couchPotatoProvider.resolveDependencies([
                        'source/controllers/SourceListCtrl',
                        // Editor dependencies:
                        'modules/forms/directives/validate/smartValidateForm',
                        'source/directives/sourceWizard',
                        'modules/forms/directives/wizard/smartFueluxWizard',
                        'modules/forms/directives/input/smartMaskedInput',
                        'modules/forms/directives/input/smartTagsinput',
                        'source/controllers/SourceBuilderCtrl'
                      ]),
                      dev: ['$stateParams', function($stateParams){
                        return $stateParams.dev;
                      }]
                    }
                  }
                },
                data: {
                    title: 'Source List'
                }
            })
            .state('app.source.create', {
                authenticate:true,
                url: '/source/create',
                onEnter: ['$stateParams', '$state', '$uibModal', '$resource', function($stateParams, $state, $uibModal, $resource) {
                    modalInstance = $uibModal.open({
                        templateUrl: 'app/source/views/source-create.html',
                        resolve: {
                          deps: $couchPotatoProvider.resolveDependencies([
                                'modules/forms/directives/validate/smartValidateForm',
                                'source/directives/sourceWizard',
                                'modules/forms/directives/wizard/smartFueluxWizard',
                                'modules/forms/directives/input/smartMaskedInput',
                                'modules/forms/directives/input/smartTagsinput',
                                'source/controllers/SourceBuilderCtrl',
                                'directives/ikNgTags'
                            ])
                        },
                        size: 'lg',
                        backdrop: 'static',
                        controller: 'SourceBuilderCtrl'
                    })
                }],
                onExit: function() {
                    if (modalInstance) {
                        modalInstance.close();
                    }
                }
            })
    });

    couchPotato.configureApp(module);

    module.run(function($couchPotato){
        module.lazy = $couchPotato;
    });

    return module;
});