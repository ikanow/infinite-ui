/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['source/module', 'lodash', 'json3', 'notification'], function(module, _, JSON){
    "use strict";

    /**
     * @ngdoc controller
     * @name app.source.controller:SourceListCtrl
     * @description
     * Controller for source list
     */

    module.registerController('SourceListCtrl', function($scope, $log, $q, $uibModal, $rootScope, $timeout, $stateParams, $state,
        userService, sourcesService, SessionService, utilityService){

      $scope.communities = [];
      $scope.currUserId = SessionService.getUserId();
      $scope.isAdmin = SessionService.isAdmin();

      $scope.sources = [];

      $scope.loadingMessage = 'Loading Table...';
      $scope.loadingPromise = null;

      $scope.tableOptions = {
        columnFilters: false,
        tableFilter: true,
        userDisplayLengthOptions: [10,25,50,100],
        predicate: 'titleFilter',
        sortingReverse: true,
        resetPageOnDataReload: false
      };

      if($stateParams.searchText) {
        $scope.tableOptions.tableFilterValue = $stateParams.searchText;
      }
      if ($stateParams.sourceIdToView) {
        $scope.sourceIdToView = $stateParams.sourceIdToView;
      }

      $scope.filters = [
        {
          model: "titleFilter",
          colHeader: "Source Name",
          dataProperty: "title",
          sortProperty: function(d){
            return _.trim(d.title.toLowerCase());
          },
          enabled: true
        },
        {
          model: "stateFilter",
          colHeader: "State",
          dataProperty: function(d){
            if(d.share){
              return 'saved';
            } else {
              if(d.searchCycle_secs < 0) {
                return 'disabled';
              } else {
                return 'enabled';
              }
            }
          },
          enabled: true,
          template: '<td>'
                      +'<span ng-if="!item.share" class="label label-success" ng-class="{\'label-danger\': item.searchCycle_secs < 0}">{{item.searchCycle_secs < 0 ? \'Disabled\' : \'Enabled\'}}</span>'
                      +'<span ng-if="item.share" class="label label-primary">Saved</span>'
                    +'</td>'
        },
        {
          model: "dateFilter",
          colHeader: "Last Data Update",
          dataProperty: function(d){
            return '';
          },
          enabled: true
        },
        /* removed until further notice
        {
          model: "ownerFilter",
          colHeader: "Owner",
          dataProperty: function(d){
            return d.ownerDisplayName;
          },
          sortProperty: function(d){
            return _.trim(d.ownerDisplayName.toLowerCase());
          },
          enabled: true
        },*/
        {
          model: "originFilter",
          colHeader: "Origin",
          dataProperty: "origin",
          sortProperty: function(d){
            if(d.origin) return _.trim(d.origin.toLowerCase());
          },
          enabled: true
        },
        {
          model: "typeFilter",
          colHeader: "Type",
          dataProperty: "dataType",
          sortProperty: function(d){
            if(d.dataType) return _.trim(d.dataType.toLowerCase());
          },
          enabled: true
        },
        {
          model: "dataGroupFilter",
          colHeader: "Data Group",
          dataProperty: function(d){
            if (d.communities && d.communities[0].name) {
              return _.trim(d.communities[0].name);
            }
          },
          sortProperty: function(d){
            if (d.communities && d.communities[0].name) {
              return _.trim(d.communities[0].name.toLowerCase());
            }
          },
          enabled: true
        },
        {
          model: "actionFilter",
          colHeader: "Actions",
          dataProperty: function(d){
            var authorized;
            if ($scope.isAdmin || (!d.share && d.ownerId === $scope.currUserId) || (d.share && d.owner._id === $scope.currUserId))
              {
                authorized = true;
              }

            if(d.share){
              return 'saved';
            } else {
              if(authorized && d.searchCycle_secs < 0) {
                return 'disabled';
              } else if(authorized) {
                return 'enabled';
              }
              return 'saved';
            }
          },
          enabled: true,
          template: "<td class=\"action-button-td\">"
                        +"<div class=\"btn-group\" ng-if=\"item.actionButtons.length > 0\">"
                          +"<button class=\"{{item.actionButtons[0].buttonClass}}\" ng-click=\"item.actionButtons[0].click()\" ng-class=\"{'no-dropdown-button': item.actionButtons.length < 2, 'action-button-dropdown': item.actionButtons.length > 1}\">"
                            +"<i class=\"{{item.actionButtons[0].iconClass}}\"></i>"
                            +"{{item.actionButtons[0].text}}"
                          +"</button>"
                          +"<button ng-if=\"item.actionButtons.length > 1\" class=\"{{item.actionButtons[0].buttonClass}} dropdown-toggle\" data-toggle=\"dropdown\"><span class=\"caret\"></span></button>"
                          +"<ul class=\"dropdown-menu\" dropdown-append-to-body>"
                            +"<li ng-repeat=\"button in item.actionButtons.slice(1)\" ng-class=\"{'divider': button.text == 'divider'}\">"
                              +"<a ng-click=\"button.click()\"><i class=\"{{button.iconClass}}\"></i>{{button.text}}</a>"
                            +"</li>"
                          +"</ul>"
                        +"</div>"
                      +"</td>"
        }
        ];

      /**
       * Open a modal dialog
       * @param templateUrl
       * @param ctrl
       */
      $scope.openModal = function(templateUrl, ctrl){
        $scope.modalInstance = $uibModal.open({
          templateUrl: templateUrl,
          size: 'lg',
          controller: ctrl,
          backdrop: 'static',
          scope:$scope
        });

        $scope.modalInstance.result.then(function (action) {
          $log.info('Modal closed at: ' + new Date());
          //if an action is passed, do something
          if(action){
            //if they clicked edit source from the source viewer
            if(action === "edit") {
              $scope.openModal('app/source/views/source-editor.html', 'SourceBuilderCtrl');
            }
          //else just reset the activeSource when modal closed
          } else {
            //remove the source ID from link once done
            $state.go('.', {
              sourceIdToView: null
            },{
              notify: false
            });
            sourcesService.activeSource = null;
          }
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
          sourcesService.activeSource = null;
          //remove the source ID from link once done
          $state.go('.', {
            sourceIdToView: null
          },{
            notify: false
          });
        });
      };


      /**
       * Generic event stopper
       * @param event
       */
      function stopEvent(event){
        event.preventDefault();
        event.stopPropagation();
      }

      /**
       *
       * @param event
       * @param source
       */
      function handleDelete(event, source){
        console.log("Dialog for: ", source);
        var modalInstance = utilityService.confirmationModal("Are you sure you want to delete " + source.title +
          "?\nWARNING: Deleting source will delete all previously harvested data.", //text to display in the confirmation popup
          'Confirm', //text of the confirm/ok button
          'Cancel', //text of the cancel/close button
          'Confirm Delete'); //header text

        // On close (user confirmed, perform delete), do nothing on cancel
        modalInstance.result.then( function(){
          if (source.share){
            sourcesService.deleteSourceShare(source._id).then( function() {
              console.log("SHARE DELETED");
              $timeout(function() { $scope.getAllSources(); });
            });
          } else {
            sourcesService.deleteSource(source).then(function(){
              console.log("SOURCE DELETED");
              $timeout(function() { $scope.getAllSources(); });
            });
          }
        });
        stopEvent(event);
      }

      /**
       * Handle Edit events from source rows
       * @param {Event} event
       * @param {SourceDef} source
       */
      function handleEdit(event, source){
        //set URL for deep linking
        $state.go('.', {
          sourceIdToView: source._id
        },{
          notify: false
        });
        if(source.share){
          sourcesService.activeSource = source;
          $scope.openModal('app/source/views/source-editor.html', 'SourceBuilderCtrl');
        } else {
          sourcesService.getHarvestedSource(source._id).then(function(sourceData){
            sourcesService.activeSource = sourceData;
            $scope.openModal('app/source/views/source-editor.html', 'SourceBuilderCtrl');
          });
        }
        stopEvent(event);
      }

      /**
       *
       * @param event
       * @param source
       */
      function handleSuspend(event, source){
        $rootScope.transmitting = true;
        sourcesService.suspendSource(source).then(function(data){
          $timeout(function() { $scope.getAllSources(); });
        }, function(err){
          $rootScope.transmitting = false;
          $log.error('ERROR: ', err);
        });
        stopEvent(event);
      }

      /**
       *
       * @param event
       * @param source
       */
      function handleTest(event, source){
        $scope.loadingMessage = "Testing Source...";
        var src = JSON.parse(source.share);
        $rootScope.transmitting = true;
        ($scope.loadingPromise = sourcesService.testSource(src.documents, src.fullText, src.testUpdates, src))
          .then(function(data){
            $rootScope.transmitting = false;
            $scope.testResultData = data;
            handleEdit(event, source);
          }, function(err){
            $rootScope.transmitting = false;
            $scope.testResultData = err;
            handleEdit(event, source);
            $log.error('ERROR: ', err);
          });
        stopEvent(event);
      }

      /**
       *
       * @param event
       * @param source
       */
      function handleView(event, source){
        //set URL for deep linking
        $state.go('.', {
          sourceIdToView: source._id
        },{
          notify: false
        });
        if(source.share){
          sourcesService.activeSource = source;
          $scope.openModal('app/source/views/source-viewer.html', 'SourceBuilderCtrl');
        } else {
          sourcesService.getHarvestedSource(source._id).then(function(harvesterSource){
            sourcesService.activeSource = harvesterSource
            $scope.openModal('app/source/views/source-viewer.html', 'SourceBuilderCtrl');
          })
        }
        stopEvent(event);
      }

      //Bind events
      $scope.$on('source-delete', handleDelete);
      $scope.$on('source-edit', handleEdit);
      $scope.$on('source-suspend', handleSuspend);
      $scope.$on('source-test', handleTest);
      $scope.$on('source-view', handleView);


      $scope.closeResults = function(){
        this.$close();
      };

      $scope.getAllSources = function(){
        var communityIndex;

        //clear and refresh datatables. both commands are needed. fixes bug where data(table) was not refreshing after a source was updated/changed
        $scope.allSources = [];

        //pull source list and user list (to map owner name)
        //loading message to show until loadingPromise is resolved
        $scope.loadingMessage = 'Loading Data...';

        //Set promise to scope then ....
        ($scope.loadingPromise = $q.all({
          userList: userService.getAll(),
          sourceList: sourcesService.getUserSources(),
          shareList: sourcesService.getSourcesShare()
        })).then(function(results) {
          results.sourceList = _.union(results.sourceList, results.shareList);
          var sourceFound = false, searchSource = $scope.sourceIdToView ? true : false;
          //map ownerId to display name from user list
          _.each(results.sourceList, function(item) {
              var owner;
              //saved sources
              if(item.owner) {
                owner = _.map(_.filter(results.userList, {_id : item.owner._id}), 'displayName')[0];
                item.ownerDisplayName = owner ? owner : item.owner.displayName;
              } else { //published sources
                owner = _.map(_.filter(results.userList, {_id : item.ownerId}), 'displayName')[0];
                item.ownerDisplayName = owner ? owner : "";
              }

              //if a source id was passed in, locate it, apply the search filter, and view it
              if($scope.sourceIdToView && $scope.sourceIdToView == item._id) { 
                $scope.tableFilter = item.title;
                sourcesService.activeSource = item;
                $scope.openModal('app/source/views/source-viewer.html', 'SourceBuilderCtrl');
                $scope.sourceIdToView = null;
                sourceFound = true;
              }
          });

          //if they were deep linked to a source, but did not or exist or do not have access, show error
          if(searchSource && !sourceFound){
            utilityService.confirmationModal("The source you are trying to access does not exist or you do not have permission to view it", //text to display in the error popup
              'Close', //text of the close/accept/okay/etc button
              null, //cancel button text, n/a
              "Alert") //header text
            //remove ID from link
            $state.go('.', {
              sourceIdToView: null
            },{
              notify: false
            });
          }

          $log.info('SUCCESS (SHARES):', results.sourceList);

          results.sourceList = _.filter(results.sourceList, function(item){
              if(item.communityIds) { //check if item is from config/source/good instead of social/share/search
                communityIndex = _.indexOf(_.map($scope.communities,'_id'), item.communityIds[0]);

                //if not an admin, only keep sources that are in a community the user belongs to
                if($rootScope.isAdmin) {
                  return true;
                } else {
                  return communityIndex >= 0;
                }
              } else {
                return true; //always keep the social/share/search results, only returns sources if you're part of the datagroup
              }
          });
          results.sourceList = _.reject(results.sourceList, function (item) {
            // Temp fix to not show the Infinit.e System sources
            return _.some(item.communities, {name: "Infinit.e System"});
          });
          // Don't show sources that are missing a type or auxiliary sources for non-admin
          if (!$scope.isAdmin) {
            results.sourceList = _.reject(results.sourceList, function (item) {
              return !item.dataType || item.dataType == 'auxiliary';
            });
          }
          
          _.each(results.sourceList,function(item){
            var authorized;
            item.actionButtons = [];

            if ($scope.isAdmin ||
                (!item.share && item.ownerId === $scope.currUserId) ||
                (item.share && item.owner._id === $scope.currUserId)) {
              authorized = true;
            }

            if (!item.share && authorized) {
              item.actionButtons.push({
                text: item.searchCycle_secs < 0 ? ' Enable' : ' Disable',
                buttonClass: item.searchCycle_secs < 0 ? 'btn btn-success' : 'btn btn-danger',
                //TODO: fa-pause-circle once we get font awesome upgraded
                iconClass: item.searchCycle_secs < 0 ? 'fa fa-play-circle fa-fw' : 'fa fa-pause fa-fw',
                click: function() { $scope.$emit('source-suspend', item); }
              });
            }

            item.actionButtons.push({
              text: ' View',
              buttonClass: 'btn btn-info',
              iconClass: 'fa fa-eye fa-fw',
              click: function () { $scope.$emit('source-view', item);}
            });

            if (item.share && authorized) {
              item.actionButtons.push({
                text: ' Test',
                buttonClass: 'btn btn-primary',
                iconClass: 'fa fa-check-circle fa-fw',
                click: function () { $scope.$emit('source-test', item); }
              });
            }

            if ((item.share || item.searchCycle_secs < 0) && authorized) {
              item.actionButtons.push({
                text: ' Edit',
                buttonClass: 'btn btn-secondary',
                iconClass: 'fa fa-edit fa-fw',
                click: function () { $scope.$emit('source-edit', item); }
              });
            }

            if (authorized) {
              //need a divider <li>
              item.actionButtons.push({
                text: 'divider',
                buttonClass: 'btn',
                iconClass: 'fa',
                click: function () {
                  $scope.$emit('source-delete', item);
                }
              });
              item.actionButtons.push({
                text: ' Delete',
                buttonClass: 'btn btn-danger',
                iconClass: 'fa fa-times-circle fa-fw',
                click: function () {
                  $scope.$emit('source-delete', item);
                }
              });
            }
          });

          $scope.sources  = results.sourceList;

          $rootScope.transmitting = false;
        }, function(err) {
          $log.error('ERROR: ', err);
          $rootScope.transmitting = false;
        });
      };

      $scope.testResultOptions = {
        lineWrapping : true,
        lineNumbers: true,
        readOnly: true,
        mode: "application/json",
        onLoad : function(_cm){
          $timeout(function() {
            _cm.refresh();
            _cm.focus();
          });
        }
      };

      $scope.getCommunities = function(){
        //store list of community IDs/names the user belongs to
        var userProfile = SessionService.getUserProfile();
        $scope.communities = userProfile.communities;
        $log.info('SUCCESS (communities):', $scope.communities);
      };

      $scope.getCommunities();
      $scope.getAllSources();

    });
});