/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['source/module', 'angular', 'lodash', 'json3', 'notification'], function(module, angular, _, JSON){
   "use strict";

    /**
     * @ngdoc controller
     * @name app.source.controller:SourceBuilderCtrl
     * @description
     * Controller for source creation wizard UI
     */
    module.registerController('SourceBuilderCtrl', function($scope, $rootScope, $state, $log, $timeout, $http, $uibModal, $uibModalInstance,
                                                            sourcesService, dataGroupService, workspaceService, utilityService){

        $scope.sources = sourcesService.getSourcesLocal();
        $scope.sourceBuilderTypeOptions = _.map($scope.sources, 'displayName');
        $scope.sourceType = null;
        $scope.authToEdit = false
        $scope.freq = {
          "types": [
            {"value":"hours", "label":"Hour(s)"},
            {"value":"days", "label":"Day(s)"},
            {"value":"weeks", "label":"Week(s)"},
            {"value":"months", "label":"Month(s)"}
          ],
          "frequencies": [
            {"value":0, "label":"0"},
            {"value":1, "label":"1"},
            {"value":2, "label":"2"},
            {"value":3, "label":"3"},
            {"value":4, "label":"4"},
            {"value":5, "label":"5"},
            {"value":6, "label":"6"}
          ]
        };
        $scope.freq.type = $scope.freq.types[1];
        $scope.freq.frequency = $scope.freq.frequencies[1];
        $scope.maxDepth = 2; //hard coded per paul/caleb for now
        var numPages = $scope.maxDepth + 1; //per caleb, depth of 2 means pages for 0,1,2, so add +1 to the max depth
        var JSONFull; //hold the source when switching back and forth between the full JSON source and the logstash/js tab

        var sourceObj = {}; //hold full source object from template

        $scope.testingMessage = 'Testing Source...';
        $scope.testingPromise = null;

        $scope.testResultError = $scope.testResultError || true;
        $scope.source = {};
        $scope.staticSrc = {};
        $scope.viewingLsTab = false;
        $scope.documents = {};
        $scope.documents.choices = ["1 document","10 documents","20 documents"];
        $scope.documents.chosen = $scope.documents.choices[0];
        $scope.cm;

        $scope.inputTime = {time: new Date()};
        $scope.inputDate = null;
        $scope.datePicker = {isOpen: false};
        $scope.dateOptions = {
          formatYear: 'yy',
          startingDay: 1
        };
        $scope.source.tags = [];

        $scope.openDatePicker = function($event){
            $event.preventDefault();
            $event.stopPropagation();
            $scope.datePicker = {isOpen: true}
        };

        $scope.inlineSelectValue = function(scope){
            if(scope.input.selectValue === "Other"){
                scope.input.value = "";
            } else {
                scope.input.value = scope.input.selectValue;
            }

            _.each($scope.mock.userInputs,function(item) {
                if(item.showhide){
                    item.showhide(scope.input.selectValue);
                }
            });
        };

        $scope.getCost = function(){
          var multiInput = _.values(_.filter($scope.mock.userInputs, {multiple: true}));

          if($scope.freq.type.value === "hours"){ 
              $scope.dailyCost = ($scope.mock.meta.cost * (multiInput[0].values.length + 1) * ( 24 / $scope.freq.frequency.value ) * numPages).toFixed(7);
              $scope.initialCost = Math.max($scope.mock.meta.cost * (multiInput[0].values.length + 1) * ( 24 / $scope.freq.frequency.value) * numPages, 0.01).toFixed(2);
          } else {
              $scope.initialCost = Math.max($scope.mock.meta.cost * (multiInput[0].values.length + 1) * numPages,0.01).toFixed(2);
          }
          if($scope.freq.type.value === "days"){ $scope.dailyCost =  ($scope.mock.meta.cost * (multiInput[0].values.length + 1) * ( 1 / $scope.freq.frequency.value ) * numPages).toFixed(7) }
          if($scope.freq.type.value === "weeks"){ $scope.dailyCost = ($scope.mock.meta.cost * (multiInput[0].values.length + 1) * ( 1 / 7 / $scope.freq.frequency.value ) * numPages).toFixed(7) }
          if($scope.freq.type.value === "months"){ $scope.dailyCost = ($scope.mock.meta.cost * (multiInput[0].values.length + 1) * ( 1 / 30 / $scope.freq.frequency.value ) * numPages).toFixed(7) }
        };

        $scope.changeFrequency = function(){
            if($scope.freq.type.value === "hours"){ 
              $scope.freq.frequencies = [];
              _.each([0,1,2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,20,21,22,23],function(item) {
                $scope.freq.frequencies.push({"value":item,"label":item});
              });
            }
            if($scope.freq.type.value === "days"){
              $scope.freq.frequencies = [];
              _.each([0,1,2,3,4,5,6],function(item) {
                $scope.freq.frequencies.push({"value":item,"label":item});
              });
              if($scope.freq.frequency.value > 6 ){ $scope.freq.frequency = $scope.freq.frequencies[1]; }
            }
            if($scope.freq.type.value === "weeks"){ 
              $scope.freq.frequencies = [];
              _.each([0,1,2,3],function(item) {
                $scope.freq.frequencies.push({"value":item,"label":item});
              });
              if($scope.freq.frequency.value > 3 ){ $scope.freq.frequency = $scope.freq.frequencies[1]; }
            }
            if($scope.freq.type.value === "months"){
              $scope.freq.frequencies = [];
              _.each([0,1,2,3,4,5,6,7,8,9,10,12],function(item) {
                $scope.freq.frequencies.push({"value":item,"label":item});
              });
              if($scope.freq.frequency.value > 12 ){ $scope.freq.frequency = $scope.freq.frequencies[1]; }
            }
        };

        //set frequency in searchCycle_secs based on dropdowns
        $scope.setFrequency = function(){
          var seconds = 0;
          if($scope.freq.frequency.value == 0) {
            seconds = 0;
          } else if($scope.freq.type.value === "hours") {
            //seconds per hour divided by number of times to run per hour
            seconds = Math.floor((60 * 60) / $scope.freq.frequency.value);
          } else if($scope.freq.type.value === "days") {
            //seconds per day divided by number of times to run per day
            seconds = Math.floor((60 * 60 * 24) / $scope.freq.frequency.value);
          } else if($scope.freq.type.value === "weeks") {
            seconds = Math.floor((60 * 60 * 24 * 7) / $scope.freq.frequency.value);
          } else if($scope.freq.type.value === "months") {
            seconds = Math.floor((60 * 60 * 24 * 7 * 4) / $scope.freq.frequency.value);
          }

          _.each($scope.mock.source.processingPipeline,function(item,idx) {
            if(item.harvest) {
              item.harvest.searchCycle_secs = seconds;
            }
          });
        };

        $scope.chooseTemplate = function(){
          $scope.mock = null;
          $scope.source = {};
          if($scope.sourceType != "Advanced Source Builder" ){
            $scope.mock = _.find($scope.sources, {displayName: $scope.sourceType}).source.TEMPLATE;
            $scope.mock.userInputs = _.sortBy(_.values($scope.mock.userInputs), ['order']);
            $scope.showAdvancedBtn = _.filter($scope.mock.userInputs, {advanced: true});
            $scope.mock.source = _.find($scope.sources, {displayName: $scope.sourceType}).source.SOURCE;
            //store copy of source object so we can restore it later
            sourceObj = angular.copy($scope.mock.source);
            if(_.filter($scope.mock.userInputs, 'values')){
              _.map(_.filter($scope.mock.userInputs, 'values'), function(input){
                input.values = [{'value':''}];
              })
            }
          } else {
            $scope.chooseSource();
          }
        };

        $scope.revert = function() {
          resetJSONMessages();
          setActiveSource(sourcesService.activeSource);
        };

        $scope.chooseSource = function(){
          resetJSONMessages();
          $scope.JSONValue = JSON.stringify(SAMPLE_SOURCES[$scope.sourceChosen.title], null, 2);
          //store the full text of the source before switching to LS/JS
          JSONFull = $scope.JSONValue;
          $scope.setValues();

        };

        $scope.addNewValue = function() {
            var idx = this.$parent.$parent.$index;
            $scope.mock.userInputs[idx].values.push({"value":''});
        };

        $scope.deleteValue = function() {
            var parentIdx = this.$parent.$parent.$index,
                idx = this.$index;
                $scope.mock.userInputs[parentIdx].values.splice(idx,1);
        };

        $scope.wizard1CompleteCallback = function(wizardData){
          if(doNext()){
            //console.log('wizard1CompleteCallback', wizardData);
            $rootScope.transmitting = true;
            $.smallBox({
                title: "Please wait, updating server",
                color: "#3276B1",
                iconSmall: "fa fa-bell swing animated",
                timeout: 2000,
                number: "1"
            }, function () {
                //scope.eg2();
            });
            $scope.saveSource();
          }
        };

        // SOURCE EDITOR FUNCTIONS:
        $scope.editorOptions = {
            lineWrapping : true,
            lineNumbers: true,
            indentWithTabs: true,
            autofocus: true,
            mode: "application/json",
            matchBrackets: true,
            indentUnit: 4,
            value: 'ENTER TEXT HERE',
            readOnly:  $scope.source.harvest && !($scope.source.searchCycle_secs < 0),
            onLoad : function(_cm){
              $scope.cm = _cm;
              $timeout(function() {
                _cm.refresh();
                _cm.focus();
              }, 1);
              _cm.on("focus", function(){ resetJSONMessages(); });
              $('textarea').attr('autocomplete', 'false');
            }
        };

        $scope.testResultOptions = {
            lineWrapping : true,
            lineNumbers: true,
            readOnly: true,
            mode: "application/json",
            onLoad : function(_cm){
              $timeout(function() {
                _cm.refresh();
                _cm.focus();
              });
            }
        };

        //check if source has LS (or JS) to edit, and to show/hide the LS/JS button
        $scope.checkForLogstash = function(checkLogstashOnly){
          try {
            var srcObj = JSON.parse(JSONFull), isLs = false;

            _.each(srcObj.processingPipeline,function(item) {
                if(checkLogstashOnly){
                  if(item.logstash) { isLs = true; }
                } else {
                  if(item.logstash || item.scriptingEngine || (item.globals && item.globals.scripts)) { isLs = true; }
                }
            });

          }
          catch (e) { return isLs; }

          return isLs;
        };

        //edit just the de-stringified logstash/JS stuff
        $scope.JSONLsTab = function(){
          if(!$scope.viewingLsTab) {
            try {
              var srcObj = JSON.parse($scope.JSONValue); //if this errors, validate the source in the catch 
              resetJSONMessages();
              //store the full text of the source before switching to LS/JS
              JSONFull = $scope.JSONValue;
              //find logstash or scripting engine in the source
              _.each(srcObj.processingPipeline,function(item) {
                //just display the unstringified LS/JS to edit
                if(item.logstash) { $scope.JSONValue = item.logstash.config; }
                else if(item.scriptingEngine) { $scope.JSONValue = item.scriptingEngine.globalScript; }
                else if(item.globals && item.globals.scripts) { $scope.JSONValue = item.globals.scripts[0]; }
              });
              
              $scope.viewingLsTab = !$scope.viewingLsTab;
            }
            catch (e) { //JSON.parse failed, source is not valid, cant switch to js/ls tab
              $scope.validateJSON();
            }
          }
        };

        //view / edit the full source
        $scope.JSONFullTab = function(){
          if($scope.viewingLsTab) {
            //turn the full source in to an object
            var srcObj = JSON.parse(JSONFull);
            resetJSONMessages();
            //update the full source with the stringified text from JS/LS tab
            _.each(srcObj.processingPipeline,function(item) {
              if(item.logstash) { item.logstash.config = $scope.JSONValue; }
              else if(item.scriptingEngine) { item.scriptingEngine.globalScript = $scope.JSONValue; }
              else if(item.globals && item.globals.scripts) { item.globals.scripts[0] = $scope.JSONValue; }
            });
            
            //stringify source and display it for editing
            $scope.JSONValue = JSON.stringify(srcObj, null, 2);
            $scope.viewingLsTab = !$scope.viewingLsTab;
          }
        };

        $scope.setValues = function(){

          //If we are from the wizard:
          if($scope.mock && $scope.mock.userInputs){
            $scope.setFrequency();
            //restore the source object so each sourcekey function will properly substitute the new values if changed
            $scope.mock.source = angular.copy(sourceObj);
            _.each($scope.mock.userInputs,function(item) {
                item.sourcekey($scope);
            });
            $scope.JSONValue = JSON.stringify($scope.mock.source);
          }

          if($scope.JSONValue){
            $scope.JSONFullTab();
            if (this.dataGroupChosen) $scope.dataGroupChosen = this.dataGroupChosen;
            //disable mediaType until we decide we want to use it again in the future
            //if (this.shareMediaType) $scope.shareMediaType = this.shareMediaType;
            if (this.dataOrigin) $scope.dataOrigin = this.dataOrigin;
            if (this.dataType) $scope.dataType = this.dataType;
            var srcObj = JSON.parse($scope.JSONValue);
            srcObj.title = $scope.source.title || srcObj.title || '';
            srcObj.tags = $scope.source.tags || srcObj.tags || [];
            //srcObj.mediaType = $scope.shareMediaType || srcObj.mediaType;
            srcObj.description = $scope.source.description || srcObj.description || '';
            if($scope.dataGroupChosen){
              if(!srcObj.communityIds) srcObj.communityIds = [];
              srcObj.communityIds[0] = $scope.dataGroupChosen._id;
            }
            srcObj.documents = parseFloat($scope.documents.chosen.split(' ')[0]) || 1;
            srcObj.fullText = $scope.source.fullText || false;
            srcObj.testUpdates = $scope.source.testUpdates || false;

            $scope.JSONValue = JSON.stringify(srcObj, null, 2);
          }
        };

        $scope.validateJSON = function(){
          resetJSONMessages();
          var success = JSHINT($scope.JSONValue);
          if(!success){
            console.log(JSHINT.errors);
            $scope.JSONErrors = JSHINT.errors;
          } else {
            $scope.JSONSuccess = true;
          }
          return success;
        };

        $scope.scrubJSON = function(){
          $scope.JSONFullTab();
          // Check overall JSON format is OK first
          if (!$scope.validateJSON()) {
            return false;
          }
          
          resetJSONMessages();

          // Convert source JSON text into JSON
          var srcObj = JSON.parse($scope.JSONValue);
          
          // Remove fields we don't care about for config
          delete srcObj.communities;
          delete srcObj.created;
          delete srcObj.harvest;
          delete srcObj.harvestBadSource;
          delete srcObj.key;
          delete srcObj.modified;
          delete srcObj.ownerId;
          delete srcObj.shah256Hash;
          delete srcObj.searchCycle_secs;

          $scope.scrubText = "Removed: .communities, .created, .harvest, .harvestBadSource, .isApproved, .key, .modified, .ownerId, .shah256Hash, .searchCycle_secs";
          
          //sourceJsonEditor.setValue(JSON.stringify(srcObj, null, "    "));
          $scope.JSONValue = JSON.stringify(srcObj, null, 2);
        };

        // TEST SOURCE SERVICES:
        $scope.testSource = function(editSource){
          if(!checkForm()) { return false; }
          if(doNext()){
            addLogstashTags();
            doTestSource(JSON.parse($scope.JSONValue));
          }
        };

        // SAVE SOURCE SERVICES
        $scope.saveSource = function(editSource){
          if(!checkForm()) { return false; }
          if(doNext()){
            addLogstashTags();
            var sourceTextObj = JSON.parse($scope.JSONValue);
            //remove system tags (internal,external,source types, etc) and push tags from type/origin dropdown
            sourceTextObj.tags = _.pull(sourceTextObj.tags,'internal','external','External','Internal','asset-database',
                'firewall','ldap','network-scan','proxy','threat-intel','auxiliary','test');
            //add the external or internal tag, re-stringify to save in the "share" property
            if($scope.dataOrigin && $scope.dataOrigin.length > 1) { sourceTextObj.tags.push($scope.dataOrigin); }
            if($scope.dataType && $scope.dataType.length > 1) { sourceTextObj.tags.push($scope.dataType); }

            $scope.JSONValue = JSON.stringify(sourceTextObj,null,2);

            var srcObj = {
              "title": sourceTextObj.title,
              "description": sourceTextObj.description,
              "communities": [{ "_id": $scope.dataGroupChosen._id }],
              "type":"source", //NEED TYPE
              "share":$scope.JSONValue
              // "share": JSON.stringify(__ikanowSourceTemplates) // This is to save off the new templates (USE CAUTIOUSLY)
            };
            if($scope.shareID){
              srcObj._id = $scope.shareID;
              doUpdateSource(srcObj);
            } else {
              doSaveSource(srcObj);
            }
          }
        };

        // PUBLISH SOURCE SERVICES:
        $scope.publishSource = function(editSource){
          if(!checkForm()) { return false; }
          if(doNext()){
            addLogstashTags();
            var sourceTextObj = JSON.parse($scope.JSONValue);
            //remove system tags (internal,external,source types, etc) and push tags from type/origin dropdown
            sourceTextObj.tags = _.pull(sourceTextObj.tags,'internal','external','External','Internal','asset-database',
                'firewall','ldap','network-scan','proxy','threat-intel','auxiliary','test');
            //add the external or internal tag, re-stringify to save in the "share" property
            if($scope.dataOrigin && $scope.dataOrigin.length > 1) { sourceTextObj.tags.push($scope.dataOrigin); }
            if($scope.dataType && $scope.dataType.length > 1) { sourceTextObj.tags.push($scope.dataType); }
            doPublishSource(sourceTextObj);
          }
        };

        // UPLOAD FILE SERVICES:
        $scope.uploadFile = function(){
          var idx = this.$parent.$parent.$index;
          if (!this.files || this.files.length === 0)  return false;
          var fd = this.files[0];
          //angular.forEach(this.files, function(file){
          //  fd.append('file', file);
          //})
          var shareObj = {'title':this.files[0].name, 'description': this.files[0].name + ' file upload'}
          sourcesService.uploadFile(shareObj, fd).then(function(data) {
            $log.info('SUCCESS SHARE UPLOADED):', data);
            $scope.uploadedFile = data;
            $scope.mock.userInputs[idx].value = $scope.uploadedFile._id;
          }, function(err) {
            $log.error('ERROR SHARE NOT UPLOADED): ', err);
            showResults(err);
          });
        };

        $scope.closeResults = function(action){
          this.$close(action);
        };

        $scope.closeResultsDashboard = function(){
          if($scope.modalInstance){
            $scope.modalInstance.close();
            $scope.getAllSources();
          }
          sourcesService.activeSource = null;
          this.$close();
          $state.go('app.manager.modal.sourceList', {"searchText": null});
        };

        function showResults(response, action){
          console.log("Show Results (" + action + "): ", response);
          $rootScope.transmitting = false;
          $scope.JSONTestResults = JSON.stringify(response, null, 2);
          $scope.testResultError = response;
          if(!$scope.sourceType || $scope.sourceType === 'Advanced Source Builder' || ($scope.mock && $scope.mock.meta && $scope.mock.meta.showTest && !response.response.success)){
            var modalInstance = $uibModal.open({
                templateUrl: 'app/source/views/source-test-results.html',
                size: 'lg',
                backdrop: 'static',
                scope: $scope
            });
            modalInstance.result.then(function () {
                $log.info('Modal closed at: ' + new Date());
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
          } else if(action != "testSource"){
            $state.go('app.manager.modal.sourceList', {"searchText": null});
          }
        }

        function loadDataGroups(){
          dataGroupService.getAll().then( function(dataGroups) {
            $scope.dataGroups = dataGroups;
            //remove datagroups that are personal communities
            $scope.dataGroups = _.reject($scope.dataGroups, {isPersonalCommunity: true});
            //remove datagroups created for projects
            $scope.dataGroups = _.reject($scope.dataGroups, {description: '__projectShare'});
            //remove managed workspaces
            $scope.dataGroups = _.filter($scope.dataGroups, function(dataGroup){
              if(_.indexOf(dataGroup.tags,'managed') > -1 || _.indexOf(dataGroup.tags,'workspaceMaster') > -1) {
                return false;
              } else {
                return true;
              }
            });
            resetJSONMessages();
            if(sourcesService.activeSource) {
              $scope.activeSource = sourcesService.activeSource;
              setActiveSource(sourcesService.activeSource)
            }
          });
        }

        //only pulling from sampleSourcesLegacy.js now, instead of combining with templates from shares
        function getLegacySourceTemplates(){
          $scope.allSources = _.map(_.keys(SAMPLE_SOURCES), function(i){
            return {title: i};
          });
          $scope.sourceChosen = $scope.allSources[0];
        }

        function doNext(){
          if (!$scope.dataGroupChosen) return false;
          resetJSONMessages();
          $scope.setValues();
          if (!$scope.validateJSON()) return false;
          $rootScope.transmitting = true;
          return true;
        }

        function addLogstashTags(){
          var srcObj = JSON.parse($scope.JSONValue);
          if($scope.dataOrigin && $scope.dataOrigin.length > 1) { srcObj.tags.push($scope.dataOrigin); }
          if($scope.dataType && $scope.dataType.length > 1) { srcObj.tags.push($scope.dataType); }
          
          //turn tags in to a string to pass in to logstash add_tag
          var tags = srcObj.tags.join('", "');
          tags = '"' + tags + '"';

          //find logstash in the source
          _.each(srcObj.processingPipeline,function(item) {
            if(item.logstash) { 
              var logstashText = item.logstash.config;
              //check for existence of add_tag, if doesn't exist, add it
              if(logstashText.search('add_tag') == -1) {
                logstashText = logstashText.replace(/(filter\s*{)/,'$1\n  mutate {\n    add_tag => []\n  }');
              }
              //replace whatever is in add_tag with updated tags
              item.logstash.config = logstashText.replace(/add_tag.*\]/,'add_tag => [' + tags +']');
            }
          });

          $scope.JSONValue = JSON.stringify(srcObj, null, 2);
        }

      /**
       * Perform the test source action
       * @param src
       */
      function doTestSource(src){
        ($scope.testingPromise = sourcesService.testSource(src.documents, src.fullText, src.testUpdates, src))
          .then(function(apiResponse){
            $log.info("doTestSource] sourcesService.testSource success", apiResponse);
            showResults(apiResponse, 'testSource');
          })
          .catch(function(err) {
            $log.info("doTestSource] sourcesService.testSource error", err);
            showResults(err);
          });
      }

      /**
       * Publish a source
       */
      function doPublishSource(src){

        sourcesService.publishSource(src).then(function(data) {
          $log.info('doPublishSource] sourcesService.publishSource success', data);
          //remove old share when publishing from share to source to insure no dupes
          if($scope.shareID){
            sourcesService.deleteSourceShare($scope.shareID).then( function() {
              console.log("SHARE DELETED");
            });
          }
          //delete existing docs if box checked
          if($scope.source.deleteDocs) {
            utilityService.confirmationModal(
              'Are you sure you want to delete all previously harvested documents for this source?', //text to display in the confirmation popup
              'Confirm', //text of the confirm/ok button
              'Cancel',  //text of the cancel/dismiss button
              'Confirm Delete' // header text
            ).result.then(function () {
              // On close (user confirmed, perform delete). cancel rejects this promise and is ignored.
              sourcesService.deleteDocsFromSource(data.data).then(function(docsDeleted){
                $log.info("DELETED OLD DOCS:", docsDeleted);
              });
            });
          }

          showResults({
            data: data,
            response: {
              success: true,
              action: "Source",
              message: "New source added successfully."
            }},
            'publishSource'
          );

          //if from the wizard
          if($scope.mock && $scope.mock.userInputs){
            if($scope.modalInstance){
              $scope.modalInstance.close();
            }
            $scope.getAllSources();
            sourcesService.activeSource = null;
          }
        })
        .catch(function(err) {
          $log.info("doPublishSource] sourcesService.publishSource error", err);
          showResults(err);
        });

      }

      /**
       * Save a source
       * @param src
       */
      function doSaveSource(src){

        sourcesService.saveSource(src).then(function(data) {
          $log.info('doSaveSource] sourcesService.saveSource success', data);
          if($scope.source._id){
            sourcesService.deleteSource($scope.source).then(function(data){
              console.log("SOURCE DELETED");
              //$scope.getAllSources();
            });
          }

          showResults({
            data: data,
            response: {
              action: "Share",
              success: true,
              message: "Share returned successfully"
            }},
            'saveSource'
          );

          //if from the wizard
          if($scope.mock && $scope.mock.userInputs){
            if($scope.modalInstance){
              $scope.modalInstance.close();
            }
            $scope.getAllSources();
            sourcesService.activeSource = null;
          }
        })
        .catch(function(err) {
          $log.info("doSaveSource] sourcesService.saveSource error", err);
          showResults(err);
        });
      }

      /**
       * Update a saved source
       * @param src
       */
      function doUpdateSource(src){

        sourcesService.updateSource(src).then(function(data) {
          $log.info('doUpdateSource] sourcesService.updateSource success', data);

          showResults({
            data: data,
            response: {
              action: "Share",
              message: "Share returned successfully",
              success: true
            }},
            'updateSource'
          );

          //if from the wizard
          if($scope.mock && $scope.mock.userInputs ){
            if($scope.modalInstance){
              $scope.modalInstance.close();
            }
            $scope.getAllSources();
            sourcesService.activeSource = null;
          }
        })
        .catch(function(err) {
          $log.info("doUpdateSource] sourcesService.updateSource error", err);
          showResults(err);
        });
      }


        function resetJSONMessages(){
          $scope.JSONErrors = null;
          $scope.JSONSuccess = null;
          $scope.JSONRevertSuccess= null;
          $scope.scrubText = null;
        }

        function setActiveSource(source){
          var tags = [];
          var id = source.share ? source.communities[0]._id : source.communityIds[0];
          $scope.dataGroupChosen = _.find($scope.dataGroups, {'_id': id}) || $scope.dataGroups[0];
          $scope.source = source.share ? JSON.parse(source.share) : source;
          
          //check if user has access to edit the source
          if($rootScope.isAdmin ||
              (!$scope.source.share && $scope.source.ownerId === $scope.currUserId) ||
              ($scope.source.share && $scope.source.owner._id === $scope.currUserId)){
              //check to make sure source is a share or suspended
              if(source.share || $scope.source.searchCycle_secs < 0){
                $scope.authToEdit = true;              }
          }

          $scope.shareID = source.share ? source._id : null;
          $scope.source.title = source.title || '';
          //$scope.shareMediaType = $scope.source.mediaType || '';
          $scope.source.description = source.description || '';
          $scope.source.isApproved = true;

          if (source.share) {
            tags = JSON.parse(source.share).tags;
          } else {
            tags = source.tags;
          }

          //if external or internal tag exists, set data origin dropdown
          if(_.indexOf(tags,'internal') >= 0) { $scope.dataOrigin = 'internal'; }
          else if(_.indexOf(tags,'external') >= 0) { $scope.dataOrigin = 'external'; }
          else { $scope.dataOrigin = '' }

          $scope.dataType = '';
          var types = ['asset-database','firewall','ldap','network-scan','proxy','threat-intel','auxiliary','test'];
          _.each(types,function(type) {
            if(_.indexOf(tags,type) >= 0) { $scope.dataType = type; }
          });

          // If source is a share, delete the ID
          if (source.share) delete $scope.source._id;
          $scope.JSONValue = JSON.stringify($scope.source, null, 2);
          JSONFull = $scope.JSONValue;
          $scope.scrubJSON();
          $scope.scrubText = null;
        }

        function checkForm(){
          var editForm = $('form[name="sourceform"]');
          if (!editForm.valid()) {
              editForm.data('validator').focusInvalid();
              return false;
          }
          return true;
        }

        loadDataGroups();
        getLegacySourceTemplates();
        $scope.changeFrequency();

        if($scope.testResultData){
          showResults($scope.testResultData);
          $scope.$parent.testResultData = null;
        }
    });
});