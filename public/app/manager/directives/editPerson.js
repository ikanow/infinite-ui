/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['manager/module', 'jquery'], function(module, $){
    "use strict";

    /**
     * @ngdoc directive
     * @name app.manager.directive:userButton
     * @restrict EA
     * @description
     * edit a row in the user list
     */

    module.registerDirective("userButton", function($log, $compile, $state){
        return {
          restrict: 'EA',
          link: function(scope, element, attrs){
            element.bind("click", function(event){

              $state.go('.', {
                userIdToEdit: scope.item._id
              },{
                notify: false
              });

              element.parent().parent().after( $compile('<tr><td colspan="5" edit-user></td></tr>')(scope) ).hide();

              scope.appScope.userIdToEdit = null;

              if (event) {
                event.stopPropagation();
              }
            });

            //if they clicked the link to edit their own profile, open it
            if (scope.appScope.editProfile && scope.appScope.currUserId === scope.item._id) {
              scope.appScope.editProfile = false;
              element.parent().parent().after( $compile('<tr><td colspan="5" edit-user></td></tr>')(scope) ).hide();
            }

            //if deep linked to a specific user, open that user
            if (scope.appScope.userIdToEdit && scope.appScope.userIdToEdit == scope.item._id && (scope.appScope.isAdmin || scope.appScope.currUserId === scope.item._id)) {
              element.parent().parent().after( $compile('<tr><td colspan="5" edit-user></td></tr>')(scope) ).hide();
            }

          }
        };
    });
    /**
     * @ngdoc directive
     * @name app.manager.directive:editUser
     * @restrict EA
     * @description
     * Directive for editing a user
     */
    module.registerDirective('editUser', function($rootScope, $log, $document, $state, userService, utilityService){
        return {
            restrict: 'EA',
            templateUrl: 'app/manager/directives/edit-user.tpl.html',
            scope:true,
            link: function(scope, element){

              var item = scope.item || {};

              scope = scope.appScope || scope;

              scope.user = item;
              scope.newUser = scope.user.displayName ? false : true;
              scope.editedUser = angular.copy(scope.user) || {};
              if (!scope.editedUser.accountType) {
                scope.editedUser.accountType = scope.accountTypeOptions[0];
              }
              if (!scope.editedUser.accountStatus) {
                scope.editedUser.accountStatus = scope.accountStatusOptions[0];
              }
              scope.editedUser.communities = _.reject(scope.editedUser.communities, function(community) {
                return _.endsWith(community.name, "PROJECTDATAGROUP") ||
                  _.endsWith(community.name, "Personal Community") ||
                  (angular.isDefined(community.type) && community.type === "user");

              });

              if(scope.user._id) {
                userService.getUser(scope.user._id).then(function (data) {
                  scope.accountType = data.accountType || 'admin';
                });
              }

              var openedRow = scope.openedRow.element;

              scope.cancel = function(elementToCancel){
                if (!elementToCancel) {
                  elementToCancel = element;
                }
                if (elementToCancel.parents('tr').length > 0){
                  elementToCancel.parents('tr').prev('tr').show();
                  elementToCancel.parents('tr').remove();
                } else {
                  elementToCancel.remove();
                }

                scope.openedRow.element = null;

                //remove id from link when closing the edit view
                $state.go('.', {
                  userIdToEdit: null
                },{
                  notify: false
                });

                $document.unbind('click');
              };

              if (openedRow) {
                scope.cancel(openedRow);
                scope.openedRow.element = element;
                if(item._id) {
                  $state.go('.', {
                    userIdToEdit: item._id
                  },{
                    notify: false
                  });
                }
              } else {
                scope.openedRow.element = element;
              }

              scope.eg1 = function () {
                  $.smallBox({
                      title: "Please wait, updating server",
                      color: "#3276B1",
                      iconSmall: "fa fa-bell swing animated",
                      timeout: 2000,
                      number: "1"
                  }, function () {
                      //scope.eg2();
                  });
              };
              scope.eg2 = function () {
                  $.smallBox({
                      title: "Success! Server updated",
                      color: "#739E73",
                      timeout: 2000,
                      iconSmall: "fa fa-check",
                      number: "2"
                  }, function () {
                      //scope.closedthis();
                  });
              };

              // Manager Actions

              scope.addUser = function(user){
                disableForm();
                userService.addUser(user).then(function(ignoredData){
                  enableForm();
                });
              };

              scope.updateUser = function(user){
                disableForm();
                userService.updateUser(user).then(function(ignoredData){
                  scope.stopEditProfile();
                  enableForm();
                });
              };

              scope.deleteUser = function(userID, name){
                var confirmationModal = utilityService.confirmationModal("Are you sure you want to delete " + name + "?", //text to display in the confirmation popup
                'Confirm', //text of the confirm/ok button
                'Cancel', //text of the cancel/close button
                'Confirm Delete'); //header text

                confirmationModal.result.then(
                  // On close (user confirmed, perform delete)
                  function () {
                    disableForm();
                    userService.deleteUser(userID).finally(function(ignoredData){
                      enableForm();
                      //remove id from link when closing the edit view
                      $state.go('.', {
                        userIdToEdit: null
                      },{
                        notify: false
                      });
                    });
                  },
                  // On dismiss
                  function () {
                    //do nothing, user canceled/closed modal
                  }
                );
              };

              function disableForm(){
                $rootScope.transmitting = true;
                scope.eg1();
              }

              function enableForm(){
                scope.eg2();
                scope.newUser = false;
                $rootScope.transmitting = false;
                scope.loadUsers();
                scope.cancel(element);
              }
              
              $document.bind('click', function(event){
                  if (element.find(event.target).length == 0){
                    scope.cancel(element);
                  }
              });

              $("body").animate({scrollTop: element.offset().top - 100}, "fast");

              scope.isUniqueEmail = function(newEmail) {
                if (scope.newUser) {
                  return !_.some(scope.userList, {email: newEmail});
                }

                if (scope.editedUser.email === scope.user.email) {
                  return true;
                } else {
                  // Copy all of the users except the current user to test for unique email
                  var allOtherUsers = _.reject(angular.copy(scope.userList), {_id: scope.user.email});
                  return !_.some(allOtherUsers, {email: newEmail});
                }
              };
            }
        }
    })

});