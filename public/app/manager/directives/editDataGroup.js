/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['manager/module', 'jquery', 'lodash'], function(module, $, _){
    "use strict";

  /**
   * @ngdoc directive
   * @name app.manager.directive:actionButton
   * @restrict EA
   * @description
   * Directive for making the edit button inject the edit data group form
   */
    module.registerDirective("actionButton", function($log, $compile, $timeout, $state){
        return {
          restrict: 'EA',
          link: function(scope, element, attrs){
            element.bind("click", function(event){

              scope.appScope.readOnly = !scope.appScope.isAdmin;
              $state.go('.', {
                groupIdToEdit: scope.item._id,
                readOnly: scope.appScope.readOnly
              },{
                notify: false
              });

              element.parent().parent().after( $compile('<tr><td colspan="5" edit-data-group></td></tr>')(scope) ).hide();

              scope.appScope.groupIdToEdit = null;

              if (event) {
                event.stopPropagation();
              }
            });

            ///if deep linked to a specific group, open that group
            if (scope.appScope.groupIdToEdit && scope.appScope.groupIdToEdit == scope.item._id) {
              scope.appScope.readOnly = !scope.appScope.isAdmin;
              scope.appScope.loadingPromise = $timeout(function() {
                element.parent().parent().after( $compile('<tr><td colspan="5" edit-data-group></td></tr>')(scope) ).hide();
              }, 1000);
            }

          }
        };
    });
    
    /**
     * @ngdoc directive
     * @name app.manager.directive:editDataGroup
     * @restrict EA
     * @scope
     * @description
     * Directive for editing data groups
     */
    module.registerDirective('editDataGroup', function($rootScope, $log, $document, $q, $state, dataGroupService, userGroupService, utilityService){
        return {
            restrict: 'EA',
            templateUrl: 'app/manager/directives/edit-data-group.tpl.html',
            scope: true,
            link: function(scope, element){

              var datagroup = scope.item || {};

              scope = scope.appScope || scope;

              scope.datagroup = datagroup || {};
              scope.newDataGroup = scope.datagroup._id ? false : true;
              scope.editedDataGroup = angular.copy(scope.datagroup) || {};
              if (scope.newDataGroup) scope.editedDataGroup.tags = [];
              var openedRow = scope.openedRow.element;

              //add [members] of the usergroup to each allUser item that is a user group, so we can display them
              userGroupService.getAll().then(function(userGroups) {
                  _.each(scope.editedDataGroup.members, function(item) {
                      if(item.type === "user_group"){
                          var members;
                          members = _.map(_.filter(userGroups, {_id : item._id}), 'members')[0];
                          item.members = members ? members : [];
                      }
                  });
              });

              scope.cancel = function(elementToCancel){
                if (!elementToCancel) {
                  elementToCancel = element;
                }
                if (elementToCancel.parents('tr').length > 0){
                  elementToCancel.parents('tr').prev('tr').show();
                  elementToCancel.parents('tr').remove();
                } else {
                  elementToCancel.remove();
                }
                //remove id from link when closing the edit view
                $state.go('.', {
                  groupIdToEdit: null
                },{
                  notify: false
                });

                scope.openedRow.element = null;

                $document.unbind('click', scope.clickHandler);
              };

              if (openedRow) {
                scope.cancel(openedRow);
                scope.openedRow.element = element;
                if(datagroup._id){
                  $state.go('.', {
                    groupIdToEdit: datagroup._id
                  },{
                    notify: false
                  });
                }
              } else {
                scope.openedRow.element = element;
              }

              scope.eg1 = function () {
                  $.smallBox({
                      title: "Please wait, updating server",
                      color: "#3276B1",
                      iconSmall: "fa fa-bell swing animated",
                      timeout: 2000,
                      number: "1"
                  }, function () {
                      //scope.eg2();
                  });
              };
              scope.eg2 = function () {
                  $.smallBox({
                      title: "Success! Server updated",
                      color: "#739E73",
                      timeout: 2000,
                      iconSmall: "fa fa-check",
                      number: "2"
                  }, function () {
                      //scope.closedthis();
                  });
              };

              scope.addDataGroup = function(){
                disableForm();
                this.editedDataGroup.communityAttributes = communityAttributes; //set the global share parameters
                var addFunc = this.editedDataGroup.type === "user" ?
                  userGroupService.add(this.editedDataGroup) :
                  dataGroupService.add(this.editedDataGroup);

                addFunc.then(
                  function(data){
                    scope.datagroup = data;
                    scope.editedDataGroup = angular.copy(data);
                    enableForm(true);
                  },
                  function(error) {
                    utilityService.confirmationModal("Unable to save group", //text to display in the confirmation popup
                      'Close', //text of the confirm/ok button
                      null, //text of the cancel/close button
                      'Error'); //header text
                    enableForm(false);
                  }
                );
              };

              scope.deleteDataGroup = function(){
                var id =  scope.editedDataGroup._id,
                    status = scope.editedDataGroup.communityStatus,
                    name = scope.editedDataGroup.name;
                var status = status == "active" ? "delete" : "delete"; //temp fix, 2 stage delete instead of disable

                var confirmationModal = utilityService.confirmationModal("Are you sure you want to " + status + " " + name + "?", //text to display in the confirmation popup
                'Confirm', //text of the confirm/ok button
                'Cancel', //text of the cancel/close button
                'Confirm Delete'); //header text

                confirmationModal.result.then(
                  // On close (user confirmed, perform delete)
                  function () {
                    disableForm();
                    var removeFunc = scope.editedDataGroup.type === "user" ? userGroupService.remove(id) : dataGroupService.remove(id);
                    removeFunc.then(function(data){
                      enableForm(true);
                      //remove id from link when closing the edit view
                      $state.go('.', {
                        groupIdToEdit: null
                      },{
                        notify: false
                      });
                      });
                  },
                  // On dismiss
                  function () {
                    //do nothing, user canceled/closed modal
                  }
                );
              };

              scope.updateDataGroup = function() {

                disableForm();

                var editedDataGroup = angular.copy(scope.editedDataGroup);
                var datagroup = scope.datagroup;

                var membersToRemove = _.difference(_.map(datagroup.members, '_id'), _.map(editedDataGroup.members, '_id'));
                var membersToAdd = _.difference(_.map(editedDataGroup.members, '_id'), _.map(datagroup.members, '_id'));

                // Transform editedGroup to a new
                delete editedDataGroup.created;
                delete editedDataGroup.modified;
                delete editedDataGroup.members;
                editedDataGroup.communityAttributes = communityAttributes; //set the global share parameters
                editedDataGroup.tags = _.hasIn(editedDataGroup.tags, 'text') ?
                  _.map(editedDataGroup.tags, 'text')
                  : editedDataGroup.tags || [' '];

                var serviceToUse = editedDataGroup.type === "user" ? userGroupService : dataGroupService;
                return $q.when(true)
                  .then(function(){ return membersToAdd.length > 0 ? serviceToUse.inviteMembers(editedDataGroup._id, membersToAdd) : true; })
                  .then(function(){ return membersToRemove.length > 0 ? serviceToUse.removeMembers(editedDataGroup._id, membersToRemove) : true; })
                  .then(function(){ return serviceToUse.update(editedDataGroup); })
                  .then(function(result){
                    enableForm(true);
                    return result;
                  })
                  .catch(function(err){
                    $log.error("Cannot update data group: ", err);
                    enableForm(false);
                    return $q.reject(err);
                  });
              };

              function disableForm(){
                $rootScope.transmitting = true;
                scope.eg1();
              }

              /**
               * Enable the form
               * @param success
               */
              function enableForm(success){
                if (success) {
                  scope.eg2();
                  scope.loadDataGroups();
                  $rootScope.transmitting = false;
                  if(!scope.newDataGroup){
                    scope.cancel(element);
                  }
                  scope.newDataGroup = false;
                  if (scope.editedDataGroup.tags && scope.editedDataGroup.tags[0] === " ") scope.editedDataGroup.tags = [];
                  scope.setCount(scope.editedDataGroup);
                }
                else {
                  $rootScope.transmitting = false;
                  scope.cancel(element);
                }

              }

              scope.clickHandler = function(event) {
                if ($(event.target).hasClass('remove-button')) return;
                if (element.find(event.target).length == 0){
                  if ($(event.target).parents().length > 2){
                    scope.cancel(element);
                  }
                }
              };
              $document.bind('click', scope.clickHandler);

              if(scope.datagroup){
                scope.inCommunities();
              }
            }
        }
    })

});
