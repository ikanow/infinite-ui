/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc directive
 * @name app.manager.directive:datagroupForm
 * @description
 * UI Form for adding data groups
 */
define(['manager/module', 'modules/forms/common', 'jquery-maskedinput', 'jquery-validation'], function (module, formsCommon) {

    'use strict';

    return module.registerDirective('datagroupForm', function ($state) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/manager/directives/datagroupForm.tpl.html',
            scope: true,
            link: function (scope, form) {

                scope = scope.appScope || scope;

                // Configure validator
                $.validator.addMethod("verifyTags",
                  function () {
                    return scope.editedDataGroup.tags.length > 0;
                  },
                  "A minimum of one tag is required"
                );

                form.validate(angular.extend({
                    rules: {
                        name: {
                            required: true
                        },
                        description: {
                            required: true
                        },
                        tagsInputField: {
                            verifyTags: true
                        }
                    },

                    // Messages for form validation
                    messages: {
                        name: {
                            required: 'Please enter your name'
                        },
                        description: {
                            required: 'Please enter your description'
                        },
                        tagsInputField: {
                            verifyTags: "A minimum of one tag is required"
                        }
                    },

                    ignore: '.ignore-validation'
                }, formsCommon.validateOptions));

                scope.processForm = function(){
                    if(!form.valid()){ return }
                    if(scope.newDataGroup){
                        scope.editedDataGroup.type = scope.selectedType;
                        if(scope.newDataGroup && scope.selectedType == "user"){scope.editedDataGroup.tags = "userGroup";}
                        scope.addDataGroup();
                    } else {
                        scope.updateDataGroup();
                    }
                    //remove id from link when closing the edit view
                    $state.go('.', {
                      groupIdToEdit: null
                    },{
                      notify: false
                    });
                }

            }
        }
    });
});