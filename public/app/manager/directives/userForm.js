/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


define(['manager/module', 'angular', 'jquery', 'modules/forms/common', 'jquery-maskedinput', 'jquery-validation'],
  function (module, angular, $, formsCommon) {

    'use strict';

    /**
     * @ngdoc directive
     * @name app.manager.directive:userForm
     * @scope
     * @description
     * A form for all user properties
     */
    return module.registerDirective('userForm', function (SessionService, $state) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/manager/directives/userForm.tpl.html',
            scope: true,
            link: function (scope, form) {

                scope = scope.appScope || scope;

                scope.passwordDisabled = true;
                scope.togglePasswordDisabled = function () {
                    scope.passwordDisabled = !scope.passwordDisabled;
                };

                if (scope.editedUser._id && scope.editedUser._id === SessionService.getUserId()) {
                  var dataGroups = SessionService.getDataGroups();
                  dataGroups = _.reject(dataGroups, {description: '__projectShare'});
                  scope.editedUser.dataGroups = dataGroups;
                }

                /**
                 * matches US phone number format
                 *
                 * where the area code may not start with 1 and the prefix may not start with 1
                 * allows '-' or ' ' as a separator and allows parens around area code
                 * some people may want to put a '1' in front of their number
                 *
                 * 1(212)-999-2345 or
                 * 212 999 2344 or
                 * 212-999-0983
                 *
                 * but not
                 * 111-123-5434
                 * and not
                 * 212 123 4567
                 */
                $.validator.addMethod("phoneUS", function (phone_number, element) {
                    phone_number = phone_number.replace(/\s+/g, "");
                    return this.optional(element) || phone_number.length > 9 &&
                      phone_number.match(/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]([02-9]\d|1[02-9])-?\d{4}$/);
                }, "Please specify a valid phone number");

                $.validator.addMethod("uniqueEmail", function (email, element) {
                    return scope.isUniqueEmail(email);
                }, "That email is already being used. Please use a different email address.");

                $.validator.addMethod("validEmail", function (email) {
                    return email.match(/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,63}$/);
                }, "Please specify a valid email address");

                $.validator.addMethod('alphaNumPassword', function(password) {
                    return (password.match(/[a-zA-Z]/) && password.match(/[0-9]/));
                }, "Password must contain at least one number and one letter");

                // $.validator.addMethod("checkTags", function(value, element) {
                //   return this.optional(element) || cleanTags(value);
                // }, "Please enter a comma separated, ie. politics, people, etc, list");

                scope.accountStatus = scope.accountStatusOptions[0];

                form.validate(angular.extend({

                    // Rules for form validation
                    rules: {
                        email: {
                            required: true,
                            email: true,
                            uniqueEmail: true,
                            validEmail: true
                        },
                        password: {
                            required: true,
                            minlength: 8,
                            maxlength: 20,
                            alphaNumPassword: true
                        },
                        passwordConfirm: {
                            required: true,
                            minlength: 8,
                            maxlength: 20,
                            equalTo: '#password'
                        },
                        firstname: {
                            required: true
                        },
                        lastname: {
                            required: true
                        }
                        // ,
                        // phone: {
                        //     required: true,
                        //     phoneUS: true
                        // }
                    },

                    // Messages for form validation
                    messages: {
                        email: {
                            required: 'Please enter your email address',
                            email: 'Please enter a VALID email address'
                        },
                        password: {
                            required: 'Please enter your password'
                        },
                        passwordConfirm: {
                            required: 'Please enter your password one more time',
                            equalTo: 'Please enter the same password as above'
                        },
                        firstname: {
                            required: 'Please select your first name'
                        },
                        lastname: {
                            required: 'Please select your last name'
                        }
                        // ,
                        // phone: {
                        //     required: 'Please insert you phone'
                        // }
                    }

                    // ,

                    // onfocusout: function(el, ev){
                    //     scope.validForm = form.valid();
                    //     console.log("scope.validForm: ", scope.validForm)
                    // }

                }, formsCommon.validateOptions));

                scope.processForm = function (user) {
                    if (!form.valid()) {
                        return
                    }

                    if (form.valid() && scope.newUser) {
                        scope.addUser(user);
                    } else {
                        scope.updateUser(user);
                    }

                    //remove id from link when closing the edit view
                    $state.go('.', {
                      userIdToEdit: null
                    },{
                      notify: false
                    });
                }

            }
        }
    });
});