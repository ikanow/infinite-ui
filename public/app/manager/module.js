/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc overview
 * @name app.manager
 * @description
 * Data group and user group management UI components
 */
define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-resource'
], function (ng, couchPotato) {
    'use strict';

    var module = ng.module('app.manager', [
        'ui.router',
        'ngResource'
    ]);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        $stateProvider
            .state('app.manager', {
                abstract: true,
                data: {
                    title: 'Manager'
                }
            })
            .state('app.manager.modal', {
              abstract: true,
              authenticate: true,
              onEnter: function($stateParams, $state, $uibModal, $log, $rootScope) {
                var previousState = $rootScope.previousState;
                var modalInstance = $uibModal.open({
                  templateUrl: 'app/manager/views/manager-modal.html',
                  backdrop: 'static',
                  windowClass: 'manager-modal-window',
                  controller: function ($scope, $timeout) {
                    $scope.$watch(
                      function() {
                        if (_.has($state, 'current.data.title')) {
                          return $state.current.data.title;
                        }
                      },
                      function(newValue, oldValue){
                        if (newValue !== oldValue) {
                          $scope.activeTab = _.has($state, 'current.data.title') ? $state.current.data.title : "";
                        }
                      }
                    );
                    $scope.activeTab = $state.current.data.title;

                    //remove any URL state/parameters for deep linking, and close the model. fixes the bug where it wasnt returning to previous state
                    $scope.closeManager = function(){
                      $state.go('.', { },{
                        notify: false
                      }).then(function(){
                        $timeout(function() { modalInstance.close(); });
                      }, function(){
                        $timeout(function() { modalInstance.close(); });
                      });
                    }
                  }
                });


                modalInstance.result.then(function () {
                  if (previousState.name === "login") {
                    $state.transitionTo($rootScope.defaultLocation);
                  } else {
                    $state.transitionTo(previousState);
                  }
                }, function () {
                  if (previousState.name === "login") {
                    $state.transitionTo($rootScope.defaultLocation);
                  } else {
                    $state.transitionTo(previousState);
                  }
                });

              }
            })
            .state('app.manager.modal.manageUsers', {
                authenticate:true,
                url: '/manager/users/:userIdToEdit',
                params: {
                  editProfile: {value: false},
                  userIdToEdit: null
                },
                views: {
                  "managerView@": {
                    controller: 'ManageUsersCtrl',
                    templateUrl: 'app/manager/views/manager-users.html',
                    resolve: {
                      deps: $couchPotatoProvider.resolveDependencies([
                        'manager/controllers/ManageUsersCtrl',
                        'manager/directives/editPerson',
                        'manager/directives/userForm',
                        'modules/forms/directives/input/smartMaskedInput'
                      ])
                    }
                  }
                },
                data:{
                    title: 'Manage Users'
                }
            })
            .state('app.manager.modal.manageDataGroups', {
                authenticate:true,
                url: '/manager/datagroups/:groupIdToEdit',
                params: {
                  searchText: {
                    value: "",
                    squash: true
                  },
                  groupIdToEdit: null,
                  readOnly: {
                    value: false,
                    squash: true
                  }
                },
                views: {
                  "managerView@": {
                    controller: 'ManageDataGroupsCtrl',
                    templateUrl: 'app/manager/views/manager-data-groups.html',
                    resolve: {
                      deps: $couchPotatoProvider.resolveDependencies([
                        'manager/controllers/ManageDataGroupsCtrl',
                        'manager/directives/editDataGroup',
                        'manager/directives/datagroupForm',
                        'directives/ikNgTags'
                      ])
                    }
                  }
                },
                data:{
                    title: 'Data Group',
                    group: "data",
                    groupicon: true
                }
            })
            .state('app.manager.modal.manageUserGroups', {
                authenticate:true,
                url: '/manager/usergroups/:groupIdToEdit',
                params: {
                  searchText: {
                    value: "",
                    squash: true
                  },
                  groupIdToEdit: null,
                  readOnly: {
                    value: false,
                    squash: true
                  }
                },
                views: {
                  "managerView@": {
                    controller: 'ManageDataGroupsCtrl',
                    templateUrl: 'app/manager/views/manager-data-groups.html',
                    resolve: {
                      deps: $couchPotatoProvider.resolveDependencies([
                        'manager/controllers/ManageDataGroupsCtrl',
                        'manager/directives/editDataGroup',
                        'manager/directives/datagroupForm'
                      ])
                    }
                  }
                },
                data:{
                    title: 'User Group',
                    group: "user",
                    groupicon: false
                }
            });
    });

    couchPotato.configureApp(module);

    module.run(function($couchPotato){
        module.lazy = $couchPotato;
    });

    return module;
});