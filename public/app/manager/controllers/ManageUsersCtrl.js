/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc controller
 * @name app.manager.controller:ManageUsersCtrl
 * @description
 * UI Controller for managing users
 */
define(['manager/module', 'lodash', 'jquery'], function (module, _, $) {
    'use strict';

    module.registerController('ManageUsersCtrl', function ($rootScope, $scope, $state, $log, $q, $stateParams, $compile, userService, SessionService, userGroupService, utilityService) {

        $scope.userList = [];
        $scope.accountStatusOptions = ['active', 'disabled'];
        $scope.accountTypeOptions = ['user', 'admin'];
        $scope.accountType = $scope.accountTypeOptions[0];
        $scope.accountStatus = $scope.accountStatusOptions[0];

        $scope.currUserId = SessionService.getUserId();
        $scope.currUserEmail = SessionService.getUserProfile().email;
        $scope.isAdmin = SessionService.isAdmin();
        $scope.editProfile = $stateParams.editProfile;

        var userListPromise = null;

        $scope.loadingMessage = 'Loading Data...';
        $scope.loadingPromise = null;

        $scope.openedRow = {element: null};

        $scope.generateApiKey = function (length, charSet) {
          var output = "";
          length = length || 15;
          charSet = charSet || "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
          for( var i = 0; i < length; i++) output += charSet[Math.floor(Math.random()*charSet.length)];
          return output;
        };

        $scope.appScope = $scope;

        $scope.tableOptions = {
          columnFilters: false,
          tableFilter: true,
          clearFilter: true,
          userDisplayLengthOptions: [10,25,50,100],
          predicate: 'nameFilter',
          sortingReverse: true,
          resetPageOnDataReload: false
        };
        $scope.filters = [
          {
            model: "nameFilter",
            colHeader: "Display Name",
            dataProperty: "displayName",
            sortProperty: function(d){
              return _.trim(d.displayName.toLowerCase());
            },
            enabled: true
          },
          {
            model: "roleFilter",
            colHeader: "User Role",
            dataProperty: function(d){
              if(d.accountType === "admin"){
                return "Admin";
              } else {
                return "User";
              }
            },
            enabled: true
          },
          {
            model: "statusFilter",
            colHeader: "Status",
            dataProperty: "accountStatus",
            enabled: true,
            template: "<td class=\"action-button-td\">"
                          +"<span class=\"label label-success\" ng-class=\"{'label-danger': item.accountStatus != 'active'}\">{{item.accountStatus}}</span>"
                        +"</td>"
          },
          {
            model: "emailFilter",
            colHeader: "Email",
            dataProperty: "email",
            enabled: true
          },
          {
            model: "actionFilter",
            colHeader: "Actions",
            dataProperty: "email",
            enabled: true,
            template: "<td ng-class=\"{'user-row-height': !appScope.isAdmin}\" class=\"action-button-td\">"
                          +"<button user-button ng-if=\"appScope.isAdmin || item._id === appScope.currUserId\" class=\"btn btn-secondary btn-sm\"><i class=\"fa fa-edit\"></i> Edit</button>"
                        +"</td>"
          }
        ];


      //apply datatable filter
      if($scope.editProfile){
        $scope.tableOptions.tableFilterValue = $scope.currUserEmail;
      }
      if ($stateParams.userIdToEdit) {
        $scope.userIdToEdit = $stateParams.userIdToEdit;
      }

      $scope.stopEditProfile = function () {
        $scope.editProfile = false;
      };

      $scope.showAddUser = function(event){
            $('.table-responsive').before($compile('<div edit-user></div>')($scope));
            event.stopPropagation();
        };

        $scope.loadUsers = function(){
          $q.all({
            userList: userService.getAll(),
            userGroups: userGroupService.getAll()
          }).then(function(results) {
            var userFound = false;
            _.each(results.userList, function(user) {
              //if we passed in an ID to view/edit, set the name of that user as the search filter
              if($scope.userIdToEdit && $scope.userIdToEdit == user._id) { 
                $scope.tableOptions.tableFilterValue = user.email;
                userFound = true;
              }
              user.userGroups = [];
              _.each(results.userGroups, function(userGroup) {
                if(_.filter(userGroup.members, {_id: user._id}).length > 0) { user.userGroups.push(userGroup); }
              });
            });
            //if they were deep linked a specific user, but that user did not exist in their user list
            if($scope.userIdToEdit && !userFound) {
              utilityService.confirmationModal("The user you are trying to access does not exist or you do not have permission", //text to display in the error popup
                'Close', //text of the close/accept/okay/etc button
                null, //cancel button text, n/a
                "Alert"); //header text
              //remove ID from link
              $state.go('.', {
                userIdToEdit: null
              },{
                notify: false
              });
            }
            $scope.userList = results.userList;
          }, function(err) {
            $log.error('ERROR: Cannot download the users.', err);
          });

          $scope.loadingPromise = userListPromise;
        };

        $scope.loadUsers();

    });

});
