/*******************************************************************************
 * Copyright 2015, The IKANOW Open Source Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/


/**
 * @ngdoc controller
 * @name app.manager.controller:ManageDataGroupsCtrl
 * @description
 * UI Controller for managing data groups and user groups
 */
define(['manager/module', 'lodash', 'jquery'], function (module, _, $) {

    'use strict';

    module.registerController('ManageDataGroupsCtrl', function ($scope, $rootScope, $log, $q, $compile, $state, $stateParams, SessionService, userGroupService, dataGroupService, userService, workspaceService, sourcesService, utilityService) {

        var origData = [];
        $scope.groupList = [];
        $scope.selectedType = $state.current.data.group;
        $scope.title = $state.current.data.title;
        $scope.groupicon = $state.current.data.groupicon;
        $scope.currUserId = SessionService.getUserId();
        $scope.isAdmin = SessionService.isAdmin();

        var groupListPromise = null;

        $scope.loadingMessage = 'Loading Data...';
        $scope.loadingPromise = null;
        $scope.readOnly = $stateParams.readOnly;
        $scope.popoverText = $scope.title === "Data Group" ?
        "Data Groups are used to contain data sources. " +
        "Please contact your Administrator to Add or Edit Data Group" :
        "User Groups are used to organize users for granting access to " +
        "data and projects. Please contact your Administrator to Add or Edit Data Group";

        //ikanow-datatables
        $scope.openedRow = {element: null};
        $scope.appScope = $scope;

        $scope.tableOptions = {
          columnFilters: false,
          tableFilter: true,
          clearFilter: true,
          userDisplayLengthOptions: [10,25,50,100],
          predicate: 'nameFilter',
          sortingReverse: true,
          resetPageOnDataReload: false
        };

        $scope.filters = [
          {
            model: "nameFilter",
            colHeader: "Display Name",
            dataProperty: function(d){
              if(d.name){
                return _.trim(d.name);
              } else {
                return "";
              }
            },
            sortProperty: function(d){
              if(d.name){
                return _.trim(d.name.toLowerCase());
              } else {
                return "";
              }
            },
            enabled: true
          },
          {
            model: "ownerFilter",
            colHeader: "Owner",
            dataProperty: function(d){
              if(d.ownerDisplayName){
                return _.trim(d.ownerDisplayName);
              } else {
                return "";
              }
            },
            sortProperty: function(d){
              if(d.ownerDisplayName){
                return _.trim(d.ownerDisplayName.toLowerCase());
              } else {
                return "";
              }
            },
            enabled: true
          },
          {
            model: "descriptionFilter",
            colHeader: "Description",
            dataProperty: function(d){
              if(d.description){
                return _.trim(d.description);
              } else {
                return "";
              }
            },
            enabled: true
          },
          {
            model: "actionFilter",
            colHeader: "Actions",
            dataProperty: "name",
            enabled: true,
            template: "<td>"
                          +"<button action-button read-only-view=1 ng-if=\"!appScope.isAdmin\" class=\"btn btn-info btn-sm\"><i class=\"fa fa-eye\"></i> Details</button>"
                          +"<button action-button ng-if=\"appScope.isAdmin || item.ownerId == currUserId || item.isPersonalCommunity\" class=\"btn btn-secondary btn-sm\"><i class=\"fa fa-edit\"></i> Edit</button>"
                        +"</td>"
          }
        ];

        if($stateParams.searchText) {
          $scope.tableOptions.tableFilterValue = $stateParams.searchText;
        }
        if ($stateParams.groupIdToEdit) {
          $scope.groupIdToEdit = $stateParams.groupIdToEdit;
        }
        // Manager Actions

        $scope.showAddDataGroup = function(event){
          $('.table-responsive').before($compile('<div edit-data-group></div>')($scope));
          event.stopPropagation();
        };

         //Tests:
        $scope.inCommunities = function(){
          var edg = this.editedDataGroup;
          edg.members = edg.members || [];
          if(this.aUser){
            $scope.users = _.reject($scope.users, {_id: this.aUser._id})
            this.aUser.type = this.aUser.type ? "user_group" : "user";
            edg.members.push(this.aUser);
          } else if(this.member){
            edg.members = _.reject(edg.members, {_id: this.member._id});
            this.member.type = this.member.type == "user_group" ? "user" : null;
            $scope.users.push(this.member);
          } else {
            $scope.users = _.filter($scope.users, function(user){
              return !(_.find(edg.members, {_id: user._id}))
            })
          }
          $scope.setCount(edg);
        };

        $scope.loadDataGroups = function(){
          var communities;
          var personalCommunities;
          var validCommunities;
          var currentUser = SessionService.getUserProfile();
          var owner;

          groupListPromise = $q.all({
            userData: userService.getAll(),
            userGroups: userGroupService.getAll(),
            dataGroups: dataGroupService.getAll(),
            userSources: sourcesService.getUserSources(),
            userShares: sourcesService.getSourcesShare()
          }).then(function(results){
            var groupFound = false;
            //give usergroups the same displayName property as users so we can sort them together
            _.each(results.userGroups, function(item){
              item.displayName = item.name;
            });

            if($scope.selectedType == "data"){
              origData = results.dataGroups;
              $scope.users = _.union(results.userData, results.userGroups);

              _.each(origData, function(dataGroup) {
                //if we passed in an ID to view/edit, set the name of that item as the search filter
                if($scope.groupIdToEdit && $scope.groupIdToEdit == dataGroup._id) { 
                  $scope.tableOptions.tableFilterValue = dataGroup.name;
                  groupFound = true;
                }
                dataGroup.sources = _.filter(results.userSources, {communityIds : [dataGroup._id]});
                // Only keep shares that are part of datagroup
                var dataGroupShares = _.filter(results.userShares.data, function(share) {
                  // Check if datagroup _id is in share's communities array
                  return _.filter(share.communities, {_id: dataGroup._id}).length > 0;
                });

                // Join sources and shares, reject sources with datatype 'auxiliary' if not admin
                dataGroup.sources = _.reject(_.union(dataGroup.sources, dataGroupShares), function(source) {
                  return (source.dataType == 'auxiliary' || !source.dataType) && !$rootScope.isAdmin;
                });
              });

            } else {
              origData = results.userGroups;
              $scope.users = results.userData;

              results.dataGroups = _.reject(results.dataGroups, {isPersonalCommunity: true});
              results.dataGroups = _.reject(results.dataGroups, {description: '__projectShare'});
              results.dataGroups = _.reject(results.dataGroups, {tags: ['workspaceMaster']});

              //add a list of datagroups the usergroup is added to
              _.each(origData, function(userGroup) {
                //if we passed in an ID to view/edit, set the name of that item as the search filter
                if($scope.groupIdToEdit && $scope.groupIdToEdit == userGroup._id) { 
                  $scope.tableOptions.tableFilterValue = userGroup.name;
                  groupFound = true;
                }
                userGroup.dataGroups = [];
                _.each(results.dataGroups, function(dataGroup) {
                  dataGroup.members = _.reject(dataGroup.members, {type: "uer"});
                  if(_.filter(dataGroup.members, {_id: userGroup._id}).length > 0) { userGroup.dataGroups.push(dataGroup); }
                });
              });
            }

            //if they were deep linked a specific group, but that group did not exist in their group list
            if($scope.groupIdToEdit && !groupFound) {
              utilityService.confirmationModal("The " + $scope.title + " you are trying to access does not exist or you do not have permission", //text to display in the error popup
                'Close', //text of the close/accept/okay/etc button
                null, //cancel button text, n/a
                "Alert"); //header text
              //remove ID from the URL
              $state.go('.', {
                groupIdToEdit: null
              },{
                notify: false
              });
            }

            origData = _.reject(origData, {isPersonalCommunity: true});
            origData = _.reject(origData, {description: '__projectShare'});
            origData = _.reject(origData, {tags: ['workspaceMaster']});

            loadProjects();

            $scope.groupList = origData;

          }, function(err) {
            $log.error('ERROR: Cannot fetch data.', err)
          });

          $scope.loadingPromise = groupListPromise;
        };

        function loadProjects(){
          workspaceService.getAll().then(function(spaces){
              $scope.projects = spaces;
          }, function(err) {
            $log.error('ERROR: Cannot download the projects.', err)
          });
        }

        $scope.setCount = function(edg){
          $scope.allUsersAdded = edg.members.length;
          $scope.usersAdded = _.filter(edg.members, {type: 'user'}).length + _.filter(edg.members, {userType: 'member'}).length;
          $scope.userGroupsAdded = _.filter(edg.members, {type: 'user_group'}).length;
          $scope.allUsersAvail = $scope.users.length;
          $scope.userGroupsAvail = _.filter($scope.users, {type: 'user'}).length;
          $scope.usersAvail = $scope.allUsersAvail - $scope.userGroupsAvail;
        };

        // Init values:

        $scope.user = null;

        // Load Default Data

        $scope.loadDataGroups();
    });

});