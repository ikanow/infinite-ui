define(['angular',
    'angular-couch-potato',
    'angular-ui-router'], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.layout', ['ui.router']);

    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider, $urlRouterProvider) {

        $stateProvider
            .state('app', {
                abstract: true,
                authenticate: true,
                views: {
                    root: {
                        templateUrl: 'app/layout/layout.tpl.html',
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                'auth/directives/loginInfo',
                                'modules/forms/directives/validate/smartValidateForm',
                                'project/directives/projectWizard',
                                'modules/forms/directives/wizard/smartFueluxWizard',
                                'modules/forms/directives/input/smartMaskedInput',
                                'project/controllers/ProjectCreateWizardCtrl',
                                'project/controllers/ProjectEditWizardCtrl'
                            ])
                        }
                    }
                }
            });
        $urlRouterProvider.otherwise(function($injector){
          var $state = $injector.get("$state");
          var $rootScope = $injector.get("$rootScope");
          $state.go($rootScope.defaultLocation);
        });

    });

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;

});
