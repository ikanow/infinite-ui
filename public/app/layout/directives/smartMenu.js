define(['layout/module'], function (module) {

    "use strict";

    (function ($) {

        $.fn.smartCollapseToggle = function (caret) {

            return this.each(function () {

                var $body = $('body');
                var $this = $(this);

                // only if not  'menu-on-top'
                if ($body.hasClass('menu-on-top')) {


                } else {

                    $body.hasClass('mobile-view-activated');

                    // toggle open
                    $this.toggleClass('open');

                    // for minified menu collapse only second level
                    if ($body.hasClass('minified')) {
                        if ($this.closest('nav ul ul').length) {
                            if(caret){
                                $this.find('>a .collapse-sign .fa').toggleClass('fa-caret-down fa-caret-up');
                            } else {
                                $this.find('>a .collapse-sign .fa').toggleClass('fa-minus-square-o fa-plus-square-o');
                            }
                            $this.find('ul:first').slideToggle(appConfig.menu_speed || 200);
                        }
                    } else {
                        // toggle expand item
                        if(caret){
                            $this.find('>a .collapse-sign .fa').toggleClass('fa-caret-down fa-caret-up');
                        } else {
                            $this.find('>a .collapse-sign .fa').toggleClass('fa-minus-square-o fa-plus-square-o');
                        }
                        $this.find('ul:first').slideToggle(appConfig.menu_speed || 200);
                    }
                }
            });
        };
    })(jQuery);

    module.registerDirective('smartMenu', function ($state, $rootScope, $timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                //wrap stuff in a function to help menus work dynamically
                var delayLoad = function () {
                    var $body = $('body');

                    var $collapsible = element.find('li[data-menu-collapse]');
                    $collapsible.each(function (idx, li) {
                        var $li = $(li);
                        $li
                            .on('click', '>a', function (e) {

                                // collapse all open siblings
                                $li.siblings('.open').smartCollapseToggle();

                                // toggle element
                                $li.smartCollapseToggle();

                                // add active marker to collapsed element if it has active childs
                                if (!$li.hasClass('open') && $li.find('li.active').length > 0) {
                                    $li.addClass('active')
                                }

                                e.preventDefault();
                            })
                            .find('>a').append('<b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>');

                        // initialization toggle
                        if ($li.find('li.active').length) {
                            $li.smartCollapseToggle();
                            $li.find('li.active').parents('li').addClass('active');
                        }
                    });

                    // click on route link
                    element.on('click', 'a[data-ui-sref]', function (e) {
                        // collapse all siblings to element parents and remove active markers
                        $(this)
                            .parents('li').addClass('active')
                            .each(function () {
                                $(this).siblings('li.open').smartCollapseToggle();
                                $(this).siblings('li').removeClass('active')
                            });

                        if ($body.hasClass('mobile-view-activated')) {
                            $rootScope.$broadcast('requestToggleMenu');
                        }
                    });


                    scope.$on('$smartLayoutMenuOnTop', function (event, menuOnTop) {
                        if (menuOnTop) {
                            $collapsible.filter('.open').smartCollapseToggle();
                        }
                    });
                };

                $timeout(delayLoad,0);
            }


        }
    });

    //new version of the same smart menu directive to support ng-repeat nested lists
    //as opposed to regular smart-menu directive, add the smart-menu-async directive to the lowest nested ng-repeat <li> instead of the highest level <ul> and each <li> conditionally expands (has + and -)  if ng-attr-collapsible = "true"
    module.registerDirective('smartMenuAsync', function ($state, $rootScope, $timeout) {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                //wrap stuff in a function so menus work with async data/ng-repeat
                var delayLoad = function () {
                    var $body = $('body'), element;
                        element = el.parent("ul");

                    //in case the <li> is nested in more than 2 <ul>s
                    if(attrs.nestlevel) {
                        _.each(_.range(attrs.nestlevel), function(item){
                            element = element.parent("li").parent("ul");
                        });
                    } else {
                        element = element.parent("li").parent("ul");
                    }

                    //find <li>s in which collapsible attribute (or ng-attr-collapsible) is "true"
                    var $collapsible = element.find('li[collapsible="true"]');

                    $collapsible.each(function (idx, li) {
                        var $li = $(li);

                        //in case this directive gets called multiple times for the same list, remove artifacts created from previous runs
                        $li.find("b").remove();
                        $li.off("click",'>a');

                        //set up the click, add collapse sign
                        $li
                            .on('click', '>a', function (e) {

                                // collapse all open siblings
                                $li.siblings('.open').smartCollapseToggle(true);

                                // toggle element
                                $li.smartCollapseToggle(true);

                                // add active marker to collapsed element if it has active childs
                                if (!$li.hasClass('open') && $li.find('li.active').length > 0) {
                                    $li.addClass('active')
                                }

                                e.preventDefault();
                            })
                            .find('>a').append('<b class="collapse-sign"><em class="fa fa-caret-down"></em></b>');

                        // initialization toggle
                        if ($li.find('li.active').length) {
                            $li.smartCollapseToggle(true);
                            $li.find('li.active').parents('li').addClass('active');
                        }
                    });


                    scope.$on('$smartLayoutMenuOnTop', function (event, menuOnTop) {
                        if (menuOnTop) {
                            $collapsible.filter('.open').smartCollapseToggle(true);
                        }
                    });
                };

                //only run if directive is called from the last item in ng-repeat
                //if (scope.$last) {
                    $timeout(delayLoad,0);
                //}
            }


        }
    });


});
