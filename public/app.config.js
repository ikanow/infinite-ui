'use strict';

//Endpoint is now relative to hosting
//For dev, a proxy is created during the development grunt task init
var apiHost = "/";
var apiBase = apiHost + "api/";
var isaApiBase = apiHost + "isa/";

var appConfig = {
      sessionTimeout: 900000, // 15 minutes until automatic logout
      sessionWarningBuffer: 180000, // How early should we warn the user about expired session? 3 minutes
      sessionMaxInactivity: 600000, // 10 minutes of no mouse clicks
      callbackSignature: 'jsonp=JSON_CALLBACK',

      //Api Base - Trailing slash so i don't break everything
      apiBaseDomain: apiBase,
      isaApiBaseDomain: isaApiBase,

      //ApiService Base paths - no trailing /
      apiAuthURI: "auth",
      apiConfigSourceURI: "config/source",
      apiCustomMapReduceURI: "custom/mapreduce",
      apiCustomSavedQueryURI: "custom/savedquery",
      apiKnowledgeDocumentURI: "knowledge/document",
      apiKnowledgeFeatureURI: "knowledge/feature",
      apiMessageURI: "message",
      apiRecordQueryURI: apiHost + "infinit.e.records/proxy",
      apiSocialCommunityURI: "social/community",
      apiSocialDataGroupURI: 'social/group/data',
      apiSocialPersonURI: "social/person",
      apiSocialShareURI: 'social/share',
      apiSocialUserGroupURI: 'social/group/user',
      apiWorkspaceURI: "social/workspace",
      apiBreachDetectionURI: "analytic/breachDetection",
      apiThreatFeedRankingURI: "analytic/threatFeedRanking",
      apiVulnerabilityMatrixURI: "analytic/vulnerabilityMatrix",
      apiQueryUi: "query",

      apiLoginURI: 'auth/login/',
      apiLogoutURI: 'auth/logout/',

      preferredLanguage: 'en-US',
      appCacheName: 'appCache',
      searchResultsPageCount: 15,
      authTokenHeaderName: 'infinite_api_key', //infinite_api_key //infinitecookie //inf_token
      maxDetailKeywordsCount: 30,
      locales: [
        {key: 'LOCALES.ENGLISH', languageCode: 'en', countryCode: 'US'},
        {key: 'LOCALES.SPANISH', languageCode: 'es', countryCode: 'MX'}
      ],
      visualizations: [
        {key:'MEDIATYPE_VOLUME_OVER_TIME', title:'Media Type Volume', resourcePath:'VISUALIZATIONS.MEDIATYPE_VOLUME_OVER_TIME' },
        {key:'GEO_MAP', title:'Media Type Map', resourcePath:'VISUALIZATIONS.GEO_MAP' },
        {key:'ENT_TYPE_SENTIMENT_FREQUENCY_RELEVANCE', title:'Entity Breakdown', resourcePath:'VISUALIZATIONS.ENT_TYPE_SENTIMENT_FREQUENCY_RELEVANCE' },
        //{key:'SENTIMENT_MEDIATYPE_OVER_TIME', title:'Media Type Sentiment',resourcePath:'VISUALIZATIONS.SENTIMENT_MEDIATYPE_OVER_TIME' },
        //{key:'ASSOCIATION_VIEWER', title:'Association Viewer', resourcePath:'VISUALIZATIONS.ASSOCIATION_VIEWER' },
        //{key:'ROUTING_MAP', title:'Routing Map', resourcePath:'VISUALIZATIONS.ROUTING_MAP' }
      ],
      cacheKeys: {
        isAuthenticated: 'isAuthenticated',
        authPerson: 'authPerson',
        lastActionTimestamp: 'lastActionTimestamp',
        authToken: 'authSessionToken'
      },
      defaultTranslation: {
        'LOCALES': {
          'ENGLISH': 'English',
          'SPANISH': 'Spanish'
        },
        'ERRORS': {
          'INVALID_USERNAME_OR_PASSWORD': 'Invalid username and/or password'
        },
        'HEADER': {
          'GREETING': 'Welcome Back {{name}}'
        },
        'LOGIN': {
          'PLACEHOLDER_USERNAME': 'Username',
          'PLACEHOLDER_PASSWORD': 'Password',
          'SUBMIT': 'Log In',
          'NOT_A_REGISTERED_USER': 'Not a Registered User?',
          'FORGOT_PASSWORD': 'Forgot Password?'
        },
        'CHANNELS': {
          'MY_CHANNELS': 'My Channels'
        }
      }
    };

appConfig.menu_speed = 200;


appConfig.smartSkin = "smart-style-0";

appConfig.skins = [
    {name: "smart-style-0",
        logo: "styles/img/logo.png",
        class: "btn btn-block btn-xs txt-color-white margin-right-5",
        style: "background-color:#4E463F;",
        label: "Smart Default"},

    {name: "smart-style-1",
        logo: "styles/img/logo-white.png",
        class: "btn btn-block btn-xs txt-color-white",
        style: "background:#3A4558;",
        label: "Dark Elegance"},

    {name: "smart-style-2",
        logo: "styles/img/logo-blue.png",
        class: "btn btn-xs btn-block txt-color-darken margin-top-5",
        style: "background:#fff;",
        label: "Ultra Light"},

    {name: "smart-style-3",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background:#f78c40",
        label: "Google Skin"},

    {name: "smart-style-4",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background: #bbc0cf; border: 1px solid #59779E; color: #17273D !important;",
        label: "PixelSmash"},

    {name: "smart-style-5",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background: rgba(153, 179, 204, 0.2); border: 1px solid rgba(121, 161, 221, 0.8); color: #17273D !important;",
        label: "Glass"}
];



appConfig.sound_path = "sound/";
appConfig.sound_on = false; 


/*
* DEBUGGING MODE
* debugState = true; will spit all debuging message inside browser console.
* The colors are best displayed in chrome browser.
*/


appConfig.debugState = false;	
appConfig.debugStyle = 'font-weight: bold; color: #00f;';
appConfig.debugStyle_green = 'font-weight: bold; font-style:italic; color: #46C246;';
appConfig.debugStyle_red = 'font-weight: bold; color: #ed1c24;';
appConfig.debugStyle_warning = 'background-color:yellow';
appConfig.debugStyle_success = 'background-color:green; font-weight:bold; color:#fff;';
appConfig.debugStyle_error = 'background-color:#ed1c24; font-weight:bold; color:#fff;';


appConfig.voice_command = true;
appConfig.voice_command_auto = false;

/*
 *  Sets the language to the default 'en-US'. (supports over 50 languages 
 *  by google)
 * 
 *  Afrikaans         ['af-ZA']
 *  Bahasa Indonesia  ['id-ID']
 *  Bahasa Melayu     ['ms-MY']
 *  CatalГ            ['ca-ES']
 *  ДЊeЕЎtina         ['cs-CZ']
 *  Deutsch           ['de-DE']
 *  English           ['en-AU', 'Australia']
 *                    ['en-CA', 'Canada']
 *                    ['en-IN', 'India']
 *                    ['en-NZ', 'New Zealand']
 *                    ['en-ZA', 'South Africa']
 *                    ['en-GB', 'United Kingdom']
 *                    ['en-US', 'United States']
 *  EspaГ±ol          ['es-AR', 'Argentina']
 *                    ['es-BO', 'Bolivia']
 *                    ['es-CL', 'Chile']
 *                    ['es-CO', 'Colombia']
 *                    ['es-CR', 'Costa Rica']
 *                    ['es-EC', 'Ecuador']
 *                    ['es-SV', 'El Salvador']
 *                    ['es-ES', 'EspaГ±a']
 *                    ['es-US', 'Estados Unidos']
 *                    ['es-GT', 'Guatemala']
 *                    ['es-HN', 'Honduras']
 *                    ['es-MX', 'MГ©xico']
 *                    ['es-NI', 'Nicaragua']
 *                    ['es-PA', 'PanamГЎ']
 *                    ['es-PY', 'Paraguay']
 *                    ['es-PE', 'PerГє']
 *                    ['es-PR', 'Puerto Rico']
 *                    ['es-DO', 'RepГєblica Dominicana']
 *                    ['es-UY', 'Uruguay']
 *                    ['es-VE', 'Venezuela']
 *  Euskara           ['eu-ES']
 *  FranГ§ais         ['fr-FR']
 *  Galego            ['gl-ES']
 *  Hrvatski          ['hr_HR']
 *  IsiZulu           ['zu-ZA']
 *  ГЌslenska         ['is-IS']
 *  Italiano          ['it-IT', 'Italia']
 *                    ['it-CH', 'Svizzera']
 *  Magyar            ['hu-HU']
 *  Nederlands        ['nl-NL']
 *  Norsk bokmГҐl     ['nb-NO']
 *  Polski            ['pl-PL']
 *  PortuguГЄs        ['pt-BR', 'Brasil']
 *                    ['pt-PT', 'Portugal']
 *  RomГўnДѓ          ['ro-RO']
 *  SlovenДЌina       ['sk-SK']
 *  Suomi             ['fi-FI']
 *  Svenska           ['sv-SE']
 *  TГјrkГ§e          ['tr-TR']
 *  Р±СЉР»РіР°СЂСЃРєРё['bg-BG']
 *  PСѓСЃСЃРєРёР№     ['ru-RU']
 *  РЎСЂРїСЃРєРё      ['sr-RS']
 *  н•њкµ­м–ґ         ['ko-KR']
 *  дё­ж–‡            ['cmn-Hans-CN', 'ж™®йЂљиЇќ (дё­е›Ѕе¤§й™†)']
 *                    ['cmn-Hans-HK', 'ж™®йЂљиЇќ (й¦™жёЇ)']
 *                    ['cmn-Hant-TW', 'дё­ж–‡ (еЏ°зЃЈ)']
 *                    ['yue-Hant-HK', 'зІµиЄћ (й¦™жёЇ)']
 *  ж—Ґжњ¬иЄћ         ['ja-JP']
 *  Lingua latД«na    ['la']
 */
appConfig.voice_command_lang = 'en-US';
/*
 *  Use localstorage to remember on/off (best used with HTML Version)
 */ 
appConfig.voice_localStorage = false;
/*
 * Voice Commands
 * Defines all voice command variables and functions
 */ 
if (appConfig.voice_command) {
        
     	appConfig.commands = {
                
        'show dashboard' : function() { window.location.hash = "dashboard" },
        'go back' :  function() { history.back(1); }, 
        'scroll up' : function () { $('html, body').animate({ scrollTop: 0 }, 100); },
        'scroll down' : function () { $('html, body').animate({ scrollTop: $(document).height() }, 100);},
        'hide navigation' : function() { 
            if ($( ":root" ).hasClass("container") && !$( ":root" ).hasClass("menu-on-top")){
                $('span.minifyme').trigger("click");
            } else {
                $('#hide-menu > span > a').trigger("click"); 
            }
        },
        'show navigation' : function() { 
            if ($( ":root" ).hasClass("container") && !$( ":root" ).hasClass("menu-on-top")){
                $('span.minifyme').trigger("click");
            } else {
                $('#hide-menu > span > a').trigger("click"); 
            }
        },
        'mute' : function() {
            appConfig.sound_on = false;
            $.smallBox({
                title : "MUTE",
                content : "All sounds have been muted!",
                color : "#a90329",
                timeout: 4000,
                icon : "fa fa-volume-off"
            });
        },
        'sound on' : function() {
            appConfig.sound_on = true;
            $.speechApp.playConfirmation();
            $.smallBox({
                title : "UNMUTE",
                content : "All sounds have been turned on!",
                color : "#40ac2b",
                sound_file: 'voice_alert',
                timeout: 5000,
                icon : "fa fa-volume-up"
            });
        },
        'stop' : function() {
            smartSpeechRecognition.abort();
            $( ":root" ).removeClass("voice-command-active");
            $.smallBox({
                title : "VOICE COMMAND OFF",
                content : "Your voice commands has been successfully turned off. Click on the <i class='fa fa-microphone fa-lg fa-fw'></i> icon to turn it back on.",
                color : "#40ac2b",
                sound_file: 'voice_off',
                timeout: 8000,
                icon : "fa fa-microphone-slash"
            });
            if ($('#speech-btn .popover').is(':visible')) {
                $('#speech-btn .popover').fadeOut(250);
            }
        },
        'help' : function() {

            $('#voiceModal').removeData('modal').modal( { remote: "app/layout/partials/voice-commands.tpl.html", show: true } );
            if ($('#speech-btn .popover').is(':visible')) {
                $('#speech-btn .popover').fadeOut(250);
            }

        },      
        'got it' : function() {
            $('#voiceModal').modal('hide');
        },  
        'logout' : function() {
            $.speechApp.stop();
            window.location = $('#logout > span > a').attr("href");
        }
    }; 
    
}


/*
* END APP.appConfig
*/